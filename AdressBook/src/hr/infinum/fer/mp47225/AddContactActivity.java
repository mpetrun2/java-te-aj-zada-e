package hr.infinum.fer.mp47225;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddContactActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contact);
		Button quit = (Button) findViewById(R.id.btnQuit);
		quit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AddContactActivity.this.finish();
			}
		});
		Button add = (Button) findViewById(R.id.btnAdd);
		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Contact contact = new Contact();
				EditText nameField = (EditText) findViewById(R.id.tfName);
				EditText phoneField = (EditText) findViewById(R.id.tfPhone);
				EditText emailField = (EditText) findViewById(R.id.tfEmail);
				EditText noteField = (EditText) findViewById(R.id.tfNote);
				EditText profileField = (EditText) findViewById(R.id.tfProfile);
				contact.setName(processInput(nameField.getText().toString()));
				contact.setPhone(processInput(phoneField.getText().toString()));
				contact.setEmail(processInput(emailField.getText().toString()));
				contact.setNote(processInput(noteField.getText().toString()));
				contact.setFacebookProfile(processInput(profileField.getText()
						.toString()));
				if (contact.getName().isEmpty() || contact.getEmail().isEmpty()
						|| contact.getPhone().isEmpty()
						|| contact.getFacebookProfile().isEmpty()) {
					Toast.makeText(AddContactActivity.this,
							"All fields except Note are required.",
							Toast.LENGTH_LONG).show();
					;
					return;
				}
				HomeActivity.contacts.add(contact);
				AddContactActivity.this.finish();
			}

			private String processInput(String string) {
				if (string == null) {
					return "";
				}
				return string;
			}
		});
	}

}
