/**
 * 
 */
package hr.infinum.fer.mp47225;

/**
 * Class representing single contact and his data.
 * @author Marin Petrunić 0036472250
 *
 */
public class Contact {
	
	/**
	 * Contacts first and last name.
	 */
	private String name;
	
	/**
	 * Contacts phone number
	 */
	private String phone;
	
	/**
	 * Contacts email adress.
	 */
	private String email;
	
	/**
	 * Contacts current note.
	 */
	private String note;
	
	/**
	 * Url to contacts facebook profile.
	 */
	private String facebookProfile;
	
	/**
	 * Constructor.
	 */
	public Contact() {
		
	}
	

	/**
	 * Constructor.
	 * @param name contact name
	 * @param phone contact phone number
	 * @param email contact email
	 * @param note 
	 * @param facebookProfile url to contacts profile
	 */
	public Contact(String name, String phone, String email, String note,
			String facebookProfile) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.note = note;
		this.facebookProfile = facebookProfile;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the facebookProfile
	 */
	public String getFacebookProfile() {
		return facebookProfile;
	}

	/**
	 * @param facebookProfile the facebookProfile to set
	 */
	public void setFacebookProfile(String facebookProfile) {
		this.facebookProfile = facebookProfile;
	}
	
	

}
