/**
 * 
 */
package hr.infinum.fer.mp47225;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adapter between list of contacts and view.
 * @author Marin Petrunić 0036472250
 * 
 */
public class ContactArrayAdapter extends ArrayAdapter<Contact> {

	/**
	 * Constructor.
	 * @param context
	 * @param objects
	 */
	public ContactArrayAdapter(Context context, List<Contact> objects) {
		super(context, 0, objects);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			
			convertView = inflater.inflate(R.layout.list_item,
					parent,
					false
				);
		}
		
		TextView nameTextView = (TextView)convertView
				.findViewById(R.id.itemName);
		TextView phoneTextView = (TextView)convertView
				.findViewById(R.id.itemPhone);
		TextView emailTextView = (TextView)convertView
				.findViewById(R.id.itemEmail);
		
		nameTextView.setText(getItem(position).getName());
		phoneTextView.setText(getItem(position).getPhone() + ", ");
		emailTextView.setText(getItem(position).getEmail());
		return convertView;
	}

}
