package hr.infinum.fer.mp47225;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hr.infinum.fer.mp47225.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

/**
 * Starting aplication activity.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class HomeActivity extends Activity {

	/**
	 * List of existing {@link Contact}
	 */
	static ArrayList<Contact> contacts = new ArrayList<Contact>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		if(contacts.isEmpty()) {
			String peoplesJSon = loadJSONFromAsset("people.json");
			fillContactList(peoplesJSon);
		}
		ListView listView = (ListView) findViewById(R.id.contactList);
		ContactArrayAdapter adapter = new ContactArrayAdapter(this, contacts);
		listView.setAdapter(adapter);
		Button addButton = (Button) findViewById(R.id.btnAdd);
		addButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HomeActivity.this,
						AddContactActivity.class);
				startActivity(intent);

			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
				intent.putExtra("name", contacts.get(position).getName());
				intent.putExtra("phone", contacts.get(position).getPhone());
				intent.putExtra("email", contacts.get(position).getEmail());
				intent.putExtra("note", contacts.get(position).getNote());
				intent.putExtra("profile", contacts.get(position).getFacebookProfile());
				startActivity(intent);
			}
		});
	}

	/**
	 * Fills list of contacts with contacts from given json file.
	 * 
	 * @param peoplesJSon
	 *            json formated string containing contacts data
	 */
	private void fillContactList(String peoplesJSon) {
		try {
			JSONObject peoplesJSON = new JSONObject(peoplesJSon);
			JSONArray contacts = peoplesJSON.getJSONArray("people");
			for (int i = 0; i < contacts.length(); i++) {
				JSONObject personObject = (JSONObject) contacts.get(i);
				JSONObject contactObject = personObject.getJSONObject("person");
				Contact contact = new Contact();
				contact.setName(contactObject.getString("name"));
				contact.setPhone(contactObject.getString("phone"));
				contact.setEmail(contactObject.getString("email"));
				contact.setNote(contactObject.getString("note"));
				contact.setFacebookProfile(contactObject
						.getString("facebook_profile"));
				HomeActivity.contacts.add(contact);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return content of file at given path as {@link String}.
	 * 
	 * @param fileName
	 *            relative path from assets to file
	 * @return String representation of file
	 */
	private String loadJSONFromAsset(String fileName) {

		String json = null;
		try {

			InputStream in = getAssets().open(fileName);

			int size = in.available();

			byte[] buffer = new byte[size];

			in.read(buffer);

			in.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return "";
		}
		return json;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
