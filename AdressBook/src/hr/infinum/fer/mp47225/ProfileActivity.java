package hr.infinum.fer.mp47225;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ProfileActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		this.setTitle(getIntent().getExtras().getString("name", "No name"));
		TextView nameText = (TextView) findViewById(R.id.tvName);
		final TextView phoneText = (TextView) findViewById(R.id.tvPhone);
		TextView emailText = (TextView) findViewById(R.id.tvEmail);
		TextView noteText = (TextView) findViewById(R.id.tvNote);
		final TextView profileText = (TextView) findViewById(R.id.tvProfile);
		nameText.setText(getIntent().getExtras().getString("name", "No name"));
		phoneText.setText(getIntent().getExtras()
				.getString("phone", "No phone"));
		emailText.setText(getIntent().getExtras()
				.getString("email", "No email"));
		noteText.setText(getIntent().getExtras()
				.getString("note", "Empty note"));
		profileText.setText(getIntent().getExtras().getString("profile",
				"No facebook profile"));
		Button callButton = (Button) findViewById(R.id.btnCall);
		callButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
						+ phoneText.getText()));
				startActivity(intent);
			}
		});
		Button visitButton = (Button) findViewById(R.id.btnVisit);
		visitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(profileText.getText().toString()));
				startActivity(intent);
			}
		});
	}
}
