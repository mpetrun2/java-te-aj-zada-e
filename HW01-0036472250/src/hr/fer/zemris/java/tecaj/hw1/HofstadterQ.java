/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

/**
 * @author Marin Petrunić
 *
 */
public class HofstadterQ {

	/**
	 * @param args int traženi član Hofstadterova Q niza
	 */
	public static void main(String[] args) {
		if(args.length == 1)//traži se samo jedan parametar
		{
			Double arg=Double.parseDouble(args[0]);
			Long i=arg.longValue();
			if(i < 0 || arg.compareTo(i.doubleValue()) != 0)//mora bit pozitivan
			{
				System.err.println("Parameter must be natural and positive.");
				System.exit(1);
			}
			System.out.println("You requested calculation of "+i.toString()
					+". number of Hofstadter's Q-sequence. The requested number is "
					+getHofstadterQMember(i).toString()+".");
		}
		else
		{
			System.err.println("Wrong number of parameters given.");
			System.exit(1);
		}
	}
	
	/**
	 * Funkcija za traženje i-tog člana Hofstadterova Q niza
	 * @param i redni broj traženog člana niza
	 * @return traženi član niza
	 */
	static Long getHofstadterQMember(long i){
		Long member=(long) 1;
		Long[] hofstadterQ=new Long[(int) (i+1)];
		//prva dva clana su zadana
		hofstadterQ[0]=(long) 1;
		hofstadterQ[1]=(long) 1;
		long j=2;
		while(j < i )
		{
			j++;
			member=hofstadterQ[(int) (j-1-hofstadterQ[(int) (j-2)])]+hofstadterQ[(int) (j-1-hofstadterQ[(int) (j-3)])];//formula za računanje članova niza
			hofstadterQ[(int) (j-1)]=member;//spremanje j-tog clana
		}
		return member;
	}

}
