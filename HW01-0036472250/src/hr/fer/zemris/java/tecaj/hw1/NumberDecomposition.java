/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

/**
 * @author Marin Petrunić
 *
 */
public class NumberDecomposition {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length ==1)
		{
			Double arg=Double.parseDouble(args[0]);
			Integer num=arg.intValue();
			if(num <= 1 || arg.compareTo(num.doubleValue()) != 0)
			{
				System.err.println("Argument must be  natural and greater then 1.");
				System.exit(1);
			}
			System.out.println("You requested decomposition of number "+num.toString()+" onto prime factors. Here they are:");
			decompositeNumber(num);
		}
		else
		{
			System.err.println("Wrong number of parameters given.");
		}
	}
	
	/**
	 * Funkcija koja rastavlja broj na prim faktore
	 * @param num pocetni broj
	 */
	static void decompositeNumber(Integer num){
		boolean is_prim=true;
		if(num < 2) return;//nema smisla rastavlja 1 na prim faktore
		for(int i=2;i<=num;i++)//traženje najmanjeg prim faktora
		{
			is_prim=true;
			for(int j=2;j<i;j++)//provjera je li trenutni broj prim faktor
			{
				if(i % 2 == 0)
				{
					is_prim=false;
					break;
				}
			}
			if(is_prim == true)
			{
				if(num % i == 0)
				{
					System.out.println(i);
					decompositeNumber(num/i);
					break;
				}
			}
		}
	}

}
