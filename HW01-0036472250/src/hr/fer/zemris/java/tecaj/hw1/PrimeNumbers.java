/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

/**
 * @author Marin Petrunić
 *
 */
public class PrimeNumbers {

	/**
	 * @param args broj traženih prim brojeva
	 */
	public static void main(String[] args) {
		if(args.length == 1)
		{
			Double arg=Double.parseDouble(args[0]);
			Integer num=arg.intValue();
			if(num <= 0 || arg.compareTo(num.doubleValue()) != 0)
			{
				System.err.println("Argument must be natural and greater then 0.");
				System.exit(1);
			}
			System.out.println("You requested calculation of "+num.toString()+" prime numbers. Here they are:");
			getPrimeNumbers(num);
		}
		else
		{
			System.err.println("Wrong number of parameters given!");
			System.exit(1);
		}
	}

	/**
	 * Funkcija koja traži prvih num prim brojeva
	 * @param num broj traženih prim brojeva
	 */
	static void getPrimeNumbers(Integer num)
	{
		int prim=2;
		boolean is_prim=true;
		System.out.println("1) "+prim);
		for(int i=1;i<num;i++)//brojac prim brojeva
		{
			is_prim=true;
			prim++;
			for(int j=2;j<prim;j++)//provjera je li prim broj
			{
				if(prim % 2 == 0) is_prim=false;
			}
			if(is_prim == false)
			{
				i--;	
			}
			else
			{
				System.out.println((i+1)+") "+prim);
			}
		}
	}
}
