/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

/**
 * @author Marin Petrunić
 *
 */
public class ProgramListe {
	
	
	static class CvorListe {
		 CvorListe sljedeci;
		String podatak;
		 }
	
	/**
	 * @param args argumenti komandne linije
	 */
	public static void main(String[] args) {
		CvorListe cvor = null;
		cvor = ubaci(cvor, "Jasna");
		cvor = ubaci(cvor, "Ana");
		cvor = ubaci(cvor, "Ivana");
		System.out.println("Ispisujem listu uz originalni poredak:");
		ispisiListu(cvor);
		 cvor = sortirajListu(cvor);
		System.out.println("Ispisujem listu nakon sortiranja:");
		ispisiListu(cvor);
		int vel = velicinaListe(cvor);
		System.out.println("Lista sadrzi elemenata: "+vel);
	}
	
	/**
	 * Funkcija koja prolazi kroz listu i vraca velicinu liste
	 * @param cvor pocetni cvor liste
	 * @return int velicinu liste
	 */
	static int velicinaListe(CvorListe cvor) {
	 	int velicina=1;
	 	CvorListe iterator=cvor;
	 	while(iterator.sljedeci != null)
	 	{
	 		iterator=iterator.sljedeci;
	 		velicina++;
	 	}
	 	return velicina;
	 }
	
	/**
	 * Funkcija koja ubacuje cvor na kraj ili stvara listu ako lista ne postoji
	 * @param prvi pocetni cvor liste
	 * @param podatak neki podatak kojeg novi cvor treba sadržavati
	 * @return prvi clan liste
	 */
	static CvorListe ubaci(CvorListe prvi, String podatak) {
		 	CvorListe novi=new CvorListe();
		 	novi.podatak=podatak;
		 	novi.sljedeci=null;
		 	//Ako lista ne postoji stvara novu
		 	if(prvi == null)
		 	{
		 		return novi;
		 	}
		 	CvorListe iterator=prvi;
		 	//iteracija kroz listu do zadnjeg cvora
		 	while(iterator.sljedeci != null)
		 	{
		 		iterator=iterator.sljedeci;
		 	}
		 	iterator.sljedeci=novi;
		 	return prvi;
	 }
	
	/**
	 * Funkcija za ispis liste
	 * @param cvor prvi cvor liste
	 */
	static void ispisiListu(CvorListe cvor) {
		CvorListe iterator=cvor;
	 	while(iterator != null)
	 	{
	 		System.out.println(iterator.podatak);
	 		iterator=iterator.sljedeci;
	 	}
	}
	
	
	/**
	 * Funkcija koja sortira listu po abecednom redu
	 * @param cvor prvi cvor liste
	 * @return prvi cvor sortirane liste
	 */
	static CvorListe sortirajListu(CvorListe cvor) {
		CvorListe iterator=cvor;
		//verzija bubble sorta
		for(int i=0;i < velicinaListe(cvor);i++)
		{
			while (iterator.sljedeci != null)
			{				
				if(iterator.podatak.compareTo(iterator.sljedeci.podatak) > 0)//uspoređivanje podataka dvaju cvorova
				{
					String tmp=iterator.sljedeci.podatak;
					iterator.sljedeci.podatak=iterator.podatak;
					iterator.podatak=tmp;
				}
				iterator=iterator.sljedeci;
			}
		}
		return cvor;
 	}


}
