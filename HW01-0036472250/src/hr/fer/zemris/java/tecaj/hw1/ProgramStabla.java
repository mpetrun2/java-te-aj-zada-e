/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

/**
 * @author Marin Petrunić
 *
 */
public class ProgramStabla {

	static class CvorStabla {
		 CvorStabla lijevi;
		 CvorStabla desni;
		 String podatak;
		 }

	/**
	 * @param args argumenti komandne linije
	 */
	public static void main(String[] args) {
		CvorStabla cvor = null;
		cvor = ubaci(cvor, "Jasna");
		cvor = ubaci(cvor, "Ana");
		cvor = ubaci(cvor, "Ivana");
		cvor = ubaci(cvor, "Anamarija");
		cvor = ubaci(cvor, "Vesna");
		cvor = ubaci(cvor, "Kristina");
		System.out.println("Ispisujem stablo inorder:");
		ispisiStablo(cvor);
		cvor = okreniPoredakStabla(cvor);
		System.out.println("Ispisujem okrenuto stablo inorder:");
		ispisiStablo(cvor);
		int vel = velicinaStabla(cvor);
		System.out.println("Stablo sadrzi elemenata: "+vel);
		boolean pronaden = sadrziPodatak(cvor, "Ivana");
		System.out.println("Trazeni podatak je pronaden: "+pronaden);
	}
	
	/**
	 * Funkcija za traženje podatka u stablu
	 * @param korijen početni korijen stabla
	 * @param podatak traženi podatak
	 * @return je li traženi podatak nađen ili nije
	 */
	static boolean sadrziPodatak(CvorStabla korijen, String podatak) {
		if(korijen == null) return false;
		else
		{
			if(podatak.contentEquals(korijen.podatak))	return true;//ako je pronađen traženi podatak
			else
			{
				if(podatak.compareTo(korijen.podatak) > 0)	return sadrziPodatak(korijen.lijevi,podatak);//ako je abecedno prije potraga se nastavlja u lijevo grani
				else	return sadrziPodatak(korijen.desni,podatak);
			}
		}
	}
	
	/**
	 * Rekurzivna funkcija koja računa broj elemenata stabla
	 * @param cvor pocetni korijen stabla
	 * @return int broj elemenata stabla
	 */
	static int velicinaStabla(CvorStabla cvor) {
		if(cvor == null) return 0;
		return velicinaStabla(cvor.desni)+velicinaStabla(cvor.lijevi)+1;
	}
	
	/**
	 * Funkcija koja ako stablo postoji ubacuje element na odgovarajuce mjesto, a ako ne postoji stvara novo stablo
	 * @param korijen glavni korijen stabla
	 * @param podatak podatak novog elementa
	 * @return glavni korijen stabla
	 */
	static CvorStabla ubaci(CvorStabla korijen, String podatak) {
		if(korijen == null)
		{
			//ako je trenutni korijen ne postoji postavljamo novi korijen
			CvorStabla novi=new CvorStabla();
			novi.podatak=podatak;
			return novi;
		}
		else
		{
			if(podatak.compareTo(korijen.podatak) <= 0)
			{
				//ako je podatak abecedno prije trenutnog cvora odlazi se u lijevo stablo
				korijen.lijevi=ubaci(korijen.lijevi,podatak);
			}
			else
			{
				//ako je podatak abecedno poslije trenutnog cvora odlazi se u desno stablo
				korijen.desni=ubaci(korijen.desni,podatak);
			}
			return korijen;
		}
	}
	
	/**
	 * Funkcija za ispis stabla inorder(lijevi cvor->korijen->desni cvor)
	 * @param cvor glavni korijen stabla
	 */
	static void ispisiStablo(CvorStabla cvor) {
		if(cvor == null) return;
		 if(cvor.lijevi == null && cvor.desni ==null)
		 {
			 //ako je to zadnji cvor ispisi ga
			 System.out.println(cvor.podatak);
			 return;
		 }
		 ispisiStablo(cvor.lijevi);//odlazak u lijevu granu
		 System.out.println(cvor.podatak);//ispisi korijen
		 ispisiStablo(cvor.desni);//odlazak u desnu granu
	}
	
	/**
	 * Funkcija koja zamijenjuje lijevu i desnu granu svakog cvora te time okreće poredak stabla
	 * @param korijen početni korijen
	 * @return pocetni korijen
	 */
	static CvorStabla okreniPoredakStabla(CvorStabla korijen) {
		if(korijen == null) return korijen;
		CvorStabla tmp;
		tmp=korijen.desni;
		korijen.desni=korijen.lijevi;
		korijen.lijevi=tmp;
		okreniPoredakStabla(korijen.lijevi);
		okreniPoredakStabla(korijen.desni);
		return korijen;
	}

}
