/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Program za računanje opsega i površine pravokutnika
 * @author Marin Petrunić
 *	
 */
public class Rectangle {

	/**
	 * @param args argumenti komandne linije
	 */
	public static void main(String[] args) throws IOException {
		if(args.length != 2 && args.length > 0)
		{
			System.err.println("Invalid number of arguments provided");
		}
		Double width = null;
		Double height = null;
		if(args.length == 2)
		{
			width=Double.parseDouble(args[0]);
			height=Double.parseDouble(args[1]);
			System.out.println("You have specified a rectangle with width "+String.format("%.1f", width)
					+" and height "+String.format("%.1f", height)
					+". Its area is "+String.format("%.1f", calculateRectangleArea(width,height))
					+" and its perimeter is "+String.format("%.1f", calculateRectanglePerimeter(width,height))+".");
			return;
		}
		// Stvoranje objekta za citanje s tipkovnice:
		 BufferedReader reader = new BufferedReader(
				 new InputStreamReader(new BufferedInputStream(System.in))
		 );
		width=getParam("Width",reader);
		height=getParam("Height",reader);
		System.out.println("You have specified a rectangle with width "+String.format("%.1f", width)
				+" and height "+String.format("%.1f", height)
				+". Its area is "+String.format("%.1f", calculateRectangleArea(width,height))
				+" and its perimeter is "+String.format("%.1f", calculateRectanglePerimeter(width,height))+".");
		return;
	}
	
	/**
	 * Funkcija za računanje površine pravokutnika
	 * @param width parametar širina tipa float
	 * @param height parametar visina tipa float
	 * @return vraća površinu pravokutnika
	 */
	static Double calculateRectangleArea(double width,double height){
		Double area=width*height;
		return area;
	}
	
	/**
	 * Funkcija za računanje opsega pravokutnika
	 * @param width parametar širina tipa float
	 * @param height parametar visina tipa float
	 * @return vraća opseg pravokutnika
	 */
	static Double calculateRectanglePerimeter(double width,double height)
	{
		Double perimeter=2*width+2*height;
		return perimeter;
	}
	
	/**
	 * Funkcija za provjeravanje unosa sa tipkovnice
	 * @param paramName String s imenom traženog parametra
	 * @param reader BufferedReader-objekt za čitanje sa tipkovnice
	 * @return vraća traženi double parametar   
	 * @throws IOException
	 */
	static Double getParam(String paramName,BufferedReader reader) throws IOException{
		Double param=null;
		while(param == null)
		{
			System.out.println("Please provide "+paramName+":");
			String unos = reader.readLine();
			if(unos != null) {
				if(unos.trim().isEmpty())
				{
					System.err.println("Nothing was given.");
				}
				else
				{
					 param = Double.parseDouble(unos);
					 if(param <= 0.0)
					 {
						 System.err.println("Width is negative.");
						 param=null;
					 }
				}
			 }
			else
			{
				break;
			}
		}
		return param;
	}

}
