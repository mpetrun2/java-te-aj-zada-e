/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw1;

import java.lang.Math;
/**
 * @author Marin Petrunić
 *
 */
public class Roots {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 3)
		{
			Double real=Double.parseDouble(args[0]);
			Double imag=Double.parseDouble(args[1]);
			Double root_arg=Double.parseDouble(args[2]);
			Integer root=root_arg.intValue() ;
			if(root <= 1 || root_arg.compareTo(root.doubleValue()) != 0)//provjera je li korijen veći od 1 i prirodan
			{
				System.err.println("Root number must be natural and greater then 1.");
				System.exit(1);
			}
			System.out.println("You requested calculation of "+root.toString()+". roots. Solutions are:");
			calculateKomplexNumberRoot(real,imag,root);
			
		}
		else
		{
			System.err.println("Wrong number of parameters given.");
			System.exit(1);
		}

	}
	
	/**
	 * Funkcija koja vraća modul kompleksnog broja sqrt(x^2+y^2)
	 * @param real realni dio kompleksnog broja
	 * @param imag imaginarni dio kompleksnog broja
	 * @return r-modul kompleksnog broja
	 */
	static Double getKomplexNumberModul(Double real,Double imag)
	{
		Double modul;
		modul=Math.sqrt(Math.pow(real,2)+Math.pow(imag,2));
		return modul;
	}
	
	/**
	 * Funkcija koja vraća argument kompleksnog broja fi=arctan(y/x)
	 * @param real realni dio kompleksnog broja
	 * @param imag imaginarni dio kompleksnog broja
	 * @return fi-argument kompleksnog broja
	 */
	static Double getKomplexNumberArg(Double real,Double imag)
	{
		Double fi;
		fi=Math.atan(imag/real);
		return fi;
	}
	
	
	/**
	 * Funkcija koja računa korijen kompleksnog broja
	 * @param real realni dio kompleksnog broja
	 * @param imag imaginarni dio kompleksnog broja
	 * @param root traženi kroijen
	 */
	static void calculateKomplexNumberRoot(Double real,Double imag,Integer root)
	{
		Double r=getKomplexNumberModul(real,imag);//računanje modula
		Double fi=getKomplexNumberArg(real,imag);//računanje argumenta
		for(int i=0;i<root;i++)//prolazak kroz riješenja
		{
			real=Math.pow(r,(double)1/root)*Math.cos((fi+2*i*Math.PI)/root);
			imag=Math.pow(r,(double)1/root)*Math.sin((fi+2*i*Math.PI)/root);
			printKomplexNumber(i+1,real,imag);//ispis
		}
	}
	
	
	/**
	 * Funkcija za ispis kompleksnih riješenja
	 * @param rb redni broj rijesenja
	 * @param real realni dio broja
	 * @param imag kompleksni dio broja
	 */
	static void printKomplexNumber(Integer rb,Double real,Double imag)
	{
		if(imag < 0)
		{
			System.out.println(rb.toString()+") "+String.format("%.0f",real)+String.format("%.0f",imag)+"i");
			return;
		}
		if(imag < 1E-6)
		{
			System.out.println(rb.toString()+") "+String.format("%.0f",real));
			return;
		}
		System.out.println(rb.toString()+") "+String.format("%.0f",real)+"+"+String.format("%.0f",imag)+"i");
	}

}
