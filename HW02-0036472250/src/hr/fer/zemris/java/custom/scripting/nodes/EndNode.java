/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * @author Marin Petrunić 0036472250
 * Used only to compose original document body.
 * It is symbol of ending non empty tags.
 */
public class EndNode extends Node {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText(){
		return "{$END$}";
	}
}
