/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;

/**
 * @author Marin Petrunić 0036472250
 *
 * Base class for all graph nodes
 */
public class Node {

	/**
	 * Internal managing of children nodes
	 */
	private ArrayBackedIndexedCollection children;
	
	/**
	 * Adding child node to array of children.
	 * Array capacity is managed internally
	 * @param child child to be added
	 */
	public void addChildNode(Node child){
		if(this.children==null)	this.children=new ArrayBackedIndexedCollection(1);
		this.children.add(child);
	}
	
	/**
	 * Find number of nodes children.
	 * 
	 * @return <code>int</code> number of children in current node
	 */
	public int numberOfChildren(){
		if(this.children==null){
			return 0;
		}else{
			return this.children.size();
		}
	}
	
	/**
	 * Retrieves Node at given position(<code>index</code>)
	 * @param index position of required Node
	 * @return node at given position
	 */
	public Node getChild(int index){
		return (Node) this.children.get(index);
	}
	
	/**
	 * Converts node into a <code>String</code> representation
	 * @return <code>String</code> representation of node
	 */
	public String asText(){
		return "";
	}
}
