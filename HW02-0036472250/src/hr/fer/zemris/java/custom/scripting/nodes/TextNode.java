/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;


/**
 * @author Marin Petrunić 0036472250
 *
 * Class that represents text as TextNode
 */
public class TextNode extends Node {

	private String text;
	
	/**
	 * Construct TextNode with text given in parameter <code>text</code>
	 * @param text <code>String</code> that contains text of TextNode
	 */
	public TextNode(String text) {
		this.text=text;
	}
	
	/**
	 * Retrieves TextNode data as <code>String</code>
	 * @return Sting representation of TextNode
	 */
	@Override
	public String asText(){
		return this.text;
	}

}
