/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.parser;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.EndNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * @author Marin Petrunić 0036472250
 *
 * Parser for structured document format.
 */
public class SmartScriptParser {

	private ObjectStack documentTree;
	
	/**
	 * Constructor that creates document tree and delegate document to parse method
	 * @param document that is parsed
	 */
	public SmartScriptParser(String document){
		this.documentTree=new ObjectStack(1);
		this.documentTree.push(new DocumentNode());
		this.parseDocument(document);
	}

	/**
	 * Method for getting root node(DocumentNode)
	 * @return <code>DocumentNode</code>
	 */
	public DocumentNode getDocumentNode() {
		if(this.documentTree.size()==0){
			throw new SmartScriptParserException("DocumentNode doesnt exist.");
		}
		while(this.documentTree.size()>1){
			this.removeParentNode();
		}
		return (DocumentNode) this.removeParentNode();
	}

	/**
	 * Restores document body from nodes.
	 * @param document starting document node(root node)
	 * 
	 * @return <code>String</code> representation of parsed document
	 */
	public String createOriginalDocumentBody(DocumentNode document) {
		return this.reconstructDocument(document);
	}
	
	/**
	 * Recursively restores document text
	 * @param node starting node
	 * @return <code>String</code> representation of document started with given node
	 */
	private String reconstructDocument(Node node){
		if(node.numberOfChildren()==0) return node.asText();
		String doc=node.asText();
		for(int i=0; i<node.numberOfChildren(); i++){
			doc+=this.reconstructDocument(node.getChild(i));
		}
		return doc;
	}
	
	/**
	 * Method for parsing document.
	 * Method separates tags from text nodes.
	 * Tags are parsed separately.
	 * @param document <code>String</code> to be parsed
	 */
	private void parseDocument(String document){
		while(document.isEmpty()==false){
			String[] splitedStrings=document.split("(?<!\\\\)\\{\\$",2);//splits doc at first occurrence of {$ without "\" before
			if(splitedStrings[0].isEmpty() == false){
				this.addNode(new TextNode(splitedStrings[0].replace("\\{", "{")));
				if(splitedStrings.length>1)
				{
					document="{$"+splitedStrings[1];//adds removed opening tag
				}
				else document="";
			}
			else{
				document=splitedStrings[1];
				splitedStrings=document.split("\\$\\}",2);//split at end of tag("&}")
				this.parseTag(splitedStrings[0]);//removes opening tag("{$")
				document=splitedStrings[1];
			}
		}
	}
	
	/**
	 * Method for parsing tag to its components
	 * Tag string is supplied as argument
	 * @param tag String tag to be parsed
	 * @throws SmartScriptParserException if tag name is unrecognized 
	 */
	private void parseTag(String tag){
		tag = tag.replaceAll("[\\s\\n\\t]+"," ").trim(); 
		String tagName=tag.split("\\s",2)[0];
		switch(tagName.toLowerCase()){
			case "for":{
				this.parseForLoop(tag.split("\\s",2)[1]);
			}break;
			case "=":{
				this.parseEchoTag(tag.split("\\s",2)[1]);
			}break;
			case "end":{
				this.addNode(new EndNode());
				this.removeParentNode();
			}break;
			default:{
				throw new SmartScriptParserException("Invalid tag name.");
			}
		}
	}
	
	
	/**
	 * Method for parsing for loop tag.
	 * Arguments are separated and ForLoopNode is constructed and set as new parent
	 * @param loopArguments supplying for loop - type <code>String</code>
	 * @throws SmartScriptParserException is wrong number of loop arguments is provided
	 */
	private void parseForLoop(String loopArguments){
		loopArguments=loopArguments.replaceAll("[\\s\\n\\t]+", " ").trim();//removes unnecessary characters
		String[] arguments=loopArguments.split("\\s");//split loop arguments
		if(arguments.length<3 || arguments.length > 4){
			throw new SmartScriptParserException("FOR loop has wrong number of arguments.");
		}
		Token stepExpression;
		ForLoopNode forLoop;
		if(arguments.length<4){//last argument is optional
			stepExpression=new Token();
		}
		else{
			stepExpression=this.parseArgument(arguments[3],"For loop");
		}
		forLoop=new ForLoopNode(new TokenVariable(arguments[0]), this.parseArgument(arguments[1],"For loop"),
				this.parseArgument(arguments[2],"For loop"), stepExpression);
		this.addNode(forLoop);//adding to parent as child
		this.documentTree.push(forLoop);//adding to stack as new parent
	}
	
	/**
	 * Parses given echo tag arguments.
	 * Allowed arguments are variable, operator, constant, function and string.
	 * @param tagArguments echo tag supplied arguments as <code>String</code>
	 */
	private void parseEchoTag(String tagArguments){
		tagArguments=tagArguments.replaceAll("[\\s\\n\\t]+", " ").trim();//removes extra characters
		Pattern pattern= Pattern.compile("(?:(\")(.*?)(?<!\\\\)(?>\\\\)*\1|([^\\s]+))");//separates tag arguments by space char.
		Matcher regexMatcher=pattern.matcher(tagArguments);//Strings space are ignored(spaces between quotes)
		int counter=0;//array index iterator
		for(counter=0; regexMatcher.find()==true; counter++);
		Token[] tagTokens=new Token[counter];
		counter=0;
		regexMatcher.reset();
		while(regexMatcher.find()==true){//iterating trough set of matched string and parsing
			String argument=regexMatcher.group();
			tagTokens[counter]=this.parseArgument(argument,"EchoTag");
			counter++;
		}
		this.addNode(new EchoNode(tagTokens));
	}

	/**
	 * Method for determine argument type.
	 * Supported argument types are variable, constant, operator, function and string.
	 * @param argument
	 * @param tagName
	 * @return
	 */
	private Token parseArgument(String argument,String tagName){
		switch(argument.charAt(0)){
			case '@':{//function argument
				return this.parseFunctionArgument(argument);
			}
			case '"':{//String argument
				return this.parseStringArgument(argument);
			}
			default:{//variable,operator or invalid
				if(Character.isAlphabetic(argument.charAt(0))==true){//starts with letter means its variable
					return this.parseVariableArgument(argument);
				}
				else{// parse to determine if operator,constant or invalid
					if(argument.length()>1 || Character.isDigit(argument.charAt(0))==true){//number constant
					   if(this.isIntegerType(argument)==true){
						   return new TokenConstantInteger(Integer.parseInt(argument));
					   }
					   else if(this.isDoubleType(argument)==true){
						   		return new TokenConstantDouble(Double.parseDouble(argument));
					   		}
					   		else{
					   			throw new SmartScriptParserException("Invalid argument suplied in "+tagName+". Argument:"+argument);
					   		}
						}
					return this.parseOperatorArgument(argument);
				}
			}
		}
	}
	
	/**
	 * Check if given argument can be parsed as <code>Integer</code> type
	 * @param numberArgument argument to be validated
	 * @return <code>true</code> if argument can be parsed as <code>Integer</code>, <code>false</code> otherwise
	 */
	private boolean isIntegerType(String numberArgument){
		try{
			Integer.parseInt(numberArgument);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/**
	 * Check if given argument can be parsed as <code>Double</code> type
	 * @param numberArgument argument to be validated
	 * @return <code>true</code> if argument can be parsed as <code>Double</code>, <code>false</code> otherwise
	 */
	private boolean isDoubleType(String numberArgument){
		try{
			Double.parseDouble(numberArgument);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/**
	 * Parsing operator argument.
	 * Supported operators are +,-,*,/,%
	 * @param operatorArgument <code>String</code> to be validated
	 * @return new instance of {@link TokenOperator} with operator argument
	 * @throws SmartScriptParserException if operator isnt supported
	 */
	private TokenOperator parseOperatorArgument(String operatorArgument){
		if(operatorArgument.matches("[\\+\\*\\/\\-\\%]")==true){
			return new TokenOperator(operatorArgument);
		}else{
			throw new SmartScriptParserException("Invalid argument suplied in echo tag. "+operatorArgument);
		}
	}
	
	/**
	 * Method for parsing variable argument.
	 * Variable name is being validated.Variable name must consist of letter following
	 * 	by zero or more letters,numbers or underscores.
	 * @param variableArgument variable name as <code>String</code> to be validated
	 * @return new instance of <code>TokenVariable</code>
	 */
	private TokenVariable parseVariableArgument(String variableArgument){
		this.isNameValid(variableArgument, "Variable");
		return new TokenVariable(variableArgument);
	}
	
	/**
	 * Parses function argument and returning its token
	 * @param functionArgument <code>String</code> containing full argument
	 * @return TokenFunction instance of supplied argument
	 */
	private TokenFunction parseFunctionArgument(String functionArgument){
		functionArgument=functionArgument.substring(1);//removes starting "@"
		this.isNameValid(functionArgument, "Function");
		return new TokenFunction(functionArgument);
	}
	
	/**
	 * Parses given <code>String</code> to be valid TokenString
	 * @param stringArgument argument to be parsed
	 * @return instance of TokenString
	 */
	private TokenString parseStringArgument(String stringArgument){
		stringArgument=stringArgument.substring(1, stringArgument.length()-1);
		stringArgument=this.replaceSpecialCharacters(stringArgument);
		return new TokenString(stringArgument);
	}
	
	/**
	 * Replace special char with suiting replacement
	 * @param string <code>String</code> to be processed
	 * @return processed <code>String</code>
	 */
	private String replaceSpecialCharacters(String string){
		string=string.replace("\\\\", "\\").replace("\\\"", "\"").replace("\\t", "\t").replace("\\r", "\r").replace("\\n", "\n");
		return string;
	}
	
	/**
	 * Checks if given variable or function name is valid.
	 * Valid name begins with letter followed by zero or more letters,digits or underscores.
	 * @param name name to be tested
	 * @return true if name is valid
	 * @throws SmartScriptParserException if variable or function name is invalid
	 */
	private boolean isNameValid(String name, String token){
		if(name.matches("^[a-zA-Z][a-zA-Z0-9_]*$")==false){
			throw new SmartScriptParserException(token+" name is invalid.");
		}
		return true;
	}
	
	/**
	 * Add children node to the parent at top of stack
	 * @param node children to be added
	 * @throws SmartScriptParserException if there was too much END tags 
	 */
	private void addNode(Node node){
		if(this.documentTree.isEmpty()){
			throw new SmartScriptParserException("Too much closing tags(End's).");
		}
		else{
			Node parent=(Node) this.documentTree.peek();
			parent.addChildNode(node);
		}
	}
	
	/**
	 * Removes parent Node from top of stack
	 * @return removed parent Node
	 */
	private Node removeParentNode(){
		if(this.documentTree.isEmpty()){
			throw new SmartScriptParserException("Too much closing tags(End's).");
		}
		else{
			return (Node) this.documentTree.pop();
		}
	}
	
}
