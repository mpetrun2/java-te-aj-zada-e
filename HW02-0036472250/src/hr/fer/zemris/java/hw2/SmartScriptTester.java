/**
 * 
 */
package hr.fer.zemris.java.hw2;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;


/**
 * @author Marin Petrunić 0036472250
 * Program for testing {@link SmartScriptParser} class
 */
public class SmartScriptTester {

	/**
	 * Command line arguments are irellevant
	 * @param args
	 */
	public static void main(String[] args) {
		String docBody = "This is s"
			+ " \\{$for 1 1 1$} ample text.\n"
			+ "Joe \"Long\" Smith\n"
			+ "\"\"Joe \\\"Long\\\" Smith\"\"\n"
			+ "{$ FoR i 1 2 					$}\n"
			+ " This isdfs \\{$= i a$}-th time this "
			+ "{$ FoR asdfi 1 2 1$}message is generated.\n"
			+ "{$eND$}\n"
			+ "{$eND$}\n"
			+ "{$FOR iasdf_456 0 10 $}\n"
			+ " sin({$= i asdf sadf $}^2) = {$= i i * @sin \"0.000\" \"\\\"\\ta\" @decfmt $}\n"
			+ "{$eND$}\n";
		SmartScriptParser parser = new SmartScriptParser(docBody);
		System.out.println(parser.createOriginalDocumentBody(parser.getDocumentNode()));
	}
}
