/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw3;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class provides support for working with <code>String</code>.
 * It provides set of method to make work with Strings easy.
 * 
 */
public class CString {

	/** Variable containing char array of class. */
	private final char[] data;
	
	/** Index from which <code>String</code> start. */
	private final int offset;
	
	/** Number of elements in char array data. */
	private final int length;
	
	/**
	 * Constructs new <code>CString</code> from given <code>char</code> array from
	 * given <code>char</code> array, offset and length.
	 * 
	 * <code>CString</code> internally contains new char array same as <code>data</code> 
	 * and not just reference.
	 * 
	 * @param data array of chars
	 * @param offset index of char that represents start of <code>CString</code>
	 * @param length number of elements in <code>CString</code>
	 */
	public CString(final char[] data, final int offset, final int length) {
		if (offset < 0 || length < 0 || (length + offset) > data.length) {
			throw new IllegalArgumentException("Invalid length given.");
		}
		this.data = this.copyCharArray(data);
		this.offset = offset;
		this.length = length;
	}
	
	/**
	 * Constructs new <code>CString</code> from given <code>char</code> array from
	 * given <code>char</code> array.Ofsett is <code>0</code> and length is equal to 
	 * number of characters in given array.
	 * 
	 * @param data array of characters which constructs <code>CString</code>
	 */
	public CString(final char[] data) {
		this.data = this.copyCharArray(data);
		this.offset = 0;
		this.length = data.length;
	}
	
	//public CString(CString original) {}
	
	/**
	 * Constructs <code>CString</code> from given <code>String</code>.
	 * <code>CString</code> length is equal to given <code>String</code> length.
	 * Offset is set to <code>0</code>.
	 * @param s given <code>String</code>
	 */
	public CString(final String s) {
		this.length = s.length();
		this.offset = 0;
		this.data = s.toCharArray();
	}
	
	/**
	 * Creates new <code>CString</code> from <code>CString</code> but allocates its own set
	 * of data.
	 * @param original CString object
	 */
	public CString(final CString original) {
		this.data = this.copyCharArray(original.toCharArray());
		this.offset = 0;
		this.length = original.length();
	}
	
	/**
	 * Private <code>CString</code> constructor. This constructor doesn't allocate
	 * new array of characters.
	 * @param offset start of new <code>CString</code>
	 * @param length length of new <code>CString</code>
	 * @param data array of characters
	 */
	private CString(final int offset, final int length, final char[] data) {
		this.data = data;
		this.offset = offset;
		this.length = length;
	}
	
	/**
	 * Returns length of <code>CString</code>. Length is equal to the number of
	 * characters in <code>String</code> array.
	 * @return <code>int</code> length
	 */
	public final int length() {
		return this.length;
	}
	/**
	 * Returns character at specified index. Index must be in range of 0 to length-1.
	 * @param index position of character in this <code>CString</code>
	 * @return character at specified index
	 * @throws {@link IllegalArgumentException} if index not in range
	 */
	public final char charAt(final int index) {
		if (index < 0 || index >= this.length) {
			throw new IllegalArgumentException("Index must be in range of 0 to length-1.");
		}
		return this.data[index + this.offset];
	}
	
	/**
	 * Converts this <code>CString</code> to a newly allocated char array and returns it.
	 * @return newly allocated char array
	 */
	public final char[] toCharArray() {
		return this.copyCharArrayOffset(this.data, this.offset, this.length);
	}
	
	/**
	 * Creates new <code>String</code> from char array in this <code>CString</code>.
	 * @return new <code>String</code> from this <code>CString</code>
	 */
	@Override 
	public final String toString() {
		return new String(this.copyCharArrayOffset(this.data, this.offset, this.length));
	}
	
	/**
	 * Returns the index within this <code>CString</code> of first occurrence of given <code>char</code>.
	 * Returns -1 if there is no such character.
	 * @param c character searched
	 * @return position of first occurrence pf character or -1 if character isnt found
	 */
	public final int indexOf(final char c) {
		for (int i = this.offset; i < this.length + this.offset; i++) {
			if (this.data[i] == c) {
				return i - this.offset;
			}
		}
		return -1;
	}
	
	/**
	 * Checks this <code>CString</code> if it starts with given <code>CString</code>.
	 * Method returns false if given <code>CString</code> length is greater then this
	 * <code>CString</code> length. Returns true if characters at same position matches
	 * or given <code>CString</code> is 0.
	 * @param s given <code>CString</code> to be compared
	 * @return <code>true</code> if starts with given <code>CString</code> or false otherwise
	 */
	public final boolean startsWith(final CString s) {
		if (s.length() > this.length) {
			return false;
		}
		boolean stringStartsWith = true;
		for (int i = 0; i < s.length(); i++) {
			if (this.data[i + this.offset] != s.charAt(i)) {
				stringStartsWith = false;
			}
		}
		return stringStartsWith;
	}
	
	/**
	 * Checks this <code>CString</code> if it ends with given <code>CString</code>.
	 * Method returns false if given <code>CString</code> length is  greater then this
	 * <code>CString</code> length. Returns true if characters at same position matches
	 * or given <code>CString</code> is 0.
	 * @param s given <code>CString</code> to be compared
	 * @return <code>true</code> if ends with given <code>CString</code> or false otherwise
	 */
	public final boolean endsWith(final CString s) {
		if (s.length() > this.length) {
			return false;
		}
		boolean stringStartsWith = true;
		for (int i = 0; i < s.length(); i++) {
			int orgIndex = i + this.offset + this.length - s.length;
			if (this.data[orgIndex] != s.charAt(i)) {
				stringStartsWith = false;
			}
		}
		return stringStartsWith;
	}
	
	/**
	 * Searches this <code>CString</code> for occurrence of given <code>CString</code>.
	 * Returns false if given <code>CString</code> length is 0.
	 * @param s given <code>CString</code> to be searched
	 * @return <code>true</code> if found occurrence of given <code>CString</code>
	 */
	public final boolean contains(final CString s) {
		boolean contains = true;
		if (s.length() == 0) {
			return false;
		}
		for (int orginalIndex = this.offset; orginalIndex <= (this.length + this.offset - s.length()); orginalIndex++) {
			contains = true;
			for (int sIndex = 0, orgIndex = orginalIndex; sIndex < s.length(); sIndex++, orgIndex++) {
				if (this.data[orgIndex] != s.charAt(sIndex)) {
					contains = false;
					break;
				}
			}
			if (contains) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Creates new <code>CString</code> from this <code>CString</code> from 
	 * position of given start index and ends at end index without including it.
	 * Complexity of this method is O(1).
	 * @param startIndex offset of this <code>CString</code>
	 * @param endIndex end of this 
	 * @return new <code>CString</code> representing substring of current <code>CString</code>
	 * @throws {@link IllegalArgumentException}
	 */
	public final CString substring(final int startIndex, final int endIndex) {
		if (startIndex < 0 || endIndex < startIndex) {
			throw new IllegalArgumentException("Start index must be greater or equal to zero and"
					+ " end index must be greater or equal to start index.");
		}
		return new CString(startIndex, endIndex - startIndex, this.data);
	}
	
	/**
	 * Returns new CString which represents starting part of original string and is of length n.
	 * @param n length of new CString
	 * @return new instance of CString with given length
	 * @throws {@link IllegalArgumentException} if length is negative or greater than original CString
	 */
	public final CString left(final int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Length must be greater or equal to 0.");
		}
		if (n > this.length) {
			throw new IllegalArgumentException("Given length must be smaller or equal then length of CString");
		}
		return new CString(0, n, this.data);
	}
	
	/**
	 * Returns new CString which represents ending part of original string and is of length n.
	 * @param n length of new CString
	 * @return new instance of CString with given length
	 * @throws {@link IllegalArgumentException} if length is negative or greater than original CString
	 */
	public final CString right(final int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Length must be greater or equal to 0.");
		}
		if (n > this.length) {
			throw new IllegalArgumentException("Given length must be smaller or equal then length of CString");
		}
		return new CString(this.length + this.offset - n, n, this.data);
	}
	
	/**
	 * Creates a new CString which is concatenation of current and given string.
	 * @param s given CString to add.
	 * @return new <code>CString</code> instance with concatenation of two <code>CString</code>s
	 * @throws {@link IllegalArgumentException} when given CString is null or empty
	 */
	public final CString add(final CString s) {
		if (s == null || s.length() == 0) {
			throw new IllegalArgumentException("Given CString is null or empty.");
		}
		char[] newData = new char[this.length + s.length];
		//copy old array
		for (int index = 0; index < this.length; index++) {
			newData[index] = this.data[index + this.offset];
		}
		//copy new array
		for (int index = 0; index <  s.length(); index++) {
			newData[index + this.length] = s.charAt(index);
		}
		return new CString(newData);
	}
	
	/**
	 * Creates a new CString in which each occurrence of old character is replaces with new character.
	 * @param oldChar old character
	 * @param newChar new character
	 * @return new CString
	 */
	public final CString replaceAll(final char oldChar, final char newChar) {
		char[] newCharArray = this.copyCharArrayOffset(this.data, this.offset, this.length);
		for (int index = 0; index < this.length; index++) {
			if (newCharArray[index + this.offset] == oldChar) {
				newCharArray[index + this.offset] = newChar;
			}
		}
		return new CString(newCharArray, this.offset, this.length);
	}
	
	/**
	 * Creates a new CString in which each occurrence of old substring is replaces with the new substring.
	 * @param oldString matched string
	 * @param newString replacement string
	 * @return new CString
	 */
	public final CString replaceAll(final CString oldString, final CString newString) {
		CString newCharArray = new CString(new char[0]);
		int index = this.offset;
		for (; index < this.offset + this.length; index++) {
			if (this.substring(index, index + oldString.length()).contains(oldString)) {
				newCharArray = newCharArray.add(newString);
				index += oldString.length() - 1;
			} else {
				newCharArray = newCharArray.add(new CString(String.valueOf(this.charAt(index))));
			}
		}
		return newCharArray;
	}
	
	/**
	 * Makes new array of char instance and copies characters from original
	 * array to new one.
	 * 
	 * @param orginalArray given array as source of characters
	 * @param offset starting offset
	 * @param length length of CString
	 * @return new character array containing characters from given array
	 */
	private char[] copyCharArrayOffset(final char[] orginalArray, final int offset, final int length) {
		char[] newData = new char[length];
		for (int i = 0; i < length; i++) {
			newData[i] = orginalArray[i + offset];
		}
		return newData;
	}
	
	/**
	 * Makes new array of char instance and copies characters from original
	 * array to new one.
	 * 
	 * @param orginalArray given array as source of characters
	 * @return new character array containing characters from given array
	 */
	private char[] copyCharArray(final char[] orginalArray) {
		char[] newData = new char[orginalArray.length];
		for (int i = 0; i < orginalArray.length; i++) {
			newData[i] = orginalArray[i];
		}
		return newData;
	}

}
