package hr.fer.zemris.java.tecaj.hw3;

import static java.lang.Math.*;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class <code>ComplexNumber</code> provides support for
 * manipulating with complex numbers.
 * <p>Complex number can be created from real and imaginary part,
 * parsed from <code>String</code> or from magnitude and angle.</p>
 * <p>Class currently supports addition, subtraction, multiplication,
 * division, exponentiation and finding root.</p>
 * @author Marin Petrunić 0036472250
 *
 */
public class ComplexNumber {

	/** Real part of complex number. */
	private final double real;
	/** Imaginary part of complex number. */
	private final double imaginary;
	
	/**
	 * Constructor for <code>ComplexNumber</code>.
	 * @param realPart any <code>double</code> type number that will represent real part of complex number
	 * @param imaginaryPart any <code>double</code> type number that will represent imaginary part of complex number
	 */
	public ComplexNumber(final double realPart, final double imaginaryPart) {
		this.real = realPart;
		this.imaginary = imaginaryPart;
	}
	
	/**
	 * Creates new <code>ComplexNumber</code> with given parameter as real part of complex number
	 * and <code>zero</code> as imaginary part.
	 * @param realPart <code>double</code> real part of complex number
	 * @return new instance of <code>ComplexNumber</code>
	 */
	public static ComplexNumber fromReal(final double realPart) {
		return new ComplexNumber(realPart, 0);
	}
	
	/**
	 * Creates new <code>ComplexNumber</code> with given parameter as imaginary part of complex number
	 * and <code>zero</code> as real part.
	 * @param imaginaryPart <code>double</code> imaginary part of complex number
	 * @return new instance of <code>ComplexNumber</code>
	 */
	public static ComplexNumber fromImaginary(final double imaginaryPart) {
		return new ComplexNumber(0, imaginaryPart);
	}
	
	/**
	 * Creating <code>ComplexNumber</code> from its magnitude and angle.
	 * Trigonometric form formula is: r*(cos(fi)+i*sin(fi)).
	 * In this method magnitude is variable <code>r</code> and angle is <code>fi</code>
	 * @param magnitude <code>double</code>  size of vector
	 * @param angle <code>double</code> angle of vector
	 * @return new instance of <code>ComplexNumber</code>
	 * @throws IllegalArgumentException when magnitude is negative number
	 */
	public static ComplexNumber fromMagnitudeAndAngle(final double magnitude, final double angle) {
		if (magnitude < 0) {
			throw new IllegalArgumentException("Magnitude must be positive.");
		}
		double realPart = magnitude * cos(angle);
		double imaginaryPart = magnitude * sin(angle);
		return new ComplexNumber(realPart, imaginaryPart);
	}
	
	/**
	 * Parse <code>String</code> as <code>ComplexNumber</code>.
	 * Examples of allowed format: "3.51", "-3.17", "-2.71i", "i", "1", "-2.71-3.15i"
	 * @param s <code>String</code> to be parsed
	 * @return new instance of <code>ComplexNumber</code> with parsed complex number
	 */
	public static ComplexNumber parse(final String s) {
		Pattern findReal = Pattern.compile("(?<!i)(?>[+-]?[ ]*[0-9]+[\\.]?[0-9]*)(?!i)"); //real part regex
		Matcher realMatcher = findReal.matcher(s);
		String match;
		try {
			realMatcher.find();
			match = realMatcher.group().replaceAll("[\\s]*", "");
		} catch (Exception e) {
			match = "0";
		}
		double realPart = Double.parseDouble(match);
		Pattern findImaginary = Pattern.compile("[+-]?[ ]*[0-9]*[\\.]?[0-9]*[\\s]*(?=i)i"); //imaginary regex
		Matcher imaginaryMatcher = findImaginary.matcher(s);
		double imaginaryPart;
		try {
			imaginaryMatcher.find();
			match = imaginaryMatcher.group().replaceAll("[\\s]*", "");
		} catch (Exception e) {
			match = "0";
		}
		if (match.length() <= 2) {
			imaginaryPart = Double.parseDouble(match.replace('i', '1'));
		} else {
			match = match.replaceFirst("i", "");
			imaginaryPart = Double.parseDouble(match);
		}
		return new ComplexNumber(realPart, imaginaryPart);
	}

	/**
	 * @return the real <code>double</code> real part of complex number
	 */
	public final double getReal() {
		return real;
	}

	/**
	 * @return the imaginary <code>double</code> imaginary part of complex number
	 */
	public final  double getImaginary() {
		return imaginary;
	}
	
	/**
	 * Calculates magnitude of complex number.
	 * Formula for magnitude is |z|=sqrt(Re^2+Im^2).
	 * @return <code>double</code> magnitude of complex number
	 */
	public final double getMagnitude() {
		return sqrt(pow(this.real, 2) + pow(this.imaginary, 2));
	}
	
	/**
	 * Calculates angle of complex number.
	 * Formula is fi=atan(Im/Re).
	 * @return <code>double</code> angle of complex number in radiant (-pi,pi)
	 */
	public final double getAngle() {
		return atan2(imaginary, real);
	}
	
	/**
	 * Returns new instance of <code>ComplexNumber</code> whose real and imaginary part
	 * are equal to current value + values in <code>c</code>. 
	 * @param c complex number to be added with current
	 * @return new <code>ComplexNumber</code> with added values
	 */
	public final ComplexNumber add(final ComplexNumber c) {
		return new ComplexNumber(this.real + c.getReal(), this.imaginary + c.getImaginary());
	}
	
	/**
	 * Returns new instance of <code>ComplexNumber</code> whose real and imaginary part
	 * are equal to current value + values in <code>c</code>.
	 * @param c complex number to be subtracted from current complex number
	 * @return new <code>ComplexNumber</code> with subtracted values
	 */
	public final ComplexNumber sub(final ComplexNumber c) {
		return new ComplexNumber(this.real - c.getReal(), this.imaginary - c.getImaginary());
	}
	
	/**
	 * Returns new instance of <code>ComplexNumber</code> whose equal to current complex number multiplied
	 * with given complex number.
	 * Formula for calculating real and imaginary part is: 
	 * <code>(a + ib) · (c + id) = ac + i^2bd + ibc + iad = (ac − bd) + i(bc + ad)</code>
	 * @param c multiplier complex number
	 * @return result as instance of complex number
	 */
	public final ComplexNumber mul(final ComplexNumber c) {
		double realPart = this.real * c.getReal() - this.imaginary * c.getImaginary();
		double imaginaryPart = this.imaginary * c.getReal() + this.real * c.getImaginary();
		return new ComplexNumber(realPart, imaginaryPart);
	}
	
	/**
	 * Returns new instance of <code>ComplexNumber</code> whose equal to current complex number divided
	 * with given complex number.
	 * Formula for calculating real and imaginary part is:
	 * (a + ib)/(c + id)=((ac + bd) + i(bc − ad))/(c^2 + d^2)
	 * @param c divider
	 * @return result as instance of complex number
	 * @throws IllegalArgumentException when |c| is zero
	 */
	public final ComplexNumber div(final ComplexNumber c) {
		if(pow(c.getReal(), 2) + pow(c.getImaginary(), 2) == 0) {
			throw new IllegalArgumentException("Division by zero");
		}
		double realPart = (this.real * c.getReal() + this.imaginary * c.getImaginary()) 
				/ (pow(c.getReal(), 2) + pow(c.getImaginary(), 2));
		double imaginaryPart = (this.imaginary * c.getReal() - this.real * c.getImaginary()) 
				/ (pow(c.getReal(), 2) + pow(c.getImaginary(), 2));
		return new ComplexNumber(realPart, imaginaryPart);
	}
	
	/**
	 * Calculates current complex number on n-th exponent.
	 * Formula for calculation: z^n=r^n(cos(n*fi)+isin(n*fi))
	 * @param n given exponent
	 * @return result as new <code>ComplexNumber</code>
	 * @throws IllegalArgumentException if exponent is negative
	 */
	public final ComplexNumber power(final int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Exponent must greater or equal to zero.");
		}
		double realPart = pow(this.getMagnitude(), n) * cos(n * this.getAngle());
		double imaginaryPart = pow(this.getMagnitude(), n) * sin(n * this.getAngle());
		return new ComplexNumber(realPart, imaginaryPart);
	}
	
	/**
	 * Calculates all <code>n</code> roots of current complex number.
	 * Returns array of <code>ComplexNumber</code> instances for each root.
	 * @param n root
	 * @return array of complex number results
	 */
	public final ComplexNumber[] root(final int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Root must be greater or equal to 0.");
		}
		ComplexNumber[] roots = new ComplexNumber[n];
		for (int i = 0; i < n; i++) {
			double realPart = pow(this.getMagnitude(), 1.0 / n) * cos((this.getAngle() + 2.0 * (double) i * PI) / (double) n);
			double imaginaryPart = pow(this.getMagnitude(), 1.0 / n)
					* sin((this.getAngle() + 2.0 * i * PI) / n);
			roots[i] = new ComplexNumber(realPart, imaginaryPart);
		}
		return roots;
	}
	
	/**
	 * @return <code>String</code> representation of complex number
	 */
	@Override
	public final String toString() {
		DecimalFormat realFormat = new DecimalFormat("0.##");
		DecimalFormat imaginaryFormat = new DecimalFormat(" + 0.##i; - 0.##i");
		return realFormat.format(this.real) + imaginaryFormat.format(this.imaginary);
	}
}
