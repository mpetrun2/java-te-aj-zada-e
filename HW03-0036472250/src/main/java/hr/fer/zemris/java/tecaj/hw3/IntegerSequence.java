/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw3;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class IntegerSequence implements Iterable<Integer> {

	/** Starting number. */
	private int start;
	/** End number. */
	private int end;
	/** Step size number. */
	private int step;
	
	/**
	 * Creates new <code>IntegerSequence</code> that will iterate from start to end with given step.
	 * Iteration from smaller to higher number and vice versa is allowed but infinite loops are not.
	 * @param start starting number
	 * @param end ending number
	 * @param step size of step
	 * @throws IllegalArgumentException if argument combination causes infinite loop
	 */
	public IntegerSequence(final int start, final int end, final int step) {
		if (start > end && step >= 0) {
			throw new IllegalArgumentException("This combination of parameters will cause infinite loop.");
		}
		if (start < end && step <= 0) {
			throw new IllegalArgumentException("This combination of parameters will cause infinite loop.");
		}
		this.start = start;
		this.end = end;
		this.step = step;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new NumberIterator(); 
	}
	
	/**
	 * @author Marin Petrunić 0036472250
	 * 
	 * Class for iterating trough {@link IntegerSequence}.
	 *
	 */
	private class NumberIterator implements Iterator<Integer> {
		
		/** Current number in loop. */
		private int current;
		
		/** End of loop. */
		private int endLimit;
		/** Step size. */
		private int  stepSize;
		
		/**
		 * Constructs new iterator and sets variables.
		 */
		public NumberIterator() {
			this.current = start - step;
			this.endLimit = end;
			this.stepSize = step;
		}

		/**
		 * Checks if there some numbers remained in loop.
		 * @return <code>True</code> if loop didn't reach end, <code>false</code> otherwise
		 */
		@Override
		public boolean hasNext() {
			if (this.stepSize > 0) {
				if (this.current + this.stepSize <= this.endLimit) {
					return true;
				} else {
					return false;
				}
			} else {
				if (this.current + this.stepSize >= this.endLimit) {
					return true;
				} else {
					return false;
				}
			}
		}

		/**
		 * Change iterator to next element in sequence.
		 * @return next number in sequence.
		 * @throws NoSuchElementException if there are no more numbers left
		 */
		@Override
		public Integer next() {
			if (!this.hasNext()) {
				throw new NoSuchElementException("There are no more elements in sequence");
			}
			this.current += this.stepSize;
			return this.current;
		}

		/**
		 * @throws UnsupportedOperationException removing numbers is not supported
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException("Brisanje brojeva nije moguće.");
		}
		
	}

}
