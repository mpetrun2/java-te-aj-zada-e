/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw3;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class CStringTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#CString(char[], int, int)}.
	 */
	@Test
	public void testCStringCharArrayIntInt() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 1, 4);
		assertEquals(4, c.length());
		assertArrayEquals(new char[] {'a','r','i','n'}, c.toCharArray());
	}
	
	@Test
	public void testCStringCString() {
		CString c = new CString("Marin");
		CString d = new CString(c);
		assertArrayEquals(c.toCharArray(), d.toCharArray());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#CString(char[], int, int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testCStringCharArrayIntIntInvalid() {
		@SuppressWarnings("unused")
		CString c = new CString(new char[] {'M','a','r','i','n'}, -1, 1);
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#CString(char[])}.
	 */
	@Test
	public void testCStringCharArray() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertEquals(5, c.length());
		assertArrayEquals(new char[] {'M','a','r','i','n'}, c.toCharArray());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#CString(java.lang.String)}.
	 */
	@Test
	public void testCStringString() {
		CString c = new CString("Marin");
		assertEquals(5, c.length());
		assertArrayEquals(new char[] {'M','a','r','i','n'}, c.toCharArray());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#length()}.
	 */
	@Test
	public void testLength() {
		CString c = new CString("Marin");
		assertEquals(5, c.length());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#charAt(int)}.
	 */
	@Test
	public void testCharAt() {
		CString c = new CString("Marin");
		assertEquals('r', c.charAt(2));
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#charAt(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testCharAtIllegal() {
		CString c = new CString("Marin");
		assertEquals('r', c.charAt(-1));
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#charAt(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testCharAtIllegal2() {
		CString c = new CString("Marin");
		assertEquals('r', c.charAt(6));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#toCharArray()}.
	 */
	@Test
	public void testToCharArray() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 2, 3);
		assertArrayEquals(new char[] {'r','i','n'}, c.toCharArray());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#toString()}.
	 */
	@Test
	public void testToString() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 2, 3);
		assertEquals("rin", c.toString());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#indexOf(char)}.
	 */
	@Test
	public void testIndexOf() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 2, 3);
		assertEquals(2, c.indexOf('n'));
		assertEquals(-1, c.indexOf('z'));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#startsWith(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test
	public void testStartsWith() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 1, 4);
		assertTrue(c.startsWith(new CString("ari")));
		assertFalse(c.startsWith(new CString("rko")));
		assertFalse(c.startsWith(new CString("Milutin")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#endsWith(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test
	public void testEndsWith() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 0, 4);
		assertTrue(c.endsWith(new CString("ari")));
		assertFalse(c.endsWith(new CString("rko")));
		assertFalse(c.endsWith(new CString("Milutin")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#contains(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test
	public void testContains() {
		CString c = new CString(new char[] {'M','a','r','i','n'}, 0, 4);
		assertFalse(c.contains(new CString("krk")));
		assertTrue(c.contains(new CString("ari")));
		assertFalse(c.contains(new CString("")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#substring(int, int)}.
	 */
	@Test
	public void testSubstring() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertArrayEquals(new char[] {'M','a'}, c.substring(0, 2).toCharArray());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#substring(int, int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSubstringIllegal() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.substring(10, 1);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#substring(int, int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testSubstringIllegal2() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.substring(-1, 1);
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#left(int)}.
	 */
	@Test
	public void testLeft() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertArrayEquals(new char[] {'M','a','r'}, c.left(3).toCharArray());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#left(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testLeftIllegal() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.left(-1);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#left(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testLeftIllegal2() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.left(10);
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#right(int)}.
	 */
	@Test
	public void testRight() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertArrayEquals(new char[] {'r','i','n'}, c.right(3).toCharArray());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#right(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testRightIllegal() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.right(-1);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#right(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testRightIllegal2() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.right(11);
	}
	

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#add(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test
	public void testAdd() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertArrayEquals(new CString("Marin Petrunic").toCharArray(), c.add(new CString(" Petrunic")).toCharArray());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#add(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testAddIllegal() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.add(new CString(""));
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#add(hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testAddIllegal2() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		c.add(null);
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#replaceAll(char, char)}.
	 */
	@Test
	public void testReplaceAllCharChar() {
		CString c = new CString(new char[] {'M','a','r','i','n'});
		assertArrayEquals(new CString("Marko").toCharArray(), c.replaceAll('i','k').replaceAll('n', 'o').toCharArray());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.tecaj.hw3.CString#replaceAll(hr.fer.zemris.java.tecaj.hw3.CString, hr.fer.zemris.java.tecaj.hw3.CString)}.
	 */
	@Test
	public void testReplaceAllCStringCString() {
		assertArrayEquals(new CString("abababababab").toCharArray(),new CString("ababab").replaceAll(new CString("ab"), new CString("abab")).toCharArray());
		assertArrayEquals(new CString("Mrain").toCharArray(),new CString("Marin").replaceAll(new CString("ar"), new CString("ra")).toCharArray());
	}

}
