package hr.fer.zemris.java.tecaj.hw3;

import static org.junit.Assert.*;
import static java.lang.Math.*;

import org.junit.Test;

public class ComplexNumberTest {

	@Test
	public void constructComplexNumberReal() {
		ComplexNumber c = new ComplexNumber(3.15, 0);
		assertEquals("Expected real part of Complex number is 3.15", c.getReal(), 3.15, 1e-8);
	}

	@Test
	public void constructComplexNumberImaginary() {
		ComplexNumber c = new ComplexNumber(0, -1.127);
		assertEquals("Expected imaginary part of Complex number is -1.127", c.getImaginary(), -1.127, 1e-8);
	}

	@Test
	public void testFromReal() {
		assertEquals("Expected real part of Complex number is 3",  3, ComplexNumber.fromReal(3).getReal(), 1e-8);
	}

	@Test
	public void testFromImaginary() {
		assertEquals("Expected imaginary part of Complex number is -1", -1, ComplexNumber.fromImaginary(-1).getImaginary(), 1e-8);
	}

	@Test
	public void testFromMagnitudeAndAngleImaginary() {
		assertEquals("Expected imaginary part of Complex number is 1.",  1, ComplexNumber.fromMagnitudeAndAngle(sqrt(2), PI/4).getImaginary(), 1e-8);
	}
	
	@Test
	public void testFromMagnitudeAndAngleReal() {
		assertEquals("Expected real part of Complex number is 1.",  1, ComplexNumber.fromMagnitudeAndAngle(sqrt(2), PI/4).getImaginary(), 1e-8);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFromMagnitudeAndAngleRealIllegalArgument() {
		ComplexNumber.fromMagnitudeAndAngle(-3, PI/4);
	}
	
	@Test
	public void testParse() {
		assertEquals("Expected real part of Complex number is -2.15.", -2.15, ComplexNumber.parse("-2.15+ 3.17i").getReal(), 1e-8);
		assertEquals("Expected imaginary part of Complex number is 3.17.", 3.17, ComplexNumber.parse("-2.15+ 3.17i").getImaginary(), 1e-8);
	}
	
	@Test
	public void testParsenoRealPart() {
		assertEquals("Expected real part of Complex number is 0.", 0, ComplexNumber.parse("3.17i").getReal(), 1e-8);
	}
	
	@Test
	public void testParsenoImaginaryPart() {
		assertEquals("Expected imaginary part of Complex number is 0.", 0, ComplexNumber.parse("3.17").getImaginary(), 1e-8);
	}

	@Test
	public void testGetReal() {
		ComplexNumber c = new ComplexNumber(3.15, 0);
		assertEquals("Expected real part of Complex number is 3.15", 3.15, c.getReal(), 1e-8);
	}

	@Test
	public void testGetImaginary() {
		ComplexNumber c = new ComplexNumber(0, -1.127);
		assertEquals("Expected imaginary part of Complex number is -1.127", -1.127, c.getImaginary(), 1e-8);
	}

	@Test
	public void testGetMagnitude() {
		ComplexNumber c = new ComplexNumber(1/2.0, -sqrt(3)/2);
		assertEquals("Expected magnitude is 1", 1, c.getMagnitude(), 1e-8);
	}

	@Test
	public void testGetAngle() {
		ComplexNumber c = new ComplexNumber(1/2.0, -sqrt(3)/2);
		assertEquals("Expected angle is -PI/3", -PI/3.0, c.getAngle(), 1e-8);
	}

	@Test
	public void testAdd() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber b = new ComplexNumber(-6.17, -3.65);
		assertEquals("Expected real part of addition is -3.03.", -3.03, a.add(b).getReal(), 1e-8);
		assertEquals("Expected imaginary part of addition is -5.8.", -5.8, a.add(b).getImaginary(), 1e-8);
	}

	@Test
	public void testSub() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber b = new ComplexNumber(-6.17, -3.65);
		assertEquals("Expected real part of subtraction is 9.31.", 9.31, a.sub(b).getReal(), 1e-8);
		assertEquals("Expected imaginary part of subtraction is 1.5.", 1.5, a.sub(b).getImaginary(), 1e-8);
	}

	@Test
	public void testMul() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber b = new ComplexNumber(-6.17, -3.65);
		assertEquals("Expected real part of multiplication is -27.2213.",-27.2213, a.mul(b).getReal(), 1e-8);
		assertEquals("Expected imaginary part of multiplication is 1.8045.", 1.8045, a.mul(b).getImaginary(), 1e-8);
	}

	@Test
	public void testDiv() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber b = new ComplexNumber(-6.17, -3.65);
		assertEquals("Expected real part of division is -0.22428460.",-0.22428460, a.div(b).getReal(), 1e-8);
		assertEquals("Expected imaginary part of division is 0.4811408.", 0.4811408134, a.div(b).getImaginary(), 1e-8);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testDivbyZero() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber b = new ComplexNumber(0, 0);
		a.div(b);
	}

	@Test
	public void testPower() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		assertEquals("Expected real part of complex number raised by 2 is 5.2371.", 5.2371, a.power(2).getReal(), 1e-8);
		assertEquals("Expected imaginary part of complex number raised by 2 is -13.502.", -13.502, a.power(2).getImaginary(), 1e-8);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPowerIllegalArgument() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		a.power(-1);
	}

	@Test
	public void testRoot() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		ComplexNumber[] roots = new ComplexNumber[2];
		roots[0]= new ComplexNumber(1.86353, -0.57686);
		roots[1]= new ComplexNumber(-1.86353, 0.57686);
		assertEquals("First root real element expected 1.86353.", roots[0].getReal(), a.root(2)[0].getReal(), 1e-5);
		assertEquals("Second root real element expected -1.86353.", roots[1].getReal(), a.root(2)[1].getReal(), 1e-5);
		assertEquals("First root imaginary element expected -0.57686.", roots[0].getImaginary(), a.root(2)[0].getImaginary(), 1e-5);
		assertEquals("First root imaginary element expected 0.57686.", roots[0].getImaginary(), a.root(2)[0].getImaginary(), 1e-5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testRootIllegalArgument() {
		ComplexNumber a = new ComplexNumber(3.14, -2.15);
		a.root(0);
	}

	@Test
	public void testToString() {
		ComplexNumber a = new ComplexNumber(2.17, -131.174);
		assertEquals("2,17 - 131,17i", a.toString());
	}

}
