package hr.fer.zemris.bool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class <code>BooleanVariable</code> is a implementation of
 * {@link NamedBooleanSource}. It represents variable.
 * Default value is <code>FALSE</code>.
 */
public class BooleanVariable implements NamedBooleanSource {

	/**
	 * Value of <code>BooleanVariable</code>.
	 */
	private BooleanValue value = BooleanValue.FALSE;
	
	/**
	 * Name associated with <code>BooleanVariable</code>.
	 */
	private String name;
	
	/**
	 * Constructs new <code>BooleanVariable</code>.
	 * @param variableName name of variable
	 */
	public BooleanVariable(final String variableName) {
		this.name = variableName;
	}
	
	/**
	 * @return <code>BooleanVariable</code> contained value
	 */
	@Override
	public BooleanValue getValue() {
		return this.value;
	}
	
	/**
	 * @param givenValue new value to be set
	 */
	public void setValue(BooleanValue givenValue) {
		this.value = givenValue;
	}

	/**
	 * @return List<BooleanVariable> containing the variable itself
	 */
	@Override
	public List<BooleanVariable> getDomain() {
		List<BooleanVariable> collection = new ArrayList<BooleanVariable>();
		collection.add(this);
		return collection;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.NamedBooleanSource#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

}
