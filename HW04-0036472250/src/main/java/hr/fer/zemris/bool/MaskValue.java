/**
 * 
 */
package hr.fer.zemris.bool;

/**
 * @author Marin Petrunić 0036472250
 * Distinct values for each variable in mask.
 */
public enum MaskValue {
	ZERO, ONE, DONT_CARE;
}
