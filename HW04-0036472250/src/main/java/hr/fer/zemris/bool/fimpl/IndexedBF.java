/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.opimpl.BooleanOperators;

/**
 * @author Marin Petrunić 0036472250
 *
 * <p>Class <code>IndexedBF</code> represents a boolean function
 * which is defined by specifying indexes of minterms (or maxterms)
 * and indexes of don't cares.
 * It allows user to define something like:
 * <p><code>f(A,B,C)=Σm(1,2,5)+Σd(0,4,7)</code></p>
 * or
 * <p><code>g(A,B,C)=ΠM(3,6)⋅Πd(0,4,7)</code></p>
 * </p>
 */
public class IndexedBF extends BasicBF {

	/**
	 * True if indexes are minterms of false
	 * if indexes are maxterms.
	 */
	private boolean indexesAreMinterms;
	
	/**
	 * Private variable with list of indexes.
	 */
	private List<Integer> indexes;
	
	/**
	 * Private variable with list of DONT_CARES.
	 */
	private List<Integer> dontCares;
	
	/**
	 * Constructs new Indexed boolean function from given parameters.
	 * @param functionName
	 * @param domainList list of variables
	 * @param indexesAreMinterms minterms or maxterms
	 * @param indexesList list of indexes
	 * @param dontCaresList list of don't cares.
	 * @throws IllegalArgumentException if minterms/maxterm and don't_care has same index
	 */
	public IndexedBF(String functionName, List<BooleanVariable> domainList,
			boolean indexesAreMinterms, List<Integer> indexesList, List<Integer> dontCaresList) {
		for(Integer i : dontCaresList) {
			if(indexesList.contains(i)) {
				throw new IllegalArgumentException("Index of minterm/maxterm and don't_care is the same.");
			}
		}
		this.name = functionName.trim();
		this.domain = new ArrayList<BooleanVariable>(domainList);
		this.indexesAreMinterms = indexesAreMinterms;
		this.indexes = new ArrayList<Integer>(indexesList);
		this.dontCares = new ArrayList<Integer>(dontCaresList);
	}

	/**
	 * Calculates function of minterms value with
	 * given variables value.
	 * @return {@link BooleanValue} as function value
	 */
	@Override
	public BooleanValue getValue() { 
			return this.sumOfMinterms();
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMinterm(int)
	 */
	@Override
	public boolean hasMinterm(int index) {
		//if in list of minterms
		if (this.indexesAreMinterms) {
			if (this.indexes.contains(index)) {
				return true;
			}
			return false;
		//if it isn't in list of maxterms or list of don't cares
		} else if (!this.indexes.contains(index) && !this.dontCares.contains(index)) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMaxterm(int)
	 */
	@Override
	public boolean hasMaxterm(int index) {
		//if in list of maxterms
		if (!this.indexesAreMinterms) {
			if (this.indexes.contains(index)) {
				return true;
			}
			return false;
		//if it isn't in list of minterms or list of don't cares
		} else if (!this.indexes.contains(index) && !this.dontCares.contains(index)) {
			return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasDontCare(int)
	 */
	@Override
	public boolean hasDontCare(int index) {
		if (this.dontCares.contains(index)) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#mintermIterable()
	 */
	@Override
	public Iterable<Integer> mintermIterable() {
		if (this.indexesAreMinterms) {
			return Collections.unmodifiableList(this.indexes);
		}
		List<Integer> minterms = new ArrayList<>();
		for (int index = 0, end = (int) (Math.pow(this.domain.size(),2) - 1);
				index < end; index++) {
			if (this.hasMinterm(index)) {
				minterms.add(index);
			}
		}
		return minterms;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#maxtermIterable()
	 */
	@Override
	public Iterable<Integer> maxtermIterable() {
		if (this.indexesAreMinterms) {
			List<Integer> maxterms = new ArrayList<>();
			for (int index = 0, end = (int) (Math.pow(this.domain.size(),2) - 1);
					index < end; index++) {
				if (this.hasMaxterm(index)) {
					maxterms.add(index);
				}
			}
			return maxterms;
		}
		return Collections.unmodifiableList(this.indexes);
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#dontcareIterable()
	 */
	@Override
	public Iterable<Integer> dontcareIterable() {
		return Collections.unmodifiableList(this.dontCares);
	}
	
	/**
	 * Calculates minterm value with given function variable values.
	 * Example:
	 * <p>
	 * f(B, C, A)<br>
	 * B = 1, C =0, A = 0;<br>
	 * minterm 3 value = NOT B AND C AND A
	 * 				   = NOT 1 AND 0 AND 0
	 * 				   = 0;
	 * </p>
	 * @param mintermIndex
	 * @return {@link BooleanVariable} with resulting value
	 */
	private BooleanVariable mintermValue(Integer minterm) {
		Mask mintermMask = Mask.fromIndex(this.domain.size(), minterm);
		return this.mintermValueVariable(mintermMask);
	}
	
	/**
	 * Calculates sum of given minterms.
	 * @return value of sum of given minterms with values of function
	 * variables
	 */
	private BooleanValue sumOfMinterms() {
		BooleanVariable result = new BooleanVariable("result");
		for (Integer minterm : this.mintermIterable()) {
			result.setValue(
					BooleanOperators.or(new BooleanVariable[] {
							result,
							this.mintermValue(minterm)
					}).getValue()
			);
		}
		return result.getValue();
	}
}
