/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import java.util.List;

import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class representing BooleanOperator AND.
 * 
 */
public class BooleanOperatorAND extends BooleanOperator {

	/**
	 * Protected <code>BooleanOperator</code> constructor. Creates new 
	 * <code>BooleanOperator</code> from given list of sources.
	 * @param givenListofSources
	 */
	public BooleanOperatorAND(List<BooleanSource> givenListofSources) {
		super(givenListofSources);
	}

	/**
	 * Applying operator AND on given {@link BooleanSource}s.
	 * If some <code>BooleanSource</code> is equal to {@link BooleanValue} FALSE
	 * method will return FALSE, otherwise will return TRUE.
	 * return {@link BooleanValue} TRUE or FALSE
	 */
	@Override
	public BooleanValue getValue() {
		List<BooleanSource> sources = this.getSources();
		BooleanValue answerValue = sources.get(0).getValue();
		for(BooleanSource source : sources) {
			answerValue = this.and(answerValue, source.getValue());
		}
		return answerValue;
	}
	
	/**
	 * Determines result value from first AND second.
	 * @param first
	 * @param second
	 * @return {@link BooleanValue} result
	 */
	private BooleanValue and(BooleanValue first, BooleanValue second) {
		if(first == BooleanValue.TRUE) {
			switch(second) {
				case TRUE: return BooleanValue.TRUE;
				case FALSE: return BooleanValue.FALSE;
				default: return BooleanValue.DONT_CARE;
			}
		}
		if(first == BooleanValue.DONT_CARE) {
			switch(second) {
				case TRUE: return BooleanValue.DONT_CARE;
				case FALSE: return BooleanValue.FALSE;
				default: return BooleanValue.DONT_CARE;
			}
		}
		return BooleanValue.FALSE;
	}

}
