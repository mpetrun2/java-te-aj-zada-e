package hr.fer.zemris.bool.opimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class representing operator NOT.
 */
public class BooleanOperatorNOT extends BooleanOperator {
	
	/**
	 * Calling parent constructor from {@link BooleanOperatorTest}
	 * @param givenSource
	 */
	public BooleanOperatorNOT(BooleanSource givenSource) {
		super(new ArrayList<BooleanSource>(Arrays.asList(givenSource)));
	}

	/**
	 * Applying operator NOT on given {@link BooleanSource}.
	 * If given source value is {@link BooleanValue} TRUE returns
	 * {@link BooleanValue} FALSE.
	 * f given source value is {@link BooleanValue} FALSE returns
	 * {@link BooleanValue} TRUE.
	 * In case of {@link BooleanValue} DON'T CARE returned value is DON'T CARE.
	 * @return {@link BooleanValue} depending of value of source
	 */
	@Override
	public BooleanValue getValue() {
		List<BooleanSource> sources = this.getSources();
		switch(sources.get(0).getValue()) {
			case TRUE : return BooleanValue.FALSE;
			case FALSE : return BooleanValue.TRUE;
			default: return BooleanValue.DONT_CARE;
		}
	}

}
