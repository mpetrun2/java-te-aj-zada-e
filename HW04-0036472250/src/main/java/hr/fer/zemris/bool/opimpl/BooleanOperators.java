/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import java.util.Arrays;

import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanSource;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class providing convenient factory methods for producing concrete
 * <code>BooleanOperator</code> instances.
 */
public class BooleanOperators {

	/**
	 * Factory method for creating instance of BooleanOperatorAND.
	 * @param sources on which AND will be performed
	 * @return instance of BooleanOperatorAND
	 */
	public static BooleanOperator and(BooleanSource ... sources) {
		return new BooleanOperatorAND(Arrays.asList(sources));
	}
	
	/**
	 * Factory method for creating instance of BooleanOperatorOR.
	 * @param sources on which OR will be performed
	 * @return instance of BooleanOperatorOR
	 */
	public static BooleanOperator or(BooleanSource ... sources) {
		return new BooleanOperatorOR(Arrays.asList(sources));
	}
	
	/**
	 * Factory method for creating instance of BooleanOperatorNOT.
	 * @param source on which NOT will be performed
	 * @return instance of BooleanOperatorNOT
	 */
	public static BooleanOperator not(BooleanSource source) {
		return new BooleanOperatorNOT(source);
	}
	
	/**
	 * Private constructor to forbid instancing of this class.
	 */
	private BooleanOperators(){
		
	}
}
