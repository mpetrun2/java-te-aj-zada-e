/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class AboveAverage {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<>();
		Scanner scanner = new Scanner(System.in, "UTF-8");
		int suma = 0;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.equals("quit")) {
				break;
			}
			numbers.add(Integer.parseInt(line));
			suma += Integer.parseInt(line);
		}
		scanner.close();
		Double average = (double) suma / numbers.size();
		Collections.sort(numbers);
		for (int number : numbers) {
			if ((number - average) / average >= 0.2) {
				System.out.println(number);
			}
		}
	}

}
