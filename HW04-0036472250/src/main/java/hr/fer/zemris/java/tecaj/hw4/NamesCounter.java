﻿package hr.fer.zemris.java.tecaj.hw4;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class NamesCounter {

	public static void main(String[] args) {
		
		Map<String, Integer> namesCounter = new HashMap<>();
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.equals("quit")) {
				break;
			}
			if(namesCounter.containsKey(line)) {
				namesCounter.put(line, namesCounter.get(line) + 1);
			} else {
				namesCounter.put(line, 1);
			}
		}
		scanner.close();
		for (String key : namesCounter.keySet()) {
			System.out.println(key + " " + namesCounter.get(key));
		}
	}

}
