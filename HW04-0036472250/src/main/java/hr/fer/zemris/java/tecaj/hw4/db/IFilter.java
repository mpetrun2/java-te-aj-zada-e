/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4.db;

/**
 * @author Marin Petrunić 0036472250
 *
 * Interface for filtering diffrent student records from database.
 */
public interface IFilter {
	
	/**
	 * Defines filter for accepting student records.
	 * @param record
	 * @return
	 */
	public boolean accepts(StudentRecord record);
	
}
