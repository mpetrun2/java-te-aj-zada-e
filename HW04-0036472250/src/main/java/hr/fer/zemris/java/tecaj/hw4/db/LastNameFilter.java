/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4.db;

/**
 * @author Marin Petrunić 0036472250
 *
 * Implementation of {@link IFilter}.
 * It filter result set by last name filter.
 */
public class LastNameFilter implements IFilter {
	
	/**
	 * Given filter.
	 */
	private String filter;
	
	/**
	 * LastNameFilter constructor.
	 * It accepts filter as parameter.
	 * @param filter
	 */
	public LastNameFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * Method that checks each record if it passes filter.
	 * Filter allows wildcard <tt>*</tt>.
	 * If wildcard is at beginning of filter function is checking if
	 * student last name ends with given filter. If its at the end function
	 * checks if last name starts with given filter.
	 * If its anywhere else function checks
	 * if last name starts with string before wildcard <b>and</b> 
	 * ends with string after wildcard.
	 * @param student record to be verified
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		int wildcardPosition = filter.indexOf('*');
		if (wildcardPosition == 0) {
			if(record.getLastName().endsWith(filter.substring(1))) {
				return true;
			} else {
				return false;
			}
		}
		if (wildcardPosition == filter.length()-1) {
			if(record.getLastName().startsWith(filter.substring(0, filter.length()-1))) {
				return true;
			} else {
				return false;
			}
		}
		if(record.getLastName().startsWith(filter.substring(0, wildcardPosition))
				&&  record.getLastName().endsWith(filter.substring(wildcardPosition+1, filter.length()))) {
			return true;
		}
		return false;
	}

}
