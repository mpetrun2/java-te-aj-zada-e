/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Class for executing query to student database.
 *
 */
public class StudentDB {

	/**
	 * Main function does not accept parameters.
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get("./database.txt"),
				StandardCharsets.UTF_8
				);
		StudentDatabase db = new StudentDatabase(lines);
		Scanner scanner = new Scanner(System.in);
		System.out.print("> ");
		while (scanner.hasNext()) {
			String input = scanner.nextLine();
			if (input.equals("quit")) {
				break;
			}
			System.out.println(db.queryExecute(input));
			System.out.print("> ");
		}
		scanner.close();
		System.out.println("Program has ended.");
	}

}
