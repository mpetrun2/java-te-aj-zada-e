/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Class representing student database.
 *
 */
public class StudentDatabase {
	
	/**
	 * List of student records.
	 */
	private List<StudentRecord> studentRecords = new ArrayList<>();
	
	/**
	 * Map for fast retrieval of students by jmbag.
	 */
	private Map<String, StudentRecord> studentRecordsMap = new HashMap<>();
	
	/**
	 * Creates database of given student records. 
	 * @param databaseRecords
	 */
	public StudentDatabase(List<String> databaseRecords) {
		for (String record : databaseRecords) {
			List<String> attributes = new ArrayList<>(Arrays.asList(record.split("\t")));
			StudentRecord student = new StudentRecord(
					attributes.get(0),
					attributes.get(1),
					attributes.get(2),
					Integer.parseInt(attributes.get(3))
			);
			studentRecords.add(student);
			studentRecordsMap.put(attributes.get(0), student);
		}
	}
	
	/**
	 * Fast retrieves student from database by given jmbag.
	 * If jmbag is <code>null</code> or doesn't exist in database,
	 * method will return <code>null</code>. 
	 * @param jmbag identifying student record in database.
	 * @return Student record for given jmbag or null.
	 */
	public StudentRecord fromJMBAG(String jmbag) {
		return studentRecordsMap.get(jmbag);
	}
	
	/**
	 * Creates new list with copies of accepted 
	 * student records.
	 * @param filter deciding accepted records
	 * @return new list of accepted student records
	 */
	public List<StudentRecord> filter(IFilter filter) {
		List<StudentRecord> filteredRecords = new ArrayList<>();
		for(StudentRecord record : this.studentRecords) {
			if(filter.accepts(record)) {
				filteredRecords.add(new StudentRecord(record));
			}
		}
		return filteredRecords;
	}
	
	/**
	 * Executes given query.
	 * Query format must be:<br>
	 * <tt>query queryName = "queryParameter"</tt>
	 * <br>Every other format will get output :"Query is invalid."
	 * @param query query to be executed
	 * @return string representation of result or "Query is invalid."
	 */
	public String queryExecute(String query) {
		query = query.trim();
		String keyword = "query";
		if (!query.startsWith(keyword)) {
			return "Query is invalid.";
		}
		query = query.substring(keyword.length());
		String queryKey;
		String queryValue;
		try {
			queryKey = query.split("=")[0].trim();
			queryValue = query.split("=")[1].trim().replaceAll("\"", "");
		} catch (Exception e) {
			return "Query is invalid.";
		}
		switch(queryKey) {
			case "jmbag" : return this.formatOutput(Arrays.asList(this.fromJMBAG(queryValue)));
			case "lastName" : return this.formatOutput(this.filter(new LastNameFilter(queryValue)));
			default: return "Query is invalid.";
		}
	}
	
	/**
	 * Method for formatting result set of database.
	 * @param records to be formated
	 * @return String representation of result set
	 */
	private String formatOutput(List<StudentRecord> records) {
		if(records.isEmpty()) {
			return "Records selected: 0";
		}
		List<Integer> columnSizes = new ArrayList<>(Collections.nCopies(4, 0));
		//finding longest column values
		for (StudentRecord record : records) {
			columnSizes.set(0, this.checkColumnSize(
					record.getJmbag(),columnSizes.get(0))
			);
			columnSizes.set(1, this.checkColumnSize(
					record.getLastName(),columnSizes.get(1))
			);
			columnSizes.set(2, this.checkColumnSize(
					record.getFirstName(),columnSizes.get(2))
			);
			columnSizes.set(3, this.checkColumnSize(
					String.valueOf(record.getFinalGrade()),columnSizes.get(3))
			);
		}
		String output = this.generateHorizontalLine(columnSizes);
		for (StudentRecord record : records) {
			output += "| " + String.format("%-"+columnSizes.get(0) + "s" 
					+ " | %-" +columnSizes.get(1) + "s" 
					+ " | %-" +columnSizes.get(2) + "s" 
					+ " | %-" + columnSizes.get(3) + "s |\n",
					record.getJmbag(), record.getLastName(), record.getFirstName(), record.getFinalGrade()
			);
		}
		output +=  this.generateHorizontalLine(columnSizes);
		output += "Records selected:" + records.size() + "\n";
		return output;
	}
	
	/**
	 * Helper function for finding max column width.
	 * @param columnValue
	 * @param currentLength
	 * @return
	 */
	private int checkColumnSize(String columnValue, int currentLength) {
		if(columnValue.length() > currentLength) {
			return columnValue.length();
		}
		return currentLength;
	}
	
	/**
	 * Helper function for output.
	 * It generates something like:<br>
	 * +=========+======+=======+===+
	 * @param columnSizes
	 * @return
	 */
	private String generateHorizontalLine(List<Integer> columnSizes) {
		String line = "+";
		for(Integer columnSize : columnSizes) {
			for(int i = 0; i < columnSize+2; i++) {
				line += "=";
			}
			line += "+";
		}
		return line + "\n";
	}
}
