/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw4.db;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class representing one student record.
 */
public class StudentRecord {

	/**
	 * Unique student identifier.
	 */
	private String jmbag;
	
	/**
	 * Student last name.
	 */
	private String lastName;
	
	/**
	 * Student first name.
	 */
	private String firstName;
	
	/**
	 * Student final grade.
	 */
	private Integer finalGrade;
	
	private int hashCode;

	/**
	 * Creates student record from given parameters.
	 * @param jmbag 
	 * @param lastName
	 * @param firstName
	 * @param finalGrade
	 */
	public StudentRecord(String jmbag, String lastName, String firstName,
			Integer finalGrade) {
		super();
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.finalGrade = finalGrade;
		this.hashCode = this.generateHashCode();
	}
	
	/**
	 * Creates new <code>StudentRecord</code> from given record.
	 * @param studentRecord
	 */
	public StudentRecord(StudentRecord studentRecord) {
		this(
				studentRecord.getJmbag(),
				studentRecord.getLastName(),
				studentRecord.getFirstName(),
				studentRecord.getFinalGrade()
		);
	}

	/**
	 * @return the jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the finalGrade
	 */
	public Integer getFinalGrade() {
		return finalGrade;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.hashCode;
	}

	/**
	 * Students are equal if their jmbag (unique identifier) is equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null)
				return false;
		} else if (!jmbag.equals(other.jmbag))
			return false;
		return true;
	}
	
	/**
	 * Method that generates hash code that will be saved in
	 * constructor and returned in hash code method.
	 * @return
	 */
	private int generateHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return "Student: JMBAG= " + this.jmbag + "\t"
				+ "Last Name= " + this.lastName + "\t"
				+ "First Name= " + this.firstName + "\t"
				+ "Final Grade= " + this.finalGrade;
	}
	
}
