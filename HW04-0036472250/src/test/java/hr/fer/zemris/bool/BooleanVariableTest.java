/**
 * 
 */
package hr.fer.zemris.bool;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanVariableTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanVariable#BooleanVariable(java.lang.String)}.
	 */
	@Test
	public void testBooleanVariable() {
		@SuppressWarnings("unused")
		BooleanVariable var = new BooleanVariable("test");
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanVariable#getValue()}.
	 */
	@Test
	public void testGetValue() {
		BooleanVariable var = new BooleanVariable("test");
		assertEquals(BooleanValue.FALSE, var.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanVariable#setValue(hr.fer.zemris.bool.BooleanValue)}.
	 */
	@Test
	public void testSetValue() {
		BooleanVariable var = new BooleanVariable("test");
		assertEquals(BooleanValue.FALSE, var.getValue());
		var.setValue(BooleanValue.TRUE);
		assertEquals(BooleanValue.TRUE, var.getValue());
		var.setValue(BooleanValue.DONT_CARE);
		assertEquals(BooleanValue.DONT_CARE, var.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanVariable#getDomain()}.
	 */
	@Test
	public void testGetDomain() {
		BooleanVariable var = new BooleanVariable("test");
		assertEquals(new ArrayList<BooleanVariable>(Arrays.asList(var)), var.getDomain());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanVariable#getName()}.
	 */
	@Test
	public void testGetName() {
		BooleanVariable var = new BooleanVariable("test");
		assertEquals("test", var.getName());
	}

}
