/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.opimpl.BooleanOperators;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BasicBFTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.BasicBF#getName()}.
	 */
	@Test
	public void testGetName() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		varA.setValue(BooleanValue.TRUE);
		BooleanOperator izraz1 = BooleanOperators.or(
			BooleanConstant.FALSE,
			varC,
			BooleanOperators.and(varA, BooleanOperators.not(varB))
		);
		BooleanFunction f1 = new OperatorTreeBF(
									"f1",
									Arrays.asList(varA, varB, varC),
									izraz1
								);
		assertEquals("f1", f1.getName());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.BasicBF#getDomain()}.
	 */
	@Test
	public void testGetDomain() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		varA.setValue(BooleanValue.TRUE);
		BooleanOperator izraz1 = BooleanOperators.or(
			BooleanConstant.FALSE,
			varC,
			BooleanOperators.and(varA, BooleanOperators.not(varB))
		);
		BooleanFunction f1 = new OperatorTreeBF(
									"f1",
									Arrays.asList(varA, varB, varC),
									izraz1
								);
		assertEquals(new ArrayList<>(Arrays.asList(varA, varB, varC)), f1.getDomain());
	}


}
