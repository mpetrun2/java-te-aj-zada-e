/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class IndexedBFTest {

	private BooleanFunction createFunction() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanFunction f1 = new IndexedBF(
				"f1",
				Arrays.asList(varA, varB, varC),
				true,
				Arrays.asList(0,1,5,7),
				Arrays.asList(2,3)
		);
		return f1;
	}
	
	private BooleanFunction createFunctionMax() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanFunction f1 = new IndexedBF(
				"f1",
				Arrays.asList(varA, varB, varC),
				false,
				Arrays.asList(0,1,5,7),
				Arrays.asList(2,3)
		);
		return f1;
	}
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#IndexedBF(java.lang.String, java.util.List, boolean, java.util.List, java.util.List)}.
	 */
	@Test
	public void testIndexedBF() {
		this.createFunction();
		this.createFunctionMax();
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#IndexedBF(java.lang.String, java.util.List, boolean, java.util.List, java.util.List)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testIndexedBFIllegal() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		@SuppressWarnings("unused")
		BooleanFunction f1 = new IndexedBF(
				"f1",
				Arrays.asList(varA, varB, varC),
				true,
				Arrays.asList(0,1,5,7),
				Arrays.asList(1,3)
		);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#getValue()}.
	 */
	@Test
	public void testGetValue() {
		BooleanFunction f1 = this.createFunction();
		assertEquals(BooleanValue.TRUE, f1.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#hasMinterm(int)}.
	 */
	@Test
	public void testHasMinterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasMinterm(4));
		assertTrue(f1.hasMinterm(5));
		BooleanFunction f2 = this.createFunctionMax();
		assertTrue(f2.hasMinterm(4));
		assertFalse(f2.hasMinterm(5));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#hasMaxterm(int)}.
	 */
	@Test
	public void testHasMaxterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse("err1",f1.hasMaxterm(5));
		assertTrue("err2",f1.hasMaxterm(4));
		BooleanFunction f2 = this.createFunctionMax();
		assertTrue("err1",f2.hasMaxterm(5));
		assertFalse("err2",f2.hasMaxterm(4));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#hasDontCare(int)}.
	 */
	@Test
	public void testHasDontCare() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasDontCare(5));
		assertTrue(f1.hasDontCare(2));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#mintermIterable()}.
	 */
	@Test
	public void testMintermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.mintermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 1, 5, 7}))));
		list.clear();
		BooleanFunction f2 = this.createFunctionMax();
		for(Integer i : f2.mintermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {4,6}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#maxtermIterable()}.
	 */
	@Test
	public void testMaxtermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.maxtermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {4,6}))));
		list.clear();
		BooleanFunction f2 = this.createFunctionMax();
		for(Integer i : f2.maxtermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 1, 5, 7}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.IndexedBF#dontcareIterable()}.
	 */
	@Test
	public void testDontcareIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.dontcareIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {2,3}))));
	}

}
