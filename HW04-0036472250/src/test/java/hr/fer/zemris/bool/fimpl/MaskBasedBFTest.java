/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Masks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class MaskBasedBFTest {

	public BooleanFunction createFunction() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanVariable varD = new BooleanVariable("D");
		BooleanFunction f1 = new MaskBasedBF(
			"f1",
			Arrays.asList(varA, varB, varC, varD),
			true,
			Masks.fromStrings("1xx1", "00x0"),
			Masks.fromStrings("10x0")
		);
		return f1;
	}
	
	public BooleanFunction createFunctionMax() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanVariable varD = new BooleanVariable("D");
		BooleanFunction f1 = new MaskBasedBF(
			"f1",
			Arrays.asList(varA, varB, varC, varD),
			false,
			Masks.fromStrings("1xx1", "00x0"),
			Masks.fromStrings("10x0")
		);
		return f1;
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#MaskBasedBF(java.lang.String, java.util.List, boolean, java.util.List, java.util.List)}.
	 */
	@Test
	public void testMaskBasedBF() {
		this.createFunction();
		this.createFunctionMax();
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#MaskBasedBF(java.lang.String, java.util.List, boolean, java.util.List, java.util.List)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testMaskBasedBFIllgeal() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanVariable varD = new BooleanVariable("D");
		@SuppressWarnings("unused")
		BooleanFunction f1 = new MaskBasedBF(
			"f1",
			Arrays.asList(varA, varB, varC, varD),
			true,
			Masks.fromStrings("1xx1", "00x0"),
			Masks.fromStrings("1xx1")
		);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#getValue()}.
	 */
	@Test
	public void testGetValue() {
		BooleanFunction f1 = this.createFunction();
		assertEquals(BooleanValue.TRUE, f1.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#hasMinterm(int)}.
	 */
	@Test
	public void testHasMinterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasMinterm(4));
		assertTrue(f1.hasMinterm(2));
		BooleanFunction f2 = this.createFunctionMax();
		assertFalse(f2.hasMinterm(8));
		assertFalse(f2.hasMinterm(2));
		assertTrue(f2.hasMinterm(4));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#hasMaxterm(int)}.
	 */
	@Test
	public void testHasMaxterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasMaxterm(2));
		assertTrue(f1.hasMaxterm(4));
		assertFalse(f1.hasMaxterm(8));
		BooleanFunction f2 = this.createFunctionMax();
		assertFalse(f2.hasMaxterm(4));
		assertTrue(f2.hasMaxterm(2));
		assertFalse(f2.hasMaxterm(8));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#hasDontCare(int)}.
	 */
	@Test
	public void testHasDontCare() {
		BooleanFunction f1 = this.createFunction();
		assertTrue("1",f1.hasDontCare(8));
		assertFalse("2",f1.hasDontCare(2));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#mintermIterable()}.
	 */
	@Test
	public void testMintermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.mintermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 2, 9, 11, 13, 15}))));
		list.clear();
		BooleanFunction f2 = this.createFunctionMax();
		for(Integer i : f2.mintermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {1, 3, 4, 5, 6, 7, 12, 14}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#maxtermIterable()}.
	 */
	@Test
	public void testMaxtermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.maxtermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {1, 3, 4, 5, 6, 7, 12, 14}))));
		list.clear();
		BooleanFunction f2 = this.createFunctionMax();
		for(Integer i : f2.maxtermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 2, 9, 11, 13, 15}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.MaskBasedBF#dontcareIterable()}.
	 */
	@Test
	public void testDontcareIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.dontcareIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {8, 10}))));
	}

}
