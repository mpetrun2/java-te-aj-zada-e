/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import static org.junit.Assert.*;

import java.util.ArrayList;

import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.opimpl.BooleanOperatorNOT;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanOperatorNOTTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorNOT#BooleanOperatorNOT(hr.fer.zemris.bool.BooleanSource)}.
	 */
	@Test
	public void testBooleanOperatorNOT() {
		@SuppressWarnings("unused")
		BooleanOperatorNOT not = new BooleanOperatorNOT(BooleanConstant.TRUE);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorNOT#getValue()}.
	 */
	@Test
	public void testGetValue() {
		BooleanOperatorNOT not = new BooleanOperatorNOT(BooleanConstant.TRUE);
		assertEquals(BooleanValue.FALSE, not.getValue());
		not = new BooleanOperatorNOT(BooleanConstant.FALSE);
		assertEquals(BooleanValue.TRUE, not.getValue());
		BooleanVariable var = new BooleanVariable("test");
		var.setValue(BooleanValue.DONT_CARE);
		not = new BooleanOperatorNOT(var);
		assertEquals(BooleanValue.DONT_CARE, not.getValue());
	}


	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanOperator#getDomain()}.
	 */
	@Test
	public void testGetDomain() {
		BooleanOperatorNOT not = new BooleanOperatorNOT(BooleanConstant.TRUE);
		assertEquals(new ArrayList<BooleanVariable>(), not.getDomain());
	}

}
