/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanOperatorsTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperators#and(hr.fer.zemris.bool.BooleanSource[])}.
	 */
	@Test
	public void testAnd() {
		BooleanVariable var1 = new BooleanVariable("var1");
		var1.setValue(BooleanValue.DONT_CARE);
		BooleanVariable var2 = new BooleanVariable("var2");
		var2.setValue(BooleanValue.TRUE);
		BooleanOperator and = BooleanOperators.and(new BooleanVariable[] {
								var1,
								var2,
								new BooleanVariable("var3")		
							}
		);
		assertEquals(BooleanValue.FALSE, and.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperators#or(hr.fer.zemris.bool.BooleanSource[])}.
	 */
	@Test
	public void testOr() {
		BooleanVariable var1 = new BooleanVariable("var1");
		var1.setValue(BooleanValue.DONT_CARE);
		BooleanVariable var2 = new BooleanVariable("var2");
		var2.setValue(BooleanValue.TRUE);
		BooleanOperator or = BooleanOperators.or(new BooleanVariable[] {
								var1,
								var2,
								new BooleanVariable("var3")		
							}
		);
		assertEquals(BooleanValue.TRUE, or.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperators#not(hr.fer.zemris.bool.BooleanSource)}.
	 */
	@Test
	public void testNot() {
		assertEquals(BooleanValue.TRUE, BooleanOperators.not(BooleanConstant.FALSE).getValue());
		assertEquals(BooleanValue.FALSE, BooleanOperators.not(BooleanConstant.TRUE).getValue());
	}

}
