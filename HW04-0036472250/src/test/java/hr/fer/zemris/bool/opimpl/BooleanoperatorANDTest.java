/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanoperatorANDTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorAND#BooleanOperatorAND(java.util.List)}.
	 */
	@Test
	public void testBooleanOperatorAND() {
		List<BooleanSource> sources = new ArrayList<>();
		sources.add(BooleanConstant.TRUE);
		sources.add(BooleanConstant.FALSE);
		@SuppressWarnings("unused")
		BooleanOperatorAND and = new BooleanOperatorAND(sources);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorAND#getValue()}.
	 */
	@Test
	public void testGetValue() {
		List<BooleanSource> sources = new ArrayList<>();
		sources.add(BooleanConstant.TRUE);
		sources.add(BooleanConstant.FALSE);
		BooleanOperatorAND and = new BooleanOperatorAND(sources);
		assertEquals(BooleanValue.FALSE, and.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanOperator#getDomain()}.
	 */
	@Test
	public void testGetDomain() {
		List<BooleanSource> sources = new ArrayList<>();
		sources.add(new BooleanVariable("var1"));
		sources.add(new BooleanVariable("var2"));
		BooleanVariable var3 = new BooleanVariable("var3");
		var3.setValue(BooleanValue.DONT_CARE);
		sources.add(var3);
		BooleanOperatorAND and = new BooleanOperatorAND(sources);
		assertEquals(sources, and.getDomain());
	}

}
