# Ovaj program ispisuje sljedećih 5 brojeva korisničkog unosa

		load r0, @msg1 ; uputa korisniku.
		load r1, @msg2 ; poruka greške.
		load r2, @msg3 ; Opisa ispisa posla.
		load r3, @msg4 ; novi red
		load r14, @brojac ; brojac
		load r15, @nula ; za usporedbu
@petlja:	echo r0;
		iinput @userInput;
		jumpIfTrue @nastavak;
		echo r1;
		jump @petlja;
@nastavak:	echo r2;
		echo r3;
		load r4, @userInput;
		increment r4;
@petlja2:	testEquals r14, r15;
		jumpIfTrue @kraj;
		echo r4;
		echo r3;
		decrement r14;
		increment r4;
		jump @petlja2
@kraj:	halt;	
# Slijede dodatni podaci koji su smjesteni u memoriju

@msg1:		DEFSTR "Unesite početni broj: "
@msg2:		DEFSTR "Unos nije moguće protumačiti kao cijeli broj.\n"
@msg3:		DEFSTR "Sljedećih 5 brojeva je:"
@msg4:		DEFSTR "\n"
@nula:		DEFINT 0
@brojac:	DEFINT 5
@userInput: DEFINT 0
