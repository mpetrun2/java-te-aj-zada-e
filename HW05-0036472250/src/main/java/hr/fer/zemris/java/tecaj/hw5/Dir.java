/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5;

import hr.fer.zemris.java.tecaj.hw5.compare.SortFiles;
import hr.fer.zemris.java.tecaj.hw5.filter.Filter;
import hr.fer.zemris.java.tecaj.hw5.print.FilesPrinter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class Dir {

	/**
	 * Given root directory.
	 */
	private File rootDir;

	/**
	 * Arguments for filtering, sorting and output.
	 */
	private List<String> arguments;

	/**
	 * Creates Dir instance from given root directory and output arguments.
	 * 
	 * @param rootDir
	 *            root Directory
	 * @param arguments
	 *            for filtering, sorting and output informations.
	 * @throws new
	 *             IllegalArgumentException if given file isn't directory
	 */
	public Dir(File rootDir, List<String> arguments) {
		if (!rootDir.isDirectory()) {
			throw new IllegalArgumentException(
					"Expected to get directory as argument.");
		}
		this.rootDir = rootDir;
		this.arguments = new ArrayList<>(arguments);
	}

	/**
	 * First argument must be path to directory followed by unlimited number of
	 * output info.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 0) {
			System.err.println("Wrong number of arguments given.");
		}
		try {
			Dir dir = new Dir(new File(args[0]), new ArrayList<>(
					Arrays.asList(args)));
			dir.printFiles();
		} catch (Exception e) {

			System.err.println(e.getLocalizedMessage());
		}
	}

	/**
	 * Takes files from root directory(only level 1) and applies filter and sort
	 * arguments to it. Returns changed list of files.
	 * 
	 * @return
	 */
	public List<File> getFiles() {
		// gets all files and filter them
		List<File> files = Arrays.asList(this.rootDir.listFiles(Filter
				.composeFilter(this.getSpecificArguments("-f:"))));
		// sort remaining files
		Collections.sort(files,
				SortFiles.getSortComparator(this.getSpecificArguments("-s:")));
		return files;
	}

	/**
	 * Prints filtered and sorted files from given directory. Files are
	 * processed and printed as defined in arguments.
	 */
	public void printFiles() {
		FilesPrinter printer = new FilesPrinter(this.getFiles(),
				this.getSpecificArguments("-w:"));
		printer.print();
	}

	/**
	 * Returns list of arguments specified with given String. For example if
	 * sort arguments are needed given matcher will be "-s:".
	 * 
	 * @param argumentMatcher
	 * @return
	 * @throws IllegalArgumentException
	 *             if wrong argument format.
	 */
	private List<String> getSpecificArguments(String argumentMatcher) {
		List<String> sortArguments = new ArrayList<>();
		for (String argument : this.arguments) {
			if (argument.startsWith(argumentMatcher)) {
				if (argument.length() <= argumentMatcher.length()) {
					throw new IllegalArgumentException("Ivalid argument format");
				}
				sortArguments.add(argument.split(":")[1].trim());
			}
		}
		return sortArguments;
	}

}
