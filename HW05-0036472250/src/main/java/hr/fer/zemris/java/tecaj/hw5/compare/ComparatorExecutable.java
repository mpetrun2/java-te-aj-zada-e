/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Compare files by their ability to execute. Executable files comes
 *         before non-executable.
 * 
 */
public class ComparatorExecutable implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return Boolean.compare(o1.canExecute(), o2.canExecute());
	}

}
