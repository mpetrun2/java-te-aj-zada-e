/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Sort files by date of last modification. Files modified recently
 *         comes before.
 * 
 */
public class ComparatorLastModified implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return Long.compare(o1.lastModified(), o2.lastModified());
	}

}
