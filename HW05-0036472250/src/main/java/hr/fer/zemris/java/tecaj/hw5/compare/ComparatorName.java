/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Compares files by their name lexicographically.
 * 
 */
public class ComparatorName implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return o1.getName().compareTo(o2.getName());
	}

}
