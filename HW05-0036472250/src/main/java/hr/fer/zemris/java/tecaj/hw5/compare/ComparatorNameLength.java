/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Sorts files by their name length. Smaller names comes before longer.
 */
public class ComparatorNameLength implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return Integer.compare(o1.getName().length(), o2.getName().length());
	}

}
