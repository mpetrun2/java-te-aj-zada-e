/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Sorts files by their size. Smaller files comes before larger.
 */
public class ComparatorSize implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return Long.compare(o1.length(), o2.length());
	}

}
