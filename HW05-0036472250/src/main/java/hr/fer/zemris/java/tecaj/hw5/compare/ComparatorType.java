/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.Comparator;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Sorts files by their type. Directories are before files.
 */
public class ComparatorType implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return Boolean.compare(o1.isFile(), o2.isFile());
	}

}
