/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Compose comparator from given comparators.
 */
public class ComposedComparator implements Comparator<File> {

	/**
	 * List of comparators.
	 */
	private List<Comparator<File>> comparators;

	/**
	 * Constructs new Comparators from given comparators.
	 * 
	 * @param comparators
	 */
	@SafeVarargs
	public ComposedComparator(Comparator<File>... comparators) {
		this.comparators = new ArrayList<>(Arrays.asList(comparators));
	}

	@Override
	public int compare(File o1, File o2) {
		for (Comparator<File> c : this.comparators) {
			int rez = c.compare(o1, o2);
			if (rez != 0) {
				return rez;
			}
		}
		return 0;
	}

}
