/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.compare;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class SortFiles {

	static class ReverseComparator implements Comparator<File> {

		/**
		 * Original file comparator to be reversed.
		 */
		private Comparator<File> orginal;

		/**
		 * Creates comparator that will reverse output of given comparator.
		 * 
		 * @param orginal
		 *            comparator to be reversed
		 */
		public ReverseComparator(Comparator<File> orginal) {
			this.orginal = orginal;
		}

		@Override
		public int compare(File o1, File o2) {
			return -1 * this.orginal.compare(o1, o2);
		}

	}

	/**
	 * Creates composed comparator from comparators defined in arguments.
	 * 
	 * @param sortArguments
	 *            arguments defining sort comparators
	 * @return composed comparator
	 * @throws IllegalArgumentException
	 *             if wrong format of arguments.
	 */
	public static Comparator<File> getSortComparator(List<String> sortArguments) {
		List<Comparator<File>> comparatos = new ArrayList<>();
		for (String sortArgument : sortArguments) {
			boolean reverse = false;
			if (sortArgument.charAt(0) == '-') {
				sortArgument = sortArgument.substring(1);// removes minus sign
				reverse = true;
			}
			// adding filters based on arguments
			switch (sortArgument.charAt(0)) {
			case 's': {
				comparatos.add(new ComparatorSize());
			}
				break;
			case 'n': {
				comparatos.add(new ComparatorName());
			}
				break;
			case 'm': {
				comparatos.add(new ComparatorLastModified());
			}
				break;
			case 't': {
				comparatos.add(new ComparatorType());
			}
				break;
			case 'l': {
				comparatos.add(new ComparatorNameLength());
			}
				break;
			case 'e': {
				comparatos.add(new ComparatorExecutable());
			}
				break;
			default: {
				throw new IllegalArgumentException("Unknown filter type.");
			}
			}
			if (reverse) {
				comparatos.set(comparatos.size() - 1, new ReverseComparator(
						comparatos.get(comparatos.size() - 1)));
			}
		}
		@SuppressWarnings("unchecked")
		ComposedComparator comparator = new ComposedComparator(
				comparatos.toArray(new Comparator[comparatos.size()]));
		return comparator;
	}

}
