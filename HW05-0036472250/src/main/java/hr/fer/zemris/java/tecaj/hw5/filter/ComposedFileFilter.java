/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class ComposedFileFilter implements FileFilter {

	private List<FileFilter> filters;
	/**
	 * 
	 */
	public ComposedFileFilter(FileFilter ... filters) {
		this.filters = new ArrayList<>(Arrays.asList(filters));
	}

	/* (non-Javadoc)
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File pathname) {
		boolean accepted = true;
		for(FileFilter filter : this.filters) {
			if(!filter.accept(pathname)) {
				accepted = false;
				break;
			}
		}
		return accepted;
	}

}
