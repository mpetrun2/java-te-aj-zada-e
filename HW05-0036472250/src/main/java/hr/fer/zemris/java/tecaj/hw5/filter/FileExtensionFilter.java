/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Marin Petrunić 0036472250
 *
 * Accepts file if it has extension.
 */
public class FileExtensionFilter implements FileFilter {

	/* (non-Javadoc)
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File pathname) {
		if(pathname.getName().contains(".")) {
			return true;
		}
		return false;
	}

}
