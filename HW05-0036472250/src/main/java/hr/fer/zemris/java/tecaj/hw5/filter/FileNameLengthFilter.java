/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Marin Petrunić 0036472250
 *
 * Accepts files based on their name length.
 */
public class FileNameLengthFilter implements FileFilter {

	private int nameSize;
	
	/**
	 * Creates filter from given maximum
	 * file name size.
	 * @param fileNameSize maximum file name size
	 * @throws IllegalArgumentException if invalid maximum
	 * filename length given
	 */
	public FileNameLengthFilter(int fileNameSize) {
		if(fileNameSize <= 0) {
			throw new IllegalArgumentException("Length of file name must be at least 1.");
		}
		this.nameSize = fileNameSize;
	}
	
	/* (non-Javadoc)
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File pathname) {
		if(pathname.getName().length() <= this.nameSize) {
			return true;
		}
		return false;
	}

}
