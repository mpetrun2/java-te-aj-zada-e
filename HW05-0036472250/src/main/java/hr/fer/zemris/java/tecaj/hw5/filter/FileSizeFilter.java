/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Accepts files based on their size.
 *
 */
public class FileSizeFilter implements FileFilter {

	/**
	 * Maximum file size.
	 */
	private int size;
	
	/**
	 * Constructs new filter that accepts 
	 * files based on their size.
	 * @param size maximum file size
	 */
	public FileSizeFilter(int size) {
		if(size < 0) {
			throw new IllegalArgumentException("Invalid file size for file size filter.");
		}
		this.size = size;
	}

	@Override
	public boolean accept(File pathname) {
		if(pathname.length() > this.size) {
			return false;
		}
		return true;
	}

}
