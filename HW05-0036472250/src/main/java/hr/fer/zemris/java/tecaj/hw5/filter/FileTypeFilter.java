/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Marin Petrunić 0036472250
 *
 * Accepts only files and not directories.
 */
public class FileTypeFilter implements FileFilter {

	/* (non-Javadoc)
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File pathname) {
		if(pathname.isFile()) {
			return true;
		}
		return false;
	}

}
