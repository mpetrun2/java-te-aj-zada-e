/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.filter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class Filter {
	
	/**
	 * @author Marin Petrunić 0036472250
	 * Reverses given FileFilter.
	 */
	public static class ReverseFileFilter implements FileFilter {

		/**
		 * Original FileFilter.
		 */
		private FileFilter orginal;
		
		/**
		 * Reverse output of given filter.
		 * @param orginal filter to be reversed
		 */
		public ReverseFileFilter(FileFilter orginal) {
			this.orginal = orginal;
		}
		
		@Override
		public boolean accept(File pathname) {
			if(this.orginal.accept(pathname)) {
				return false;
			}
			return true;
		}
		
	}

	/**
	 * Composes filter from given arguments.
	 * @param filterArguments
	 * @return FileFilter containing all of the filters defined in arguments
	 */
	public static FileFilter composeFilter(List<String> filterArguments) {
		
		List<FileFilter> filters = new ArrayList<>();
		for (String filterArgument : filterArguments) {
			boolean reverse = false;
			if (filterArgument.charAt(0) == '-') {
				filterArgument = filterArgument.substring(1);//removes minus sign
				reverse = true;
			}
			//adding filters based on arguments
			switch(filterArgument.charAt(0)) {
				case 's': {
					filterArgument = filterArgument.substring(1);
					filters.add(new FileSizeFilter(Integer.parseInt(filterArgument)));
				} break;
				case 'f': {
					filters.add(new FileTypeFilter());
				} break;
				case 'l': {
					filterArgument = filterArgument.substring(1);
					filters.add(new FileNameLengthFilter(Integer.parseInt(filterArgument)));
				} break;
				case 'e': {
					filters.add(new FileExtensionFilter());
				} break;
				default: {
					throw new IllegalArgumentException("Unknown filter type.");
				}
			}
			if(reverse) {
				filters.set(
						filters.size() - 1,
						new ReverseFileFilter(filters.get(filters.size() -1))
				);
			}
		}
		FileFilter filter = new ComposedFileFilter(
					filters.toArray(new FileFilter[filters.size()])
				);
		return filter;
	}

}
