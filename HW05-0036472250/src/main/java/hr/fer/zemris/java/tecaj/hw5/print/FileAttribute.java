/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Interface for getting file attribute.
 */
public interface FileAttribute {

	/**
	 * Returns specific file attribute as <code>String</code>.
	 * 
	 * @param file
	 *            source of attribute
	 * @return String representation of file attribute
	 */
	public String getAttribute();

	/**
	 * Prints attribute to standard output with fixed length. If rest of output
	 * is filled with spaces. Depending of attribute output can be left or right
	 * aligned.
	 * 
	 * @param file
	 * @param outputLength
	 */
	public void printAttribute(int outputLength);
}
