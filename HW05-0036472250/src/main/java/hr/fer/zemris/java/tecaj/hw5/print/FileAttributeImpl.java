/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public abstract class FileAttributeImpl implements FileAttribute {

	/**
	 * Instance of file.
	 */
	protected File file;

	public FileAttributeImpl(File file) {
		this.file = file;
	}

	@Override
	public void printAttribute(int outputLength) {
		System.out.print(String.format(" %-" + outputLength + "s ",
				this.getAttribute()));
	}

}
