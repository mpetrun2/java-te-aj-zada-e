/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class FilesPrinter {

	/**
	 * List of files to be printed.
	 */
	private List<File> files;

	/**
	 * List of printing arguments.
	 */
	private List<String> arguments;

	/**
	 * 
	 */
	public FilesPrinter(List<File> files, List<String> arguments) {
		if (arguments.size() == 0) {
			this.arguments = new ArrayList<>();
			this.arguments.add("-w:n");
		} else {
			this.arguments = new ArrayList<>(arguments);
		}
		this.files = files;
	}

	/**
	 * Formats and outputs given files to standard output.
	 */
	public void print() {
		Map<File, List<FileAttribute>> attributes = this.getAttributes();
		List<Integer> sizes = this.getMaxAttributeSizes(attributes);
		this.printTableLine(sizes);
		for (File file : this.files) {
			this.printTableRow(attributes.get(file), sizes);
		}
		this.printTableLine(sizes);
	}

	/**
	 * Prints line that looks like: +-----------+-------+
	 * 
	 * @param columnSizes
	 */
	private void printTableLine(List<Integer> columnSizes) {
		System.out.print("+");
		for (Integer columnSize : columnSizes) {
			for (int i = 0; i < columnSize + 2; i++) {
				System.out.print("-");
			}
			System.out.print("+");
		}
		System.out.print("\n");
	}

	/**
	 * Prints single table row.
	 * 
	 * @param attributes
	 * @param columnSizes
	 */
	private void printTableRow(List<FileAttribute> attributes,
			List<Integer> columnSizes) {
		System.out.print("|");
		for (FileAttribute attribute : attributes) {
			attribute.printAttribute(columnSizes.get(attributes
					.indexOf(attribute)));
			System.out.print("|");
		}
		System.out.print("\n");
	}

	/**
	 * Creates map with files as keys and list of {@link FileAttribute} as
	 * values.
	 * 
	 * @return Map with file attributes
	 */
	private Map<File, List<FileAttribute>> getAttributes() {
		Map<File, List<FileAttribute>> fileAttributes = new LinkedHashMap<>();
		for (File file : this.files) {
			List<FileAttribute> attributes = new ArrayList<>();
			for (String attributeArg : this.arguments) {
				attributes.add(this.getFileAttribute(file, attributeArg));
			}
			fileAttributes.put(file, attributes);
		}
		return fileAttributes;
	}

	/**
	 * Creates new {@link FileAttribute} from given attribute argument and file.
	 * 
	 * @param file
	 *            given file
	 * @param attributeArg
	 *            given attribute argument
	 * @return specific FileAttribute
	 */
	private FileAttribute getFileAttribute(File file, String attributeArg) {
		String arg = attributeArg.substring(attributeArg.indexOf(':') + 1);
		switch (arg) {
		case "n": {
			return new NameAttribute(file);
		}
		case "t": {
			return new TypeAttribute(file);
		}
		case "s": {
			return new SizeAttribute(file);
		}
		case "m": {
			return new LastModificationAttribute(file);
		}
		case "h": {
			return new HiddenAttribute(file);
		}
		default: {
			throw new IllegalArgumentException("Invalid print argument.");
		}
		}
	}

	/**
	 * Gets maximum size for each file attribute column and returns then as
	 * list.
	 * 
	 * @param attributes
	 * @return
	 */
	private List<Integer> getMaxAttributeSizes(
			Map<File, List<FileAttribute>> attributes) {
		List<Integer> sizes = new ArrayList<>(Collections.nCopies(attributes
				.get(files.get(0)).size(), 1));
		for (File file : this.files) {
			for (FileAttribute attribute : attributes.get(file)) {
				if (attribute.getAttribute().length() > sizes.get(attributes
						.get(file).indexOf(attribute))) {
					sizes.set(attributes.get(file).indexOf(attribute),
							attribute.getAttribute().length());
				}
			}
		}
		return sizes;
	}

}
