/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         File hidden attribute.
 * 
 */
public class HiddenAttribute extends FileAttributeImpl {

	/**
	 * @param file
	 */
	public HiddenAttribute(File file) {
		super(file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj.hw5.print.FileAttribute#getAttribute()
	 */
	@Override
	public String getAttribute() {
		if (this.file.isHidden()) {
			return "h";
		}
		return " ";
	}

}
