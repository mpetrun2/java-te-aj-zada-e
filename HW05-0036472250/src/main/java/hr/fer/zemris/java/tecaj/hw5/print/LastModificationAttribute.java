/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Marin Petrunić 0036472250 File last modification argument.
 */
public class LastModificationAttribute extends FileAttributeImpl {

	/**
	 * @param file
	 */
	public LastModificationAttribute(File file) {
		super(file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj.hw5.print.FileAttribute#getAttribute()
	 */
	@Override
	public String getAttribute() {
		Date date = new Date(this.file.lastModified());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = sdf.format(date);
		return format;
	}

}
