/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Gets file name attribute.
 */
public class NameAttribute extends FileAttributeImpl {

	public NameAttribute(File file) {
		super(file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw5.print.Attribute#getAttribute(java.io.File)
	 */
	@Override
	public String getAttribute() {
		return file.getName();
	}

}
