/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         File size attribute.
 */
public class SizeAttribute extends FileAttributeImpl {

	public SizeAttribute(File file) {
		super(file);
	}

	@Override
	public String getAttribute() {
		return String.valueOf(this.file.length());
	}

	@Override
	public void printAttribute(int outputLength) {
		System.out.print(String.format(" %" + outputLength + "s ",
				this.getAttribute()));
	}

}
