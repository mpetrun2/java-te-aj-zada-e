/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw5.print;

import java.io.File;

/**
 * @author Marin Petrunić 0036472250 Gets file type attribute. For files returns
 *         <code>f</code> and for directories <code>d</code>.
 */
public class TypeAttribute extends FileAttributeImpl {

	public TypeAttribute(File file) {
		super(file);
	}

	@Override
	public String getAttribute() {
		if (file.isDirectory()) {
			return "d";
		}
		return "f";
	}

}
