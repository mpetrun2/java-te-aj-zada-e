/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp;

import hr.fer.zemris.java.tecaj_2.jcomp.impl.ComputerImpl;
import hr.fer.zemris.java.tecaj_2.jcomp.impl.ExecutionUnitImpl;
import hr.fer.zemris.java.tecaj_2.jcomp.parser.InstructionCreatorImpl;
import hr.fer.zemris.java.tecaj_2.jcomp.parser.ProgramParser;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class Simulator {

	/**
	 * @param args
	 *            single argument with path and name of program to execute
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		if (args.length != 1) {
			throw new IllegalArgumentException(
					"Only 1 argument from command line expected");
		}

		// Stvori računalo s 256 memorijskih lokacija i 16 registara
		Computer comp = new ComputerImpl(256, 16);

		// Stvori objekt koji zna stvarati primjerke instrukcija
		InstructionCreator creator = new InstructionCreatorImpl(
				"hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions");

		// Napuni memoriju računala programom iz datoteke; instrukcije stvaraj
		// uporabom predanog objekta za stvaranje instrukcija
		ProgramParser.parse(args[0], comp, creator);

		// Stvori izvršnu jedinicu
		ExecutionUnit exec = new ExecutionUnitImpl();

		// Izvedi program
		exec.go(comp);
	}

}
