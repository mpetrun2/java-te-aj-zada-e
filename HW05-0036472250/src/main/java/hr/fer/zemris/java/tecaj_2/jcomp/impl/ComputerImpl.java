/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Memory;
import hr.fer.zemris.java.tecaj_2.jcomp.Registers;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Implementation of data part of computer.
 */
public class ComputerImpl implements Computer {

	/**
	 * Computer memory
	 */
	private Memory memory;

	/**
	 * Computer registers.
	 */
	private Registers registers;

	/**
	 * Creates computer with given memory size and number of registers.
	 */
	public ComputerImpl(int memorySize, int registerNumber) {
		this.memory = new MemoryImpl(memorySize);
		this.registers = new RegistersImpl(registerNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Computer#getRegisters()
	 */
	@Override
	public Registers getRegisters() {
		return this.registers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Computer#getMemory()
	 */
	@Override
	public Memory getMemory() {
		return this.memory;
	}

}
