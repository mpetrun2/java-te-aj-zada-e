/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.ExecutionUnit;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class representing computer controller.
 * 
 */
public class ExecutionUnitImpl implements ExecutionUnit {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.ExecutionUnit#go(hr.fer.zemris.java.
	 * tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean go(Computer computer) {
		try {
			computer.getRegisters().setProgramCounter(0);
			while (true) {
				int programCounter = computer.getRegisters()
						.getProgramCounter();
				Instruction instruction = (Instruction) computer.getMemory()
						.getLocation(programCounter);
				computer.getRegisters().setProgramCounter(++programCounter);
				if (instruction.execute(computer)) {
					break;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
