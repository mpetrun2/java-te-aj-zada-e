/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl;

import hr.fer.zemris.java.tecaj_2.jcomp.Memory;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Implementation of computer memory. This memory
 * accepts Object on each address.
 *
 */
public class MemoryImpl implements Memory {
	
	/**
	 * Arrays of objects representing computer memory.
	 */
	private Object[] memory;
	
	/**
	 * Crates computer memory with given size.
	 * Computer memory must have at least size 1.
	 * @param size memory size
	 * @throws IllegalArgumentException if invalid size given
	 */
	public MemoryImpl(int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Memory must have size at least 1.");
		}
		this.memory = new Object[size];
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Memory#setLocation(int, java.lang.Object)
	 */
	@Override
	public void setLocation(int location, Object value) {
		if(location < 0 || location >= this.memory.length) {
			throw new IllegalArgumentException("Memory does not have this location.");
		}
		this.memory[location] = value;

	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Memory#getLocation(int)
	 */
	@Override
	public Object getLocation(int location) {
		if(location < 0 || location >= this.memory.length) {
			throw new IllegalArgumentException("Memory does not have this location.");
		}
		return this.memory[location];
	}

}
