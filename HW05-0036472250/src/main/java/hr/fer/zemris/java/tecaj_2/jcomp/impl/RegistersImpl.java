/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl;

import hr.fer.zemris.java.tecaj_2.jcomp.Registers;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Implementation of computer registers. This implementation has user
 *         defined number of general purpose registers, program counter and a
 *         flag;
 * 
 */
public class RegistersImpl implements Registers {

	/**
	 * Array of computer general purpose registers.
	 */
	private Object[] registers;

	/**
	 * Address of next instruction.
	 */
	int programCounter;

	/**
	 * Computer flag value.
	 */
	boolean flag;

	/**
	 * Initialize array of general purpose registers to given number.
	 * 
	 * @param regsLen
	 *            number of computer registers
	 * @throws IllegalArgumentException
	 *             if number of registers is less than zero
	 */
	public RegistersImpl(int regsLen) {
		if (regsLen < 0) {
			throw new IllegalArgumentException();
		}
		this.registers = new Object[regsLen];
		this.programCounter = 0;
		this.flag = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#getRegisterValue(int)
	 */
	@Override
	public Object getRegisterValue(int index) {
		if (index < 0 || index >= this.registers.length) {
			throw new IllegalArgumentException(
					"Registers does not conrain that index");
		}
		return this.registers[index];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#setRegisterValue(int,
	 * java.lang.Object)
	 */
	@Override
	public void setRegisterValue(int index, Object value) {
		if (index < 0 || index >= this.registers.length) {
			throw new IllegalArgumentException("Index out of register range.");
		}
		this.registers[index] = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#getProgramCounter()
	 */
	@Override
	public int getProgramCounter() {
		return this.programCounter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#setProgramCounter(int)
	 */
	@Override
	public void setProgramCounter(int value) {
		if (value < 0) {
			throw new IllegalArgumentException("First adress starts at 0.");
		}
		this.programCounter = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#incrementProgramCounter()
	 */
	@Override
	public void incrementProgramCounter() {
		this.programCounter++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#getFlag()
	 */
	@Override
	public boolean getFlag() {
		return this.flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.tecaj_2.jcomp.Registers#setFlag(boolean)
	 */
	@Override
	public void setFlag(boolean value) {
		this.flag = value;
	}

}
