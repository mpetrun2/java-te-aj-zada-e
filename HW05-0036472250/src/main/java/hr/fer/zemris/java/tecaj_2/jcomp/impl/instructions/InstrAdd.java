/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents addition instruction. Example:
 *         <code>add r0, r1, r2;</code>
 * 
 */
public class InstrAdd implements Instruction {

	/**
	 * Register 1 index
	 */
	private int registerIndex1;

	/**
	 * Register 2 index
	 */
	private int registerIndex2;

	/**
	 * Register 3 index
	 */
	private int registerIndex3;

	/**
	 * Constructs new instance of <code>add</code> instruction.
	 * 
	 * @param arguments
	 *            given arguments for instruction
	 * @throw IllegalArgumentException if wrong number or type of arguments
	 *        given.
	 */
	public InstrAdd(List<InstructionArgument> arguments) {
		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException("Type mismatch for argument 0!");
		}
		if (!arguments.get(1).isRegister()) {
			throw new IllegalArgumentException("Type mismatch for argument 1!");
		}
		if (!arguments.get(2).isRegister()) {
			throw new IllegalArgumentException("Type mismatch for argument 2!");
		}
		this.registerIndex1 = ((Integer) arguments.get(0).getValue())
				.intValue();
		this.registerIndex2 = ((Integer) arguments.get(1).getValue())
				.intValue();
		this.registerIndex3 = ((Integer) arguments.get(2).getValue())
				.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters()
				.getRegisterValue(registerIndex2);
		Object value2 = computer.getRegisters()
				.getRegisterValue(registerIndex3);
		computer.getRegisters().setRegisterValue(
				this.registerIndex1,
				Integer.valueOf(((Integer) value1).intValue()
						+ ((Integer) value2).intValue()));
		return false;
	}

}
