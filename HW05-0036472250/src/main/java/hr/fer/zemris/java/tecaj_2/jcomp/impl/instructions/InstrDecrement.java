/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Instruction for decrementing given register value.
 * 
 */
public class InstrDecrement implements Instruction {

	/**
	 * Given register index.
	 */
	private int registerIndex;

	/**
	 * Construct new decrement instruction with one register argument.
	 * 
	 * @param arguments
	 *            given register
	 * @throws IllegalArgumentException
	 *             if wrong number of type of arguments
	 */
	public InstrDecrement(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException(
					"Expected number of parameters is 1.");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException("Argument must be register.");
		}
		this.registerIndex = ((Integer) arguments.get(0).getValue()).intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getRegisters().getRegisterValue(registerIndex);
		computer.getRegisters().setRegisterValue(registerIndex,
				Integer.valueOf(((Integer) value).intValue() - 1));
		return false;
	}

}
