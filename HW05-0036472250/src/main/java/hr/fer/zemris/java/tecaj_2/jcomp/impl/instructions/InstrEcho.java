/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250 Instruction which gets value from given
 *         register and prints it to console. Instruction example:
 *         <code>echo r0;</code>
 * 
 */
public class InstrEcho implements Instruction {

	/**
	 * Given register index.
	 */
	private int registerIndex;

	/**
	 * Creates new instance of echo instruction. Instruction accepts only one
	 * argument of type register.
	 * 
	 * @param arguments
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments
	 */
	public InstrEcho(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException(
					"Expected number of parameters is 1.");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException("Argument must be register.");
		}
		this.registerIndex = ((Integer) arguments.get(0).getValue()).intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		System.out.print(computer.getRegisters()
				.getRegisterValue(registerIndex));
		return false;
	}

}
