/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class representing instruction <code>halt</code> This instruction
 *         stops computer. Example: <code> halt;</code>
 */
public class InstrHalt implements Instruction {

	/**
	 * Creates new instance of instruction halt.
	 * 
	 * @param arguments
	 *            no parameters expected
	 * @throws IllegalArgumentException
	 *             if wrong number of arguments
	 */
	public InstrHalt(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("No arguments was expected.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		return true;
	}

}
