/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents Iinput instruction. Reads line from std input. Interprets
 *         given input as Integer. Sets flag to true if everything went without
 *         errors.
 */
public class InstrIinput implements Instruction {

	/**
	 * Memory address.
	 */
	private int memoryLocation;

	/**
	 * Creates new Iinput instruction from given arguments.
	 * 
	 * @param arguments
	 *            memory location on which user input will be stored.
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments given.
	 */
	public InstrIinput(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Only 1 argument expected");
		}
		if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Wrong type of argument 1 given.");
		}
		this.memoryLocation = ((Integer) arguments.get(0).getValue())
				.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Scanner scanner = new Scanner(System.in, "UTF-8");
		try {
			String line = scanner.nextLine();
			Integer value = Integer.parseInt(line);
			computer.getMemory().setLocation(memoryLocation, value);
			scanner.close();
		} catch (Exception e) {
			computer.getRegisters().setFlag(false);
			return false;
		}
		computer.getRegisters().setFlag(true);
		return false;
	}

}
