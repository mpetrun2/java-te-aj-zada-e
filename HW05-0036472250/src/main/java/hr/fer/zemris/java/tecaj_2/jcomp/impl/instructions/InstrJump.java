/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents jump instruction. Program jumps to given location.
 * 
 */
public class InstrJump implements Instruction {

	/**
	 * Location of next instruction.
	 */
	private int instructionLocation;

	/**
	 * Constructs new jump instructions with given argument.
	 * 
	 * @param arguments
	 *            given location of next instruction
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments given.
	 */
	public InstrJump(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException(
					"Expected number of parameters is 1.");
		}
		if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Expected argument to be location.");
		}
		this.instructionLocation = ((Integer) arguments.get(0).getValue())
				.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		computer.getRegisters().setProgramCounter(instructionLocation);
		return false;
	}

}
