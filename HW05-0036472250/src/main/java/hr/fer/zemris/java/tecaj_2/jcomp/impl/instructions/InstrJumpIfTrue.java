/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Representing instruction jumpiftrue. Jumps to instruction at given
 *         location if flag set to true.
 * 
 */
public class InstrJumpIfTrue implements Instruction {

	/**
	 * Jump instruction.
	 */
	private InstrJump jump;

	/**
	 * Constructs jump instruction from given arguments.
	 * 
	 * @param arguments
	 */
	public InstrJumpIfTrue(List<InstructionArgument> arguments) {
		this.jump = new InstrJump(arguments);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		if (computer.getRegisters().getFlag()) {
			this.jump.execute(computer);
		}
		return false;
	}

}
