/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250 Instruction which gets value from given
 *         memory adress and puts it in given register. Example of instruction:
 *         <code>load r0, 14;</code>
 * 
 */
public class InstrLoad implements Instruction {

	/**
	 * Index of register in which value will be stored.
	 */
	private int registerIndex;

	/**
	 * Memory location on which values is stored.
	 */
	private int memoryLocation;

	/**
	 * Creates command instance from given arguments. First argument must be
	 * register and second memory address.
	 * 
	 * @param arguments
	 *            given arguments for instruction
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments.
	 */
	public InstrLoad(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException(
					"Expected first argument to be register.");
		}
		if (!arguments.get(1).isNumber()) {
			throw new IllegalArgumentException("Type missmatch for argument 1.");
		}
		this.registerIndex = ((Integer) arguments.get(0).getValue()).intValue();
		this.memoryLocation = ((Integer) arguments.get(1).getValue())
				.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getMemory().getLocation(memoryLocation);
		computer.getRegisters().setRegisterValue(registerIndex, value);
		return false;
	}

}
