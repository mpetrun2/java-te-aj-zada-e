/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents move instruction. Moves value from one register to
 *         another.
 * 
 */
public class InstrMove implements Instruction {

	/**
	 * Index of register 1.
	 */
	private int registerIndex1;

	/**
	 * Index of register 1.
	 */
	private int registerIndex2;

	/**
	 * Constructs new move instruction from given arguments.
	 * 
	 * @param arguments
	 *            given registers
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments given.
	 */
	public InstrMove(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments.");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException(
					"Expected first argument to be register.");
		}
		if (!arguments.get(1).isRegister()) {
			throw new IllegalArgumentException(
					"Expected first argument to be register.");
		}
		this.registerIndex1 = ((Integer) arguments.get(0).getValue())
				.intValue();
		this.registerIndex2 = ((Integer) arguments.get(1).getValue())
				.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value = computer.getRegisters().getRegisterValue(registerIndex2);
		computer.getRegisters().setRegisterValue(registerIndex1, value);
		return false;
	}

}
