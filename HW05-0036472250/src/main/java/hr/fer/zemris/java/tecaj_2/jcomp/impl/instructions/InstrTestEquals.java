/**
 * 
 */
package hr.fer.zemris.java.tecaj_2.jcomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.tecaj_2.jcomp.Computer;
import hr.fer.zemris.java.tecaj_2.jcomp.Instruction;
import hr.fer.zemris.java.tecaj_2.jcomp.InstructionArgument;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents test equals instruction.
 * 
 */
public class InstrTestEquals implements Instruction {

	/**
	 * Index of register 1.
	 */
	private int registerIndex1;

	/**
	 * Index of register 2.
	 */
	private int registerIndex2;

	/**
	 * Constructs new test equals instructions.
	 * 
	 * @param arguments
	 *            given registers
	 * @throws IllegalArgumentException
	 *             if wrong number or type of arguments
	 */
	public InstrTestEquals(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments.");
		}
		if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException(
					"Argument 1 expected to be register.");
		}
		if (!arguments.get(1).isRegister()) {
			throw new IllegalArgumentException(
					"Argument 2 expected to be register.");
		}
		this.registerIndex1 = ((Integer) arguments.get(0).getValue())
				.intValue();
		this.registerIndex2 = ((Integer) arguments.get(1).getValue())
				.intValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj_2.jcomp.Instruction#execute(hr.fer.zemris.java
	 * .tecaj_2.jcomp.Computer)
	 */
	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters()
				.getRegisterValue(registerIndex1);
		Object value2 = computer.getRegisters()
				.getRegisterValue(registerIndex2);
		if (value1.equals(value2)) {
			computer.getRegisters().setFlag(true);
		} else {
			computer.getRegisters().setFlag(false);
		}
		return false;
	}
}
