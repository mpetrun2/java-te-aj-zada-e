/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class representing multistack object. It contains multiple stack
 *         specified by {@link String} keys.
 */
public class ObjectMultistack {

	/**
	 * Map instance containing {@link MultistackEntry} at specific
	 * {@link String} keys.
	 */
	private Map<String, MultistackEntry> multiStack;

	/**
	 * Constructs new {@link ObjectMultistack}. No arguments needed.
	 */
	public ObjectMultistack() {
		this.multiStack = new HashMap<>();
	}

	/**
	 * If given name key exist, given {@link ValueWrapper} will be pushed on
	 * existing stack at given key. If given name key doesn't exist new stack
	 * instance will be created and given {@link ValueWrapper} will be its only
	 * element.
	 * 
	 * @param name
	 *            stack key
	 * @param valueWrapper
	 *            object to be pushed on stack at given key
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		if (multiStack.containsKey(name)) {
			multiStack.get(name).push(valueWrapper);
		} else {
			multiStack.put(name, new MultistackEntry(valueWrapper));
		}
	}

	/**
	 * Pops last element added to the stack at given name key.
	 * 
	 * @param name
	 *            stack key
	 * @return {@link ValueWrapper} poped
	 * @throws IllegalArgumentException
	 *             if given name key doesn't exist
	 * @throws UnsupportedOperationException
	 *             if pop used on empty stack
	 */
	public ValueWrapper pop(String name) {
		if (!multiStack.containsKey(name)) {
			throw new IllegalArgumentException("Given key doesn't exist.");
		}
		return multiStack.get(name).pop();
	}

	/**
	 * Gets element last added to the stack but doesn't removes it.
	 * 
	 * @param name
	 *            stack key
	 * @return {@link ValueWrapper} peeked element
	 * @throws IllegalArgumentException
	 *             if given name key doesn't exist
	 * @throws UnsupportedOperationException
	 *             if pop used on empty stack
	 */
	public ValueWrapper peek(String name) {
		if (!multiStack.containsKey(name)) {
			throw new IllegalArgumentException("Given key doesn't exist.");
		}
		return multiStack.get(name).peek();
	}

	/**
	 * Checks if stack at given name key is empty.
	 * 
	 * @param name
	 *            stack key
	 * @return <code>true</code> if is empty, <code>false</code> otherwise.
	 * @throws IllegalArgumentException
	 *             if given key doesn't exist
	 */
	public boolean isEmpty(String name) {
		if (!multiStack.containsKey(name)) {
			throw new IllegalArgumentException("Given key doesn't exist.");
		}
		return multiStack.get(name).isEmpty();
	}

	/**
	 * 
	 * @author Marin Petrunić 0036472250
	 * 
	 *         Class representing stack with private instance of linked list.
	 */
	private static class MultistackEntry {

		/**
		 * Private instance of list representing stack.
		 */
		private List<ValueWrapper> stack;

		/**
		 * Default constructor of {@link MultistackEntry}. Its purpose is to
		 * simply instance stack.
		 */
		public MultistackEntry() {
			stack = new LinkedList<>();
		}

		/**
		 * Constructor which instance stack and push given {@link ValueWrapper}
		 * object.
		 * 
		 * @param value
		 *            object to be pushed
		 */
		public MultistackEntry(ValueWrapper value) {
			this();
			this.push(value);
		}

		/**
		 * Push given {@link ValueWrapper} to private stack.
		 * 
		 * @param value
		 *            given object to be pushed
		 */
		public void push(ValueWrapper value) {
			stack.add(new ValueWrapper(value.getValue()));
		}

		/**
		 * Pops last element added to the private list.
		 * 
		 * @return {@link ValueWrapper} poped value
		 * @throws UnsupportedOperationException
		 *             if trying to pop empty stack.
		 */
		public ValueWrapper pop() {
			if (stack.isEmpty()) {
				throw new UnsupportedOperationException(
						"Trying to pop empty stack.");
			}
			return stack.remove(stack.size() - 1);
		}

		/**
		 * Gets element last added to the stack but doesnt removes it.
		 * 
		 * @return {@link ValueWrapper} last element
		 * @throws UnsupportedOperationException
		 *             if called upon empty stack.
		 */
		public ValueWrapper peek() {
			if (stack.isEmpty()) {
				throw new UnsupportedOperationException(
						"Trying to peek empty stack.");
			}
			return stack.get(stack.size() - 1);
		}

		/**
		 * Checks if stack is empty.
		 * 
		 * @return <code>true</code> if it doesnt contain any element,
		 *         <code>false</code> otherwise.
		 */
		public boolean isEmpty() {
			return stack.isEmpty();
		}
	}
}
