/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class ValueWrapper {

	/**
	 * Private object of type {@link Integer} or {@link Double}.
	 */
	private Object value;

	/**
	 * Constructs {@link ValueWrapper} from given value. Accepted {@link Object}
	 * s are String(parsable into {@link Double}), Integer, Double and
	 * null(converted to 0)
	 * 
	 * @param value
	 *            given value
	 */
	public ValueWrapper(Object value) {
		this.value = this.processObject(value);
	}

	/**
	 * Returns value stored in {@link ValueWrapper}.
	 * 
	 * @return value in {@link ValueWrapper}
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Sets value in {@link ValueWrapper}
	 * 
	 * @param value
	 *            given to be stored
	 */
	public void setValue(Object value) {
		this.value = processObject(value);
	}

	/**
	 * Increments stored value by given value. If at least one of values is
	 * instance of {@link Double} result is stored as {@link Double}.
	 * 
	 * @param incValue
	 *            increment value
	 * 
	 */
	public void increment(Object incValue) {
		incValue = this.processObject(incValue);
		if (this.areDouble(value, incValue)) {
			this.setValue(((Number) value).doubleValue()
					+ ((Number) incValue).doubleValue());
		} else {
			this.setValue(((Number) value).intValue()
					+ ((Number) incValue).intValue());
		}
	}

	/**
	 * Decrements stored value by given value. If at least one of values is
	 * instance of {@link Double} result is stored as {@link Double}.
	 * 
	 * @param decValue
	 *            decrement value
	 * 
	 */
	public void decrement(Object decValue) {
		decValue = this.processObject(decValue);
		if (this.areDouble(value, decValue)) {
			this.setValue(((Number) value).doubleValue()
					- ((Number) decValue).doubleValue());
		} else {
			this.setValue(((Number) value).intValue()
					- ((Number) decValue).intValue());
		}
	}

	/**
	 * Multiply stored value by given value. If at least one of values is
	 * instance of {@link Double} result is stored as {@link Double}.
	 * 
	 * @param mulValue
	 *            multiply value
	 * 
	 */
	public void multiply(Object mulValue) {
		mulValue = this.processObject(mulValue);
		if (this.areDouble(value, mulValue)) {
			this.setValue(((Number) value).doubleValue()
					* ((Number) mulValue).doubleValue());
		} else {
			this.setValue(((Number) value).intValue()
					* ((Number) mulValue).intValue());
		}
	}

	/**
	 * Divide stored value by given value. If at least one of values is instance
	 * of {@link Double} result is stored as {@link Double}.
	 * 
	 * @param divValue
	 *            divider value
	 * 
	 */
	public void divide(Object divValue) {
		divValue = this.processObject(divValue);
		if (isZero(divValue)) {
			throw new IllegalArgumentException("Dividing with zero.");
		}
		if (this.areDouble(value, divValue)) {
			this.setValue(((Number) value).doubleValue()
					/ ((Number) divValue).doubleValue());
		} else {
			this.setValue(((Number) value).intValue()
					/ ((Number) divValue).intValue());
		}
	}

	/**
	 * Compares value storred in {@link ValueWrapper} and given value. The
	 * method returns an integer less than zero if currently stored value is
	 * smaller than argument, an integer greater than zero if currently stored
	 * value is larger than argument or an integer 0 if they are equal.
	 * 
	 * @param withValue
	 *            value to be compared to
	 * @return int result
	 */
	public int numCompare(Object withValue) {
		return ((Double) ((Number) value).doubleValue())
				.compareTo(((Number) withValue).doubleValue());
	}

	/**
	 * Checks if at least one of given values is instance of {@link Double}.
	 * 
	 * @param value1
	 *            first value
	 * @param value2
	 *            second value
	 * @return true if at least one of given values is instance of
	 *         {@link Double}
	 */
	private boolean areDouble(Object value1, Object value2) {
		if (this.isDouble(value1) || this.isDouble(value2)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if given {@link Object} is instance of {@link Double}.
	 * 
	 * @param value
	 *            to be tested
	 * @return true if it is instance of {@link Double}, false otherwise.
	 */
	private boolean isDouble(Object value) {
		if (value instanceof Double) {
			return true;
		}
		return false;
	}

	/**
	 * Process given Object into supported values. Accepted values are
	 * String(parsable as {@link Double}), Double, Integer and <code>null</code>
	 * converted to 0.
	 * 
	 * @param value
	 *            given value to process
	 * @return supported class({@link Integer} or {@link Double})
	 * @throws RuntimeException
	 *             if unsupported type of {@link Object} given
	 */
	private Object processObject(Object value) {
		if (value == null) {
			return new Integer(0);
		}
		if (value instanceof Integer || value instanceof Double) {
			return value;
		}
		if (value instanceof String) {
			if (((String) value).contains(".")
					|| ((String) value).contains("E")) {
				return new Double((String) value);
			} else {
				return new Integer((String) value);
			}
		}
		throw new RuntimeException("Unsuported type of value.");
	}

	/**
	 * Checks if given number is equal to zero.
	 * 
	 * @param num
	 * @return
	 */
	private boolean isZero(Object num) {
		if (num instanceof Integer) {
			if ((Integer) num == 0) {
				return true;
			}
		} else if (num instanceof Double) {
			if (Double.compare((Double) num, 0.0) == 0) {
				return true;
			}
		}
		return false;
	}
}
