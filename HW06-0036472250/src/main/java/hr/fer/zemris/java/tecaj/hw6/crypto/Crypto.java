/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Program for encrypting, decrypting and checking sha signature.
 */
public class Crypto {

	/**
	 * Size of Input buffer.
	 */
	private static int BUFFER_SIZE = 4024;

	/**
	 * First argument must be name of command. Acceptable commands are
	 * "encrypt", "decrypt" and "checksha".<br>
	 * Next arguments are depending on command:<br>
	 * "encrypt" => path to file to be encrypted and path of destination file<br>
	 * "decrypt" => path to file to be decrypted and path of destination file<br>
	 * "checksha" => path to file to be checked<br>
	 * 
	 * @param args
	 *            program arguments as defined in program description
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			throw new IllegalArgumentException("Name of command not provided");
		}
		switch (args[0]) {
		case "encrypt": {
			if (args.length != 3) {
				throw new IllegalArgumentException(
						"Invalid number of arguments for given command");
			}
			crypt(true, Paths.get(args[1]), Paths.get(args[2]));
		}
			break;
		case "decrypt": {
			if (args.length != 3) {
				throw new IllegalArgumentException(
						"Invalid number of arguments for given command");
			}
			crypt(false, Paths.get(args[1]), Paths.get(args[2]));
		}
			break;
		case "checksha": {
			if (args.length != 2) {
				throw new IllegalArgumentException(
						"Invalid number of arguments for given command");
			}
			checksha(Paths.get(args[1]));
		}
			break;
		default: {
			throw new IllegalArgumentException("Unknown command.");
		}
		}
	}

	/**
	 * Checks if given files sha is equal to user inputed sha signature.
	 * 
	 * @param file
	 *            file to be checked
	 */
	private static void checksha(Path file) {

		System.out.println("Please provide expected sha signature for "
				+ file.getFileName() + ":");
		System.out.print("> ");
		Scanner scanner = new Scanner(System.in, "UTF-8");
		String shaSignature = scanner.nextLine();
		scanner.close();
		try (BufferedInputStream input = new BufferedInputStream(
				new FileInputStream(file.toFile()), BUFFER_SIZE)) {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] inputByteBuffer = new byte[BUFFER_SIZE];
			int nRead = 0;
			while ((nRead = input.read(inputByteBuffer)) != -1) {
				sha.update(inputByteBuffer, 0, nRead);
			}
			input.close();
			System.out.print("Digesting completed. ");
			String digestedSignature = byteToHexChar(sha.digest());
			if (shaSignature.equals(digestedSignature.toString())) {
				System.out.println("Digest of " + file.getFileName()
						+ " matches expected digest.");
			} else {
				System.out.println("Digest of " + file.getFileName()
						+ " does not match the expected digest. Digest was: "
						+ digestedSignature);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Encrypts or decrypts given source file based on encrypt variable into
	 * destination file.
	 * 
	 * @param encrypt
	 *            encrypt or decrypt
	 * @param srcFile
	 *            source file
	 * @param destFile
	 *            dest file
	 */
	private static void crypt(boolean encrypt, Path srcFile, Path destFile) {
		System.out.println("Please provide password"
				+ " as hex-encoded text (16 bytes, i.e. 32 hex-digits):");
		System.out.print("> ");
		Scanner scanner = new Scanner(System.in, "UTF-8");
		String password = scanner.nextLine();
		System.out
				.println("Please provide initialization vector as hex-encoded text (32 hex-digits):");
		System.out.print("> ");
		String initializationVector = scanner.nextLine();
		scanner.close();
		try (BufferedInputStream input = new BufferedInputStream(
				new FileInputStream(srcFile.toFile()), BUFFER_SIZE);
				BufferedOutputStream output = new BufferedOutputStream(
						new FileOutputStream(destFile.toFile()), BUFFER_SIZE)) {
			byte[] ByteBuffer = new byte[BUFFER_SIZE];
			SecretKeySpec keySpec = new SecretKeySpec(hextobyte(password),
					"AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(
					hextobyte(initializationVector));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE,
					keySpec, paramSpec);
			int nread;
			while ((nread = input.read(ByteBuffer)) != -1) {
				output.write(cipher.update(ByteBuffer, 0, nread));
			}
			try {
				ByteBuffer = cipher.doFinal();
			} catch (IllegalBlockSizeException e1) {
				ByteBuffer = null;
			} catch (BadPaddingException e2) {
				ByteBuffer = null;
			}
			if (ByteBuffer != null) {
				output.write(ByteBuffer);
			}
			System.out
					.println("Decryption/enryption completed. Generated file "
							+ destFile.getFileName() + " based on file "
							+ srcFile.getFileName() + ".");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private static byte[] hextobyte(String text) {
		int len = text.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(text.charAt(i), 16) << 4) + Character
					.digit(text.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * Converts given byte array to hex char string.
	 * 
	 * @param mdbytes
	 *            given array off bytes
	 * @return
	 */
	private static String byteToHexChar(byte[] mdbytes) {
		StringBuffer digestedSignature = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			digestedSignature.append(Integer.toString(
					(mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return digestedSignature.toString();
	}
}
