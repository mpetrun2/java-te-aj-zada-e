/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1a;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class ChangeCounter implements IntegerStorageObserver {

	private int changeCounter = 0;

	/**
	 * Logs each time value stored in {@link IntegerStorage} and outputs to
	 * standard output.
	 * 
	 * @param istorage
	 *            given {@link IntegerStorage} with changed value
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		this.changeCounter++;
		System.out.println("Number of value changes since tracking: "
				+ changeCounter);
	}

}
