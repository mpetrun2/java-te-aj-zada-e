/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1a;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Outputs double value of changed value in {@link IntegerStorage} to
 *         standard output. After second value change it deregister itself from
 *         {@link IntegerStorage}.
 */
public class DoubleValue implements IntegerStorageObserver {

	private int counter = 0;

	/**
	 * Outputs double value of value changed in given {@link IntegerStorage}.
	 * 
	 * @param istorage
	 *            given {@link IntegerStorage}
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Double value: " + istorage.getValue() * 2);
		counter++;
		if (counter == 2) {
			istorage.removeObserver(this);
		}
	}

}
