/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1a;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class for storing {@link Integer} values.
 */
public class IntegerStorage {
	private int value;

	/**
	 * List of class observers.
	 */
	private List<IntegerStorageObserver> observers;

	/**
	 * Creates new {@link IntegerStorage} ant stores given value.
	 * 
	 * @param initialValue
	 *            given initial value
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
	}

	/**
	 * Adds new observer to the list of observers.
	 * 
	 * @param observer
	 *            observer to be added
	 * @throws IllegalArgumentException
	 *             if null pointer sent
	 */
	public void addObserver(IntegerStorageObserver observer) {
		if (observer == null) {
			throw new IllegalArgumentException("Null object not expected.");
		}
		if (observers == null) {
			observers = new ArrayList<>();
		}
		this.observers.add(observer);
	}

	/**
	 * Removes given observer from list of observers. If such observer doesn't
	 * exist nothing is going to happen.
	 * 
	 * @param observer
	 *            given observer to be removed
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		ListIterator<IntegerStorageObserver> it = observers.listIterator();
		while (it.hasNext()) {
			IntegerStorageObserver currentObserver = it.next();
			if (observer.equals(currentObserver)) {
				it.remove();
			}
		}
	}

	/**
	 * Removes all observers from list of observers.
	 */
	public void clearObservers() {
		if (observers != null) {
			this.observers.clear();
		}
	}

	/**
	 * Returns stored value.
	 * 
	 * @return stored value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Updates stored value if it's different from old one. If update occurs
	 * stored observers will be notified.
	 * 
	 * @param value
	 *            given value
	 */
	public void setValue(int value) {
		// Update only if given value is different from current
		if (value != this.value) {
			this.value = value;
			if (observers != null) {
				for (IntegerStorageObserver observer : new CopyOnWriteArrayList<>(
						observers)) {
					observer.valueChanged(this);
				}
			}
		}
	}
}
