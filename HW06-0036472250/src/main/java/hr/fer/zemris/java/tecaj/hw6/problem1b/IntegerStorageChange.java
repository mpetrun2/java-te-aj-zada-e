/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1b;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class IntegerStorageChange {

	/**
	 * Private read-only instance of {@link IntegerStorage}.
	 */
	private IntegerStorage iStorage;

	/**
	 * Value in {@link IntegerStorage} before change.
	 */
	private int oldValue;

	/**
	 * New value in {@link IntegerStorage}.
	 */
	private int newValue;

	/**
	 * Cr4eates new {@link IntegerStorageChange} instace from given arguments.
	 * 
	 * @param iStorage
	 *            given {@link IntegerStorage}
	 * @param oldValue
	 *            value in {@link IntegerStorage} before change
	 * @param newValue
	 *            new value in {@link IntegerStorage}
	 * @throws IllegalArgumentException
	 *             if {@link IntegerStorage} is null
	 */
	public IntegerStorageChange(IntegerStorage iStorage, int oldValue,
			int newValue) {
		if (iStorage == null) {
			throw new IllegalArgumentException(
					"Given IntegerStorage cannot be null.");
		}
		this.iStorage = iStorage;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	/**
	 * Returns new instance of {@link IntegerStorage} with value of original
	 * {@link IntegerStorage}.
	 * 
	 * @return {@link IntegerStorage}
	 */
	public IntegerStorage getiStorage() {
		return this.iStorage;
	}

	/**
	 * Returns value of {@link IntegerStorage} after change.
	 * 
	 * @return {@link Integer}
	 */
	public int getNewValue() {
		return newValue;
	}

	/**
	 * Returns value of {@link IntegerStorage} before change occurred.
	 * 
	 * @return {@link Integer}
	 */
	public int getOldValue() {
		return oldValue;
	}

}
