/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1b;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public interface IntegerStorageObserver {

	/**
	 * Called when something in {@link IntegerStorage} is changed.
	 * 
	 * @param istorage
	 *            changed object
	 */
	public void valueChanged(IntegerStorageChange istorage);

}
