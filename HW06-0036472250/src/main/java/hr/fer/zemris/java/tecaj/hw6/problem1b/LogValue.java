/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1b;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Path;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class that logs changed values into file.
 */
public class LogValue implements IntegerStorageObserver {

	/**
	 * File in which log will be written.
	 */
	private Path file;

	/**
	 * Constructs new {@link IntegerStorageObserver} with path to file in which
	 * log values will be written.
	 * 
	 * @param file
	 */
	public LogValue(Path file) {
		this.file = file;
		// Create new empty file
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file.toString()), "UTF-8"))) {
			writer.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Appends changed value to file log.txt in current directory.
	 * 
	 * @param istorage
	 *            given {@link IntegerStorage} with changed value
	 */
	@Override
	public void valueChanged(IntegerStorageChange istorage) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file.toString(), true), "UTF-8"))) {
			writer.write(istorage.getNewValue() + "\n");
			writer.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

}
