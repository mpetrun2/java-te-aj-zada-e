/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.problem1b;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Class for outputting square value of value in {@link IntegerStorage}.
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * When called prints square value of value stored in given object to
	 * standard output.
	 * 
	 * @param istorage
	 *            given subject with changed value
	 */
	@Override
	public void valueChanged(IntegerStorageChange istorage) {
		int value = istorage.getNewValue();
		System.out.println("Provided new value: " + value + ", square is: "
				+ value * value);
	}

}
