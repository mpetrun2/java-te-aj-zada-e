/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell;

import hr.fer.zemris.java.tecaj.hw6.shell.commands.CatShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.CharsetShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.CopyShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.ExitShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.HelpShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.HexdumpShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.LsShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.MkdirShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.SymbolShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.TreeShellCommand;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Marin Petrunić
 * @version 1.0
 * 
 *          MyShell is command line program simulating shell(text terminal).
 */
public class MyShell {

	/**
	 * Class with shell symbols.
	 */
	private ShellSymbols symbols;

	/**
	 * Default buffer size for reader.
	 */
	private int BUFFERSIZE = 512;

	/**
	 * List of acceptable shell commands.
	 */
	private Map<String, ShellCommand> commands;

	/**
	 * Program does not care for arguments.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 0) {
			System.out.println("Given arguments will be ignored");
		}
		MyShell shell = new MyShell();
		shell.start();
	}

	/**
	 * Only writes program description text.
	 */
	public MyShell() {
		System.out.println("Welcome to MyShell v1.0");
		symbols = new ShellSymbols();
		commands = new HashMap<>();
		commands.put("exit", new ExitShellCommand());
		commands.put("symbol", new SymbolShellCommand(symbols));
		commands.put("charsets", new CharsetShellCommand());
		commands.put("cat", new CatShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("tree", new TreeShellCommand());
		commands.put("copy", new CopyShellCommand());
		commands.put("mkdir", new MkdirShellCommand());
		commands.put("hexdump", new HexdumpShellCommand());

		// help must be last
		commands.put("help",
				new HelpShellCommand(new ArrayList<>(commands.keySet())));
	}

	/**
	 * Starting to take commands and their arguments and executing given
	 * commands.
	 */
	public void start() {
		Scanner reader = new Scanner(System.in, "UTF-8");
		String input = "";
		// Initialize and opens I/O streams for commands
		BufferedReader in = null;
		BufferedWriter out = null;
		try {
			in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"),
					BUFFERSIZE);
			out = new BufferedWriter(
					new OutputStreamWriter(System.out, "UTF-8"), BUFFERSIZE);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		ShellStatus status = ShellStatus.CONTINUE;
		// Loop reading shell commands
		do {
			System.out.print(symbols.getPROMTSYMBOL() + " ");
			input = reader.nextLine();
			input = input.trim();
			while (input.endsWith(symbols.getMORELINESYMBOL().toString())) {
				// removing multiline symbol
				input = input.substring(0, input.length() - 1);
				System.out.print(symbols.getMULTILINESYMBOL() + " ");
				input += reader.nextLine();
				input = input.trim();
			}
			// Extracting command name and arguments
			String[] arguments = input.split(" ");
			String commandName = arguments[0];
			if (arguments.length > 1) {
				arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
			} else {
				arguments = new String[0];
			}
			ShellCommand command;
			// Getting and executing command
			if ((command = commands.get(commandName)) != null) {
				try {
					status = command.executeCommand(in, out, arguments);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			} else {
				System.out.println("Unknown command name");
			}
		} while (status != ShellStatus.TERMINATE);
		// closing streams
		reader.close();
		try {
			in.close();
			out.close();
		} catch (IOException ignorable) {

		}
	}

}
