/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author Marin Petrunić
 * 
 *         Interface for shell commands.
 */
public interface ShellCommand {

	/**
	 * Executes command and returns {@link ShellStatus} CONTINUE if shell should
	 * continue getting commands or TERMINATE if shell should end his work.
	 * 
	 * @param in
	 *            wraped stdin
	 * @param out
	 *            wraped stdout
	 * @param arguments
	 *            required command arguments
	 * @return {@link ShellStatus} depending on command
	 * @throws IOException
	 */
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException;
}
