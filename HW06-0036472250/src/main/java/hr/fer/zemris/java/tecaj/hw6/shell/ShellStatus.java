/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell;

/**
 * @author Marin Petrunić
 * 
 *         Providing two shell states.
 */
public enum ShellStatus {
	CONTINUE, TERMINATE
}
