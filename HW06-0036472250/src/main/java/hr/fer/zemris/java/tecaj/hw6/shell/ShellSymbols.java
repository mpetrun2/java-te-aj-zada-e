/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell;

/**
 * @author Marin
 * 
 *         Class containing shell symbols.
 */
public class ShellSymbols {

	/**
	 * Variable containing prompt symbol. Default prompt symbol is ">".
	 */
	private Character PROMTSYMBOL = '>';

	/**
	 * Variable containing symbol for multiline. Default multiline symbol is "|"
	 */
	private Character MULTILINESYMBOL = '|';

	/**
	 * Variable containing symbol for moreline. Default morelin symbol is "\"
	 */
	private Character MORELINESYMBOL = '\\';

	/**
	 * @return the pROMTSYMBOL
	 */
	public Character getPROMTSYMBOL() {
		return PROMTSYMBOL;
	}

	/**
	 * @param pROMTSYMBOL
	 *            the pROMTSYMBOL to set
	 */
	public void setPROMTSYMBOL(Character pROMTSYMBOL) {
		PROMTSYMBOL = pROMTSYMBOL;
	}

	/**
	 * @return the mULTILINESYMBOL
	 */
	public Character getMULTILINESYMBOL() {
		return MULTILINESYMBOL;
	}

	/**
	 * @param mULTILINESYMBOL
	 *            the mULTILINESYMBOL to set
	 */
	public void setMULTILINESYMBOL(Character mULTILINESYMBOL) {
		MULTILINESYMBOL = mULTILINESYMBOL;
	}

	/**
	 * @return the mORELINESYMBOL
	 */
	public Character getMORELINESYMBOL() {
		return MORELINESYMBOL;
	}

	/**
	 * @param mORELINESYMBOL
	 *            the mORELINESYMBOL to set
	 */
	public void setMORELINESYMBOL(Character mORELINESYMBOL) {
		MORELINESYMBOL = mORELINESYMBOL;
	}

}
