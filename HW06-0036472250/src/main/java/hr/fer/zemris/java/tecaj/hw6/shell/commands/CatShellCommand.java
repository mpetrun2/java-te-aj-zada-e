/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         This command opens given file and writes its content to console.
 */
public class CatShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		Path file = null;
		Charset charset = null;
		if (arguments.length == 2) {
			file = Paths.get(arguments[0]);
			if (!Charset.availableCharsets().containsKey(arguments[1])) {
				throw new IllegalCharsetNameException(
						"Charset with that name is not available");
			}
			charset = Charset.forName(arguments[1]);
		} else if (arguments.length == 1) {
			file = Paths.get(arguments[0]);
			charset = Charset.defaultCharset();
		}
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(file.toFile()), charset))) {
			String lineString;
			while ((lineString = reader.readLine()) != null) {
				out.write(lineString + "\n");
			}
			out.write("\n");
			out.flush();
		} catch (IOException e) {
			out.write(e.getMessage() + "\n");
			out.flush();
		}
		return ShellStatus.CONTINUE;
	}

}
