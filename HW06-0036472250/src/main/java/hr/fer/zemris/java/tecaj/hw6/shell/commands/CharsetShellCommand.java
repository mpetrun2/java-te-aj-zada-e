/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         Outputs available platform charset names to given
 *         {@link BufferedWriter} one charset name per line.
 */
public class CharsetShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 0) {
			out.write("This command does not accept arguments.");
			out.write(" Given arguments will be ignored\n");
		}
		out.write("Available charsets: \n");
		for (String charset : Charset.availableCharsets().keySet()) {
			out.write(charset + "\n");
		}
		out.flush();
		return ShellStatus.CONTINUE;
	}

}
