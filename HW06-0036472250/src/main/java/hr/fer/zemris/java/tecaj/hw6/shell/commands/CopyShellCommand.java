/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         This command copies content from given source file to destination
 *         file. If destination is directory, source file will be copied in
 *         given directory with original name. If user tries to overwrite file,
 *         his permission will be asked.
 */
public class CopyShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 2) {
			throw new IllegalArgumentException(
					"This command expects two arguments.");
		}
		Path sourceFile = Paths.get(arguments[0]);
		if (Files.isDirectory(sourceFile)) {
			throw new IllegalArgumentException(
					"First argument must be path to file.");
		}
		Path destFile = Paths.get(arguments[1]);
		// if dest file path is directory orginal file name is appended
		if (Files.isDirectory(destFile)) {
			destFile = Paths.get(destFile.toString(), sourceFile.getFileName()
					.toString());
		}
		// ask for permission to overwrite
		if (Files.exists(destFile)) {
			out.write("Do you want to overwite " + destFile.getFileName()
					+ " [y/n]? [y] ");
			out.flush();
			if (in.readLine().equalsIgnoreCase("n")) {
				out.write("File overwrite permission denied.\n");
				out.flush();
				return ShellStatus.CONTINUE;
			}
		}
		copyFile(sourceFile, destFile, out);
		out.write("File copied!\n");
		out.flush();
		return ShellStatus.CONTINUE;
	}

	/**
	 * Copy content of source file to destination file.
	 * 
	 * @param sourceFile
	 * @param destinationFile
	 * @param out
	 * @throws IOException
	 */
	private void copyFile(Path sourceFile, Path destinationFile,
			BufferedWriter out) throws IOException {
		FileChannel input = null, output = null;
		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		try {
			fileInputStream = new FileInputStream(sourceFile.toFile());
			input = fileInputStream.getChannel();
			fileOutputStream = new FileOutputStream(destinationFile.toFile());
			output = fileOutputStream.getChannel();
			input.transferTo(0, sourceFile.toFile().length(), output);
		} catch (IOException e) {
			out.write(e.getMessage() + "\n");
			out.flush();
		} finally {
			input.close();
			output.close();
			fileInputStream.close();
			fileOutputStream.close();
		}
	}

}
