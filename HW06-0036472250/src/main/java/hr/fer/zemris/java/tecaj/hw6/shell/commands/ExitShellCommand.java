/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin
 * 
 *         This command terminates shell.
 */
public class ExitShellCommand implements ShellCommand {

	/**
	 * Terminates shell. Returns {@link ShellStatus} TERMINATE
	 * 
	 * @param in
	 *            wraped input reader
	 * @param out
	 *            wrpaed output writer
	 * @param arguments
	 *            no arguments needed
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) {
		return ShellStatus.TERMINATE;
	}

}
