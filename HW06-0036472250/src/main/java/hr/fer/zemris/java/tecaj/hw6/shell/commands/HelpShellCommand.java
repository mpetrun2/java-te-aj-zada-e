/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         Outputs list of available commands.
 */
public class HelpShellCommand implements ShellCommand {

	/**
	 * List of available commands.
	 */
	private List<String> commands;

	/**
	 * Constructs {@link HelpShellCommand} with list of available commands.
	 * 
	 * @param commands
	 */
	public HelpShellCommand(List<String> commands) {
		this.commands = commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		Collections.sort(commands);
		out.write("Available MyShell commands:\n");
		for (String command : commands) {
			out.write(command + "\n");
		}
		out.flush();
		return ShellStatus.CONTINUE;
	}

}
