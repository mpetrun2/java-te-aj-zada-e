/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         Command for hexdumping given file.
 * 
 */
public class HexdumpShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 1) {
			throw new IllegalArgumentException(
					"This command accepts only one argument.");
		}
		Path file = Paths.get(arguments[0]);
		if (!Files.isRegularFile(file)) {
			throw new IllegalArgumentException(
					"This command accepts only path to file.");
		}
		hexdump(file, out);
		out.flush();
		return ShellStatus.CONTINUE;
	}

	/**
	 * Prints to given {@link BufferedReader} hexdump of file.
	 * 
	 * @param file
	 *            to be hexdumped
	 * @param out
	 *            output stream
	 * @throws IOException
	 */
	private void hexdump(Path file, BufferedWriter out) throws IOException {
		try (BufferedReader input = new BufferedReader(new InputStreamReader(
				new FileInputStream(file.toFile()), Charset.forName("UTF-8")))) {
			out.write("Hexdump for file " + file.getFileName() + ":\n");
			int line = 0;
			boolean end = false;
			while (!end) {
				StringBuilder orginalText = new StringBuilder();
				out.write(String.format("%08X ", line * 16));
				for (int j = 0; j < 16; j++) {

					int value = input.read();
					if (value == -1 && j == 0) {
						end = true;
						break;
					}
					if (value == -1) {
						end = true;
						out.write("    ");
					} else if (value < 32 || value > 127) {
						out.write(String.format(" %02X ", value));
						orginalText.append(".");
					} else {
						orginalText.append((char) value);
						out.write(String.format(" %02X ", value));
					}
					if ((j + 1) % 8 == 0) {
						out.write("|");
					}
				}
				out.write(orginalText.toString());
				out.write("\n");
				line++;
			}
		} catch (IOException e) {
			out.write(e.getMessage() + "\n");
			out.flush();
		}
	}

}
