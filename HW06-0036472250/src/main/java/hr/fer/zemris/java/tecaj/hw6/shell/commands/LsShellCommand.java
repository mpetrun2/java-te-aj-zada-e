/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         This command outputs details about every file in given directory
 *         non-recursively.
 * 
 */
public class LsShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 1) {
			out.write("This command expects single argument");
		}
		Path directory = Paths.get(arguments[0]);
		if (!Files.isDirectory(directory)) {
			throw new IllegalArgumentException(
					"This command accepts onyl path to directory.");
		}
		List<Path> files = getFiles(directory, out);
		Map<Path, List<String>> fileAttributes = getFileAttributes(files);
		printFileAttributeTable(fileAttributes, out);
		return ShellStatus.CONTINUE;
	}

	/**
	 * Returns list of files in given directory.
	 * 
	 * @param directory
	 *            root directory
	 * @param out
	 *            for writing errors to stdout
	 * @return
	 * @throws IOException
	 */
	private List<Path> getFiles(Path directory, BufferedWriter out)
			throws IOException {
		List<Path> files = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files
				.newDirectoryStream(directory)) {
			for (Path path : directoryStream) {
				files.add(path);
			}
		} catch (IOException e) {
			out.write(e.getMessage());
		}
		return files;
	}

	/**
	 * Returns map with files as key and list of their attributes as value.
	 * 
	 * @param files
	 *            list of files
	 * @return map
	 * @throws IOException
	 */
	private Map<Path, List<String>> getFileAttributes(List<Path> files)
			throws IOException {
		Map<Path, List<String>> fileAttributes = new LinkedHashMap<>();
		for (Path file : files) {
			List<String> attributes = new LinkedList<>();
			attributes.add(getReadWriteAttribute(file));
			attributes.add(getFileSize(file));
			attributes.add(getFileCreationTime(file));
			attributes.add(file.getFileName().toString());
			fileAttributes.put(file, attributes);
		}
		return fileAttributes;
	}

	/**
	 * Returns string representation of file access details.
	 * 
	 * @param file
	 *            to be checked
	 * @return
	 */
	private String getReadWriteAttribute(Path file) {
		String attribute = new String();
		if (Files.isDirectory(file)) {
			attribute += "d";
		} else {
			attribute += "-";
		}
		if (Files.isReadable(file)) {
			attribute += "r";
		} else {
			attribute += "-";
		}
		if (Files.isWritable(file)) {
			attribute += "w";
		} else {
			attribute += "-";
		}
		if (Files.isExecutable(file)) {
			attribute += "x";
		} else {
			attribute += "-";
		}
		return attribute + " ";
	}

	/**
	 * Returns String with size of given file. Size value is right aligned on 10
	 * spaces.
	 * 
	 * @param file
	 *            given file
	 * @return
	 * @throws IOException
	 */
	private String getFileSize(Path file) throws IOException {
		String sizeAttribute = "";
		sizeAttribute += String.format(" %" + 10 + "s ", Files.size(file));
		return sizeAttribute;
	}

	/**
	 * Returns formated date/time of file creation.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private String getFileCreationTime(Path file) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat(" yyyy-MM-dd HH:mm:ss ");
		BasicFileAttributeView faView = Files.getFileAttributeView(file,
				BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
		BasicFileAttributes attributes = faView.readAttributes();
		FileTime fileTime = attributes.creationTime();
		String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
		return formattedDateTime;
	}

	/**
	 * Prints everything from given map of file attributes to given output
	 * stream.
	 * 
	 * @param fileAttributes
	 * @param out
	 * @throws IOException
	 */
	private void printFileAttributeTable(
			Map<Path, List<String>> fileAttributes, BufferedWriter out)
			throws IOException {
		for (Path file : fileAttributes.keySet()) {
			List<String> attributes = fileAttributes.get(file);
			for (String attribute : attributes) {
				out.write(attribute);
			}
			out.write("\n");
		}
		out.flush();
	}
}
