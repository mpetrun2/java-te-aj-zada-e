/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;

/**
 * @author Marin Petrunić
 * 
 *         THis command creates given directory structure.
 * 
 */
public class MkdirShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 1) {
			throw new IllegalArgumentException(
					"This command accepts only one argument.");
		}
		Path directoryStructure = Paths.get(arguments[0]);
		if (!Files.isDirectory(directoryStructure)) {
			throw new IllegalArgumentException(
					"Argument of this command must be directory structure.");
		}
		Files.createDirectories(directoryStructure);
		out.write("Directory structure created!\n");
		out.write("\n");
		return ShellStatus.CONTINUE;
	}
}
