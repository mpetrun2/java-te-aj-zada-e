/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellSymbols;

/**
 * @author Marin
 * 
 */
public class SymbolShellCommand implements ShellCommand {

	/**
	 * Instance of {@link ShellSymbols}.
	 */
	private ShellSymbols symbols;

	/**
	 * Constructs {@link SymbolShellCommand} with argument {@link ShellSymbols}
	 * to be used later.
	 * 
	 * @param symbols
	 */
	public SymbolShellCommand(ShellSymbols symbols) {
		this.symbols = symbols;
	}

	/**
	 * Depending on the number of arguments function will either output symbol
	 * for given symbol name or change to new symbol if new symbol is suplied as
	 * argument. Accepts 1 or 2 arguments.
	 * 
	 * @param in
	 *            input stream
	 * @param out
	 *            output stream
	 * @param arguments
	 * @throws IOException
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length == 1) {
			printTargetedSymbol(arguments[0], out);
		} else if (arguments.length == 2) {
			changeTargetedSymbol(arguments[0], arguments[1], out);
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * Prints given output to given writer with new line.
	 * 
	 * @param output
	 *            test to be printed
	 * @param out
	 *            output stream
	 * @throws IOException
	 */
	private void printOutput(String output, BufferedWriter out)
			throws IOException {
		out.write(output);
		out.newLine();
		out.flush();
	}

	/**
	 * Prints targeted symbol name.
	 * 
	 * @param symbolName
	 * @param out
	 *            output stream
	 * @throws IOException
	 */
	private void printTargetedSymbol(String symbolName, BufferedWriter out)
			throws IOException {
		switch (symbolName) {
		case "PROMPT": {
			printOutput("Symbol for PROMPT is '" + symbols.getPROMTSYMBOL()
					+ "'", out);
		}
			break;
		case "MORELINES": {
			printOutput(
					"Symbol for MORELINES is '" + symbols.getMORELINESYMBOL()
							+ "'", out);
		}
			break;
		case "MULTILINE": {
			printOutput(
					"Symbol for MULTILINE is '" + symbols.getMULTILINESYMBOL()
							+ "'", out);
		}
			break;
		default: {
			throw new IllegalArgumentException(
					"Symbol with given name does not exist.");
		}
		}
	}

	/**
	 * Changes symbol for given symbol name to new symbol and prints appropriate
	 * message. If not character provided as new symbol, new symbol will be
	 * first char in argument
	 * 
	 * @param symbolName
	 * @param newSymbol
	 *            symbol to be set
	 * @param out
	 *            output stream
	 * @throws IOException
	 */
	private void changeTargetedSymbol(String symbolName, String newSymbol,
			BufferedWriter out) throws IOException {
		switch (symbolName) {
		case "PROMPT": {
			Character oldSymbol = symbols.getPROMTSYMBOL();
			symbols.setPROMTSYMBOL(newSymbol.charAt(0));
			printOutput("Symbol for PROMPT changed from '" + oldSymbol
					+ "' to '" + newSymbol.charAt(0) + "'", out);
		}
			break;
		case "MULTILINE": {
			Character oldSymbol = symbols.getMULTILINESYMBOL();
			symbols.setMULTILINESYMBOL(newSymbol.charAt(0));
			printOutput("Symbol for MULTILINE changed from '" + oldSymbol
					+ "' to '" + newSymbol.charAt(0) + "'", out);
		}
			break;
		case "MORELINES": {
			Character oldSymbol = symbols.getMORELINESYMBOL();
			symbols.setMORELINESYMBOL(newSymbol.charAt(0));
			printOutput("Symbol for MORELINE changed from '" + oldSymbol
					+ "' to '" + newSymbol.charAt(0) + "'", out);
		}
			break;
		default: {
			throw new IllegalArgumentException(
					"Symbol with given name does not exist.");
		}
		}
	}

}
