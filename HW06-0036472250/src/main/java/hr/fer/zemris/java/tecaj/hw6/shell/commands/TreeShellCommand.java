/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw6.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw6.shell.commands.fileVisiters.TreeOutputVisitor;

/**
 * @author Marin
 * 
 *         Recursively visits every file and outputs tree structure of
 *         directory.
 */
public class TreeShellCommand implements ShellCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.tecaj.hw6.shell.ShellCommand#executeCommand(java.io
	 * .BufferedReader, java.io.BufferedWriter, java.lang.String[])
	 */
	@Override
	public ShellStatus executeCommand(BufferedReader in, BufferedWriter out,
			String[] arguments) throws IOException {
		if (arguments.length != 1) {
			throw new IllegalArgumentException(
					"This command accepts only one argument.");
		}
		Path directory = Paths.get(arguments[0]);
		if (!Files.isDirectory(directory)) {
			throw new IllegalArgumentException(
					"This command accepts only path to directory.");
		}
		TreeOutputVisitor treeVisit = new TreeOutputVisitor();
		Files.walkFileTree(directory, treeVisit);
		out.write(treeVisit.getDirectoryStructure());
		out.flush();
		return ShellStatus.CONTINUE;
	}
}
