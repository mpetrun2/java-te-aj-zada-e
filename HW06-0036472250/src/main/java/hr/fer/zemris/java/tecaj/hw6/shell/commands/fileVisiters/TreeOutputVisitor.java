/**
 * 
 */
package hr.fer.zemris.java.tecaj.hw6.shell.commands.fileVisiters;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Marin Petrunić
 * 
 *         Iteration through files and composing tree structure.
 */
public class TreeOutputVisitor implements FileVisitor<Path> {

	private int padding = 0;

	private String directoryStructure = "";

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc)
			throws IOException {
		padding--;
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
			throws IOException {
		if (padding == 0) {
			directoryStructure += dir.toAbsolutePath().toString() + "\n";
		} else {
			directoryStructure += String.format("%" + padding + "s", " ")
					+ dir.getFileName() + "\n";
		}
		padding++;
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			throws IOException {
		directoryStructure += String.format("%" + padding + "s", " ")
				+ file.getFileName() + "\n";
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc)
			throws IOException {
		return FileVisitResult.CONTINUE;
	}

	/**
	 * @return the directoryStructure
	 */
	public String getDirectoryStructure() {
		return directoryStructure;
	}

}
