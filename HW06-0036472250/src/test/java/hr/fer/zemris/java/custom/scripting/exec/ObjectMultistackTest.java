/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class ObjectMultistackTest {

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#ObjectMultistack()}
	 * .
	 */
	@Test
	public void testObjectMultistack() {
		@SuppressWarnings("unused")
		ObjectMultistack test = new ObjectMultistack();
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#push(java.lang.String, hr.fer.zemris.java.custom.scripting.exec.ValueWrapper)}
	 * .
	 */
	@Test
	public void testPush() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.push("test1", new ValueWrapper(6));
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#pop(java.lang.String)}
	 * .
	 */
	@Test
	public void testPop() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.push("test1", new ValueWrapper(6));
		assertEquals("Expecting 6.", 6, test.pop("test1").getValue());
		assertEquals("Expecting 5.", 5, test.pop("test1").getValue());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#pop(java.lang.String)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testPopException() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.push("test1", new ValueWrapper(6));
		test.pop("test2");
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#peek(java.lang.String)}
	 * .
	 */
	@Test
	public void testPeek() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.push("test1", new ValueWrapper(6));
		assertEquals("Expecting 6.", 6, test.peek("test1").getValue());
		assertEquals("Expecting 6.", 6, test.peek("test1").getValue());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#peek(java.lang.String)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testPeekException() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.push("test1", new ValueWrapper(6));
		test.peek("test2");
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#isEmpty(java.lang.String)}
	 * .
	 */
	@Test
	public void testIsEmpty() {
		ObjectMultistack test = new ObjectMultistack();
		ObjectMultistack test2 = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test2.push("test2", new ValueWrapper(5));
		test2.pop("test2");
		assertTrue("Expected true.", test2.isEmpty("test2"));
		assertFalse("Expected false.", test.isEmpty("test1"));
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack#isEmpty(java.lang.String)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testIsEmptyException() {
		ObjectMultistack test = new ObjectMultistack();
		test.push("test1", new ValueWrapper(5));
		test.isEmpty("test2");
	}

}
