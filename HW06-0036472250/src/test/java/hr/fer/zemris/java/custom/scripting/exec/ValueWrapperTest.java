/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class ValueWrapperTest {

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#ValueWrapper(java.lang.Object)}
	 * .
	 */
	@Test
	public void testValueWrapper() {
		@SuppressWarnings("unused")
		ValueWrapper test = new ValueWrapper(5);
		test = new ValueWrapper(5.1);
		test = new ValueWrapper("2E3");
		test = new ValueWrapper("25");
		test = new ValueWrapper("5.1");
		test = new ValueWrapper(null);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#ValueWrapper(java.lang.Object)}
	 * .
	 */
	@Test(expected = RuntimeException.class)
	public void testValueWrapperException() {
		@SuppressWarnings("unused")
		ValueWrapper test = new ValueWrapper('5');
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#getValue()}.
	 */
	@Test
	public void testGetValue() {
		ValueWrapper test = new ValueWrapper(5);
		assertEquals("Expected to get Integer 5.", 5, test.getValue());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#setValue(java.lang.Object)}
	 * .
	 */
	@Test
	public void testSetValue() {
		ValueWrapper test = new ValueWrapper(5);
		test.setValue("5.1");
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#increment(java.lang.Object)}
	 * .
	 */
	@Test
	public void testIncrement() {
		ValueWrapper test = new ValueWrapper(5);
		test.increment(5);
		test.increment(1.1);
		assertEquals("Expecting to get 11.1", 11.1, test.getValue());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#decrement(java.lang.Object)}
	 * .
	 */
	@Test
	public void testDecrement() {
		ValueWrapper test = new ValueWrapper(10);
		test.decrement(5);
		test.decrement(1.1);
		assertEquals("Expecting to get 3.9", 3.9, test.getValue());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#multiply(java.lang.Object)}
	 * .
	 */
	@Test
	public void testMultiply() {
		ValueWrapper test = new ValueWrapper(5);
		test.multiply(5);
		test.multiply(1.1);
		assertEquals("Expecting to get 27.5", 27.5, (double) test.getValue(),
				1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#divide(java.lang.Object)}
	 * .
	 */
	@Test
	public void testDivide() {
		ValueWrapper test = new ValueWrapper(10);
		test.divide(5);
		test.divide(2.5);
		assertEquals("Expecting to get 0.8", 0.8, (double) test.getValue(),
				1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#divide(java.lang.Object)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testDivideException() {
		ValueWrapper test = new ValueWrapper(10);
		test.divide(0);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#divide(java.lang.Object)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testDivideException2() {
		ValueWrapper test = new ValueWrapper(10);
		test.divide(0.0);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.custom.scripting.exec.ValueWrapper#numCompare(java.lang.Object)}
	 * .
	 */
	@Test
	public void testNumCompare() {
		ValueWrapper test = new ValueWrapper(10);
		assertEquals("Expected 1.", 1, test.numCompare(8));
		assertEquals("Expected 0.", 0, test.numCompare(10));
		assertEquals("Expected -1.", -1, test.numCompare(11));
	}

}
