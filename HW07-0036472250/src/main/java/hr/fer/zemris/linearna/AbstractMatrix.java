/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         {@link IMatrix} abstraction holding common methods for each matrix.
 * 
 */
public abstract class AbstractMatrix implements IMatrix {

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#getRowsCount()
	 */
	@Override
	public abstract int getRowsCount();

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#getColsCount()
	 */
	@Override
	public abstract int getColsCount();

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#get(int, int)
	 */
	@Override
	public abstract double get(int row, int col);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#set(int, int, double)
	 */
	@Override
	public abstract IMatrix set(int row, int col, double value);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#copy()
	 */
	@Override
	public abstract IMatrix copy();

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#newInstance(int, int)
	 */
	@Override
	public abstract IMatrix newInstance(int rows, int cols);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#nTranspose(boolean)
	 */
	@Override
	public IMatrix nTranspose(boolean liveView) {
		if (liveView) {
			return new MatrixTransposeView(this);
		}
		return new MatrixTransposeView(this.copy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#add(hr.fer.zemris.linearna.IMatrix)
	 */
	@Override
	public IMatrix add(IMatrix other) throws IncompatibleOperandException {
		if (this.getColsCount() != other.getColsCount()
				|| this.getRowsCount() != other.getRowsCount()) {
			throw new IncompatibleOperandException(
					"Matrix have diffrent dimension.");
		}
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				this.set(i, j, this.get(i, j) + other.get(i, j));
			}
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#nAdd(hr.fer.zemris.linearna.IMatrix)
	 */
	@Override
	public IMatrix nAdd(IMatrix other) {
		return this.copy().add(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#sub(hr.fer.zemris.linearna.IMatrix)
	 */
	@Override
	public IMatrix sub(IMatrix other) throws IncompatibleOperandException {
		if (this.getColsCount() != other.getColsCount()
				|| this.getRowsCount() != other.getRowsCount()) {
			throw new IncompatibleOperandException(
					"Matrix have diffrent dimension.");
		}
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				this.set(i, j, this.get(i, j) - other.get(i, j));
			}
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#nSub(hr.fer.zemris.linearna.IMatrix)
	 */
	@Override
	public IMatrix nSub(IMatrix other) {
		return this.copy().sub(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.linearna.IMatrix#nMultiply(hr.fer.zemris.linearna.IMatrix)
	 */
	@Override
	public IMatrix nMultiply(IMatrix other) throws IncompatibleOperandException {
		if (this.getColsCount() != other.getRowsCount()) {
			throw new IncompatibleOperandException(
					"Matrix have invalid dimensions.");
		}
		IMatrix newMatrix = this.newInstance(this.getRowsCount(),
				other.getColsCount());
		for (int i = 0, end1 = this.getRowsCount(); i < end1; i++) {
			for (int j = 0, end2 = other.getColsCount(); j < end2; j++) {
				double sum = 0;
				for (int k = 0, end3 = other.getRowsCount(); k < end3; k++) {
					sum = sum + this.get(i, k) * other.get(k, j);
				}
				newMatrix.set(i, j, sum);
			}
		}
		return newMatrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#determinant()
	 */
	@Override
	public double determinant() throws IncompatibleOperandException {
		if (this.getColsCount() != this.getRowsCount()) {
			throw new IncompatibleOperandException("Matrix is not square");
		}
		return calculateDeterminant(this);
	}

	private double calculateDeterminant(IMatrix matrix) {
		if (matrix.getRowsCount() == 1 && matrix.getColsCount() == 1) {
			return matrix.get(0, 0);
		}
		double sum = 0;
		for (int i = 0, end = matrix.getColsCount(); i < end; i++) {
			sum += Math.pow(-1, i) * matrix.get(0, i)
					* calculateDeterminant(matrix.subMatrix(0, i, false));
		}
		return sum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#subMatrix(int, int, boolean)
	 */
	@Override
	public IMatrix subMatrix(int row, int col, boolean liveView) {
		if (liveView) {
			return new MatrixSubMatrixView(this, row, col);
		}
		return new MatrixSubMatrixView(this.copy(), row, col);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#nInvert()
	 */
	@Override
	public IMatrix nInvert() {
		double determinant = this.determinant();
		if (Double.compare(determinant, 0.0) == 0) {
			throw new UnsupportedOperationException("Matrix isn't invertible.");
		}
		IMatrix adjunct = this.newInstance(this.getRowsCount(),
				this.getColsCount());
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				adjunct.set(i, j, Math.pow(-1, i + j + 2)
						* calculateDeterminant(this.subMatrix(i, j, false)));
			}
		}
		return adjunct.nTranspose(false).scalarMultiply(1.0 / determinant);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#toArray()
	 */
	@Override
	public double[][] toArray() {
		double[][] matrixArray = new double[this.getRowsCount()][this
				.getColsCount()];
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				matrixArray[i][j] = this.get(i, j);
			}
		}
		return matrixArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#toVector(boolean)
	 */
	@Override
	public IVector toVector(boolean liveView) {
		if (liveView) {
			return new VectorMatrixView(this.copy());
		}
		return new VectorMatrixView(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#nScalarMultiply(double)
	 */
	@Override
	public IMatrix nScalarMultiply(double value) {
		return this.copy().scalarMultiply(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#scalarMultiply(double)
	 */
	@Override
	public IMatrix scalarMultiply(double value) {
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				this.set(i, j, this.get(i, j) * value);
			}
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IMatrix#makeIdentity()
	 */
	@Override
	public IMatrix makeIdentity() {
		if (this.getRowsCount() != this.getColsCount()) {
			throw new IncompatibleOperandException("Matrix must be square.");
		}
		for (int i = 0, endrows = this.getRowsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getColsCount(); j < endcols; j++) {
				this.set(i, j, 0);
				if (i == j) {
					this.set(i, i, 1);
				}
			}
		}
		return this;
	}

	/**
	 * Returns string representation of {@link IMatrix} with given precision.
	 * 
	 * @param precision
	 * @return
	 */
	public String toString(int precision) {
		StringBuilder sb = new StringBuilder("");
		for (int i = 0, end = this.getRowsCount(); i < end; i++) {
			for (int j = 0, endCols = this.getColsCount(); j < endCols; j++) {
				sb.append(String.format("%." + precision + "f", this.get(i, j))
						+ " ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * Returns string representation of {@link IMatrix} with precision set to 3.
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return toString(3);
	}
}
