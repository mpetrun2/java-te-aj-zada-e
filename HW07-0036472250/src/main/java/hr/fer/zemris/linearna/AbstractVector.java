/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         {@link IVector} abstraction holding common methods for each vector.
 * 
 */
public abstract class AbstractVector implements IVector {

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#get(int)
	 */
	public abstract double get(int index);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#set(int, double)
	 */
	public abstract IVector set(int index, double value);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#getDimension()
	 */
	public abstract int getDimension();

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#copy()
	 */
	public abstract IVector copy();

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#newInstance(int)
	 */
	public abstract IVector newInstance(int dimension);

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#copyPart(int)
	 */
	@Override
	public IVector copyPart(int n) {
		IVector newVector = this.newInstance(n);
		for (int i = 0; i < n; i++) {
			try {
				newVector.set(i, this.get(i));
			} catch (IndexOutOfBoundsException ignorable) {
				newVector.set(i, 0);
			}
		}
		return newVector;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#add(hr.fer.zemris.linearna.IVector)
	 */
	@Override
	public IVector add(IVector other) throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException(
					"Vectors have different dimensions.");
		}
		for (int i = 0, d = other.getDimension(); i < d; i++) {
			this.set(i, this.get(i) + other.get(i));
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#nAdd(hr.fer.zemris.linearna.IVector)
	 */
	@Override
	public IVector nAdd(IVector other) throws IncompatibleOperandException {
		return this.copy().add(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#sub(hr.fer.zemris.linearna.IVector)
	 */
	@Override
	public IVector sub(IVector other) throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException(
					"Vectors have different dimensions.");
		}
		for (int i = 0, d = other.getDimension(); i < d; i++) {
			this.set(i, this.get(i) - other.get(i));
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#nSub(hr.fer.zemris.linearna.IVector)
	 */
	@Override
	public IVector nSub(IVector other) throws IncompatibleOperandException {
		return this.copy().sub(other);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#scalarMultiply(double)
	 */
	@Override
	public IVector scalarMultiply(double byValue) {
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			this.set(i, this.get(i) * byValue);
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#nScalarMultiply(double)
	 */
	@Override
	public IVector nScalarMultiply(double byValue) {
		return this.copy().scalarMultiply(byValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#norm()
	 */
	@Override
	public double norm() {
		double sum = 0;
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			sum += Math.pow(this.get(i), 2);
		}
		return Math.sqrt(sum);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#normalize()
	 */
	@Override
	public IVector normalize() throws IncompatibleOperandException {
		double norm = this.norm();
		if (Double.compare(norm, 0.0) == 0) {
			throw new IncompatibleOperandException(
					"Cannot normalize null vector");
		}
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			this.set(i, this.get(i) / norm);
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#nNormalize()
	 */
	@Override
	public IVector nNormalize() {
		return this.copy().normalize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.linearna.IVector#cosine(hr.fer.zemris.linearna.IVector)
	 */
	@Override
	public double cosine(IVector other) throws IncompatibleOperandException {
		double thisNorm = this.norm();
		double otherNorm = other.norm();
		if (Double.compare(thisNorm, 0.0) == 0
				|| Double.compare(otherNorm, 0.0) == 0) {
			throw new IncompatibleOperandException(
					"Vectors can't be null-vectors.");
		}
		return this.scalarProduct(other) / (thisNorm * otherNorm);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.linearna.IVector#scalarProduct(hr.fer.zemris.linearna.IVector
	 * )
	 */
	@Override
	public double scalarProduct(IVector other)
			throws IncompatibleOperandException {
		double scalarProduct = 0;
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException(
					"Vectors have different dimensions.");
		}
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			try {
				scalarProduct += this.get(i) * other.get(i);
			} catch (IndexOutOfBoundsException ignorable) {

			}
		}
		return scalarProduct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.linearna.IVector#nVectorProduct(hr.fer.zemris.linearna.
	 * IVector)
	 */
	@Override
	public IVector nVectorProduct(IVector other)
			throws IncompatibleOperandException {
		if (this.getDimension() != 3 || other.getDimension() != 3) {
			throw new IncompatibleOperandException(
					"Vectos dimension must be 3.");
		}
		IVector newVector = this.copy();
		newVector.set(0,
				this.get(1) * other.get(2) - this.get(2) * other.get(1));
		newVector.set(1,
				this.get(2) * other.get(0) - this.get(0) * other.get(2));
		newVector.set(2,
				this.get(0) * other.get(1) - this.get(1) * other.get(0));
		return newVector;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#nFromHomogeneus()
	 */
	@Override
	public IVector nFromHomogeneus() throws IncompatibleOperandException {
		if (this.getDimension() <= 1) {
			throw new IncompatibleOperandException(
					"Vector dimension must be greater than 1.");
		}
		double nDimension = this.get(this.getDimension() - 1);
		if (Double.compare(nDimension, 0.0) == 0) {
			throw new IncompatibleOperandException(
					"N-th dimension must not be null");
		}
		IVector newVector = this.copyPart(this.getDimension() - 1);
		newVector.scalarMultiply(1.0 / nDimension);
		return newVector;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#toRowMatrix(boolean)
	 */
	@Override
	public IMatrix toRowMatrix(boolean liveView) {
		if (liveView) {
			return new MatrixVectorView(this, true);
		}
		return new MatrixVectorView(this.copy(), true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#toColumnMatrix(boolean)
	 */
	@Override
	public IMatrix toColumnMatrix(boolean liveView) {
		if (liveView) {
			return new MatrixVectorView(this, false);
		}
		return new MatrixVectorView(this.copy(), false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.IVector#toArray()
	 */
	@Override
	public double[] toArray() {
		double[] vectorArray = new double[this.getDimension()];
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			vectorArray[i] = this.get(i);
		}
		return vectorArray;
	}

	/**
	 * Returns {@link String} representation of Vector with vector elements
	 * formated to given precision.
	 * 
	 * @param precision
	 * @return
	 */
	public String toString(int precision) {
		StringBuilder sb = new StringBuilder("");
		for (int i = 0, end = this.getDimension(); i < end; i++) {
			sb.append(String.format("%." + precision + "f", this.get(i)) + " ");
		}
		return sb.toString();
	}

	/**
	 * Returns {@link String} representation of this vector with precision set
	 * to 3.
	 * 
	 * @return string representation
	 */
	public String toString() {
		return toString(3);
	}

}
