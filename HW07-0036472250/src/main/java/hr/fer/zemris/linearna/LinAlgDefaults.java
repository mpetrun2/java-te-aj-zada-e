/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Factory class for producing instances of {@link IMatrix} and
 *         {@link IVector}.
 */
public class LinAlgDefaults {

	/**
	 * Returns new default instance of {@link IMatrix}
	 * 
	 * @param rows
	 *            number of rows
	 * @param cols
	 *            number of cols
	 * @return
	 */
	public static IMatrix defaulIMatrix(int rows, int cols) {
		return new Matrix(rows, cols);
	}

	/**
	 * Returns new default instance of {@link IVector}
	 * 
	 * @param dimension
	 *            vector dimension
	 * @return
	 */
	public static IVector defaultVector(int dimension) {
		return new Vector(new double[dimension]);
	}

}
