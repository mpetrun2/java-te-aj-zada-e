/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents matrix which stores elements as double array.
 */
public class Matrix extends AbstractMatrix {

	/**
	 * List of matrix elements as 2D {@link Double} array.
	 */
	protected double[][] elements;

	/**
	 * Number of matrix rows.
	 */
	protected int rows;

	/**
	 * Number of matrix columns.
	 */
	protected int cols;

	/**
	 * Creates nul-matrix with given number of rows and cols.
	 * 
	 * @param rows
	 * @param cols
	 * @throws IllegalArgumentException
	 *             if invalid number of rows or columns given
	 */
	public Matrix(int rows, int cols) {
		if (rows <= 0 || cols <= 0) {
			throw new IllegalArgumentException(
					"Dimension of matrix must be greater than zero.");
		}
		elements = new double[rows][cols];
		this.rows = rows;
		this.cols = cols;
	}

	/**
	 * Creates new {@link Matrix} with given number of rows and columns. If
	 * argument reusable set to true given array of elements will be internally
	 * used else new array of elements will be allocated.
	 * 
	 * @param rows
	 *            number fo rows
	 * @param cols
	 *            number of cols
	 * @param elements
	 *            elements array
	 * @param reusable
	 *            if true class will use given elements array
	 * @throws IllegalArgumentException
	 *             if invalid arguments given.
	 */
	public Matrix(int rows, int cols, double[][] elements, boolean reusable) {
		if (reusable) {
			if (elements.length != rows) {
				throw new IllegalArgumentException(
						"Number of rows of given elements does not match given row count.");
			}
			for (int i = 0; i < rows; i++) {
				if (elements[i].length != cols) {
					throw new IllegalArgumentException(
							"Number of columns of given elements does not match given columncount.");
				}
			}
			this.elements = elements;
		} else {
			this.elements = new double[rows][cols];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					try {
						this.elements[i][j] = elements[i][j];
					} catch (Exception ignorable) {
						this.elements[i][j] = 0;
					}
				}
			}
		}
		this.rows = rows;
		this.cols = cols;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getRowsCount()
	 */
	@Override
	public int getRowsCount() {
		return rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getColsCount()
	 */
	@Override
	public int getColsCount() {
		return cols;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#get(int, int)
	 */
	@Override
	public double get(int row, int col) {
		if (row < 0 || row > rows || col < 0 || col > cols) {
			throw new IllegalArgumentException(
					"This matrix does not have such indexes.");
		}
		return elements[row][col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#set(int, int, double)
	 */
	@Override
	public IMatrix set(int row, int col, double value) {
		if (row < 0 || row > rows || col < 0 || col > cols) {
			throw new IllegalArgumentException(
					"This matrix does not have such indexes.");
		}
		elements[row][col] = value;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#copy()
	 */
	@Override
	public IMatrix copy() {
		return new Matrix(rows, cols, elements, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#newInstance(int, int)
	 */
	@Override
	public IMatrix newInstance(int rows, int cols) {
		return new Matrix(rows, cols);
	}

	/**
	 * Parses given string as matrix. Matrix rows are divided with '|' and
	 * columns are separated by " ".
	 * 
	 * @param matrix
	 * @return
	 */
	public static IMatrix parseSimple(String matrix) {
		String[] matrixRows = matrix.split("\\|");
		double[][] elements = new double[matrixRows.length][];
		int i = 0;
		int maxCols = 0;
		for (String matrixRow : matrixRows) {
			matrixRow = matrixRow.trim();
			String[] matrixCols = matrixRow.split("\\s");
			elements[i] = new double[matrixCols.length];
			if (matrixCols.length > maxCols) {
				maxCols = matrixCols.length;
			}
			for (int j = 0, end = matrixCols.length; j < end; j++) {
				elements[i][j] = Double.parseDouble(matrixCols[j]);
			}
			i++;
		}
		return new Matrix(matrixRows.length, maxCols, elements, true);
	}

}
