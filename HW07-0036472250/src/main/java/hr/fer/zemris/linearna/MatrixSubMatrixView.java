/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Represents View on given matrix with removed(hidden) rows with given
 *         indexes.
 */
public class MatrixSubMatrixView extends AbstractMatrix {

	/**
	 * Indexes of rows in this view.
	 */
	private int[] rowIndexes;

	/**
	 * Indexes of columns in this view.
	 */
	private int[] colIndexes;

	/**
	 * Reference to original {@link IMatrix}.
	 */
	private IMatrix orginal;

	/**
	 * Constructs new {@link MatrixSubMatrixView} from given {@link IMatrix}
	 * without row and column with given index.
	 * 
	 * @param orginalMatrix
	 *            original {@link IMatrix}
	 * @param deletedRowIndex
	 * @param deletedColumnIndex
	 */
	public MatrixSubMatrixView(IMatrix orginalMatrix, int deletedRowIndex,
			int deletedColumnIndex) {
		this(orginalMatrix, getAcceptedIndexes(deletedRowIndex,
				orginalMatrix.getRowsCount()), getAcceptedIndexes(
				deletedColumnIndex, orginalMatrix.getColsCount()));
	}

	/**
	 * Creates new {@link MatrixSubMatrixView} containing given row and column
	 * indexes.
	 * 
	 * @param orginalMatrix
	 *            original matrix
	 * @param rowIndexes
	 *            contained row indexes
	 * @param colIndexes
	 *            contained column indexes
	 */
	private MatrixSubMatrixView(IMatrix orginalMatrix, int[] rowIndexes,
			int[] colIndexes) {
		this.rowIndexes = rowIndexes;
		this.colIndexes = colIndexes;
		this.orginal = orginalMatrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getRowsCount()
	 */
	@Override
	public int getRowsCount() {
		return rowIndexes.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getColsCount()
	 */
	@Override
	public int getColsCount() {
		return colIndexes.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#get(int, int)
	 */
	@Override
	public double get(int row, int col) {
		return orginal.get(rowIndexes[row], colIndexes[col]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#set(int, int, double)
	 */
	@Override
	public IMatrix set(int row, int col, double value) {
		orginal.set(rowIndexes[row], colIndexes[col], value);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#copy()
	 */
	@Override
	public IMatrix copy() {
		return orginal.copy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#newInstance(int, int)
	 */
	@Override
	public IMatrix newInstance(int rows, int cols) {
		return orginal.newInstance(rows, cols);
	}

	/**
	 * Utility method for getting array of indexes without given index.
	 * 
	 * @param unAcceptedIndex
	 * @param totalIndexCount
	 * @return
	 */
	private static int[] getAcceptedIndexes(int unAcceptedIndex,
			int totalIndexCount) {
		if (unAcceptedIndex < 0 || unAcceptedIndex >= totalIndexCount) {
			throw new IllegalArgumentException(
					"Given matrix does not have row with given index.");
		}
		int[] indexes = new int[totalIndexCount - 1];
		for (int i = 0, j = 0, endRows = totalIndexCount - 1; i < endRows; i++, j++) {
			if (j == unAcceptedIndex) {
				i--;
				continue;
			}
			indexes[i] = j;
		}
		return indexes;
	}

	/**
	 * Returns new {@link MatrixSubMatrixView} without given row and column. If
	 * liveView set to true new submatrix will hold reference to this.
	 * 
	 * @param row
	 *            row to be removed
	 * @param col
	 *            to be removed
	 * @param liveView
	 * @return new {@link IMatrix} as submatrix of this.
	 */
	@Override
	public IMatrix subMatrix(int row, int col, boolean liveView) {
		return new MatrixSubMatrixView(this, row, col);
	}
}
