/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Provides live view into transposed given matrix.
 */
public class MatrixTransposeView extends AbstractMatrix {

	/**
	 * Reference to original matrix on which is giving live view.
	 */
	private IMatrix matrix;

	/**
	 * Constructs new {@link MatrixTransposeView} and accepts original
	 * {@link IMatrix} on which it provides live view.
	 * 
	 * @param orginalMatrix
	 */
	public MatrixTransposeView(IMatrix orginalMatrix) {
		matrix = orginalMatrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getRowsCount()
	 */
	@Override
	public int getRowsCount() {
		return matrix.getColsCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getColsCount()
	 */
	@Override
	public int getColsCount() {
		return matrix.getRowsCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#get(int, int)
	 */
	@Override
	public double get(int row, int col) {
		return matrix.get(col, row);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#set(int, int, double)
	 */
	@Override
	public IMatrix set(int row, int col, double value) {
		matrix.set(col, row, value);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#copy()
	 */
	@Override
	public IMatrix copy() {
		return matrix.copy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#newInstance(int, int)
	 */
	@Override
	public IMatrix newInstance(int rows, int cols) {
		return matrix.newInstance(rows, cols);
	}

	@Override
	public double[][] toArray() {
		double[][] matrixArray = new double[this.getColsCount()][this
				.getRowsCount()];
		for (int i = 0, endrows = this.getColsCount(); i < endrows; i++) {
			for (int j = 0, endcols = this.getRowsCount(); j < endcols; j++) {
				matrixArray[j][i] = this.get(i, j);
			}
		}
		return matrixArray;
	}

}
