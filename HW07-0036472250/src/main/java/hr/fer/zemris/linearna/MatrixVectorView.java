/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Provides matrix from given vector. Matrix can show given
 *         {@link IVector} as row or column.
 * 
 */
public class MatrixVectorView extends AbstractMatrix {

	/**
	 * Reference to given {@link IVector}.
	 */
	private IVector vector;

	/**
	 * If true given vector will be masked into one row matrix.
	 */
	private boolean asRowMatrix;

	/**
	 * Constructs {@link MatrixVectorView} from given {@link IVector}. If
	 * asRowMatrix set to true, given vector will be masked as one row matrix.
	 * 
	 * @param vector
	 * @param asRowMatrix
	 * @throws IllegalArgumentException
	 *             if null reference sent as {@link IVector}
	 */
	public MatrixVectorView(IVector vector, boolean asRowMatrix) {
		if (vector == null) {
			throw new IllegalArgumentException(
					"Null references are not accepted.");
		}
		this.vector = vector;
		this.asRowMatrix = asRowMatrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getRowsCount()
	 */
	@Override
	public int getRowsCount() {
		if (asRowMatrix) {
			return 1;
		}
		return vector.getDimension();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#getColsCount()
	 */
	@Override
	public int getColsCount() {
		if (asRowMatrix) {
			return vector.getDimension();
		}
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#get(int, int)
	 */
	@Override
	public double get(int row, int col) {
		if (row < 0 || row >= this.getRowsCount() || col < 0
				|| col >= this.getColsCount()) {
			throw new IllegalArgumentException(
					"This matrix doe not contain given indexes.");
		}
		if (asRowMatrix) {
			return vector.get(col);
		}
		return vector.get(row);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#set(int, int, double)
	 */
	@Override
	public IMatrix set(int row, int col, double value) {
		if (row < 0 || row >= this.getRowsCount() || col < 0
				|| col >= this.getColsCount()) {
			throw new IllegalArgumentException(
					"This matrix doe not contain given indexes.");
		}
		if (asRowMatrix) {
			vector.set(col, value);
		} else {
			vector.set(row, value);
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#copy()
	 */
	@Override
	public IMatrix copy() {
		return new Matrix(this.getRowsCount(), this.getColsCount(),
				this.toArray(), false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractMatrix#newInstance(int, int)
	 */
	@Override
	public IMatrix newInstance(int rows, int cols) {
		return LinAlgDefaults.defaulIMatrix(rows, cols);
	}

}
