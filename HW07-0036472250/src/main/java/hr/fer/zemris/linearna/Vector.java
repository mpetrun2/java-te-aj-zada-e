/**
 * 
 */
package hr.fer.zemris.linearna;

import java.util.Arrays;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Implementation of IVector that saves vector elements into array of
 *         doubles.
 * 
 */
public class Vector extends AbstractVector {

	/**
	 * Array of vector elements.
	 */
	private double[] elements;

	/**
	 * Vector dimension.
	 */
	private int dimension;

	/**
	 * ReadOnly flag.
	 */
	private boolean readOnly;

	/**
	 * Creates new {@link Vector} with read-write permission and without to
	 * given array.
	 * 
	 * @param vectorArray
	 *            vectorArray given array of vector elements
	 */
	public Vector(double... vectorArray) {
		this(false, false, vectorArray);
	}

	/**
	 * Constructs new {@link Vector} from given arguments. If reusable is set to
	 * true reference to given array will be kept.
	 * 
	 * @param readOnly
	 *            set readOnly permission
	 * @param reusable
	 *            set usage of given array reference
	 * @param vectorArray
	 *            given array of vector elements
	 */
	public Vector(boolean readOnly, boolean reusable, double... vectorArray) {
		this.readOnly = readOnly;
		if (reusable) {
			elements = vectorArray;
		} else {
			elements = Arrays.copyOf(vectorArray, vectorArray.length);
		}
		dimension = vectorArray.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#get(int)
	 */
	/**
	 * @throws IndexOutOfBoundsException
	 */
	@Override
	public double get(int index) {
		if (index < 0 || index >= this.getDimension()) {
			throw new IndexOutOfBoundsException();
		}
		return elements[index];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#set(int, double)
	 */
	/**
	 * @throws IndexOutOfBoundsException
	 * @throws UnmodifiableObjectException
	 *             if vector is readOnly
	 */
	@Override
	public IVector set(int index, double value) {
		if (readOnly) {
			throw new UnmodifiableObjectException("This is read-only vector.");
		}
		if (index < 0 || index >= this.getDimension()) {
			throw new IndexOutOfBoundsException();
		}
		elements[index] = value;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#getDimension()
	 */
	@Override
	public int getDimension() {
		return dimension;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#copy()
	 */
	@Override
	public IVector copy() {
		return new Vector(readOnly, false, elements);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#newInstance(int)
	 */
	@Override
	public IVector newInstance(int dimension) {
		return new Vector(new double[dimension]);
	}

	/**
	 * Parses {@link String} with components of vector divided with one or more
	 * spaces.
	 * 
	 * @param vector
	 * @return
	 */
	public static IVector parseSimple(String vector) {
		vector = vector.trim();
		String[] vectorElements = vector.split("\\s");
		double[] elements = new double[vectorElements.length];
		int i = 0;
		for (String vectorElement : vectorElements) {
			elements[i] = Double.parseDouble(vectorElement);
			i++;
		}
		return new Vector(false, true, elements);
	}

}
