/**
 * 
 */
package hr.fer.zemris.linearna;

/**
 * @author Marin Petrunić 0036472250 Shows given matrix as vector.
 * 
 */
public class VectorMatrixView extends AbstractVector {

	/**
	 * Reference to original {@link IMatrix}.
	 */
	private IMatrix matrix;

	/**
	 * Constructs new {@link VectorMatrixView} from given {@link IMatrix}.
	 * 
	 * @param matrix
	 * @throws IllegalArgumentException
	 */
	public VectorMatrixView(IMatrix matrix) {
		if (matrix == null) {
			throw new IllegalArgumentException(
					"Null reference to matrix is not accepted");
		}
		this.matrix = matrix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#get(int)
	 */
	@Override
	public double get(int index) {
		return matrix.get(0, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#set(int, double)
	 */
	@Override
	public IVector set(int index, double value) {
		matrix.set(0, index, value);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#getDimension()
	 */
	@Override
	public int getDimension() {
		return matrix.getColsCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#copy()
	 */
	@Override
	public IVector copy() {
		return new Vector(matrix.toArray()[0]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.linearna.AbstractVector#newInstance(int)
	 */
	@Override
	public IVector newInstance(int dimension) {
		return LinAlgDefaults.defaultVector(dimension);
	}

}
