/**
 * 
 */
package hr.fer.zemris.linearna.demo;

import hr.fer.zemris.linearna.IMatrix;
import hr.fer.zemris.linearna.Matrix;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Solving equation.
 */
public class Prog3 {

	/**
	 * Does not need arguments.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		IMatrix a = Matrix.parseSimple("3 5 | 2 10");
		IMatrix r = Matrix.parseSimple("2 | 8");
		IMatrix v = a.nInvert().nMultiply(r);
		System.out.println("Rješenje sustava je :");
		System.out.println(v);
	}

}
