/**
 * 
 */
package hr.fer.zemris.linearna.demo;

import hr.fer.zemris.linearna.IMatrix;
import hr.fer.zemris.linearna.Matrix;

/**
 * @author Marin Petrunić 0036472250
 * 
 *         Calculating barycentric coordinates by solving equation.
 */
public class Prog4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IMatrix a = Matrix.parseSimple("1 5 3 | 0 0 8 | 1 1 1");
		IMatrix r = Matrix.parseSimple("3 | 4 | 1");
		IMatrix v = a.nInvert().nMultiply(r);
		System.out.println("Rješenje sustava je :");
		System.out.println(v);
	}

}
