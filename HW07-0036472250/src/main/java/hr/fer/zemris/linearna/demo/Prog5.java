/**
 * 
 */
package hr.fer.zemris.linearna.demo;

import hr.fer.zemris.linearna.IVector;
import hr.fer.zemris.linearna.Vector;

/**
 * @author Marin Petrunić 0036472250 Finding reflexive vectors.
 */
public class Prog5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IVector m2 = new Vector(2, 3);
		IVector n2 = new Vector(3, 3);
		IVector r2 = n2.nNormalize().scalarMultiply(2)
				.scalarMultiply(m2.scalarProduct(n2.nNormalize())).nSub(m2);
		System.out.println(r2);
		IVector m3 = new Vector(2, 3, 1);
		IVector n3 = new Vector(3, 3, 5);
		IVector r3 = n3.nNormalize().scalarMultiply(2)
				.scalarMultiply(m3.scalarProduct(n3.nNormalize())).nSub(m3);
		System.out.println(r3);
	}

}
