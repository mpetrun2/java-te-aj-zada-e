/**
 * 
 */
package hr.fer.zemris.linearna;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class VectorTest {

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#get(int)}.
	 */
	@Test
	public void testGet() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		assertEquals("Expected to get third element = 3.", 3, vector.get(2),
				1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#get(int)}.
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetException() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		vector.get(5);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#set(int, double)}.
	 */
	@Test
	public void testSet() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		vector.set(3, -3);
		assertArrayEquals("Expected vectors to be equal.", new double[] { 1, 2,
				3, -3, 9 }, vector.toArray(), 1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#set(int, double)}.
	 */
	@Test(expected = UnmodifiableObjectException.class)
	public void testSetException() {
		IVector vector = new Vector(true, false, 1, 2, 3, 4);
		vector.set(3, -3);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#set(int, double)}.
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testSetException1() {
		IVector vector = new Vector(false, false, 1, 2, 3, 4);
		vector.set(5, -3);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#getDimension()}.
	 */
	@Test
	public void testGetDimension() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		assertEquals("Expected to get dimension of vector 5", 5,
				vector.getDimension());
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#copy()}.
	 */
	@Test
	public void testCopy() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		assertArrayEquals("Copied vector must have same elements",
				vector.toArray(), vector.copy().toArray(), 1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#newInstance(int)}.
	 */
	@Test
	public void testNewInstance() {
		IVector vector = new Vector(1, 2, 3, 7, 9);
		assertArrayEquals(
				"Expected new instance with same dimension and all elements set to 0.",
				new double[5], vector.newInstance(5).toArray(), 1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.Vector#Vector(double[])}.
	 */
	@Test
	public void testVectorDoubleArray() {
		new Vector(1, 3, 5);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.Vector#Vector(boolean, boolean, double[])}.
	 */
	@Test
	public void testVectorBooleanBooleanDoubleArray() {
		new Vector(true, true, 1, 4, 5);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.Vector#parseSimple(java.lang.String)}.
	 */
	@Test
	public void testParseSimple() {
		IVector vector = Vector.parseSimple("1 3 4");
		assertArrayEquals("Expected to get given vector elements",
				new double[] { 1, 3, 4 }, vector.toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#copyPart(int)}.
	 */
	@Test
	public void testCopyPart() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		assertArrayEquals("Expected to get part of vector", new double[] { 1,
				3, 3 }, vector.copyPart(3).toArray(), 1E-6);
		assertArrayEquals("Expected to get part of vector", new double[] { 1,
				3, 3, 4, 5, 0 }, vector.copyPart(6).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#add(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testAdd() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5);
		assertArrayEquals("Expected add vector.", new double[] { 3, 7, 2, 8.5,
				10 }, vector.add(vector2).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#add(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testAddException() {
		IVector vector = new Vector(1, 3, 3, 4, 5, 6);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5);
		vector.add(vector2);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nAdd(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testNAdd() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5);
		assertArrayEquals("Expected added vector.", new double[] { 3, 7, 2,
				8.5, 10 }, vector.nAdd(vector2).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#sub(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testSub() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5);
		assertArrayEquals("Expected subdivided vector.", new double[] { -1, -1,
				4, -0.5, 0 }, vector.sub(vector2).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#sub(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testSubException() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5, 2);
		vector.sub(vector2).toArray();
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nSub(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testNSub() {
		IVector vector = new Vector(1, 3, 3, 4, 5);
		IVector vector2 = new Vector(2, 4, -1, 4.5, 5);
		assertArrayEquals("Expected subdivided vector.", new double[] { -1, -1,
				4, -0.5, 0 }, vector.nSub(vector2).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#scalarMultiply(double)}.
	 */
	@Test
	public void testScalarMultiply() {
		IVector vector = new Vector(1, 3, 3);
		assertArrayEquals("Expected multyplied vector.",
				new double[] { 2, 6, 6 }, vector.scalarMultiply(2).toArray(),
				1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nScalarMultiply(double)}.
	 */
	@Test
	public void testNScalarMultiply() {
		IVector vector = new Vector(1, 3, 3);
		assertArrayEquals("Expected multyplied vector.",
				new double[] { 2, 6, 6 }, vector.nScalarMultiply(2).toArray(),
				1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.AbstractVector#norm()}.
	 */
	@Test
	public void testNorm() {
		IVector vector = new Vector(1, 3, 3);
		assertEquals("Expected norm to be sqrt(19)", Math.sqrt(19), vector.norm(), 1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.AbstractVector#normalize()}
	 * .
	 */
	@Test
	public void testNormalize() {
		IVector vector = new Vector(1, 2, 2);
		assertArrayEquals("Expected normilized vector.", new double[] { 1.0 / 3,
				2.0 / 3, 2.0 / 3 }, vector.normalize().toArray(), 1E-6);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.AbstractVector#normalize()}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testNormalizeException() {
		IVector vector = new Vector(0, 0, 0);
		vector.normalize();
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nNormalize()}.
	 */
	@Test
	public void testNNormalize() {
		IVector vector = new Vector(1, 2, 2);
		assertArrayEquals("Expected normilized vector.", new double[] { 1.0 / 3,
				2.0 / 3, 2.0 / 3 }, vector.nNormalize().toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#cosine(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testCosine() {
		IVector vector = new Vector(2, -3, 1);
		IVector vector2 = new Vector(1, 1, 0);
		assertEquals("Expected cosinus -1/2sqrt(7)", -1 / (2 * Math.sqrt(7)),
				vector.cosine(vector2), 1E-6);

	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#cosine(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testCosineException() {
		IVector vector = new Vector(0, 0, 0);
		IVector vector2 = new Vector(0, 0, 0);
		vector.cosine(vector2);

	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#scalarProduct(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testScalarProduct() {
		IVector vector = new Vector(2, -3, 1);
		IVector vector2 = new Vector(1, 1, 0);
		assertEquals("Expected product -1", -1, vector.scalarProduct(vector2),
				1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#scalarProduct(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testScalarProductException() {
		IVector vector = new Vector(2, -3, 1, 3);
		IVector vector2 = new Vector(1, 1, 0);
		vector.scalarProduct(vector2);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nVectorProduct(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test
	public void testNVectorProduct() {
		IVector vector = new Vector(2, -3, 1);
		IVector vector2 = new Vector(1, 1, 0);
		assertArrayEquals("Expected vector product vector.", new double[] { -1,
				1, 5 }, vector.nVectorProduct(vector2).toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nVectorProduct(hr.fer.zemris.linearna.IVector)}
	 * .
	 */
	@Test(expected = IncompatibleOperandException.class)
	public void testNVectorProductException() {
		IVector vector = new Vector(2, -3);
		IVector vector2 = new Vector(1, 1);
		vector.nVectorProduct(vector2);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nFromHomogeneus()}.
	 */
	@Test
	public void testNFromHomogeneus() {
		IVector vector = new Vector(2, -3, 1);
		assertArrayEquals("Expected homogen vector.", new double[] { 2,
				-3 }, vector.nFromHomogeneus().toArray(), 1E-6);
	}
	
	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nFromHomogeneus()}.
	 */
	@Test(expected=IncompatibleOperandException.class)
	public void testNFromHomogeneusException() {
		IVector vector = new Vector(2, -3, 0);
		vector.nFromHomogeneus();
	}
	
	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#nFromHomogeneus()}.
	 */
	@Test(expected=IncompatibleOperandException.class)
	public void testNFromHomogeneusException1() {
		IVector vector = new Vector(2);
		vector.nFromHomogeneus();
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#toRowMatrix(boolean)}.
	 */
	@Test
	public void testToRowMatrix() {
		IVector vector = new Vector(2, -3, 0);
		assertArrayEquals("Expected row matrix.", new double[] { 2,
				-3, 0 }, vector.toRowMatrix(false).toArray()[0], 1E-6);
		assertArrayEquals("Expected row matrix.", new double[] { 2,
				-3, 0 }, vector.toRowMatrix(true).toArray()[0], 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#toColumnMatrix(boolean)}.
	 */
	@Test
	public void testToColumnMatrix() {
		IVector vector = new Vector(2, -3, 0);
		vector.toColumnMatrix(false);
		vector.toColumnMatrix(true);
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.AbstractVector#toArray()}.
	 */
	@Test
	public void testToArray() {
		IVector vector = new Vector(2, -3, 0);
		assertArrayEquals("Expected row matrix.", new double[] { 2,
				-3, 0 }, vector.toArray(), 1E-6);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.linearna.AbstractVector#toString(int)}.
	 */
	@Test
	public void testToStringInt() {
		IVector vector = new Vector(2, -3, 0);
		assertEquals("2,0000 -3,0000 0,0000 ",(String)((Vector) vector).toString(4));
	}

	/**
	 * Test method for {@link hr.fer.zemris.linearna.AbstractVector#toString()}.
	 */
	@Test
	public void testToString() {
		IVector vector = new Vector(2, -3, 0);
		assertEquals("2,000 -3,000 0,000 ",vector.toString());
	}

}
