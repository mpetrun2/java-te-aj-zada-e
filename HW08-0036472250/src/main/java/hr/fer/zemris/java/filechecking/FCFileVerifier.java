/**
 * 
 */
package hr.fer.zemris.java.filechecking;

import hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer;
import hr.fer.zemris.java.filechecking.program.syntax.ProgramParser;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Class that compiles given program, extracts given archive
 * and verify extracted archive against program.
 * @author Marin Petrunić 0036472250
 * 
 */
public class FCFileVerifier {
	
	/**
	 * Path to file.
	 */
	private Path filePath;
	
	/**
	 * Original file name.
	 */
	@SuppressWarnings("unused")
	private String fileName;
	
	/**
	 * Program against file will be verified.
	 */
	@SuppressWarnings("unused")
	private String program;
	
	/**
	 * Map with initial variable data.
	 */
	private Map<String, String> variables;
	
	/**
	 * List of validation errors.
	 */
	private List<String> errors;
	

	/**
	 * Constructor.
	 * @param file
	 * @param fileName
	 * @param program
	 * @param initialData
	 */
	public FCFileVerifier(File file, String fileName, String program,
			Map<String, String> initialData) {

		if(file == null || fileName == null || program == null || initialData == null) {
			throw new IllegalArgumentException("Internal error, some null argument given.");
		}
		System.out.println("Starting file validation.");
		this.fileName = fileName;
		this.program = program;
		variables = new HashMap<>(initialData);
		filePath = extract(file);
		ProgramNodeVisitor executor = new ProgramNodeVisitorImpl(variables, filePath.toString(), fileName);
		System.out.println("Compiling program...");
		ProgramTokenizer tokenizer = new ProgramTokenizer(program);
		if(tokenizer.hasErrors()) {
			throw new IllegalArgumentException("Given program has lexical errors.");
		}
		ProgramParser parser = new ProgramParser(tokenizer);
		if(!parser.getErrors().isEmpty()) {
			if(tokenizer.hasErrors()) {
				throw new IllegalArgumentException("Given program has syntax errors.");
			}
		}
		System.out.println("Program compiled.");
		System.out.println("Validating file...");
		parser.getProgramNode().accept(executor);
		errors = new ArrayList<>(executor.getErrors());
		System.out.println("Validation completed.");
	}


	/**
	 * Helper method for extracting given archive.
	 * Currently only supported format is zip.
	 * @param  file archive
	 * @return Path to extracted directory structure.
	 */
	private Path extract(File file) {
		System.out.println("Extracting file...");
		String path = file.getParent();
		int BUFFER = 1024;
		Path filePath = null;
		if(file.getName().endsWith(".zip")) {
			ZipFile zip = null;
			try {
				zip = new ZipFile(file);
				filePath = Paths.get(zip.getName().substring(0, zip.getName().lastIndexOf('.')));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Enumeration<? extends ZipEntry> entrys = zip.entries();
			while(entrys.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entrys.nextElement();
				if(entry.isDirectory()) {
					try {
						Files.createDirectories(Paths.get(path+entry.getName()));
					} catch (IOException e) {
						System.err.println(e.getMessage());
					}
					continue;
				}
				try {
					BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));

					byte data[] = new byte[BUFFER];
					FileOutputStream fos = new 
					  FileOutputStream(path+entry.getName());
					BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
					int count;
					while ((count = is.read(data, 0, BUFFER)) != -1) {
					   dest.write(data, 0, count);
					}
					is.close();
					dest.flush();
					dest.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		} else {
			throw new IllegalArgumentException("Unable archive extension.");
		}
		System.out.println("File extracted.");
		return filePath;
	}


	/**
	 * @return the errors
	 */
	public List<String> getErrors() {
		return errors;
	}
	
	/**
	 * @return true if some errors occurred
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
}
