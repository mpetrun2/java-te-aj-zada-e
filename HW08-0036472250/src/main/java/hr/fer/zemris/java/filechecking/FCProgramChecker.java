/**
 * 
 */
package hr.fer.zemris.java.filechecking;

import hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer;
import hr.fer.zemris.java.filechecking.program.syntax.ProgramParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class from validating given program with rules
 * for validating file.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
public class FCProgramChecker {

	
	/**
	 * List of errors.
	 */
	private List<String> errors;
	
	/**
	 * Constructs {@link FCProgramChecker} with given
	 * program which will be validated.
	 * 
	 * @param program to be validated
	 */
	public FCProgramChecker(String program) {
		ProgramTokenizer tokenizer = new ProgramTokenizer(program);
		errors = new ArrayList<>();
		ProgramParser parser = new ProgramParser(tokenizer);
		errors.addAll(tokenizer.errors());
		errors.addAll(parser.getErrors());
	}
	
	/**
	 * 
	 * @return true if there is some errors.
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	/**
	 * 
	 * @return list of errors
	 */
	public List<String> errors() {
		return Collections.unmodifiableList(errors);
	}
}
