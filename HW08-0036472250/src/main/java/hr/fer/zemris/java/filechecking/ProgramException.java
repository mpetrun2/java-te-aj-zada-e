/**
 * 
 */
package hr.fer.zemris.java.filechecking;

/**
 * Program execution exception.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramException extends RuntimeException {

	/**
	 * Exception serialVerionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructorč.
	 */
	public ProgramException() {
	}

	/**
	 * Constructor.č
	 * @param message opis pogreške
	 */
	public ProgramException(String message) {
		super(message);
	}
}
