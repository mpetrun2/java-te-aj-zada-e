/**
 * 
 */
package hr.fer.zemris.java.filechecking.demo;

import hr.fer.zemris.java.filechecking.FCFileVerifier;
import hr.fer.zemris.java.filechecking.FCProgramChecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Program for demonstration of writen file checker support.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class FCDemo {

	/**
	 * Accepts path to archive, path to program and original archive name
	 * and demonstrates validation with these arguments.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 3) {
			throw new IllegalArgumentException("Ivalid number of command line arguments.");
		}
		File file = new File(args[0]); // zadaj neku ZIP arhivu
		String program = ucitaj(args[1]); // učitaj program iz datoteke
		String fileName = args[2]; // definiraj stvarno ime arhive
		
		FCProgramChecker checker = new FCProgramChecker(program);
		if(checker.hasErrors()) {
			System.out.println("Given program contains errors!");
			for(String error : checker.errors()) {
				System.out.println(error);
			}
			System.out.println("I give up.");
			System.exit(0);
		}
		
		Map<String,String> initialData = new HashMap<>();
		initialData.put("jmbag", "0012345678");
		initialData.put("lastName", "Perić");
		initialData.put("firstName", "Pero");
		FCFileVerifier verifier = new FCFileVerifier(
		file, fileName, program, initialData);
		if(!verifier.hasErrors()) {
			System.out.println("Yes! Success! This upload will be accepted.");
		} else {
			System.out.println("Oops! This upload is denied! What is wrong?");
			for(String error : verifier.getErrors()) {
				System.out.println(error);
			}
		}
	}

	/**
	 * Helper method for reading from file at given path.
	 * @param string
	 * @return
	 */
	private static String ucitaj(String string) {
		String program = new String("");
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(Paths.get(string).toFile()),
				Charset.forName("UTF-8")))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				program += sCurrentLine + "\n";
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return program;
	}

}
