/**
 * Provides classes neccessary for validating
 * given file against rules writen in specific language.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
package hr.fer.zemris.java.filechecking;