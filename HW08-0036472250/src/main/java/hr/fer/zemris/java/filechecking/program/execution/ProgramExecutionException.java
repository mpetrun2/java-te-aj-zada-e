/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.execution;

import hr.fer.zemris.java.filechecking.ProgramException;

/**
 * Exception for errors during execution.
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramExecutionException extends ProgramException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7215336796556014877L;

	/**
	 * 
	 */
	public ProgramExecutionException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ProgramExecutionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
