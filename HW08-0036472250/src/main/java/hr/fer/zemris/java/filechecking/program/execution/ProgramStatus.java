/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.execution;

/**
 * Program statuses.
 * @author Marin Petrunić 0036472250
 *
 */
public enum ProgramStatus {
	CONTINUE,
	TERMINATE,
	SKIP
}
