/**
 * Classes for program execution.
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.filechecking.program.execution;