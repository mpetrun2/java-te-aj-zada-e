/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.lexical;


/**
 * Represents single program token.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramToken {
	
		/**
		 * Token type.
		 */
		private ProgramTokenType tokenType;
		/**
		 * Token value.
		 */
		private Object value;
		
		/**
		 * Constructor.
		 * @param tokenType token type
		 * @param value token value
		 */
		public ProgramToken(ProgramTokenType tokenType, Object value) {
			super();
			if(tokenType==null) throw new IllegalArgumentException("Token type can not be null.");
			this.tokenType = tokenType;
			this.value = value;
		}
		
		/**
		 * Token type getter.
		 * @return token type
		 */
		public ProgramTokenType getTokenType() {
			return tokenType;
		}
		
		/**
		 * Token value getter.
		 * @return token value or <code>null</code> if token doesn't have value.
		 */
		public Object getValue() {
			return value;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((tokenType == null) ? 0 : tokenType.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof ProgramToken)) {
				return false;
			}
			ProgramToken other = (ProgramToken) obj;
			if (tokenType != other.tokenType) {
				return false;
			}
			if (value == null) {
				if (other.value != null) {
					return false;
				}
			} else if (!value.equals(other.value)) {
				return false;
			}
			return true;
		}
		
		
}
