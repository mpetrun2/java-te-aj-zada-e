/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.lexical;

/**
 * Token type enumeration contained in program.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
public enum ProgramTokenType {
	/** Sign for end of tokens.	 */
	EOF,
	/**	Sign marking start of comment	*/
	COMMENT,
	/**	Identifier	*/
	IDENT,
	/** Keyword */
	KEYWORD,
	/** Colon	*/
	COLON,
	/** Inverter */
	INVERS,
	/** Fail message sign */
	FAIL_MESSAGE,
	/** Open parentheses */
	OPEN_PARENTHESES,
	/** Closed parentheses **/
	CLOSED_PARENTHESES,
	/** Sign marking that string is case insensitive */
	CASE_INSENSITIVE,
	/** String separator */
	PATH_SEPARATOR,
	/** Point */
	POINT,
	/** Variable sign */
	VARIABLE,
	STRING
}
