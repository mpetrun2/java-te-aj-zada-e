/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.lexical;

import hr.fer.zemris.java.filechecking.ProgramException;

/**
 * Exception describing errors while tokenizing program.
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramTokenizerException extends ProgramException{

	public ProgramTokenizerException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
