/**
 * Classes for lexical program analyze.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.filechecking.program.lexical;