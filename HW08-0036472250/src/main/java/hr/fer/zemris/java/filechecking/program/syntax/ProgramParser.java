package hr.fer.zemris.java.filechecking.program.syntax;

import hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenType;
import hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FailStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.TerminateStatement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for parsing programs source code into tree of statements.
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramParser {

	/**
	 * Source code tokenizer {@link ProgramTokenizer}.
	 */
	private ProgramTokenizer tokenizer;
	/**
	 * Tree containing statements of parsed program.
	 */
	private ProgramNode startNode;
	
	/**
	 * List of errors while parsing source code.
	 * If there is no errors list will be emmpty.
	 */
	private List<String> errors = new LinkedList<>();
	
	/**
	 * Constructor.
	 * @param tokenizer Source code tokenizer {@link ProgramTokenizer}
	 */
	public ProgramParser(ProgramTokenizer tokenizer) {
		this.tokenizer = tokenizer;
		startNode = new StartNode(parse(ProgramTokenType.EOF));
	}

	/**
	 * Returns tree of statements after parsing.
	 * @return program tree
	 */
	public ProgramNode getProgramNode() {
		return startNode;
	}
	
	/**
	 * Method for recursively parsing program.
	 * Parsing is stopped and {@link List} of statements
	 * is returned after parser reaches <code>endToken</code>.
	 * @param endToken parser stop sign
	 * @return list of statements
	 */
	private List<ProgramNode> parse(ProgramTokenType endToken) {
		List<ProgramNode> statements = new ArrayList<>();
		while(true) {
			try {

				boolean inverse = false;
				
				// If stop token reached.
				if(tokenizer.getCurrentToken().getTokenType()==endToken) {
					break;
				}
				
				//If there is inverse sign before keyword:
				if(tokenizer.getCurrentToken().getTokenType() == ProgramTokenType.INVERS) {
					inverse = true;
					tokenizer.nextToken();
				}
				
				// else keyword is expected:
				if(tokenizer.getCurrentToken().getTokenType()!=ProgramTokenType.KEYWORD) {
					String current = tokenizer.getCurrentToken().getTokenType().toString();
					tokenizer.nextToken();
					throw new ProgramSyntaxException("Keyword expected, recieved " + current + " .");
				}
				// Statement "def":
				if("def".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(parseDef());
					continue;
				}
				// Statement "terminate":
				if("terminate".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(new TerminateStatement());
					continue;
				}
				// Statement "exists":
				if("exists".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(parseExists(inverse));
					continue;
				}
				// Statement "filename":
				if("filename".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(parseFilename(inverse));
					continue;
				}
				// Statement "format":
				if("format".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(parseFormat(inverse));
					continue;
				}
				// Statement "fail":
				if("fail".equals(tokenizer.getCurrentToken().getValue())) {
					tokenizer.nextToken();
					statements.add(parseFail(inverse));
					continue;
				}
				errors.add("Unknown statement");
				tokenizer.nextToken();
			} catch (ProgramSyntaxException e) {
				if(e.getMessage() != null) {
					errors.add(e.getMessage());
				} else {
					errors.add("Unknown syntaks error.");
				}
			}
		}
		// return of statements
		return statements;
	}

	/**
	 * Helper method for parsing arguments of
	 * "fail" statement.
	 * @param inverse inverted result flag
	 * @return {@link FailStatement}
	 */
	private ProgramNode parseFail(boolean inverse) {
		String message = getFailMessage();
		List<ProgramNode> statements = getSubStatements();
		return new FailStatement(statements, inverse, message);
	}

	/**
	 * Helper method for parsing arguments of
	 * "format" statement.
	 * @param inverse inverted result flag
	 * @return {@link FormatStatement}
	 */
	private ProgramNode parseFormat(boolean inverse) {
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.IDENT) {
			throw new ProgramSyntaxException("In format statement expected extension as argument.");
		}
		String extension = (String)tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		String failMessage = getFailMessage();
		List<ProgramNode> statements = getSubStatements();
		return new FormatStatement(extension, statements, inverse, failMessage);
	}

	/**
	 * Helper method for parsing arguments of
	 * "filename" statement.
	 * @param inverse inverted result flag
	 * @return {@link FileNameStatement}
	 */
	private ProgramNode parseFilename(boolean inverse) {
		boolean caseInsensitive = false;
		if(tokenizer.getCurrentToken().getTokenType() == ProgramTokenType.CASE_INSENSITIVE) {
			caseInsensitive = true;
		}
		tokenizer.nextToken();
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.STRING) {
			throw new ProgramSyntaxException("In filename statement expected path as argument.");
		}
		String fileName = (String) tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		String failMessage = getFailMessage();
		List<ProgramNode> statements = getSubStatements();
		return new FileNameStatement(fileName, caseInsensitive, statements, inverse, failMessage);
	}

	/**
	 * Helper method for parsing arguments of
	 * "exist" statement.
	 * @param inverse inverted result flag
	 * @return {@link ExistStatement}
	 */
	private ProgramNode parseExists(boolean inverse) {
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.IDENT) {
			throw new ProgramSyntaxException("In exists statement expected object type.");
		}
		String objectType = (String)tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.STRING) {
			throw new ProgramSyntaxException("In exist statement expected path as argument.");
		}
		String path = (String) tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		String failMessage = getFailMessage();
		List<ProgramNode> statements = getSubStatements();
		return new ExistStatement(objectType, path, inverse, statements, failMessage);
	}
	
	/**
	 * Helper method for parsing arguments of
	 * "exist" statement.
	 * 
	 * @return {@link DefStatement}
	 */
	private ProgramNode parseDef() {
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.IDENT) {
			throw new ProgramSyntaxException("In def statement expected variable name.");
		}
		String varName = (String)tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.STRING) {
			throw new ProgramSyntaxException("In def statement expected path as argument.");
		}
		String varValue = (String)tokenizer.getCurrentToken().getValue();
		tokenizer.nextToken();
		return new DefStatement(varName, varValue);
	}

	/**
	 * Helper method for getting fail message argument if any.
	 * @return {@link String} containing fail message
	 */
	private String getFailMessage() {
		String failMessage = "";
		if(tokenizer.getCurrentToken().getTokenType() == ProgramTokenType.FAIL_MESSAGE) {
			tokenizer.nextToken();
			if(tokenizer.getCurrentToken().getTokenType() != ProgramTokenType.STRING) {
				throw new ProgramSyntaxException("In statement after \"@\" expected fail message as argument.");
			}
			failMessage = (String) tokenizer.getCurrentToken().getValue();
			tokenizer.nextToken();
		}
		return failMessage;
	}

	/**
	 * Helper method for parsing block of command statements.
	 * @return
	 */
	private List<ProgramNode> getSubStatements() {
		List<ProgramNode> statements = new ArrayList<>();
		if(tokenizer.getCurrentToken().getTokenType() == ProgramTokenType.OPEN_PARENTHESES) {
			tokenizer.nextToken();
			statements = parse(ProgramTokenType.CLOSED_PARENTHESES);
			tokenizer.nextToken();
		}
		return statements;
	}
	
	/**
	 * Returns list of errors happened during syntax analysis.
	 * If no errors occurred empty list will be returned.
	 * @return {@link List}
	 */
	public List<String> getErrors() {
		return Collections.unmodifiableList(errors);
	}

}
