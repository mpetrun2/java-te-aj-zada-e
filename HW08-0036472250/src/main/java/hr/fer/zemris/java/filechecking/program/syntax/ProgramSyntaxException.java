/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax;

import hr.fer.zemris.java.filechecking.ProgramException;

/**
 * Exception in syntax .
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramSyntaxException extends ProgramException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3489687034257438296L;

	/**
	 * 
	 */
	public ProgramSyntaxException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ProgramSyntaxException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
