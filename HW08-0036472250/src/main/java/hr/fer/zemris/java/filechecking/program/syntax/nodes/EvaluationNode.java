/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes;

import java.util.ArrayList;
import java.util.List;

/**
 * Every evaluation statement has substatements
 * that will execute if evaluation statement
 * evaluates succesfully.
 * @author Marin Petrunić 0036472250
 *
 */
public abstract class EvaluationNode extends ProgramNode {

	/**
	 * Statement sequence.
	 */
	private List<ProgramNode> statements;
	
	/**
	 * If inverted result is expected.
	 */
	private boolean inverse;
	
	/**
	 * Message returned in case evaluation fails.
	 */
	private String failMessage;
	
	/**
	 * Constructor.
	 * @param statements statement sequence
	 * @param inverse 
	 */
	public EvaluationNode(List<ProgramNode> statements, boolean inverse, String failMessage) {
		this.statements = new ArrayList<>(statements);
		this.inverse = inverse;
		this.failMessage = failMessage;
	}
	

	/**
	 * @return the inverse
	 */
	public boolean isInverse() {
		return inverse;
	}

	/**
	 * Getter for statement sequence.
	 * @return statement sequence
	 */
	public List<ProgramNode> getStatements() {
		return new ArrayList<>(statements);
	}


	/**
	 * @return the failMessage
	 */
	public String getFailMessage() {
		return failMessage;
	}
	
	
}
