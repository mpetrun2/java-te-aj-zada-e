/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * Abstract node of program tree.
 * Each node implementation represents some
 * program statement.
 * @author Marin Petrunić 0036472250
 *
 */
public abstract class ProgramNode {

	/**
	 * Node visitor acception.
	 * @param visitor node visitor with statement execution implementation
	 * @return {@link ProgramStatus}
	 */
	public abstract ProgramStatus accept(ProgramNodeVisitor visitor);
}
