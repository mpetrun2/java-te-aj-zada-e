/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * Class representing root node of program.
 * @author Marin Petrunić 0036472250
 *
 */
public class StartNode extends ProgramNode {

	/**
	 * Statement sequence.
	 */
	private List<ProgramNode> statements;
	
	public StartNode(List<ProgramNode> statements) {
		this.statements = new ArrayList<>(statements);
	}
	
	/**
	 * Getter for statement sequence.
	 * @return statement sequence
	 */
	public List<ProgramNode> getStatements() {
		return new ArrayList<>(statements);
	}
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}

}
