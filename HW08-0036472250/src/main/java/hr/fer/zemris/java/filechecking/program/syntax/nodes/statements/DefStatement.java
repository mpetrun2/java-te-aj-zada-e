/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.ProgramSyntaxException;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * CLass representing program statement "def".
 * @author Marin Petrunić 0036472250
 *
 */
public class DefStatement extends ProgramNode {

	/**
	 * Name of variable to be defined
	 */
	private String variableName;
	
	/**
	 * Variable value.
	 */
	private String value;
	
	/**
	 * Konstruktor.
	 * @param variables lista imena varijabli
	 * @param varType naziv tipa
	 */
	public DefStatement(String variable, String value) {
		if(!validateName(variable)) {
			throw new ProgramSyntaxException("Invalid variable name");
		}
		this.variableName = variable;
		this.value = value;
	}
	
	

	private boolean validateName(String variable) {
		return variable.trim().matches("^[a-zA-Z_.][a-zA-Z0-9_.]+$");
	}



	/**
	 * @return the variable name
	 */
	public String getVariable() {
		return variableName;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}

}
