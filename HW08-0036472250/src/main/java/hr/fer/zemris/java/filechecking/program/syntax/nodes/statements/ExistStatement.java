/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.EvaluationNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * 
 * Represents exists statement. It evaluates if given file is of same type as
 * given in arguments(directory or file).
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class ExistStatement extends EvaluationNode {

	/**
	 * Type of file(file or directory).
	 */
	private String fileType;

	/**
	 * Path to file.
	 */
	private String filePath;

	/**
	 * Constructor.
	 * 
	 * @param fileType
	 *            type of file
	 * @param filePath
	 *            path to file
	 * @param inverse
	 *            if inverted result is expected
	 * @param statements
	 *            list of statement to be executed if successfully evaluated
	 */
	public ExistStatement(String fileType, String filePath, boolean inverse,
			List<ProgramNode> statements, String failMessage) {
		super(statements, inverse, failMessage);
		this.fileType = fileType;
		this.filePath = filePath;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept
	 * (hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.
	 * ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public String getFailMessage() {
		String failMessage = super.getFailMessage();
		if(failMessage == null || failMessage.isEmpty()) {
			if(isInverse()) {
				failMessage = filePath+" exist but it shouldn't.";
			} else {
				failMessage = filePath+" does not exist.";
			}
		}
		return failMessage;
	}

}
