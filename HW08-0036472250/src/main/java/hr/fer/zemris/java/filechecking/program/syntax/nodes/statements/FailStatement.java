/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.EvaluationNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class FailStatement extends EvaluationNode {

	/**
	 * Constructor.
	 * @param statements
	 * @param inverse
	 * @param failMessage
	 */
	public FailStatement(List<ProgramNode> statements, boolean inverse,
			String failMessage) {
		super(statements, inverse, failMessage);
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}

}
