/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.EvaluationNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * Class representing program statement filename.
 * This statement checks if given file name matches
 * given name.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class FileNameStatement extends EvaluationNode {

	/**
	 * Expected name of file
	 */
	private String fileName;
	
	/**
	 * Is given filename caseInsensitive.
	 */
	private boolean caseInsensitive;

	/**
	 * @param statements
	 * @param inverse
	 * @param failMessage
	 */
	public FileNameStatement(String filename, boolean caseInsensitive,
			List<ProgramNode> statements, boolean inverse, String failMessage) {
		super(statements, inverse, failMessage);
		this.fileName = filename;
		this.caseInsensitive = caseInsensitive;
	}
	
	

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}



	/**
	 * @return the caseInsensitive
	 */
	public boolean isCaseInsensitive() {
		return caseInsensitive;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept
	 * (hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.
	 * ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public String getFailMessage() {
		String failMessage = super.getFailMessage();
		if(failMessage == null || failMessage.isEmpty()) {
			if(isInverse()) {
				failMessage = fileName+" does match given archive name but it shouldn't.";
			} else {
				failMessage = fileName+" does not match given archive name.";
			}
		}
		return failMessage;
	}

}
