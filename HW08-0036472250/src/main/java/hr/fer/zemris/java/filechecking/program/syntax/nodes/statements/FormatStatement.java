/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.EvaluationNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class FormatStatement extends EvaluationNode {

	/**
	 * Format to be tested.
	 */
	private String format;
	
	/**
	 * Constructor.
	 * @param format expected file format
	 * @param statements
	 * @param inverse
	 * @param failMessage
	 */
	public FormatStatement(String format, List<ProgramNode> statements, boolean inverse,
			String failMessage) {
		super(statements, inverse, failMessage);
		this.format = format;
	}
	
	

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}



	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	public String getFailMessage() {
		String failMessage = super.getFailMessage();
		if(failMessage == null || failMessage.isEmpty()) {
			if(isInverse()) {
				failMessage = "File should have "+format+" extension";
			} else {
				failMessage =  "File doesn't have "+format+" extension";
			}
		}
		return failMessage;
	}
}
