/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class TerminateStatement extends ProgramNode {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)
	 */
	@Override
	public ProgramStatus accept(ProgramNodeVisitor visitor) {
		return visitor.visit(this);
	}

}
