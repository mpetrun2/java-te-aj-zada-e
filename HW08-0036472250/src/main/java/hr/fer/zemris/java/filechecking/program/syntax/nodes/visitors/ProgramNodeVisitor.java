/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors;

import java.util.List;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FailStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.TerminateStatement;

/**
 * Interface of node visitors.
 * @author Marin Petrunić 0036472250
 *
 */
public interface ProgramNodeVisitor {

	public ProgramStatus visit(TerminateStatement stat);
	
	public ProgramStatus visit(DefStatement stat);
	
	public ProgramStatus visit(ExistStatement stat);
	
	public ProgramStatus visit(FileNameStatement stat);
	
	public ProgramStatus visit(FormatStatement stat);
	
	public ProgramStatus visit(FailStatement stat);
	
	public ProgramStatus visit(StartNode stat);
	
	public List<String> getErrors();
}
