/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.filechecking.program.execution.ProgramExecutionException;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FailStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.TerminateStatement;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor;

/**
 * Class with implementation for executing specific statements.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class ProgramNodeVisitorImpl implements ProgramNodeVisitor {
	
	/**
	 * Map of defined variables.
	 */
	private Map<String, String> variables;
	
	/**
	 * List of failed validation rules messages.
	 */
	private List<String> errors = new ArrayList<>();
	
	/**
	 * Path to archive3 to be tested.
	 */
	private String archivePath;
	
	/**
	 * Orginal arhive name.
	 */
	private String archiveName;
	
	/**
	 * Constructor.
	 * Fills map of variables with already defined.
	 * @param variables
	 */
	public ProgramNodeVisitorImpl(Map<String, String> variables, String archivePath, String archiveName) {
		this.variables = new HashMap<>(variables);
		this.archivePath = archivePath;
		this.archiveName = archiveName;
	}

	/**
	 * This statement terminates validation of file.
	 * @param stat {@link TerminateStatement} to be executed
	 * @return {@link ProgramStatus}.TERMINATE
	 */
	@Override
	public ProgramStatus visit(TerminateStatement stat) {
		return ProgramStatus.TERMINATE;
	}

	/**
	 * This statement defines variable with its value
	 * or if variable with that name already exists, 
	 * replaces old value with new value.
	 * @param stat {@link DefStatement}
	 * @return {@link ProgramStatus}
	 */
	@Override
	public ProgramStatus visit(DefStatement stat) {
		variables.put(stat.getVariable(), resolveVariables(stat.getValue()));
		return ProgramStatus.CONTINUE;
	}

	/**
	 * This command checks if given path to file or directory
	 * exist as file or directory. Result is depending on isInverse flag,
	 * @param stat {@link ExistStatement}
	 * @return {@link ProgramStatus}
	 */
	@Override
	public ProgramStatus visit(ExistStatement stat) {
		String path = resolvePath(stat.getFilePath());
		Path file = Paths.get(path);
		String dir = "dir";
		ProgramStatus status = ProgramStatus.CONTINUE;
		if(dir.startsWith(stat.getFileType())) {
			if(stat.isInverse() == Files.isDirectory(file)) {
				errors.add(stat.getFailMessage());
				status =  ProgramStatus.SKIP;
			}
		} else {
			if(stat.isInverse() == Files.isRegularFile(file)) {
				errors.add(stat.getFailMessage());
				status = ProgramStatus.SKIP;
			}
		}
		if(status != ProgramStatus.SKIP) {
			status = executeSubStatements(stat.getStatements());
		}
		return status;
	}

	/**
	 * This command checks if given archive matches expected name.
	 * @param stat {@link FileNameStatement}
	 * @return {@link ProgramStatus}
	 */
	@Override
	public ProgramStatus visit(FileNameStatement stat) {
		String fileName = resolveVariables(stat.getFileName());
		ProgramStatus status = ProgramStatus.CONTINUE;
		if(stat.isCaseInsensitive()) {
			if(stat.isCaseInsensitive() == (fileName.compareToIgnoreCase(archiveName) == 0)) {
				errors.add(stat.getFailMessage());
				status = ProgramStatus.SKIP;
			}
		} else {
			if(stat.isInverse() == (fileName.compareTo(archiveName) == 0)) {
				errors.add(stat.getFailMessage());
				status = ProgramStatus.SKIP;
			}
		}
		if(status != ProgramStatus.SKIP) {
			status = executeSubStatements(stat.getStatements());
		}
		return status;
	}

	/**
	 * This command checks if given archive has correct extension.
	 * @param stat {@link FormatStatement}
	 * @return {@link ProgramStatus}
	 */
	@Override
	public ProgramStatus visit(FormatStatement stat) {
		ProgramStatus status = ProgramStatus.CONTINUE;
		if(stat.isInverse() == archiveName.endsWith("."+stat.getFormat())) {
			errors.add(stat.getFailMessage());
			status = ProgramStatus.SKIP;
		}
		if(status != ProgramStatus.SKIP) {
			status = executeSubStatements(stat.getStatements());
		}
		return status;
	}

	/**
	 * This commands fails by default.
	 * @param stat {@link FailStatement}
	 * @return {@link ProgramStatus}
	 */
	@Override
	public ProgramStatus visit(FailStatement stat) {
		ProgramStatus status = ProgramStatus.CONTINUE;
		if(!stat.isInverse()) {
			errors.add(stat.getFailMessage());
			status = ProgramStatus.SKIP;
		}
		if(status != ProgramStatus.SKIP) {
			status = executeSubStatements(stat.getStatements());
		}
		return status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.
	 * ProgramNodeVisitor
	 * #visit(hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode)
	 */
	@Override
	public ProgramStatus visit(StartNode stat) {
		return executeSubStatements(stat.getStatements());
	}
	
	private ProgramStatus executeSubStatements(List<ProgramNode> statements) {
		ProgramStatus status = ProgramStatus.CONTINUE;
		for(ProgramNode node : statements) {
			status = node.accept(this);
			if(status == ProgramStatus.TERMINATE) {
				status = ProgramStatus.TERMINATE;
				break;
			}
			if(status == ProgramStatus.SKIP) {
				status = ProgramStatus.CONTINUE;
			}
		}
		return status;
	}

	private String resolvePath(String filePath) {
		String path = filePath;
		path = resolveVariables(path);
		if(path.indexOf(":") != -1) {
			String[] paths = path.split(":");
			paths[1] = paths[1].replace('.', '/');
			path = paths[0]+"/"+paths[1];
		}
		return archivePath+"/"+path;
	}

	private String resolveVariables(String path) {
		Pattern p = Pattern.compile("\\$\\{([^}]*)\\}");
		Matcher m = p.matcher(path);
		while(m.find()) {
			String varName = m.group(1);
			if(!variables.containsKey(varName.trim())) {
				throw new ProgramExecutionException("Variable with name " + varName +" doesnt exist.");
			}
			path = path.replaceAll(Pattern.quote("${"+varName+"}"), variables.get(varName).trim());
		}
		return path;
	}

	/**
	 * @return the variables
	 */
	public Map<String, String> getVariables() {
		return variables;
	}

	/**
	 * @return the errors
	 */
	public List<String> getErrors() {
		return errors;
	}
	
	

}
