/**
 * Implementation of statements execution.
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl;