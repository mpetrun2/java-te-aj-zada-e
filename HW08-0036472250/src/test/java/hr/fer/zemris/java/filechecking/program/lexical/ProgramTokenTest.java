/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.lexical;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramTokenTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramToken#ProgramToken(hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenType, java.lang.Object)}.
	 */
	@Test
	public void testProgramToken() {
		@SuppressWarnings("unused")
		ProgramToken token = new ProgramToken(ProgramTokenType.IDENT, "string");
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramToken#ProgramToken(hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenType, java.lang.Object)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testProgramTokenIlleagal() {
		@SuppressWarnings("unused")
		ProgramToken token = new ProgramToken(null, "string");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramToken#getTokenType()}.
	 */
	@Test
	public void testGetTokenType() {
		ProgramToken token = new ProgramToken(ProgramTokenType.IDENT, "string");
		assertEquals("Token types not equal.",ProgramTokenType.IDENT, token.getTokenType());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramToken#getValue()}.
	 */
	@Test
	public void testGetValue() {
		ProgramToken token = new ProgramToken(ProgramTokenType.IDENT, "string");
		assertEquals("Diffrent token value","string", token.getValue());
	}
	
	

}
