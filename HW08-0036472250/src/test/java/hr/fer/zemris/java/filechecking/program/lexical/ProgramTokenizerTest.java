/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.lexical;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class ProgramTokenizerTest {
	
	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#ProgramTokenizer(java.lang.String)}.
	 */
	@Test
	public void testProgramTokenizer() {
		@SuppressWarnings("unused")
		ProgramTokenizer tokenizer = new ProgramTokenizer("neki program");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#hasErrors()}.
	 */
	@Test
	public void testHasErrors() {
		ProgramTokenizer tokenizer = new ProgramTokenizer("neki program");
		while(tokenizer.nextToken().getTokenType() != ProgramTokenType.EOF);
		assertFalse("Tokenizer has errors.",tokenizer.hasErrors());
		ProgramTokenizer tokenizer2 = new ProgramTokenizer("* neki pro3-.a*gram");
		while(tokenizer2.nextToken().getTokenType() != ProgramTokenType.EOF);
		assertTrue("Tokenizer has no errors.",tokenizer2.hasErrors());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#errors()}.
	 */
	@Test
	public void testErrors() {
		ProgramTokenizer tokenizer2 = new ProgramTokenizer("* neki pro3-.a*gram");
		while(tokenizer2.nextToken().getTokenType() != ProgramTokenType.EOF);
		assertEquals("Wrong error","Invalid character found: '*'.",tokenizer2.errors().get(0));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#getCurrentToken()}.
	 */
	@Test
	public void testGetCurrentToken() {
		ProgramTokenizer tokenizer = new ProgramTokenizer("#komentar\n var abc");
		assertEquals("Diffrent token got.",new ProgramToken(ProgramTokenType.IDENT, "var"), tokenizer.getCurrentToken());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#nextToken()}.
	 */
	@Test
	public void testNextToken() {
		ProgramTokenizer tokenizer = new ProgramTokenizer("!exists var\t i\"neki string\"");
		assertEquals("Wrong token got.",new ProgramToken(ProgramTokenType.KEYWORD, "exists"), tokenizer.nextToken());
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer#nextToken()}.
	 */
	@Test(expected=ProgramTokenizerException.class)
	public void testNextTokenIlleagal() {
		ProgramTokenizer tokenizer = new ProgramTokenizer("");
		tokenizer.nextToken();
	}

}
