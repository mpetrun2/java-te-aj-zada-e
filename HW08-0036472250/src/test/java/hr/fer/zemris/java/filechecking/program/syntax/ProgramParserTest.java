/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class ProgramParserTest {
	private ProgramTokenizer tokenizer;
	
	@Before
	public void setUp() {
		tokenizer = new ProgramTokenizer("def src \"homework04/src/main/java\"\n"
				+ "filename i\"dz1.zip\"\n"
				+ "format zip{\n"
				+ "!exists dir \"homework04/bin\"\n"
				+ "fail @\"Datoteka koju ste uploadali nije dozvoljenog formata.\"\n"
				+ "terminate\n"
				+ "}");
	}


	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.ProgramParser#ProgramParser(hr.fer.zemris.java.filechecking.program.lexical.ProgramTokenizer)}
	 * .
	 */
	@Test
	public void testProgramParser() {
		@SuppressWarnings("unused")
		ProgramParser parser = new ProgramParser(tokenizer);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.ProgramParser#getProgramNode()}
	 * .
	 */
	@Test
	public void testGetProgramNode() {
		ProgramParser parser = new ProgramParser(tokenizer);
		assertTrue(parser.getProgramNode() instanceof StartNode);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.ProgramParser#getErrors()}
	 * .
	 */
	@Test
	public void testGetErrors() {
		ProgramParser parser = new ProgramParser(tokenizer);
		
		assertEquals(new ArrayList<String>(Arrays.asList("Keyword expected, recieved CLOSED_PARENTHESES .")), parser.getErrors());
	}

}
