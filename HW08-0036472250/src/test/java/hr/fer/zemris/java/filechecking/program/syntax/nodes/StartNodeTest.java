/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class StartNodeTest {

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}
	 * .
	 */
	@Test
	public void testAccept() {
		StartNode start = new StartNode(new ArrayList<ProgramNode>());
		assertEquals("Program status not equal.", ProgramStatus.CONTINUE,
				start.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode#StartNode(java.util.List)}
	 * .
	 */
	@Test
	public void testStartNode() {
		@SuppressWarnings("unused")
		StartNode start = new StartNode(new ArrayList<ProgramNode>());
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.StartNode#getStatements()}
	 * .
	 */
	@Test
	public void testGetStatements() {
		StartNode start = new StartNode(new ArrayList<ProgramNode>());
		assertEquals("Invalid list of statement.",new ArrayList<>(), start.getStatements());
	}

}
