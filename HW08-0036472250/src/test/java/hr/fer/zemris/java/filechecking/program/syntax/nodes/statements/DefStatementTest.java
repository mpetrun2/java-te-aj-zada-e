/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;

import java.util.HashMap;

import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.ProgramSyntaxException;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class DefStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		DefStatement def = new DefStatement("src", "hr.fer.zemris");
		assertEquals("Program status not equal.", ProgramStatus.CONTINUE,
				def.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement#DefStatement(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDefStatement() {
		@SuppressWarnings("unused")
		DefStatement def = new DefStatement("src", "hr.fer.zemris");
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement#DefStatement(java.lang.String, java.lang.String)}.
	 */
	@Test(expected=ProgramSyntaxException.class)
	public void testDefStatementIllegal() {
		@SuppressWarnings("unused")
		DefStatement def = new DefStatement("3src", "hr.fer.zemris");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement#getVariable()}.
	 */
	@Test
	public void testGetVariable() {
		DefStatement def = new DefStatement("src", "hr.fer.zemris");
		assertEquals("Variable names dont match.","src", def.getVariable());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.DefStatement#getValue()}.
	 */
	@Test
	public void testGetValue() {
		DefStatement def = new DefStatement("src", "hr.fer.zemris");
		assertEquals("Variable value dont match.","hr.fer.zemris", def.getValue());
	}

}
