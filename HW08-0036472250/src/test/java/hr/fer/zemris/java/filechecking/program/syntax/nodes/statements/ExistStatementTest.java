/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class ExistStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		ExistStatement exist = new ExistStatement("dir", "D:/Hw06", false, new ArrayList<ProgramNode>(), "fail");
		assertEquals("Program status not equal.", ProgramStatus.SKIP,
				exist.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement#getFailMessage()}.
	 */
	@Test
	public void testGetFailMessage() {
		ExistStatement exist = new ExistStatement("dir", "D:/Hw06", true, new ArrayList<ProgramNode>(), "fail");
		assertEquals("Wrong fail message.", "fail", exist.getFailMessage());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement#ExistStatement(java.lang.String, java.lang.String, boolean, java.util.List, java.lang.String)}.
	 */
	@Test
	public void testExistStatement() {
		@SuppressWarnings("unused")
		ExistStatement exist = new ExistStatement("dir", "D:/Hw06", true, new ArrayList<ProgramNode>(), "fail");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement#getFileType()}.
	 */
	@Test
	public void testGetFileType() {
		ExistStatement exist = new ExistStatement("dir", "D:/Hw06", true, new ArrayList<ProgramNode>(), "fail");
		assertEquals("Wrong file type message.", "dir", exist.getFileType());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.ExistStatement#getFilePath()}.
	 */
	@Test
	public void testGetFilePath() {
		ExistStatement exist = new ExistStatement("dir", "D:/Hw06", true, new ArrayList<ProgramNode>(), "fail");
		assertEquals("Wrong file path message.", "D:/Hw06", exist.getFilePath());
	}

}
