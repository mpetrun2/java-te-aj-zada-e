/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class FailStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FailStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		FailStatement fail = new FailStatement(new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Program status not equal.", ProgramStatus.SKIP,
				fail.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FailStatement#FailStatement(java.util.List, boolean, java.lang.String)}.
	 */
	@Test
	public void testFailStatement() {
		@SuppressWarnings("unused")
		FailStatement fail = new FailStatement(new ArrayList<ProgramNode>(), false, "fail");
	}

}
