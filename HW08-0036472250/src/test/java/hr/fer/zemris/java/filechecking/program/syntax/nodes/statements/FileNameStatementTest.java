/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class FileNameStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		FileNameStatement filename = new FileNameStatement("file", false, new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Program status not equal.", ProgramStatus.SKIP,
				filename.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement#getFailMessage()}.
	 */
	@Test
	public void testGetFailMessage() {
		FileNameStatement filename = new FileNameStatement("file", false, new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Wrong fail message.", "fail", filename.getFailMessage());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement#FileNameStatement(java.lang.String, boolean, java.util.List, boolean, java.lang.String)}.
	 */
	@Test
	public void testFileNameStatement() {
		@SuppressWarnings("unused")
		FileNameStatement filename = new FileNameStatement("file", false, new ArrayList<ProgramNode>(), false, "fail");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement#getFileName()}.
	 */
	@Test
	public void testGetFileName() {
		FileNameStatement filename = new FileNameStatement("file", false, new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Wrong filename message.", "file", filename.getFileName());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FileNameStatement#isCaseInsensitive()}.
	 */
	@Test
	public void testIsCaseInsensitive() {
		FileNameStatement filename = new FileNameStatement("file", true, new ArrayList<ProgramNode>(), false, "fail");
		assertTrue("Wrong case insensitive flag set.", filename.isCaseInsensitive());
	}

}
