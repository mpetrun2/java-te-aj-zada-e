/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.ProgramNode;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class FormatStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		FormatStatement format = new FormatStatement("zip", new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Program status not equal.", ProgramStatus.SKIP,
				format.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement#getFailMessage()}.
	 */
	@Test
	public void testGetFailMessage() {
		FormatStatement format = new FormatStatement("zip", new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Wrong fail message.", "fail", format.getFailMessage());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement#FormatStatement(java.lang.String, java.util.List, boolean, java.lang.String)}.
	 */
	@Test
	public void testFormatStatement() {
		new FormatStatement("zip", new ArrayList<ProgramNode>(), true, "fail");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.FormatStatement#getFormat()}.
	 */
	@Test
	public void testGetFormat() {
		FormatStatement format = new FormatStatement("zip", new ArrayList<ProgramNode>(), false, "fail");
		assertEquals("Wrong format.", "zip", format.getFormat());
	}

}
