/**
 * 
 */
package hr.fer.zemris.java.filechecking.program.syntax.nodes.statements;

import static org.junit.Assert.*;
import hr.fer.zemris.java.filechecking.program.execution.ProgramStatus;
import hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.impl.ProgramNodeVisitorImpl;

import java.util.HashMap;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class TerminateStatementTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.filechecking.program.syntax.nodes.statements.TerminateStatement#accept(hr.fer.zemris.java.filechecking.program.syntax.nodes.visitors.ProgramNodeVisitor)}.
	 */
	@Test
	public void testAccept() {
		TerminateStatement terminate = new TerminateStatement();
		assertEquals("Program status not equal.", ProgramStatus.TERMINATE,
				terminate.accept(new ProgramNodeVisitorImpl(
						new HashMap<String, String>(), "arhive", "name")));
	}

}
