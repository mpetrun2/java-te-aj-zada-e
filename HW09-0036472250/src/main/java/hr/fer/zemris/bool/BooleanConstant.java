/**
 * 
 */
package hr.fer.zemris.bool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Unmodifiable class providing constant value
 * <code>TRUE</code> or <code>FALSE</code> depending on initialization.
 *
 */
public class BooleanConstant implements BooleanSource {

	/**
	 * Private variable containing <code>BooleanConstant</code> value.
	 */
	private final BooleanValue value;
	
	/**
	 * Static class instance initialize to hold an instance of constant
	 * always providing value <code>TRUE</code>.
	 */
	public final static BooleanConstant TRUE  = new BooleanConstant(BooleanValue.TRUE);
	
	/**
	 * Static class instance initialize to hold an instance of constant
	 * always providing value <code>FALSE</code>.
	 */
	public final static BooleanConstant FALSE = new BooleanConstant(BooleanValue.FALSE);
	
	/**
	 * Private constructor for <code>BooleanConstant</code> that sets this
	 * value to given value.
	 * @param givenValue given <code>BooleanValue</code> value
	 */
	private BooleanConstant(final BooleanValue givenValue) {
		this.value = givenValue;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanSource#getValue()
	 */
	@Override
	public BooleanValue getValue() {
		return this.value;
	}


	/**
	 * For class <code>BooleanConstant</code> returns empty collection.
	 * @return empty collection({@link List}) of <code>BooleanVariable</code>
	 */
	@Override
	public List<BooleanVariable> getDomain() {
		return new ArrayList<BooleanVariable>();
	}

}
