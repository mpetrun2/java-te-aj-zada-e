package hr.fer.zemris.bool;

/**
 * @author Marin Petrunić 0036472250
 *
 * Interface that represents abstract boolean function.
 * This interface defines few methods that needs to be implemented.
 */
public interface BooleanFunction extends NamedBooleanSource {

	/**
	 * Checks if function contains appropriate minterm.
	 * Function contains minterm <code>index</code> if for inferred value 
	 * assignment function value is TRUE.
	 * @param index specifies index for domain's variables
	 * @return <code>true</code> if contains, <code>false</code> otherwise
	 */
	boolean hasMinterm(int index);
	
	/**
	 * Checks if function contains appropriate maxterm.
	 * Function contains maxterm <code>index</code> if for inferred value assignment 
	 * function value is FALSE.
	 * @param index specifies index for domain's variables
	 * @return <code>true</code> if contains, <code>false</code> otherwise
	 */
	boolean hasMaxterm(int index);
	
	/**
	 * Checks if function contains appropriate Don't care.
	 * @param index specifies index for domain's variables
	 * @return <code>true</code> if contains, <code>false</code> otherwise
	 */
	boolean hasDontCare(int index);
	
	/**
	 * Provides iteration over minterms.
	 * @return
	 */
	Iterable<Integer> mintermIterable();
	
	/**
	 * Provides iteration over maxterms.
	 * @return
	 */
	Iterable<Integer> maxtermIterable();
	
	/**
	 * Provides iteration over dontcares.
	 * @return
	 */
	Iterable<Integer> dontcareIterable();
	
}
