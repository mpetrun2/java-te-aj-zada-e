package hr.fer.zemris.bool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 * <code>BooleanOperator</code> is an abstraction over boolean operators.
 * It has private list of sources based on which final result is calculated.
 */
public abstract class BooleanOperator implements BooleanSource {

	/**
	 * Private list of sources based on which final result is calculated.
	 */
	private List<BooleanSource> sources;
	
	/**
	 * Protected <code>BooleanOperator</code> constructor. Creates new 
	 * <code>BooleanOperator</code> from given list of sources.
	 * @param givenListofSources list of {@link BooleanSource}s
	 */
	protected BooleanOperator(List<BooleanSource> givenListofSources) {
		this.sources = new ArrayList<>(givenListofSources);
	}
	
	/**
	 * @return <code>BooleanOperator</code> list of sources.
	 */
	protected List<BooleanSource> getSources() {
		return new ArrayList<BooleanSource>(this.sources);
	}

	/**
	 * Provides union of domains of every {@link BooleanSource} in
	 * <code>BooleanOperator</code>.
	 * @return list with union of {@link BooleanSource} domains
	 */
	@Override
	public List<BooleanVariable> getDomain() {
		List<BooleanVariable> domains = new ArrayList<>();
		for (BooleanSource source : this.sources) {
			domains.addAll(source.getDomain());
		}
		return domains;
	}

}
