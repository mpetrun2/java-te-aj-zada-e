/**
 * 
 */
package hr.fer.zemris.bool;

import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 * <p>Interface <code>BooleanSource</code> represents any source capable of 
 * producing legal <code>BooleanValue</code>.</p>
 * 
 * <p>It declares that it is capable of producing this value (method <code>getValue()</code>) 
 * and that it can provide an information based on which variables
 * is this value produced (method <code>getDomain()</code>).</p>
 */
public interface BooleanSource {

	/**
	 * @return class value.
	 */
	abstract BooleanValue getValue();
	
	/**
	 * @return information based on which variables is this value produced.
	 */
	abstract List<BooleanVariable> getDomain();
}
