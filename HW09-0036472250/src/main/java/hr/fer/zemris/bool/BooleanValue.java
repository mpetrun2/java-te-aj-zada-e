package hr.fer.zemris.bool;

/**
 * @author Marin Petrunić 0036472250
 *
 * Enum containing 3 possible states of boolean algebra.
 */
public enum BooleanValue {
	TRUE, FALSE, DONT_CARE;
}
