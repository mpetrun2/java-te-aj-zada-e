/**
 * 
 */
package hr.fer.zemris.bool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 * Represents single mask. Each mask will be
 * stored as array of {@link MaskValue}.
 */
public class Mask {
	
	/**
	 * Private variable containing mask as
	 * array of {@link MaskValue}.
	 */
	private  List<MaskValue> mask;
	
	/**
	 * Private constructor from given mask.
	 * @param givenMask
	 */
	private Mask(List<MaskValue> givenMask) {
		this.mask = givenMask;
	}
	
	/**
	 * Parses given <code>String</code> into valid mask.
	 * Allowed signs are '1', '0' and 'x'. If illegal character found
	 * exception will be thrown.
	 * @param maskString String to be parsed
	 * @return instance of <code>Mask</code>
	 * @throws IllegalArgumentException if illegal character 
	 * found in string or string empty
	 */
	public static Mask parse(String maskString) {
		if (maskString.isEmpty()) {
			throw new IllegalArgumentException("String is empty.");
		}
		List<MaskValue> parsedMask = new ArrayList<>(maskString.length());
		for (char character : maskString.toLowerCase().toCharArray()) {
			switch(character) {
				case '1': parsedMask.add(MaskValue.ONE);
							break;
				case '0' : parsedMask.add(MaskValue.ZERO);
							break;
				case 'x' : parsedMask.add(MaskValue.DONT_CARE);
							break;
				default: throw new IllegalArgumentException("String has invalid"
							+ " characters so it can't be parsed as mask.");
			}
		}
		return new Mask(parsedMask);
	}
	
	/**
	 * Creates <code>Mask</code> with given size from given number.
	 * @param size of mask
	 * @param number converted to binary to represent mask
	 * @return new <code>Mask</code>
	 * @throws IllegalArgumentException if size isn't greater than zero
	 */
	public static Mask fromIndex(int size, int number) {
		if (size <= 0) {
			throw new IllegalArgumentException("Mask must have at least 1 value");
		}
		if(number > Math.pow(2, size)-1) {
			throw new IllegalArgumentException("Mask must have at least 1 value");
		}
		List<MaskValue> newMask = new ArrayList<>();
		String binaryNumber = Integer.toBinaryString(number);
		int counter = 0;
		//copy given number to mask
		for (int index = 0; index < binaryNumber.length(); index++,counter++) {
			if(binaryNumber.charAt(index) == '1') {
				newMask.add(MaskValue.ONE);
			} else {
				newMask.add(MaskValue.ZERO);
			}
		}
		//fills extra space with MaskValue.ZERO
		if (counter < size) {
			newMask.addAll(0, new ArrayList<>(Collections.nCopies(size-counter, MaskValue.ZERO)));
		}
		return new Mask(newMask);
	}
	
	/**
	 * Returns list of minterms that are covered with given mask.
	 * @param mask
	 * @return
	 */
	public List<Integer> getMaskMinterms() {
		List<Integer> minterms = new ArrayList<>();
		for(int i = 0, end = (int) Math.pow(2, this.getSize()); i < end; i++) {
			if(this.isMoreGeneralThan(Mask.fromIndex(this.getSize(), i))) {
				minterms.add(i);
			}
		}
		return minterms;
	}

	/**
	 * Constructs Mask from array of {@link MaskValue}s.
	 * @param maskValues
	 * @throws IllegalArgumentException if some value given in mask is null
	 */
	public Mask(MaskValue[] maskValues) {
		this.mask = new ArrayList<MaskValue>(maskValues.length);
		for (int index = 0, end = maskValues.length; index < end; index++) {
			if(maskValues[index] == null) {
				throw new IllegalArgumentException("Given field of mask values contains null values.");
			}
			mask.add(maskValues[index]);
		}
	}
	
	/**
	 * Returns {@link MaskValue} at given index position.
	 * @param index position of requested {@link MaskValue}
	 * @return {@link MaskValue} at given index in mask
	 * @throws IllegalArgumentException if index out of mask range
	 */
	public MaskValue getValue(int index) {
		if (index < 0 || index >= this.mask.size()) {
			throw new IllegalArgumentException("Index out of mask range.");
		}
		return this.mask.get(index);
	}
	
	/**
	 * Checks if given Mask is more general than current mask.
	 * @param givenMask
	 * @return <code>true</code> if it is or <code>false</code> otherwise
	 */
	public boolean isMoreGeneralThan(Mask givenMask) {
		if(this.mask.size() != givenMask.getSize()) {
			return false;
		}
		int difference = 0;
		for (int index = 0, end = this.mask.size(); index < end; index++) {
			if (this.mask.get(index) != givenMask.mask.get(index)) {
				difference++;
				if (this.mask.get(index) != MaskValue.DONT_CARE) {
					return false;
				}
			}
		}
		if (difference > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Combines two mask into one more general.Combination is impossible
	 * if given masks are equal or have more than one difference
	 * @param firstMask
	 * @param secondMask
	 * @return null if they are incompatible or new combined mask
	 */
	public static Mask combine(Mask firstMask, Mask secondMask) {
		if ((firstMask.getSize() != secondMask.getSize())) {
			throw new IllegalArgumentException("Masks must be not null and equal in size.");
		}
		List<MaskValue> newMask = new ArrayList<>(firstMask.getSize());
		int difference = 0;
		for (int index = 0; index < firstMask.getSize(); index++) {
			MaskValue firstvalue = firstMask.getValue(index);
			MaskValue secondvalue = secondMask.getValue(index);
			if (firstvalue != secondvalue) {
				if(firstvalue == MaskValue.DONT_CARE && secondvalue != MaskValue.DONT_CARE) {
					return null;//incompatible
				}
				if(secondvalue == MaskValue.DONT_CARE && firstvalue != MaskValue.DONT_CARE) {
					return null;//incompatible
				}
				newMask.add(MaskValue.DONT_CARE);
				difference++;
			} else {
				newMask.add(firstvalue);
			}
		}
		//combination is impossible if there is more than 1 difference
		// null is returned if two masks are equals
		if(difference > 1 || difference == 0) {
			return null;
		}
		return new Mask(newMask);
	}
	
	/**
	 * Caunt's number of <code>MaskValue.ZERO</code>s.
	 * @return number of zeroes in mask
	 */
	public int getNumberOfZeros() {
		return this.countValues(MaskValue.ZERO);
	}
	
	/**
	 * Caunt's number of <code>MaskValue.ONE</code>s.
	 * @return number of ones in mask
	 */
	public int getNumberOfOnes() {
		return this.countValues(MaskValue.ONE);
	}
	
	/**
	 * Caunt's number of <code>MaskValue.DONT_CARE</code>s.
	 * @return number of dont_cares in mask
	 */
	public int getNumberOfDontCares() {
		return this.countValues(MaskValue.DONT_CARE);
	}
	
	/**
	 * Counts number of given <code>MaskValue</code>.
	 * @return number of hits
	 */
	private int countValues(MaskValue cauntedValue) {
		int count = 0;
		for (MaskValue value : this.mask) {
			if (value == cauntedValue) {
				count++;
			}
		}
		return count;
	}

	/**
	 * @return number of values in mask.
	 */
	public int getSize() {
		return this.mask.size();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mask == null) ? 0 : mask.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Mask)) {
			return false;
		}
		Mask other = (Mask) obj;
		if (mask == null) {
			if (other.mask != null) {
				return false;
			}
		} else if (!mask.equals(other.mask)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		String mask = "[ ";
		for(MaskValue val : this.mask) {
			switch(val) {
			case ONE: mask+= 1;
			break;
			case ZERO: mask+= 0;
			break;
			case DONT_CARE: mask+= "X";
			break;
			}
			//mask += val+" ";
		}
		return mask+" ]";
	}
	
	
	
}
