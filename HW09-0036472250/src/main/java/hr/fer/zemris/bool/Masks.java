/**
 * 
 */
package hr.fer.zemris.bool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marin Petrunić 0036472250
 *
 * Represents factory class for instancing
 * multiple {@link Mask}s.
 */
public class Masks {

	private Masks(){}
	
	/**
	 * Provides instance of {@link Mask} for each
	 * number given as param.
	 * @param size of mask
	 * @param numbers user defined number of arguments for masks
	 * @return list of Mask instances
	 */
	public static List<Mask> fromIndexes(int size, int ... numbers) {
		List<Mask> masks = new ArrayList<>();
		for (int number : numbers) {
			masks.add(Mask.fromIndex(size, number));
		}
		return masks;
	}
	
	/**
	 * Provides instance of {@link Mask} for each
	 * <code>String</code> given as param.
	 * @param strings to be parsed
	 * @return list of Mask instances
	 */
	public static List<Mask> fromStrings(String ... strings) {
		List<Mask> masks = new ArrayList<>();
		for(String string : strings) {
			masks.add(Mask.parse(string));
		}
		return masks;
	}
	
}
