package hr.fer.zemris.bool;

/**
 * @author Marin Petrunić 0036472250
 *
 * Interface <code>NamedBooleanSource</code> represents a source with associated name.
 */
public interface NamedBooleanSource extends BooleanSource {

	/**
	 * @return source associated name.
	 */
	String getName();
}
