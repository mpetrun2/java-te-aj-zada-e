/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import java.util.Collections;
import java.util.List;

import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.MaskValue;
import hr.fer.zemris.bool.opimpl.BooleanOperators;

/**
 * @author Marin Petrunić 0036472250
 * Abstraction over Boolean Functions while providing
 * implementation of some common functions.
 */
abstract class BasicBF implements BooleanFunction {

	/**
	 * <code>String</code> variable containing
	 * function name.
	 */
	 protected String name;
	
	/**
	 * Private variable containing list of variables.
	 */
	protected List<BooleanVariable> domain;
	
	/**
	 * @return function name
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * @return set of variables in function as List.
	 */
	@Override
	public List<BooleanVariable> getDomain() {
		return Collections.unmodifiableList(this.domain);
	}

	/**
	 * Calculates minterm value with given function variable values.
	 * Example:
	 * <p>
	 * f(B, C, A)<br>
	 * B = 1, C =0, A = 0;<br>
	 * minterm 3 value = NOT B AND C AND A
	 * 				   = NOT 1 AND 0 AND 0
	 * 				   = 0;
	 * </p>
	 * @param mintermIndex
	 * @return {@link BooleanVariable} with resulting value
	 */
	protected BooleanVariable mintermValueVariable(Mask mintermMask) {
		BooleanVariable result = new BooleanVariable("mintermValue");
		result.setValue(BooleanValue.TRUE);
		for (int index = 0, end = mintermMask.getSize(); index < end; index++) {
			if (mintermMask.getValue(index) == MaskValue.ONE) {
				result.setValue(BooleanOperators.and(
						new BooleanVariable[] {
								result,
								this.domain.get(index)
						}).getValue());
			} else {
				result.setValue(BooleanOperators.and(
						new BooleanSource[] {
								result,
								BooleanOperators.not(this.domain.get(index))
						}).getValue());
			}
		}
		return result;
	}

}
