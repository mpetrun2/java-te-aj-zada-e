/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.opimpl.BooleanOperators;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class MaskBasedBF extends BasicBF {
	
	/**
	 * True if masks are minterms of false
	 * if masks are maxterms.
	 */
	private boolean masksAreMinterms;
	
	/**
	 * Private variable with list of masks.
	 */
	private List<Mask> masks;
	
	/**
	 * Private variable with list of DONT_CARE masks.
	 */
	private List<Mask> dontCareMasks;
	
	/**
	 * Constructs new mask based boolean function from
	 * given parameters. List of Masks should not contain same mask as
	 * list of Don't_Care masks.
	 * @param functionName name of function
	 * @param domainList list of function variables
	 * @param areMinterms if list contains minterms or maxterms
	 * @param masksList list of masks
	 * @param dontCareMaskList list of don't_care masks
	 * @throws IllegalArgumentException if list of minterms/maxterms contains same masks as
	 * list of DONT_CARES
	 */
	public MaskBasedBF(String functionName, List<BooleanVariable> domainList, 
			boolean areMinterms, List<Mask> masksList, List<Mask> dontCareMaskList) {
		this.name = functionName;
		this.domain = new ArrayList<BooleanVariable>(domainList);
		this.masksAreMinterms = areMinterms;
		this.masks = new ArrayList<>(masksList);
		this.dontCareMasks = new ArrayList<>(dontCareMaskList);
		for (Mask m : this.dontCareMasks) {
			if (this.masks.contains(m)) {
				throw new IllegalArgumentException("Mask of minterm/maxterm and don't_care is the same.");
			}
		}
	}

	/**
	 * Function value is value of function defined with
	 * minterms/maxterms and value of variables given to constructor.
	 * @return function value.
	 */
	@Override
	public BooleanValue getValue() {
		return this.sumOfMinterms();
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMinterm(int)
	 */
	@Override
	public boolean hasMinterm(int index) {
		Mask createdMask = Mask.fromIndex(this.domain.size(), index);
		//if in list of minterms
		if (this.masksAreMinterms) {
			for (Mask m : this.masks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return true;
				}
			}
			return false;
		//if it isn't in list of maxterms or list of don't cares
		} else {
			for (Mask m : this.masks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return false;
				}
			}
			for (Mask m : this.dontCareMasks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return false;
				}
			}
			return true;
		}
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMaxterm(int)
	 */
	@Override
	public boolean hasMaxterm(int index) {
		Mask createdMask = Mask.fromIndex(this.domain.size(), index);
		//if in list of maxterms
		if (!this.masksAreMinterms) {
			for (Mask m : this.masks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return true;
				}
			}
			return false;
		//if it isn't in list of minterms or list of don't cares
		} else {
			for (Mask m : this.masks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return false;
				}
			}
			for (Mask m : this.dontCareMasks) {
				if (m.isMoreGeneralThan(createdMask)) {
					return false;
				}
			}
			return true;
		}
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasDontCare(int)
	 */
	@Override
	public boolean hasDontCare(int index) {
		for (Mask m : this.dontCareMasks) {
			if (m.isMoreGeneralThan(Mask.fromIndex(this.domain.size(), index))) {
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#mintermIterable()
	 */
	@Override
	public Iterable<Integer> mintermIterable() {
		List<Integer> iteration= new ArrayList<>();
		if (this.masksAreMinterms) {
			iteration = this.maskToIntegerList(this.masks);
		} else {
			for (int index = 0, end = (int) Math.pow(this.domain.size(),2) - 1;
					index < end; index++) {
				if (this.hasMinterm(index)) {
					iteration.add(index);
				}	
			}
		}
		return iteration;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#maxtermIterable()
	 */
	@Override
	public Iterable<Integer> maxtermIterable() {
		List<Integer> iteration= new ArrayList<>();
		if (this.masksAreMinterms) {
			for (int index = 0, end = (int) Math.pow(this.domain.size(),2) - 1;
					index < end;index++) {
				if (this.hasMaxterm(index)) {
					iteration.add(index);
				}	
			}
		} else {
			iteration = this.maskToIntegerList(this.masks);
		}
		return iteration;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#dontcareIterable()
	 */
	@Override
	public Iterable<Integer> dontcareIterable() {
		return this.maskToIntegerList(this.dontCareMasks);
	}
	
	/**
	 * Calculates minterm value with given function variable values.
	 * Example:
	 * <p>
	 * f(B, C, A)<br>
	 * B = 1, C =0, A = 0;<br>
	 * minterm 3 value = NOT B AND C AND A
	 * 				   = NOT 1 AND 0 AND 0
	 * 				   = 0;
	 * </p>
	 * @param mintermIndex
	 * @return {@link BooleanVariable} with resulting value
	 */
	private BooleanVariable mintermValue(Integer minterm) {
		Mask mintermMask = Mask.fromIndex(this.domain.size(), minterm);
		return this.mintermValueVariable(mintermMask);
	}
	
	/**
	 * Calculates sum of given minterms.
	 * @return value of sum of given minterms with values of function
	 * variables
	 */
	private BooleanValue sumOfMinterms() {
		BooleanVariable result = new BooleanVariable("result");
		for (Integer mintermIndex : this.mintermIterable()) {
			result.setValue(
					BooleanOperators.or(new BooleanVariable[] {
							result,
							this.mintermValue(mintermIndex)
					}).getValue()
			);
		}
		return result.getValue();
	}
	
	/**
	 * Converts all masks to integer
	 * @param masks
	 * @return
	 */
	private List<Integer> maskToIntegerList(List<Mask> masks) {
		List<Integer> list= new ArrayList<>();
		for (int i = 0, end = (int) Math.pow(this.domain.size(), 2); i < end; i++) {
			for(Mask m : masks) {
				if(m.isMoreGeneralThan(Mask.fromIndex(this.domain.size(), i)) || m.getNumberOfDontCares() == 0) {
					list.add(i);
				}
			}
		}
		Collections.sort(list);
		return list;
	}
	
	/**
	 * Returns unmodifiable list of given minterm masks
	 * or maxterm masks depending on <code>areMinterms</code>
	 * flag in contructor.
	 * @return {@link List} of masks
	 */
	public List<Mask> getMasks() {
		return Collections.unmodifiableList(masks);
	}
	
	/**
	 * Returns unmodifiable list of given dontCare masks.
	 * @return {@link List} of dont care masks.
	 */
	public List<Mask> getDontCareMasks() {
		return Collections.unmodifiableList(dontCareMasks);
	}
	
	/** 
	 * @return true if masks are products, false otherwise.
	 */
	public boolean areMasksProducts() {
		return !masksAreMinterms;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dontCareMasks == null) ? 0 : dontCareMasks.hashCode());
		result = prime * result + ((masks == null) ? 0 : masks.hashCode());
		result = prime * result + (masksAreMinterms ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MaskBasedBF)) {
			return false;
		}
		MaskBasedBF other = (MaskBasedBF) obj;
		if (dontCareMasks == null) {
			if (other.dontCareMasks != null) {
				return false;
			}
		} else if (!dontCareMasks.equals(other.dontCareMasks)) {
			return false;
		}
		if (masks == null) {
			if (other.masks != null) {
				return false;
			}
		} else if (!masks.equals(other.masks)) {
			return false;
		}
		if (masksAreMinterms != other.masksAreMinterms) {
			return false;
		}
		return true;
	}
	
	

}
