/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.MaskValue;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class OperatorTreeBF extends BasicBF {
	
	/**
	 * Private variable containing Boolean operator expression.
	 */
	private BooleanOperator operatorTree;
	
	/**
	 * Constructs new OperatorTreeBF from given parameters.
	 * Operator variables must have dependencies in domain of 
	 * OperatorTreeBF.
	 * @param functionName
	 * @param domainList
	 * @param operator
	 * @throws IllegalArgumentException if number of variables 
	 * in BooleanOperator is not supported
	 */
	public OperatorTreeBF(String functionName,
			List<BooleanVariable> domainList, BooleanOperator operator) {
		this.name = functionName.trim();
		if (!domainList.containsAll(operator.getDomain())) {
			throw new IllegalArgumentException("That many variables is not supported.");
		}
		this.domain = new ArrayList<BooleanVariable>();
		for(BooleanVariable var : domainList) {
			BooleanVariable temp = new BooleanVariable(var.getName());
			temp.setValue(var.getValue());
			this.domain.add(var);
		}
		this.operatorTree = operator; 
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMinterm(int)
	 */
	@Override
	public boolean hasMinterm(int index) {
		this.setVariables(index);
		if(this.operatorTree.getValue() == BooleanValue.TRUE) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasMaxterm(int)
	 */
	@Override
	public boolean hasMaxterm(int index) {
		this.setVariables(index);
		if(this.operatorTree.getValue() == BooleanValue.FALSE) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#hasDontCare(int)
	 */
	@Override
	public boolean hasDontCare(int index) {
		this.setVariables(index);
		if(this.operatorTree.getValue() == BooleanValue.DONT_CARE) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#mintermIterable()
	 */
	@Override
	public Iterable<Integer> mintermIterable() {
		List<Integer> iteration = new ArrayList<>();
		for (int number = 0, end = (int) Math.pow(this.operatorTree.getDomain().size(), 2) - 1;
				number < end; number++) {
			if (this.hasMinterm(number)) {
				iteration.add(number);
			}
		}
		return iteration;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#maxtermIterable()
	 */
	@Override
	public Iterable<Integer> maxtermIterable() {
		List<Integer> iteration = new ArrayList<>();
		for (int number = 0, end = (int) Math.pow(this.operatorTree.getDomain().size(), 2) - 1;
				number < end; number++) {
			if (this.hasMaxterm(number)) {
				iteration.add(number);
			}
		}
		return iteration;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.bool.BooleanFunction#dontcareIterable()
	 */
	@Override
	public Iterable<Integer> dontcareIterable() {
		List<Integer> iteration = new ArrayList<>();
		for (int number = 0, end = (int) Math.pow(this.operatorTree.getDomain().size(), 2) - 1;
				number < end; number++) {
			if (this.hasDontCare(number)) {
				iteration.add(number);
			}
		}
		return iteration;
	}

	/**
	 * @return value of this function
	 */
	@Override
	public BooleanValue getValue() {
		for(BooleanVariable opVariable : this.operatorTree.getDomain()) {
			for(BooleanVariable domVariable : this.domain) {
				if(opVariable.getName().equals(domVariable.getName())) {
					opVariable.setValue(domVariable.getValue());
				}
			}
		}
		return this.operatorTree.getValue();
	}
	
	/**
	 * Sets variables to corresponding values from binary
	 * form of given number.
	 * @param number
	 */
	private void setVariables(int number) {
		Mask binaryNumberMask = Mask.fromIndex(this.operatorTree.getDomain().size(), number);
		for (int varIndex = 0, end = this.operatorTree.getDomain().size();
				varIndex < end; varIndex++) {
			for (BooleanVariable var : this.operatorTree.getDomain()) {
				if (var.getName().equals(this.domain.get(varIndex).getName())) {
					if (binaryNumberMask.getValue(varIndex) == MaskValue.ONE) {
						var.setValue(BooleanValue.TRUE);
					}
					if (binaryNumberMask.getValue(varIndex) == MaskValue.ZERO) {
						var.setValue(BooleanValue.FALSE);
					}
				}
			}
		}
	}
}
