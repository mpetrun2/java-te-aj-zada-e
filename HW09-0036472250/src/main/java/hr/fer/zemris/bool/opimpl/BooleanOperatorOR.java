/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import java.util.List;

import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class representing BooleanOperator OR.
 */
public class BooleanOperatorOR extends BooleanOperator {

	/**
	 * Protected <code>BooleanOperator</code> constructor. Creates new 
	 * <code>BooleanOperator</code> from given list of sources.
	 * @param givenListofSources
	 */
	public BooleanOperatorOR(List<BooleanSource> givenListofSources) {
		super(givenListofSources);
	}

	/**
	 * Applying operator OR on given {@link BooleanSource}s.
	 * If some <code>BooleanSource</code> is equal to {@link BooleanValue} TRUE
	 * method will return TRUE, otherwise will return FALSE.
	 * return {@link BooleanValue} TRUE or FALSE
	 */
	@Override
	public BooleanValue getValue() {
		List<BooleanSource> sources = this.getSources();
		BooleanValue answerValue = sources.get(0).getValue();
		for(BooleanSource source : sources) {
			answerValue = this.or(answerValue, source.getValue());
		}
		return answerValue;
	}
	
	/**
	 * Determines result value from first OR second.
	 * @param first
	 * @param second
	 * @return {@link BooleanValue} result
	 */
	private BooleanValue or(BooleanValue first, BooleanValue second) {
		if(first == BooleanValue.FALSE) {
			switch(second) {
				case TRUE: return BooleanValue.TRUE;
				case FALSE: return BooleanValue.FALSE;
				default: return BooleanValue.DONT_CARE;
			}
		}
		if(first == BooleanValue.DONT_CARE) {
			switch(second) {
				case TRUE: return BooleanValue.TRUE;
				case FALSE: return BooleanValue.DONT_CARE;
				default: return BooleanValue.DONT_CARE;
			}
		}
		return BooleanValue.TRUE;
	}

}
