/**
 * 
 */
package hr.fer.zemris.bool.qmc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.fimpl.MaskBasedBF;

/**
 * Class for minimizing given {@link BooleanFunction}
 * using Quin-MCCluskey method.
 * FUnction is considered minimal if it has
 * least products in her sum and not by number of variables.
 * @author Marin Petrunić 0036472250
 * 
 */
public class QMCMinimizer {

	/**
	 * Original {@link BooleanFunction} to be minimized.
	 */
	private BooleanFunction bf;

	/**
	 * Flag for determination whether results will be returned as products or
	 * sums.
	 */
	private boolean resultsASProducts;

	/***
	 * Map containing possible function mask, if possible masks will be
	 * combined. Base map contains number of TRUE flags in mask and value is
	 * another map containing mask as key and boolean flag as value. Flag is
	 * mark of source if source was don't care flag will be true, false
	 * otherwise.
	 */
	private Map<Integer, Map<Mask, Boolean>> combinationTable;

	/**
	 * Table with minterms as keys and sets of masks that covers
	 * corresponding minterm.
	 */
	private Map<Integer, Set<Mask>> coverageTable;

	/**
	 * List of prim implicants.
	 * Prim implicants are separated from rest
	 * for optimizing implicants combination finding.
	 */
	private Set<Mask> primImplicants;

	/**
	 * All possible combinations of implicants.
	 */
	private Set<Set<Mask>> implicantsCombination = new HashSet<>();

	/**
	 * Constructor.
	 * 
	 * @param bf
	 *            {@link BooleanFunction} to be minimized
	 * @param resultaAsProducts
	 *            flag for determination whether result will be returned as
	 *            products or sums.
	 */
	public QMCMinimizer(BooleanFunction bf, boolean resultaAsProducts) {
		if (bf == null) {
			throw new IllegalArgumentException(
					"Expected valid BooleanFunction.");
		}
		this.bf = bf;
		this.resultsASProducts = resultaAsProducts;
		fillCombinationTable();
		reduceTable();
		createCoverageTable();
		findPrimImplicants();
		getAllImplicantsCombination(new HashSet<Mask>(), new HashMap<>(
				coverageTable));
		reduceImplicantsCombination();
	}

	/**
	 * Method is filtering combination of implicants by leaving 
	 * only combinations with smallest number of masks.
	 */
	private void reduceImplicantsCombination() {
		boolean reduced = true;
		int minSize = Integer.MAX_VALUE;
		while(reduced) {
			reduced = false;
			Iterator<Set<Mask>> it = implicantsCombination.iterator();
			while(it.hasNext()) {
				Set<Mask> set = it.next();
				if(set.size() <= minSize) {
					minSize = set.size();
				} else {
					it.remove();
					reduced = true;
				}
			}
		}
	}

	/**
	 * Method is passing recursively through map of minterm coverage
	 * and creates all possible combination of implicants that covers given function.
	 * FOr better optimization it is recommended to remove primImplicants from table first.
	 * @param maskList
	 * @param coverageTable2
	 */
	private void getAllImplicantsCombination(HashSet<Mask> maskList,
			Map<Integer, Set<Mask>> coverageTable2) {
		if (coverageTable2.isEmpty()) {
			maskList.addAll(primImplicants);
			implicantsCombination.add(maskList);
			return;
		}
		Entry<Integer, Set<Mask>> entry = coverageTable2.entrySet().iterator().next();
		coverageTable2.remove(entry.getKey());
		for(Mask mask: entry.getValue()) {
			HashSet<Mask> newMasksList = new HashSet<>(maskList);
			newMasksList.add(mask);
			Map<Integer, Set<Mask>> newCoverageMap = new HashMap<>(coverageTable2);
			newCoverageMap = removeCoveredMinterms(newCoverageMap, mask);
			getAllImplicantsCombination(newMasksList, newCoverageMap);
		}
	}

	/**
	 * Find prim implicants in table and adds them to set of prim implicants.
	 */
	private void findPrimImplicants() {
		primImplicants = new HashSet<>();
		Iterator<Entry<Integer, Set<Mask>>> it = coverageTable.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry<Integer, Set<Mask>> entry = it.next();
			if (entry.getValue().size() == 1) {
				primImplicants.addAll(entry.getValue());
				it.remove();
			}
		}
		coverageTable = removeCoveredMinterms(coverageTable, primImplicants);
	}

	/**
	 * Removes all minterms covered by masks in given list.
	 * 
	 * @param implicants
	 *            list of mask that covers minterms
	 */
	private Map<Integer, Set<Mask>> removeCoveredMinterms(Map<Integer, Set<Mask>> coverageTable2, Set<Mask> implicants) {
		for (Mask mask : implicants) {
			coverageTable2 = removeCoveredMinterms(coverageTable2, mask);
		}
		return coverageTable2;
	}

	/**
	 * Removes all minterms that are covered with given mask.
	 * 
	 * @param mask
	 *            that covers minterms
	 */
	private Map<Integer, Set<Mask>> removeCoveredMinterms(Map<Integer, Set<Mask>> coverageTable2,Mask mask) {
		Iterator<Entry<Integer, Set<Mask>>> it = coverageTable2.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry<Integer, Set<Mask>> entry = it.next();
			if (entry.getValue().contains(mask)) {
				it.remove();
			}
		}
		return coverageTable2;
	}

	/**
	 * Creates coverage table and fills it with masks at coresponding keys.
	 */
	private void createCoverageTable() {
		coverageTable = new HashMap<>();
		Set<Mask> implicants = getImplicants();
		for (Mask mask : implicants) {
			List<Integer> minterms = mask.getMaskMinterms();
			for (Integer minterm : minterms) {
				if (!coverageTable.containsKey(minterm)) {
					coverageTable.put(minterm,
							new HashSet<Mask>(Arrays.asList(mask)));
				} else {
					coverageTable.get(minterm).add(mask);
				}
			}
		}
	}

	/**
	 * Iterates through combination table and returns set with implicants.
	 * 
	 * @return set with implicants.
	 */
	private Set<Mask> getImplicants() {
		Set<Mask> implicants = new HashSet<>();
		for (int i = 0; i < combinationTable.size(); i++) {
			Map<Mask, Boolean> entry = combinationTable.get(i);
			for (Entry<Mask, Boolean> mask : entry.entrySet()) {
				implicants.add(mask.getKey());
			}
		}
		return implicants;
	}

	/**
	 * Creates new combination table and initialize it. Table is then filled
	 * with given {@link BooleanFunction} minterm {@link Mask}s.
	 */
	private void fillCombinationTable() {
		combinationTable = new LinkedHashMap<>();
		for (int i = 0, end = bf.getDomain().size(); i <= end; i++) {
			combinationTable.put(i, new LinkedHashMap<Mask, Boolean>());
		}
		for (Integer minterm : bf.mintermIterable()) {
			Mask m = Mask.fromIndex(bf.getDomain().size(), minterm);
			combinationTable.get(m.getNumberOfOnes()).put(m, false);
		}
		for (Integer dontcare : bf.dontcareIterable()) {
			Mask m = Mask.fromIndex(bf.getDomain().size(), dontcare);
			combinationTable.get(m.getNumberOfOnes()).put(m, true);
		}
		return;
	}

	/**
	 * Reduces combination table by combining combinable masks. Reduction is
	 * ended when only non-combinable masks are left.
	 */
	private void reduceTable() {
		boolean combined = true;
		while (combined) {
			combined = false;
			for (int i = 0, end = combinationTable.size(); i < end - 1; i++) {
				Map<Mask, Boolean> firstMap = combinationTable.get(i);
				Map<Mask, Boolean> secondMap = combinationTable.get(i + 1);
				combined = combineMasks(firstMap, secondMap) || combined;
				if (i == end - 2 && secondMap != null) {
					removeAllCoveredMasks();
					if (secondMap.isEmpty()) {
						combinationTable.remove(i + 1);
					}
				}
			}
		}
		removeDontCares();
		removeAllCoveredMasks();
	}

	/**
	 * Iterates through combination table and removes masks that are already
	 * covered.
	 */
	private void removeAllCoveredMasks() {
		for (int i = 0; i < combinationTable.size() - 1; i++) {
			removeCoveredMasks(combinationTable.get(i),
					combinationTable.get(i + 1));
		}
	}

	/**
	 * Method trys to combine every mask from first map with every mask in
	 * second map. If mask can combine new mask replaces mask in first map.
	 * 
	 * @param firstMap
	 * @param secondMap
	 */
	private boolean combineMasks(Map<Mask, Boolean> firstMap,
			Map<Mask, Boolean> secondMap) {
		Map<Mask, Boolean> newMasks = new HashMap<>();
		Iterator<Entry<Mask, Boolean>> it = firstMap.entrySet().iterator();
		boolean combined = false;
		while (it.hasNext()) {
			Entry<Mask, Boolean> mask1 = it.next();
			if (secondMap != null) {
				boolean remove = false;
				for (Entry<Mask, Boolean> mask2 : secondMap.entrySet()) {
					Mask newMask = Mask.combine(mask1.getKey(), mask2.getKey());
					if (newMask != null) {
						remove = true;
						if (!newMasks.containsKey(newMask)) {
							newMasks.put(newMask,
									mask1.getValue() && mask2.getValue());
						}
						combined = true;
					}
				}
				if (remove) {
					it.remove();
				}
			}
		}
		firstMap.putAll(newMasks);
		return combined;
	}

	/**
	 * Removes covered masks from second Map
	 * 
	 * @param firstMap
	 * @param secondMap
	 */
	private void removeCoveredMasks(Map<Mask, Boolean> firstMap,
			Map<Mask, Boolean> secondMap) {
		if (secondMap != null) {
			Iterator<Entry<Mask, Boolean>> it = secondMap.entrySet().iterator();
			while (it.hasNext()) {
				Entry<Mask, Boolean> m = it.next();
				for (Entry<Mask, Boolean> mask : firstMap.entrySet()) {
					if (mask.getKey().isMoreGeneralThan(m.getKey())) {
						it.remove();
						break;
					}
				}
			}
		}
	}

	/**
	 * Removes don't care masks in combination table.
	 */
	private void removeDontCares() {
		Iterator<Entry<Integer, Map<Mask, Boolean>>> tableIterator = combinationTable
				.entrySet().iterator();
		while (tableIterator.hasNext()) {
			Entry<Integer, Map<Mask, Boolean>> tableEntry = tableIterator
					.next();
			Iterator<Entry<Mask, Boolean>> tableEntrysIterator = tableEntry
					.getValue().entrySet().iterator();
			while (tableEntrysIterator.hasNext()) {
				Entry<Mask, Boolean> entry = tableEntrysIterator.next();
				if (entry.getValue()) {
					tableEntrysIterator.remove();
				}
			}
		}
	}
	
	/**
	 * @return List of minimized {@link MaskBasedBF} functions.
	 */
	public List<MaskBasedBF> getListOfMinimizedFunctions() {
		List<MaskBasedBF> functions = new ArrayList<>();
		for(Set<Mask> combination : implicantsCombination) {
			List<Mask> masks = new ArrayList<>(combination);
			MaskBasedBF f = new MaskBasedBF("f", bf.getDomain(), !resultsASProducts, masks, new ArrayList<Mask>());
			functions.add(f);
		}
		return functions;
	}
	
	/**
	 * Minimizes function and returns array of minimized
	 * functions.
	 * @param f function to be minimized
	 * @param resultAsProducts result as products
	 * @return Array of {@link MaskBasedBF}
	 */
	public static MaskBasedBF[] minimize(BooleanFunction f, boolean resultAsProducts) {
		QMCMinimizer minimizer = new QMCMinimizer(f, resultAsProducts);
		List<MaskBasedBF> minimizedFunctions = minimizer.getListOfMinimizedFunctions();
		MaskBasedBF[] minFunctions = new MaskBasedBF[minimizedFunctions.size()];
		minimizedFunctions.toArray(minFunctions);
		return minFunctions;
	}

}
