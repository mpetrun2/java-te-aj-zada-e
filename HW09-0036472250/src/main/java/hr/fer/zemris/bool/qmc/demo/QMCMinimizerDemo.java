package hr.fer.zemris.bool.qmc.demo;

import java.util.ArrayList;
import java.util.Arrays;

import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.fimpl.IndexedBF;
import hr.fer.zemris.bool.fimpl.MaskBasedBF;
import hr.fer.zemris.bool.qmc.QMCMinimizer;

/**
 * Program for demonstration of {@link QMCMinimizer}.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class QMCMinimizerDemo {

	/**
	 * Program does not need arguments.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		BooleanFunction f1 = new IndexedBF(
				"f1",
				new ArrayList<>(Arrays.asList(new BooleanVariable("A"),
						new BooleanVariable("B"), new BooleanVariable("C"),
						new BooleanVariable("D"))),
				true,
				new ArrayList<>(Arrays.asList(1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14)),
				new ArrayList<Integer>());
		boolean želimoDobitiProdukte = true;
		long t0 = System.currentTimeMillis();
		MaskBasedBF[] fje = QMCMinimizer.minimize(f1, želimoDobitiProdukte);
		long t1 = System.currentTimeMillis();
		System.out.println("Vrijeme: " + (t1-t0)+" ms");
		System.out.println("Minimalnih oblika ima: " + fje.length);
		for(MaskBasedBF f : fje) {
			System.out.println("Mogući minimalni oblik:");
			for(Mask m : f.getMasks()) {
				System.out.print(" " + m + " ");
			}
			System.out.println();
		}
	}

}
