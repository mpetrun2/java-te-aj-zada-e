/**
 * 
 */
package hr.fer.zemris.java.sorters;

/**
 * Implementacija algoritna za quicksort koji
 * paralelne poslove obavlja u paralelnim dretvama.
 * 
 * @author Marin Petrunić 0036472250
 *
 */
public class QSortParallel {
	/**
	 * Prag koji govori koliko elemenata u podpolju minimalno mora biti da bi se
	 * sortiranje nastavilo paralelno; ako elemenata ima manje, algoritam
	 * prelazi na klasično rekurzivno (slijedno) sortiranje.
	 */
	private final static int P_THRESHOLD = 6000;
	/**
	 * Prag za prekid rekurzije. Ako elemenata ima više od ovoga, quicksort
	 * nastavlja rekurzivnu dekompoziciju. U suprotnom ostatak sortira
	 * algoritmom umetanja.
	 */
	private final static int CUT_OFF = 5;

	/**
	 * Sučelje prema klijentu: prima polje i vraća se tek kada je polje
	 * sortirano. Primjenjujući gornje pragove najprije posao paralelizira a
	 * kada posao postane dovoljno mali, rješava ga slijedno.
	 * 
	 * @param array
	 *            polje koje treba sortirati
	 */
	public static void sort(int[] array) {
		new QSortJob(array, 0, array.length - 1).run();
	}

	/**
	 * Model posla sortiranja podpolja čiji su elementi na pozicijama koje su
	 * veće ili jednake <code>startIndex</code> i manje ili jednake
	 * <code>endIndex</code>.
	 */
	static class QSortJob implements Runnable {
		private int[] array;
		private int startIndex;
		private int endIndex;

		/**
		 * Konstruktor.
		 * @param array niz koji ce bit sortiran
		 * @param startIndex početni index niza
		 * @param endIndex završni index niza 
		 */
		public QSortJob(int[] array, int startIndex, int endIndex) {
			super();
			this.array = array;
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}

		@Override
		public void run() {
			if(endIndex-startIndex+1 > CUT_OFF) {
				boolean doInParallel = endIndex-startIndex+1 > P_THRESHOLD;
				int i = selectPivot();
				swap(array, i, endIndex);
				int pivot = array[endIndex];
				int leftPointer = startIndex;
				int rightPointer = endIndex-1;
				while(true) {
					while(leftPointer < rightPointer && array[leftPointer] < pivot) ++leftPointer;
					while(leftPointer < rightPointer && array[rightPointer] > pivot) --rightPointer;
					if(leftPointer >= rightPointer) break;
					swap(array, leftPointer++, rightPointer--);
				}
				if(array[leftPointer] > pivot) {
					swap(array, leftPointer, endIndex);
				} else {
					leftPointer++;
					swap(array, leftPointer, endIndex);
				}
				Thread t1 = null;
				if(startIndex < leftPointer) {
					QSortJob job = new QSortJob(array, startIndex, leftPointer-1);
					t1 = executeJob(doInParallel, job);
				}
				Thread t2 = null;
				if(endIndex > leftPointer) {
					QSortJob job = new QSortJob(array, leftPointer+1, endIndex);
					t2 = executeJob(doInParallel, job);
				}
				
				if(t1 != null && t1.isAlive()) {
					try {
						t1.join();
					} catch (InterruptedException ignorable) {
						
					}
				}
				
				if(t2 != null && t2.isAlive()) {
					try {
						t2.join();
					} catch (InterruptedException ignorable) {
						
					}
				}
				
			} else {
				doInsertionSort(array, startIndex, endIndex);
			}
		}

		/**
		 * Sortira polje pomoću "Insertion sort" metode.
		 * @param array polje koje treba sortirati
		 * @param startIndex pocetni index polja
		 * @param endIndex krajnji index polja
		 */
		private void doInsertionSort(int[] array, int startIndex,
				int endIndex) {
			for(int i = startIndex+1; i <= endIndex; ++i) {
				for(int j = i; j > 0 && array[j-1] > array[j]; --j) {
					swap(array, j, j-1);
				}
			}
			
		}

		/**
		 * Direktno izvodi zadani posao pozivom run() i tada vraća
		 * <code>null</code> ili pak stvara novu dretvu, njoj daje taj posao i
		 * pokreće je te vraća referencu na stvorenu dretvu (u tom slučaju ne
		 * čeka da posao završi).
		 * 
		 * @param doInParallel
		 *            treba li posao pokrenuti u novoj dretvi
		 * @param job
		 *            posao
		 * @return <code>null</code> ili referencu na pokrenutu dretvu
		 */
		private Thread executeJob(boolean doInParallel, QSortJob job) {
			if(doInParallel) {
				Thread jobThread = new Thread(job);
				jobThread.start();
				return jobThread;
			}
			job.run();
			return null;
		}

		/**
		 * Odabir pivota metodom medijan-od-tri u dijelu polja
		 * <code>array</code> koje je ograđeno indeksima <code>startIndex</code>
		 * i <code>endIndex</code> (oba uključena).
		 * 
		 * @return vraća indeks na kojem se nalazi odabrani pivot
		 */
		public int selectPivot() {
			int  middle = (startIndex+endIndex)/2;
			if(array[startIndex] > array[middle]) {
				swap(array, startIndex, middle);
			}
			if(array[startIndex] > array[endIndex]) {
				swap(array, startIndex,	endIndex);
			}
			if(array[middle] > array[endIndex]) {
				swap(array, middle, endIndex);
			}
			return middle;
		}

		/**
		 * U predanom polju <code>array</code> zamjenjuje elemente na pozicijama
		 * <code>i</code> i <code>j</code>.
		 * 
		 * @param array
		 *            polje u kojem treba zamijeniti elemente
		 * @param i
		 *            prvi indeks
		 * @param j
		 *            drugi indeks
		 */
		public static void swap(int[] array, int i, int j) {
			int tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * Pomoćna metoda koja provjerava je li predano polje doista sortirano
	 * uzlazno.
	 * 
	 * @param array
	 *            polje * @return <code>true</code> ako je, <code>false</code>
	 *            inače
	 */
	public static boolean isSorted(int[] array) {
		for(int i = 0, end = array.length-1; i < end; i++) {
			if(array[i] > array[i+1]) {
				return false;
			}
		}
		return true;
	}
}