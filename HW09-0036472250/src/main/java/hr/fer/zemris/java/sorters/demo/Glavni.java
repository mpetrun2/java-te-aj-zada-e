package hr.fer.zemris.java.sorters.demo;

import hr.fer.zemris.java.sorters.QSortParallel;

import java.util.Random;

/**
 * Program for demonstation of Quicksort. 
 * @author Marin Petrunić 0036472250
 *
 */
public class Glavni {
	
	/**
	 * Program does not need arguments.
	 * @param args
	 */
	public static void main(String[] args) {
		final int SIZE = 15000;
		Random rand = new Random();
		int[] data = new int[SIZE];
		for(int i = 0; i < data.length; i++) {
			data[i] = rand.nextInt();
		}
		long t0 = System.currentTimeMillis();
		QSortParallel.sort(data);
		long t1 = System.currentTimeMillis();
		System.out.println("Sortirano: " + QSortParallel.isSorted(data));System.out.println("Vrijeme: " + (t1-t0)+" ms");
	}
}

