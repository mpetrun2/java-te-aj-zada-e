/**
 * 
 */
package hr.fer.zemris.bool;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanConstantTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanConstant#getValue()}.
	 */
	@Test
	public void testGetValue() {
		assertEquals(BooleanValue.TRUE, BooleanConstant.TRUE.getValue());
		assertEquals(BooleanValue.FALSE, BooleanConstant.FALSE.getValue());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.BooleanConstant#getDomain()}.
	 */
	@Test
	public void testGetDomain() {
		assertEquals(new ArrayList<BooleanVariable>(), BooleanConstant.TRUE.getDomain());
	}

}
