/**
 * 
 */
package hr.fer.zemris.bool;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class MaskTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#parse(java.lang.String)}.
	 */
	@Test
	public void testParse() {
		Mask.parse("0x1110x");
		Mask.parse("1111");
		Mask.parse("0000");
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#parse(java.lang.String)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testParseIllegal1() {
		Mask.parse("");
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#parse(java.lang.String)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testParseIllegal2() {
		Mask.parse("1x00a");
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#fromIndex(int, int)}.
	 */
	@Test
	public void testFromIndex() {
		Mask.fromIndex(5, 10);
		Mask.fromIndex(3, 7);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#fromIndex(int, int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testFromIndexIllegal1() {
		Mask.fromIndex(0, 10);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#fromIndex(int, int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testFromIndexIllegal2() {
		Mask.fromIndex(2, 10);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#Mask(hr.fer.zemris.bool.MaskValue[])}.
	 */
	@Test
	public void testMask() {
		MaskValue[] mask = new MaskValue[3];
		mask[0] = MaskValue.ONE;
		mask[1] = MaskValue.ZERO;
		mask[2] = MaskValue.DONT_CARE;
		@SuppressWarnings("unused")
		Mask newMask = new Mask(mask);
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#Mask(hr.fer.zemris.bool.MaskValue[])}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testMaskIllegal() {
		MaskValue[] mask = new MaskValue[3];
		mask[0] = MaskValue.ONE;
		mask[1] = MaskValue.ZERO;
		@SuppressWarnings("unused")
		Mask newMask = new Mask(mask);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getValue(int)}.
	 */
	@Test
	public void testGetValue() {
		Mask mask = Mask.parse("0x1110x");
		assertEquals(MaskValue.DONT_CARE, mask.getValue(1));
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getValue(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testGetValueIllegal1() {
		Mask mask = Mask.parse("0x1110x");
		assertEquals(MaskValue.DONT_CARE, mask.getValue(-1));
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getValue(int)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testGetValueIllegal2() {
		Mask mask = Mask.parse("0x1110x");
		assertEquals(MaskValue.DONT_CARE, mask.getValue(7));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#isMoreGeneralThan(hr.fer.zemris.bool.Mask)}.
	 */
	@Test
	public void testIsMoreGeneralThan() {
		Mask mask1 = Mask.parse("0x1110x");
		Mask mask2 = Mask.parse("0x1x10x");
		assertTrue(mask2.isMoreGeneralThan(mask1));
		assertFalse(mask1.isMoreGeneralThan(mask2));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#combine(hr.fer.zemris.bool.Mask, hr.fer.zemris.bool.Mask)}.
	 */
	@Test
	public void testCombine() {
		Mask mask1 = Mask.parse("0x10");
		Mask mask2 = Mask.parse("0x11");
		Mask mask3 = Mask.parse("0x1x");
		Mask mask4 = Mask.parse("1x0x");
		Mask result = Mask.parse("0x1x");
		assertEquals(null, Mask.combine(mask1, mask3));
		assertEquals(null, Mask.combine(mask3, mask1));
		assertEquals(null, Mask.combine(mask3, result));
		assertEquals(null, Mask.combine(mask3, mask4));
		Mask combined = Mask.combine(mask1, mask2);
		for (int index = 0, end = result.getSize(); index < end; index++) {
			assertEquals(result.getValue(index), combined.getValue(index));
		}
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#combine(hr.fer.zemris.bool.Mask, hr.fer.zemris.bool.Mask)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testCombineIllegal1() {
		Mask mask1 = Mask.parse("0x10");
		MaskValue[] mask = new MaskValue[3];
		Mask newMask = new Mask(mask);
		Mask.combine(mask1, newMask);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getNumberOfZeros()}.
	 */
	@Test
	public void testGetNumberOfZeros() {
		Mask mask1 = Mask.parse("0x10");
		assertEquals(2, mask1.getNumberOfZeros());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getNumberOfOnes()}.
	 */
	@Test
	public void testGetNumberOfOnes() {
		Mask mask1 = Mask.parse("0x10");
		assertEquals(1, mask1.getNumberOfOnes());
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Mask#getNumberOfDontCares()}.
	 */
	@Test
	public void testGetNumberOfDontCares() {
		Mask mask1 = Mask.parse("0x10xxX");
		assertEquals(4, mask1.getNumberOfDontCares());
	}


}
