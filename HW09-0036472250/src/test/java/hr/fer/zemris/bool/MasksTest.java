/**
 * 
 */
package hr.fer.zemris.bool;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class MasksTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.Masks#fromIndexes(int, int[])}.
	 */
	@Test
	public void testFromIndexes() {
		Masks.fromIndexes(10, 1, 4, 6, 9, 15);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.Masks#fromStrings(java.lang.String[])}.
	 */
	@Test
	public void testFromStrings() {
		Masks.fromStrings("01x1","1111","xXxX","010101");
	}

}
