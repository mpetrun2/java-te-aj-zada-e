/**
 * 
 */
package hr.fer.zemris.bool.fimpl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanOperator;
import hr.fer.zemris.bool.BooleanValue;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.opimpl.BooleanOperators;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class OperatorTreeBFTest {

	public BooleanFunction createFunction() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		varA.setValue(BooleanValue.TRUE);
		BooleanOperator izraz1 = BooleanOperators.or(
			BooleanConstant.FALSE,
			varC,
			BooleanOperators.and(varA, BooleanOperators.not(varB))
		);
		BooleanFunction f1 = new OperatorTreeBF(
									"f1",
									Arrays.asList(varA, varB, varC),
									izraz1
								);
		return f1;
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#OperatorTreeBF(java.lang.String, java.util.List, hr.fer.zemris.bool.BooleanOperator)}.
	 */
	@Test
	public void testOperatorTreeBF() {
		this.createFunction();
	}
	
	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#OperatorTreeBF(java.lang.String, java.util.List, hr.fer.zemris.bool.BooleanOperator)}.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testOperatorTreeBFIllegal() {
		BooleanVariable varA = new BooleanVariable("A");
		BooleanVariable varB = new BooleanVariable("B");
		BooleanVariable varC = new BooleanVariable("C");
		BooleanVariable varD = new BooleanVariable("D");
		BooleanVariable varE = new BooleanVariable("E");
		varA.setValue(BooleanValue.TRUE);
		BooleanOperator izraz1 = BooleanOperators.or(
			BooleanConstant.FALSE,
			varC,
			BooleanOperators.and(varA, BooleanOperators.not(varB)),
			varD,
			varE
		);
		@SuppressWarnings("unused")
		BooleanFunction f1 = new OperatorTreeBF(
									"f1",
									Arrays.asList(varA, varB, varC),
									izraz1
								);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#hasMinterm(int)}.
	 */
	@Test
	public void testHasMinterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasMinterm(2));
		assertTrue(f1.hasMinterm(3));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#hasMaxterm(int)}.
	 */
	@Test
	public void testHasMaxterm() {
		BooleanFunction f1 = this.createFunction();
		assertFalse("err1",f1.hasMaxterm(1));
		assertTrue("err2",f1.hasMaxterm(6));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#hasDontCare(int)}.
	 */
	@Test
	public void testHasDontCare() {
		BooleanFunction f1 = this.createFunction();
		assertFalse(f1.hasDontCare(5));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#mintermIterable()}.
	 */
	@Test
	public void testMintermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.mintermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] { 1, 3, 4, 5, 7}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#maxtermIterable()}.
	 */
	@Test
	public void testMaxtermIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.maxtermIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 2, 6}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#dontcareIterable()}.
	 */
	@Test
	public void testDontcareIterable() {
		BooleanFunction f1 = this.createFunction();
		List<Integer> list = new ArrayList<>();
		for(Integer i : f1.dontcareIterable()) {
			list.add(i);
		}
		assertTrue(list.equals(new ArrayList<Integer>(Arrays.asList(new Integer[] {}))));
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.fimpl.OperatorTreeBF#getValue()}.
	 */
	@Test
	public void testGetValue() {
		BooleanFunction f1 = this.createFunction();
		assertEquals(BooleanValue.TRUE, f1.getValue());
	}

}
