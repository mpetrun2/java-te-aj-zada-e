/**
 * 
 */
package hr.fer.zemris.bool.opimpl;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanConstant;
import hr.fer.zemris.bool.BooleanSource;
import hr.fer.zemris.bool.BooleanValue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BooleanOperatorORTest {

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorOR#BooleanOperatorOR(java.util.List)}.
	 */
	@Test
	public void testBooleanOperatorOR() {
		List<BooleanSource> sources = new ArrayList<>();
		sources.add(BooleanConstant.TRUE);
		sources.add(BooleanConstant.FALSE);
		@SuppressWarnings("unused")
		BooleanOperatorOR or = new BooleanOperatorOR(sources);
	}

	/**
	 * Test method for {@link hr.fer.zemris.bool.opimpl.BooleanOperatorOR#getValue()}.
	 */
	@Test
	public void testGetValue() {
		List<BooleanSource> sources = new ArrayList<>();
		sources.add(BooleanConstant.TRUE);
		sources.add(BooleanConstant.FALSE);
		BooleanOperatorOR or = new BooleanOperatorOR(sources);
		assertEquals(BooleanValue.TRUE, or.getValue());
	}

}
