/**
 * 
 */
package hr.fer.zemris.bool.qmc;

import static org.junit.Assert.*;
import hr.fer.zemris.bool.BooleanFunction;
import hr.fer.zemris.bool.BooleanVariable;
import hr.fer.zemris.bool.Mask;
import hr.fer.zemris.bool.fimpl.IndexedBF;
import hr.fer.zemris.bool.fimpl.MaskBasedBF;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class QMCMinimizerTest {

	/**
	 * Test method for
	 * {@link hr.fer.zemris.bool.qmc.QMCMinimizer#QMCMinimizer(hr.fer.zemris.bool.BooleanFunction, boolean)}
	 * .
	 */
	@Test
	public void testQMCMinimizer() {
		BooleanFunction f1 = new IndexedBF("f1", new ArrayList<>(Arrays.asList(
				new BooleanVariable("A"), new BooleanVariable("B"),
				new BooleanVariable("C"), new BooleanVariable("D"))), true,
				new ArrayList<>(Arrays.asList(1, 3, 4, 5, 6, 7, 8, 9, 10, 11,
						12, 13, 14)), new ArrayList<Integer>());
		@SuppressWarnings("unused")
		QMCMinimizer minimizer = new QMCMinimizer(f1, false);
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.bool.qmc.QMCMinimizer#getListOfMinimizedFunctions()}
	 * .
	 */
	@Test
	public void testGetListOfMinimizedFunctions() {
		List<BooleanVariable> domain = new ArrayList<>(Arrays.asList(
				new BooleanVariable("A"), new BooleanVariable("B"),
				new BooleanVariable("C"), new BooleanVariable("D")));
		BooleanFunction f1 = new IndexedBF("f1", domain, true, new ArrayList<>(
				Arrays.asList(0, 1, 4, 5, 11, 15)), new ArrayList<Integer>());
		QMCMinimizer minimizer = new QMCMinimizer(f1, false);
		assertEquals("Wrong number of minimized functions",1, minimizer.getListOfMinimizedFunctions().size());
		assertTrue("Wrong minimization",minimizer
				.getListOfMinimizedFunctions()
				.iterator()
				.next()
				.getMasks()
				.containsAll(
						new ArrayList<>(Arrays.asList(Mask.parse("1x11"),
								Mask.parse("0x0x")))));
	}

	/**
	 * Test method for
	 * {@link hr.fer.zemris.bool.qmc.QMCMinimizer#minimize(hr.fer.zemris.bool.BooleanFunction, boolean)}
	 * .
	 */
	@Test
	public void testMinimize() {
		List<BooleanVariable> domain = new ArrayList<>(Arrays.asList(
				new BooleanVariable("A"), new BooleanVariable("B"),
				new BooleanVariable("C"), new BooleanVariable("D")));
		BooleanFunction f1 = new IndexedBF("f1", domain, true, new ArrayList<>(
				Arrays.asList(0, 1, 4, 5, 11, 15)), new ArrayList<Integer>());
		MaskBasedBF[] minimizedFunctions = QMCMinimizer.minimize(f1, false);
		assertEquals(1, minimizedFunctions.length);
	}

}
