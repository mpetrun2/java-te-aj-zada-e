/**
 * 
 */
package hr.fer.zemris.java.sorters;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class QSortParallelTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.sorters.QSortParallel#sort(int[])}.
	 */
	@Test
	public void testSort() {
		final int SIZE = 15000;
		Random rand = new Random();
		int[] data = new int[SIZE];
		for(int i = 0; i < data.length; i++) {
			data[i] = rand.nextInt();
		}
		QSortParallel.sort(data);
		assertTrue(QSortParallel.isSorted(data));
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.sorters.QSortParallel#isSorted(int[])}.
	 */
	@Test
	public void testIsSorted() {
		final int SIZE = 15000;
		Random rand = new Random();
		int[] data = new int[SIZE];
		for(int i = 0; i < data.length; i++) {
			data[i] = rand.nextInt();
		}
		QSortParallel.sort(data);
		assertTrue(QSortParallel.isSorted(data));
	}

}
