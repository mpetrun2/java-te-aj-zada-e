/**
 * 
 */
package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.commands.DigitCommand;
import hr.fer.zemris.java.gui.calc.commands.DotCommand;
import hr.fer.zemris.java.gui.calc.commands.NumberCommand;
import hr.fer.zemris.java.gui.calc.commands.SignCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.ClearCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.CosCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.CtgCommang;
import hr.fer.zemris.java.gui.calc.commands.functions.FunctionCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.InversCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.LnCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.LogCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.SinCommand;
import hr.fer.zemris.java.gui.calc.commands.functions.TanCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.AddCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.DivCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.MulCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.OperatorCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.PowCommand;
import hr.fer.zemris.java.gui.calc.commands.operators.SubCommand;
import hr.fer.zemris.java.gui.calc.commands.stack.PopCommand;
import hr.fer.zemris.java.gui.calc.commands.stack.PushCommand;
import hr.fer.zemris.java.gui.calc.commands.stack.StackCommand;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * Calculator similar to Windows calculator with bunch of functions.
 * 
 * @author Marin Petrunić 0036472250
 * @version 1.0
 */
public class Calculator extends JFrame {

	/**
	 * ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * First operator needed for {@link OperatorCommand}s.
	 */
	private Double firstNumber;

	/**
	 * Last operator.
	 */
	private OperatorCommand operator;

	/**
	 * If number needs to be reseted. THis is true after {@link OperatorCommand}
	 * is called.
	 */
	private boolean secondNumber = false;

	/**
	 * Calculator display.
	 */
	private JLabel display;

	/**
	 * Function inverse flag.
	 */
	private JCheckBox invers;

	/**
	 * Stack.
	 */
	private Stack<Double> stack = new Stack<>();

	/**
	 * Map with {@link JButton} connected to its {@link DigitCommand}.
	 */
	private Map<JButton, DigitCommand> digitCommands = new HashMap<>();

	/**
	 * Map with {@link JButton} connected to its {@link FunctionCommand}.
	 */
	private Map<JButton, FunctionCommand> functionCommands = new HashMap<>();

	/**
	 * Map with {@link JButton} connected to its {@link StackCommand}.
	 */
	private Map<JButton, StackCommand> stackCommands = new HashMap<>();

	/**
	 * Map with {@link JButton} connected to its {@link OperatorCommand}.
	 */
	private Map<JButton, OperatorCommand> operatorCommands = new HashMap<>();

	/**
	 * Calculator start.
	 * 
	 * @param args
	 *            - does not need params
	 */
	public static void main(String[] args) {
		new Calculator();
	}

	/**
	 * Constructor. Initializes GUI.
	 */
	public Calculator() {
		initGUI();
	}

	/**
	 * Initializes GUI.
	 */
	private void initGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				setTitle("Calculator");
				setVisible(true);
				setBounds(100, 100, 600, 400);
				JPanel panel = new JPanel(new CalcLayout(10));
				JLabel display = new JLabel("", SwingConstants.RIGHT);
				display.setBackground(Color.LIGHT_GRAY);
				display.setOpaque(true);
				display.setBorder(BorderFactory.createEmptyBorder(20, 20, 20,
						20));
				display.setFont(new Font("Serif", Font.BOLD, 24));
				Calculator.this.display = display;
				panel.add(display, new RCPosition(1, 1));
				JButton equalButt = new JButton("=");
				equalButt.setFont(new Font("Serif", Font.BOLD, 24));
				equalButt.setFocusable(false);
				panel.add(equalButt, new RCPosition(6, 1));
				JButton clearButt = new JButton("clr");
				clearButt.setFont(new Font("Serif", Font.BOLD, 20));
				clearButt.setFocusable(false);
				panel.add(clearButt, new RCPosition(7, 1));
				functionCommands.put(clearButt, new ClearCommand());
				JButton inversButt = new JButton("1/x");
				inversButt.setFont(new Font("Serif", Font.BOLD, 20));
				inversButt.setFocusable(false);
				panel.add(inversButt, new RCPosition(1, 2));
				functionCommands.put(inversButt, new InversCommand());
				JButton sinButt = new JButton("sin");
				sinButt.setFont(new Font("Serif", Font.BOLD, 20));
				sinButt.setFocusable(false);
				panel.add(sinButt, new RCPosition(2, 2));
				functionCommands.put(sinButt, new SinCommand());
				JButton sevenButt = new JButton("7");
				sevenButt.setFont(new Font("Serif", Font.BOLD, 20));
				sevenButt.setFocusable(false);
				panel.add(sevenButt, new RCPosition(3, 2));
				digitCommands.put(sevenButt, new NumberCommand("7"));
				JButton eightButt = new JButton("8");
				eightButt.setFont(new Font("Serif", Font.BOLD, 20));
				eightButt.setFocusable(false);
				panel.add(eightButt, new RCPosition(4, 2));
				digitCommands.put(eightButt, new NumberCommand("8"));
				JButton nineButt = new JButton("9");
				nineButt.setFont(new Font("Serif", Font.BOLD, 20));
				nineButt.setFocusable(false);
				panel.add(nineButt, new RCPosition(5, 2));
				digitCommands.put(nineButt, new NumberCommand("9"));
				JButton divideButt = new JButton("/");
				divideButt.setFont(new Font("Serif", Font.BOLD, 20));
				divideButt.setFocusable(false);
				panel.add(divideButt, new RCPosition(6, 2));
				operatorCommands.put(divideButt, new DivCommand());
				JButton resButt = new JButton("res");
				resButt.setFont(new Font("Serif", Font.BOLD, 20));
				resButt.setFocusable(false);
				panel.add(resButt, new RCPosition(7, 2));
				JButton logButt = new JButton("log");
				logButt.setFont(new Font("Serif", Font.BOLD, 20));
				logButt.setFocusable(false);
				panel.add(logButt, new RCPosition(1, 3));
				functionCommands.put(logButt, new LogCommand());
				JButton cosButt = new JButton("cos");
				cosButt.setFont(new Font("Serif", Font.BOLD, 20));
				cosButt.setFocusable(false);
				panel.add(cosButt, new RCPosition(2, 3));
				functionCommands.put(cosButt, new CosCommand());
				JButton fourButt = new JButton("4");
				fourButt.setFont(new Font("Serif", Font.BOLD, 20));
				fourButt.setFocusable(false);
				panel.add(fourButt, new RCPosition(3, 3));
				digitCommands.put(fourButt, new NumberCommand("4"));
				JButton fiveButt = new JButton("5");
				fiveButt.setFont(new Font("Serif", Font.BOLD, 20));
				fiveButt.setFocusable(false);
				panel.add(fiveButt, new RCPosition(4, 3));
				digitCommands.put(fiveButt, new NumberCommand("5"));
				JButton sixButt = new JButton("6");
				sixButt.setFont(new Font("Serif", Font.BOLD, 20));
				sixButt.setFocusable(false);
				panel.add(sixButt, new RCPosition(5, 3));
				digitCommands.put(sixButt, new NumberCommand("6"));
				JButton mulButt = new JButton("*");
				mulButt.setFont(new Font("Serif", Font.BOLD, 20));
				mulButt.setFocusable(false);
				panel.add(mulButt, new RCPosition(6, 3));
				operatorCommands.put(mulButt, new MulCommand());
				JButton pushButt = new JButton("push");
				pushButt.setFont(new Font("Serif", Font.BOLD, 20));
				pushButt.setFocusable(false);
				panel.add(pushButt, new RCPosition(7, 3));
				stackCommands.put(pushButt, new PushCommand());
				JButton lnButt = new JButton("ln");
				lnButt.setFont(new Font("Serif", Font.BOLD, 20));
				lnButt.setFocusable(false);
				panel.add(lnButt, new RCPosition(1, 4));
				functionCommands.put(lnButt, new LnCommand());
				JButton tanButt = new JButton("tan");
				tanButt.setFont(new Font("Serif", Font.BOLD, 20));
				tanButt.setFocusable(false);
				panel.add(tanButt, new RCPosition(2, 4));
				functionCommands.put(tanButt, new TanCommand());
				JButton oneButt = new JButton("1");
				oneButt.setFont(new Font("Serif", Font.BOLD, 20));
				oneButt.setFocusable(false);
				panel.add(oneButt, new RCPosition(3, 4));
				digitCommands.put(oneButt, new NumberCommand("1"));
				JButton twoButt = new JButton("2");
				twoButt.setFont(new Font("Serif", Font.BOLD, 20));
				twoButt.setFocusable(false);
				panel.add(twoButt, new RCPosition(4, 4));
				digitCommands.put(twoButt, new NumberCommand("2"));
				JButton threeButt = new JButton("3");
				threeButt.setFont(new Font("Serif", Font.BOLD, 20));
				threeButt.setFocusable(false);
				panel.add(threeButt, new RCPosition(5, 4));
				digitCommands.put(threeButt, new NumberCommand("3"));
				JButton minButt = new JButton("-");
				minButt.setFont(new Font("Serif", Font.BOLD, 20));
				minButt.setFocusable(false);
				panel.add(minButt, new RCPosition(6, 4));
				operatorCommands.put(minButt, new SubCommand());
				JButton popButt = new JButton("pop");
				popButt.setFont(new Font("Serif", Font.BOLD, 20));
				popButt.setFocusable(false);
				panel.add(popButt, new RCPosition(7, 4));
				stackCommands.put(popButt, new PopCommand());
				JButton powButt = new JButton("x^n");
				powButt.setFont(new Font("Serif", Font.BOLD, 20));
				powButt.setFocusable(false);
				panel.add(powButt, new RCPosition(1, 5));
				operatorCommands.put(powButt, new PowCommand());
				JButton ctgButt = new JButton("ctg");
				ctgButt.setFont(new Font("Serif", Font.BOLD, 20));
				ctgButt.setFocusable(false);
				panel.add(ctgButt, new RCPosition(2, 5));
				functionCommands.put(ctgButt, new CtgCommang());
				JButton zeroButt = new JButton("0");
				zeroButt.setFont(new Font("Serif", Font.BOLD, 20));
				zeroButt.setFocusable(false);
				panel.add(zeroButt, new RCPosition(3, 5));
				digitCommands.put(zeroButt, new NumberCommand("0"));
				JButton minusButt = new JButton("+/-");
				minusButt.setFont(new Font("Serif", Font.BOLD, 20));
				minusButt.setFocusable(false);
				panel.add(minusButt, new RCPosition(4, 5));
				digitCommands.put(minusButt, new SignCommand());
				JButton dotButt = new JButton(".");
				dotButt.setFont(new Font("Serif", Font.BOLD, 20));
				dotButt.setFocusable(false);
				panel.add(dotButt, new RCPosition(5, 5));
				digitCommands.put(dotButt, new DotCommand());
				JButton plusButt = new JButton("+");
				plusButt.setFont(new Font("Serif", Font.BOLD, 20));
				plusButt.setFocusable(false);
				panel.add(plusButt, new RCPosition(6, 5));
				operatorCommands.put(plusButt, new AddCommand());
				JCheckBox invers = new JCheckBox("Inv");
				invers.setFont(new Font("Serif", Font.BOLD, 20));
				invers.setFocusable(false);
				panel.add(invers, new RCPosition(7, 5));
				Calculator.this.invers = invers;
				add(panel);
				initCommands();
				equalButt.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (operator != null) {
							Double otherNumber = getNumber();
							if (otherNumber != null) {
								firstNumber = operator.execute(Calculator.this,
										firstNumber, otherNumber,
										Calculator.this.invers.isSelected());
								setText(firstNumber);
							}
						}
						operator = null;
						secondNumber = false;
					}
				});
				resButt.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						operator = null;
						firstNumber = null;
						Calculator.this.invers.setSelected(false);
						stack.removeAllElements();
						setText("");
					}
				});
			}
		});
	}

	/**
	 * Initialize action for each button.
	 */
	private void initCommands() {
		for (final Entry<JButton, DigitCommand> entry : digitCommands
				.entrySet()) {
			entry.getKey().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					setText(entry.getValue().execute(Calculator.this,
							getText(), secondNumber));
					secondNumber = false;
				}
			});
		}
		for (final Entry<JButton, FunctionCommand> entry : functionCommands
				.entrySet()) {
			entry.getKey().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Double number = getNumber();
					if (number != null) {
						setText(entry.getValue().execute(Calculator.this,
								number, Calculator.this.invers.isSelected()));
					}
				}
			});
		}
		for (final Entry<JButton, StackCommand> entry : stackCommands
				.entrySet()) {
			entry.getKey().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					setText(entry.getValue().execute(Calculator.this, getNumber(), stack));
				}
			});
		}

		for (final Entry<JButton, OperatorCommand> entry : operatorCommands
				.entrySet()) {
			entry.getKey().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (operator == null) {
						firstNumber = getNumber();
						if (firstNumber == null) {
							operator = null;
						} else {
							operator = entry.getValue();
							secondNumber = true;
						}
					} else {
						Double otherNumbner = getNumber();
						if (otherNumbner != null) {
							firstNumber = operator.execute(Calculator.this,
									Calculator.this.firstNumber, otherNumbner,
									Calculator.this.invers.isSelected());
							setText(firstNumber);
							operator = entry.getValue();
							secondNumber = true;
						}
					}
				}
			});
		}
	}

	/**
	 * Sets given text to display of {@link Calculator}.
	 * 
	 * @param text
	 *            to be displayed
	 */
	public void setText(final String text) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				display.setText(text);
			}
		});
	}

	/**
	 * Displays given number to display of {@link Calculator}.
	 * 
	 * @param number
	 *            to be displayed
	 */
	public void setText(double number) {
		setText(String.valueOf(number));
	}

	/**
	 * Gets text displayed on {@link Calculator}
	 * 
	 * @return
	 */
	public String getText() {
		return display.getText();
	}

	/**
	 * Parses text displayed on {@link Calculator} as {@link Double} and returns
	 * it. If it cannot be parsed error message will be displayed.
	 * 
	 * @return
	 */
	public Double getNumber() {
		try {
			return Double.parseDouble(getText());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Invalid number!", "Error",
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

}
