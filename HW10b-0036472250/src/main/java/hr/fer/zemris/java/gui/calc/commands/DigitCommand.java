/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Commands that adds clicked digit or symbol to {@link Calculator} display.
 * @author Marin Petrunić 0036472250
 *
 */
public interface DigitCommand {

	/**
	 * Executes command specific code and returns result as {@link String}.
	 * @param calc instance of {@link Calculator}
	 * @param calculatorValue value written on {@link Calculator} display
	 * @param reset if output needs to be reseted
	 * @return result
	 */
	public String execute(Calculator calc, String calculatorValue, boolean reset);
}
