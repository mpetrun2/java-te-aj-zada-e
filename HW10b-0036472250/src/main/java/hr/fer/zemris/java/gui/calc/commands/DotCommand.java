/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import hr.fer.zemris.java.gui.calc.Calculator;

import javax.swing.JOptionPane;

/**
 * Adds decimal point.
 * @author Marin Petrunić 0036472250
 * 
 */
public class DotCommand implements DigitCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.gui.calc.commands.DigitCommand#execute(javax.swing
	 * .JLabel)
	 */
	@Override
	public String execute(Calculator calc, String calculatorValue, boolean reset) {
		if(reset) {
			return ".";
		}
		if (calculatorValue.contains(".")) {
			JOptionPane.showMessageDialog(calc,
					"Multiple dots in number are not allowed!",
					"Error inserting dot", JOptionPane.ERROR_MESSAGE);
			return "";
		} else {
			return calculatorValue + ".";
		}

	}

}
