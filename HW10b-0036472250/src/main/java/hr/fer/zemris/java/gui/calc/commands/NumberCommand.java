/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Adds digit to the {@link Calculator}
 * @author Marin Petrunić 0036472250
 *
 */
public class NumberCommand implements DigitCommand {

	/**
	 * digit.
	 */
	private String digit;
	
	/**
	 * Constructor.
	 * @param digit to be added
	 */
	public NumberCommand(String digit) {
		this.digit = digit;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.DigitCommand#execute(javax.swing.JLabel, javax.swing.JButton)
	 */
	@Override
	public String execute(Calculator calculator, String calculatorValue, boolean reset) {
		if(reset) {
			return digit;
		} else {
			return calculatorValue + digit;
		}
	}

}
