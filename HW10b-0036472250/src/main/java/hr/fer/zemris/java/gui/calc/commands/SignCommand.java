/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Multiplies number in {@link Calculator} with -1.
 * @author Marin Petrunić 0036472250
 *
 */
public class SignCommand implements DigitCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.DigitCommand#execute(javax.swing.JLabel)
	 */
	@Override
	public String execute(Calculator calculator, String calculatorValue, boolean reset) {
		if(reset) {
			return "-";
		} else {
			if(calculatorValue.startsWith("-")) {
				return calculatorValue.substring(1);
			} else {
				return "-" + calculatorValue;
			}
		}
		
	}

}
