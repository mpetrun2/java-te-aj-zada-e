/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Clears current number on display.
 * @author Marin Petrunić 0036472250
 *
 */
public class ClearCommand implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		return "";
	}

}
