/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Calculates cosinus of current number(angle) or
 * arc cosinus in invers is set to <code>true</code>.
 * Assumes number is in radians.
 * @author Marin Petrunić 0036472250
 *
 */
public class CosCommand implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.functions.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result;
		if (inverse) {
			result = Math.acos(calculatorValue);
		} else {
			result = Math.cos(calculatorValue);
		}
		return String.valueOf(result);
	}

}
