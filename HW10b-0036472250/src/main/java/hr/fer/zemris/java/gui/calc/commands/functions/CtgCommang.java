/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;


/**
 * Calculates cot of curent number or tan if invers is set to true.
 * @author Marin Petrunić 0036472250
 *
 */
public class CtgCommang implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.functions.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result;
		if (inverse) {
			result = Math.tan(calculatorValue);
		} else {
			result = Math.atan(calculatorValue);
		}
		return String.valueOf(result);
	}

}
