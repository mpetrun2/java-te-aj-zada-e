/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Interface for commands executing on current number adn setting result.
 * @author Marin Petrunić 0036472250
 *
 */
public interface FunctionCommand {

	/**
	 * Executes command specific code and returns result as {@link String}
	 * @param calculator instance of {@link Calculator}
	 * @param calculatorValue valuze written on {@link Calculator} display
	 * @param inverse if inverse function is required
	 * @return result
	 */
	public String execute(Calculator calculator,Double calculatorValue, boolean inverse);
}
