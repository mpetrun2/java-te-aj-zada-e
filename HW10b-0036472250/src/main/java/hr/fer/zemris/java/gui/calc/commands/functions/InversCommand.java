/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;


/**
 * Calculates invers of current number.
 * @author Marin Petrunić 0036472250
 *
 */
public class InversCommand implements FunctionCommand {
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		return String.valueOf(1/calculatorValue);
	}

}
