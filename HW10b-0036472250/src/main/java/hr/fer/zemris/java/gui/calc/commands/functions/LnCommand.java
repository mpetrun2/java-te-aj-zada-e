/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Calculates natural logarithm of current number or
 * e^(current number) if invers is set to <code>true</code>.
 * @author Marin Petrunić 0036472250
 *
 */
public class LnCommand implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.functions.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result;
		if(inverse) {
			result = Math.exp(calculatorValue);
		} else {
			result = Math.log(calculatorValue);
		}
		return String.valueOf(result);
	}

}
