/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;


/**
 * Calculates logarithm of current number or 10^(current number)
 * if invers is set to <code>true</code>
 * @author Marin Petrunić 0036472250
 *
 */
public class LogCommand implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result;
		if(inverse) {
			result = Math.pow(10, calculatorValue);
		} else {
			result = Math.log10(calculatorValue);
		}
		return String.valueOf(result);
	}

}
