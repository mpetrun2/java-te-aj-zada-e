/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Sinus command. Calculates sinus from angle number in display. Assumes angle
 * is in radians.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class SinCommand implements FunctionCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.gui.calc.commands.FunctionCommand#execute(javax.swing
	 * .JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result = 0;
		if (inverse) {
			result = Math.asin(calculatorValue);
		} else {
			result = Math.sin(calculatorValue);
		}
		return String.valueOf(result);
	}

}
