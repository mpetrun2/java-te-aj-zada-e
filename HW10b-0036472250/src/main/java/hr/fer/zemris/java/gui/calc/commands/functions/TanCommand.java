/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import hr.fer.zemris.java.gui.calc.Calculator;


/**
 * Calculates tan or cot if invers is set to true.
 * @author Marin Petrunić 0036472250
 *
 */
public class TanCommand implements FunctionCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.functions.FunctionCommand#execute(javax.swing.JLabel, boolean)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, boolean inverse) {
		double result = 0;
		if (inverse) {
			result = Math.atan(calculatorValue);
		} else {
			result = Math.tan(calculatorValue);
		}
		return String.valueOf(result);
	}

}
