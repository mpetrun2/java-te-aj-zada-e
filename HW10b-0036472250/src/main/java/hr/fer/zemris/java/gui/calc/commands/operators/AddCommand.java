/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Adds two numbers and prints result to display.
 * @author Marin Petrunić 0036472250
 *
 */
public class AddCommand implements OperatorCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.operators.OperatorCommand#execute(javax.swing.JLabel, double, boolean)
	 */
	@Override
	public Double execute(Calculator calculator, Double firstNumber, Double secondNumber, boolean inverse) {
		return secondNumber + firstNumber;
	}

}
