/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Divides first number with second one.
 * @author Marin Petrunić 0036472250
 * 
 */
public class DivCommand implements OperatorCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.gui.calc.commands.operators.OperatorCommand#execute
	 * (hr.fer.zemris.java.gui.calc.Calculator, double, boolean)
	 */
	@Override
	public Double execute(Calculator calculator, Double firstNumber, Double secondNumber,
			boolean inverse) {
		return firstNumber / secondNumber;
	}

}
