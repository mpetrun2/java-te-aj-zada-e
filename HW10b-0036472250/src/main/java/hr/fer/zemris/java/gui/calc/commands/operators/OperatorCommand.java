/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Represents command that requires two numbers.
 * @author Marin Petrunić 0036472250
 *
 */
public interface OperatorCommand {
	
	/**
	 * Executes specific operation and returns result as {@link Double}.
	 * @param calculator instance of {@link Calculator}
	 * @param firstNumber saved first number of operation
	 * @param secondNumber number currently written on {@link Calculator} display
	 * @param inverse if inverse operation needed
	 * @return
	 */
	public Double execute(Calculator calculator, Double firstNumber, Double secondNumber, boolean inverse); 

}
