/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * Raises first argument to the power of second.
 * @author Marin Petrunić 0036472250
 *
 */
public class PowCommand implements OperatorCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.operators.OperatorCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, double, boolean)
	 */
	@Override
	public Double execute(Calculator calculator, Double firstNumber, Double secondNumber,
			boolean inverse) {
		if(inverse) {
			return Math.pow(firstNumber, -secondNumber);
		}
		return Math.pow(firstNumber, secondNumber);
	}

}
