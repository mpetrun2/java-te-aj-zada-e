/**
 * Package containing all command neccesary for Calculator to work properly.
 */
/**
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.gui.calc.commands;