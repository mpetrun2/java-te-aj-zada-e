/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.stack;

import hr.fer.zemris.java.gui.calc.Calculator;

import java.util.Stack;
import javax.swing.JOptionPane;

/**
 * Pops element from stack and outputs it to display.
 * In case of emoty stack error message is thrown.
 * @author Marin Petrunić 0036472250
 * 
 */
public class PopCommand implements StackCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.gui.calc.commands.stack.StackCommand#execute(javax
	 * .swing.JLabel, java.util.Stack)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, Stack<Double> stack) {
		if (stack.isEmpty()) {
			JOptionPane.showMessageDialog(calculator,
					"Stack is empty!", "Error",
					JOptionPane.ERROR_MESSAGE);
			return String.valueOf(calculatorValue);
		}
		return String.valueOf(stack.pop());
	}

}
