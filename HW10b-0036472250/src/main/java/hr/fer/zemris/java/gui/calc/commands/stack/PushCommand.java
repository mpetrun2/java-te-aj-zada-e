/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.stack;

import hr.fer.zemris.java.gui.calc.Calculator;

import java.util.Stack;



/**
 * Pushes curreent number in display to stack.
 * @author Marin Petrunić 0036472250
 *
 */
public class PushCommand implements StackCommand {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.gui.calc.commands.stack.StackCommand#execute(javax.swing.JLabel, java.util.Stack)
	 */
	@Override
	public String execute(Calculator calculator, Double calculatorValue, Stack<Double> stack) {
		stack.push(calculatorValue);
		return String.valueOf(calculatorValue);
	}

}
