package hr.fer.zemris.java.gui.calc.commands.stack;

import hr.fer.zemris.java.gui.calc.Calculator;

import java.util.Stack;

/**
 * Represents commands related to {@link Stack}.
 * @author Marin Petrunić 0036472250
 *
 */
public interface StackCommand {

	/**
	 * Executes functions related to stack.
	 * Commands work directly on stack instance
	 * @param calculator instance of {@link Calculator}
	 * @param calculatorValue value written on {@link Calculator} display
	 * @param stack working instance of stack
	 * @return
	 */
	public String execute(Calculator calculator, Double calculatorValue, Stack<Double> stack);
}
