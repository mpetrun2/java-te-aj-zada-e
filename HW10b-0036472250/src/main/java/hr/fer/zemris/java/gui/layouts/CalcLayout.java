/**
 * 
 */
package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Represents layout with 5 rows and 7 columns fixed. First column is stretched
 * through 5 columns.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class CalcLayout implements LayoutManager2 {

	/**
	 * Space between two components in pixels.
	 */
	private int spacing;

	/**
	 * Map of components. Each component has key that is its own index in field
	 * of components.
	 */
	private Map<RCPosition, Component> components = new HashMap<>();

	/**
	 * Default constructor. Accepts space between components in pixels.
	 * 
	 * @param spacing
	 *            space between components
	 * @throws IllegalArgumentException
	 *             if negative spacing given
	 */
	public CalcLayout(int spacing) {
		if (spacing < 0) {
			throw new IllegalArgumentException(
					"Space must be greater or equal zero.");
		}
		this.spacing = spacing;
	}

	/**
	 * Constructor that creates {@link CalcLayout} with spacing set to 0.
	 */
	public CalcLayout() {
		this(0);
	}

	/**
	 * Required by {@link LayoutManager}.
	 */
	@Override
	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp, name);
	}

	/**
	 * @throws IllegalArgumentException
	 *             if given component doesn't exist
	 */
	@Override
	public void removeLayoutComponent(Component comp) {
		if (!components.containsValue(comp)) {
			throw new IllegalArgumentException(
					"GIven component doesn't exist in this layout.");
		}
		Iterator<Entry<RCPosition, Component>> it = components.entrySet()
				.iterator();
		while (it.hasNext()) {
			if (it.next().getValue().equals(comp)) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager#preferredLayoutSize(java.awt.Container)
	 */
	@Override
	public Dimension preferredLayoutSize(Container parent) {
		Insets insets = parent.getInsets();
		int width = insets.left + insets.right;
		int height = insets.bottom + insets.top;
		int compWidth = 0;
		int compHeight = 0;
		for (Entry<RCPosition, Component> entry : components.entrySet()) {
			if (entry.getValue().getPreferredSize().width > compWidth
					&& entry.getKey().getrow() != 1
					&& entry.getKey().getcolumn() != 1) {
				compWidth = entry.getValue().getPreferredSize().width;
			}
			if (entry.getValue().getPreferredSize().height > compHeight) {
				compHeight = entry.getValue().getPreferredSize().height;
			}
		}
		width += 7 * compWidth + 6 * spacing;
		height += 5 * compHeight + 4 * spacing;
		System.out.println(width + " "+ height);
		return new Dimension(width, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
	 */
	@Override
	public Dimension minimumLayoutSize(Container parent) {
		Insets insets = parent.getInsets();
		int width = insets.left + insets.right;
		int height = insets.bottom + insets.top;
		int compWidth = 0;
		int compHeight = 0;
		for (Entry<RCPosition, Component> entry : components.entrySet()) {
			if (entry.getValue().getMinimumSize().width > compWidth
					&& entry.getKey().getrow() != 1
					&& entry.getKey().getcolumn() != 1) {
				compWidth = entry.getValue().getMinimumSize().width;
			}
			if (entry.getValue().getMinimumSize().height > compHeight) {
				compHeight = entry.getValue().getMinimumSize().height;
			}
		}
		width += 7 * compWidth + 6 * spacing;
		height += 5 * compHeight + 4 * spacing;
		return new Dimension(width, height);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager#layoutContainer(java.awt.Container)
	 */
	@Override
	public void layoutContainer(Container parent) {
		Insets insets = parent.getInsets();
		int conteinerHeight = parent.getHeight();
		int conteinerWidth = parent.getWidth();
		Dimension preferredDimension = preferredLayoutSize(parent);
		if (conteinerHeight < preferredDimension.getHeight()) {
			parent.setSize(parent.getWidth(),
					(int) preferredDimension.getHeight());
		}
		if (conteinerWidth < preferredDimension.getWidth()) {
			parent.setSize((int) preferredDimension.getWidth(),
					parent.getHeight());
		}
		conteinerHeight -= insets.top + insets.bottom + spacing;
		conteinerWidth -= insets.left + insets.right + spacing;
		double compWidth = (conteinerWidth - 7 * spacing) / 7;
		double compHeight = (conteinerHeight - 5 * spacing) / 5;
		for (Entry<RCPosition, Component> entry : components.entrySet()) {
			if (entry.getKey().getcolumn() == 1 && entry.getKey().getrow() == 1) {
				entry.getValue().setBounds(insets.left+spacing, insets.top+spacing,
						(int) (5 * compWidth + 4 * spacing), (int) compHeight);
			} else {
				int x = (int) (insets.left+ spacing +(entry.getKey().getcolumn() - 1) * (spacing + compWidth));
				int y = (int) (insets.top+ spacing + (entry.getKey().getrow() - 1) * (spacing + compHeight));
				entry.getValue().setBounds(x, y, (int)compWidth, (int)compHeight);
			}
		}
		
	}

	/**
	 * @throws IllegalArgumentException
	 *             if invalid constraint given or there is already component at
	 *             given constraint.
	 */
	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		RCPosition cordinates = null;
		if (constraints instanceof RCPosition) {
			cordinates = (RCPosition) constraints;
		} else if (constraints instanceof String) {
			if (((String) constraints).trim().matches("^[0-9]+,[0-9]+$")) {
				String[] coordinatesArguments = ((String) constraints).trim()
						.split(",");
				cordinates = new RCPosition(
						Integer.parseInt(coordinatesArguments[0]),
						Integer.parseInt(coordinatesArguments[1]));
			}
		} else {
			throw new IllegalArgumentException("Invalid constraint type");
		}
		if (components.containsKey(cordinates)) {
			throw new IllegalArgumentException(
					"There is already component at that position.");
		}
		components.put(cordinates, comp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager2#maximumLayoutSize(java.awt.Container)
	 */
	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(target.getMaximumSize());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager2#getLayoutAlignmentX(java.awt.Container)
	 */
	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager2#getLayoutAlignmentY(java.awt.Container)
	 */
	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.LayoutManager2#invalidateLayout(java.awt.Container)
	 */
	@Override
	public void invalidateLayout(Container target) {
	}

}
