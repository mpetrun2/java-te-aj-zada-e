/**
 * 
 */
package hr.fer.zemris.java.gui.layouts;

/**
 * Representing constraints for {@link CalcLarowout}.
 * @author Marin Petrunić 0036472250
 *
 */
public class RCPosition {

	/**
	 * column coordinate of component.
	 */
	private int column;
	
	/**
	 * row coordinate of component.
	 */
	private int row;
	
	/**
	 * Constructor.
	 * @param column
	 * @param row
	 * @throws IllegalArgumentEcolumnception if invalid coordinates
	 */
	public RCPosition(int column, int row) {
		if(column < 1 || row < 1 || column > 7 || row > 5 || (column > 1 && column < 6 && row==1)) {
			throw new IllegalArgumentException("Invalid constraint arguments");
		}
		this.column = column;
		this.row = row;
	}

	/**
	 * @return the column
	 */
	public int getcolumn() {
		return column;
	}

	/**
	 * @return the row
	 */
	public int getrow() {
		return row;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RCPosition)) {
			return false;
		}
		RCPosition other = (RCPosition) obj;
		if (column != other.column) {
			return false;
		}
		if (row != other.row) {
			return false;
		}
		return true;
	}
	
	
	
	
}
