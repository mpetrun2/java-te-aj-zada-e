/**
 * Package containing GUI layouts and components.
 */
/**
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.gui.layouts;