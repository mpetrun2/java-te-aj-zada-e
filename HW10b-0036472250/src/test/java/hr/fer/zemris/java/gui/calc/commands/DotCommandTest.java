/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class DotCommandTest {
	

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.DotCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.String, boolean)}.
	 */
	@Test
	public void testExecute() {
		DotCommand com = new DotCommand();
		assertEquals("Expected dot.",".",com.execute(new Calculator(), "", false));
		assertEquals("Expected dot.",".",com.execute(new Calculator(), "123", true));
		assertEquals("Expected 2.","2.",com.execute(new Calculator(), "2", false));
	}

}
