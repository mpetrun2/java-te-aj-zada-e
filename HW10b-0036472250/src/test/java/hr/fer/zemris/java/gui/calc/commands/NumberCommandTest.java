/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class NumberCommandTest {


	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.NumberCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.String, boolean)}.
	 */
	@Test
	public void testExecute() {
		NumberCommand com = new NumberCommand("8");
		assertEquals("Expected number 8","8" , com.execute(new Calculator(), "", false));
		assertEquals("Expected number 18","18" , com.execute(new Calculator(), "1", false));
	}

}
