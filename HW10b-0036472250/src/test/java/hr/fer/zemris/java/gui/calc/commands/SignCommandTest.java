/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class SignCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.SignCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.String, boolean)}.
	 */
	@Test
	public void testExecute() {
		SignCommand com = new SignCommand();
		assertEquals("Expected negative number.", "-8", com.execute(new Calculator(), "8", false));
		assertEquals("Expected positive number.", "8", com.execute(new Calculator(), "-8", false));
		assertEquals("Expected negative minus.", "-", com.execute(new Calculator(), "8", true));
		
	}

}
