	/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class ClearCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.ClearCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		ClearCommand com = new ClearCommand();
		assertEquals("Expected empty string.", "", com.execute(new Calculator(), 123D, false));
	}

}
