/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class CosCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.CosCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		CosCommand com = new CosCommand();
		assertEquals("Expected cosinus of 2.754", Math.cos(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, false)), 1E-6);
		assertEquals("Expected arc cosinus of 2.754", Math.acos(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, true)), 1E-6);
	}

}
