/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class CtgCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.CtgCommang#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		CtgCommang com = new CtgCommang();
		assertEquals("Expected ctg of 2.754", Math.atan(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, false)), 1E-6);
		assertEquals("Expected tan of 2.754", Math.tan(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, true)), 1E-6);
	}

}
