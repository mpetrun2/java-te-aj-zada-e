/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class InversCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.InversCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		InversCommand com = new InversCommand();
		assertEquals("Expected inversed number.", 1/2.74, Double.parseDouble(com.execute(new Calculator(), 2.74, false)), 1E-6);
	}

}
