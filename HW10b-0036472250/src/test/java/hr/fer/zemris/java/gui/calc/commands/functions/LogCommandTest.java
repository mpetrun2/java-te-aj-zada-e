/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class LogCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.LogCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		LogCommand com = new LogCommand();
		assertEquals("Expected logarithm of 2.74.", Math.log10(2.74),
				Double.parseDouble(com.execute(new Calculator(), 2.74, false)),
				1E-6);
		assertEquals("Expected 10^2.74.", Math.pow(10,2.74),
				Double.parseDouble(com.execute(new Calculator(), 2.74, true)),
				1E-6);
	}

}
