/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.functions;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class SinCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.functions.SinCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		SinCommand com = new SinCommand();
		assertEquals("Expected sinus of 2.754", Math.sin(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, false)), 1E-6);
		assertEquals("Expected arc sinus of 2.754", Math.asin(2.754), Double.parseDouble(com.execute(new Calculator(), 2.754, true)), 1E-6);
	}

}
