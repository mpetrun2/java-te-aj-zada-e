/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class AddCommandTest {

	/**
	 * Test method for
	 * {@link hr.fer.zemris.java.gui.calc.commands.operators.AddCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.lang.Double, boolean)}
	 * .
	 */
	@Test
	public void testExecute() {
		AddCommand com = new AddCommand();
		assertEquals("Expected added numbers", 3.17,
				com.execute(new Calculator(), 3.0, 0.17, false), 1E-6);
	}

}
