/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class DivCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.operators.DivCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		DivCommand com = new DivCommand();
		assertEquals("Expected divided numbers", 4,
				com.execute(new Calculator(), 2.0, 0.5, false), 1E-6);
	}

}
