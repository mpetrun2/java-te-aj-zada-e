/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class MulCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.operators.MulCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		MulCommand com = new MulCommand();
		assertEquals("Expected multiplied numbers", 3.00,
				com.execute(new Calculator(), 2.0, 1.5, false), 1E-6);
	}

}
