/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class PowCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.operators.PowCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		PowCommand com = new PowCommand();
		assertEquals("Expected first number raised to the power of second number", 8,
				com.execute(new Calculator(), 2.0, 3.0, false), 1E-6);
		assertEquals("Expected first number raised to the power of second number", 0.125,
				com.execute(new Calculator(), 2.0, 3.0, true), 1E-6);
	}

}
