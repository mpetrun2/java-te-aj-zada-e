/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.operators;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class SubCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.operators.SubCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.lang.Double, boolean)}.
	 */
	@Test
	public void testExecute() {
		SubCommand com = new SubCommand();
		assertEquals("Expected substracted numbers", -1.0,
				com.execute(new Calculator(), 2.0, 3.0, false), 1E-6);
	}

}
