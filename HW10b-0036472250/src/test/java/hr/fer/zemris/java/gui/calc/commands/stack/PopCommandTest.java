/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.stack;

import static org.junit.Assert.*;

import java.util.Stack;

import hr.fer.zemris.java.gui.calc.Calculator;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class PopCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.stack.PopCommand#execute(javax.swing.JLabel, java.util.Stack)}.
	 */
	@Test
	public void testExecute() {
		PopCommand com = new PopCommand();
		Stack<Double> stack = new Stack<>();
		stack.add(2.74);
		assertEquals("Expected last item added to stack.","2.74", com.execute(new Calculator(), 3.14, stack));
	}

}
