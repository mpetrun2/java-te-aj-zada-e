/**
 * 
 */
package hr.fer.zemris.java.gui.calc.commands.stack;

import static org.junit.Assert.*;
import hr.fer.zemris.java.gui.calc.Calculator;

import java.util.Stack;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class PushCommandTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.gui.calc.commands.stack.PushCommand#execute(hr.fer.zemris.java.gui.calc.Calculator, java.lang.Double, java.util.Stack)}.
	 */
	@Test
	public void testExecute() {
		PushCommand com = new PushCommand();
		Stack<Double> stack = new Stack<>();
		Stack<Double> stack2 = new Stack<>();
		stack2.add(2.74);
		assertEquals("Expected same number as sent to command.", "2.74"	, com.execute(new Calculator(), 2.74, stack));
		assertEquals("Expected to get same stack", stack2, stack);
	}

}
