package hr.fer.zemris.java.hw11.jvdraw;

import hr.fer.zemris.java.hw11.jvdraw.components.DrawingListModel;
import hr.fer.zemris.java.hw11.jvdraw.components.JStatusLabel;
import hr.fer.zemris.java.hw11.jvdraw.components.canvas.JCanvas;
import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorChooseAction;
import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.JColorArea;
import hr.fer.zemris.java.hw11.jvdraw.components.property.PropertyDialog;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModelImpl;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.ShapeType;
import hr.fer.zemris.java.hw11.jvdraw.jvd.JVDFile;
import hr.fer.zemris.java.hw11.jvdraw.jvd.JVDParserException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * GUI program for drawing lines, cricles and rectangles onto canvas.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class JVDraw extends JFrame {

	/**
	 * Instance of {@link JCanvas}
	 */
	private JCanvas canvas;

	/**
	 * Objects model.
	 */
	private DrawingModel objects = new DrawingModelImpl();

	/**
	 * Last saved file.
	 */
	private Path lastSavedFile;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Starts program Does not need arguments
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new JVDraw();
			}
		});
	}

	/**
	 * Opens new {@link JVDraw} window.
	 */
	public JVDraw() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLocation(100, 100);
		setSize(800, 600);
		setVisible(true);
		initGui();
	}

	/**
	 * Initializes GUI.
	 */
	private void initGui() {
		getContentPane().setLayout(new BorderLayout());
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitAction.actionPerformed(null);
			}
		});
		canvas = new JCanvas();
		objects.addDrawingModelListener(canvas);
		DrawingListModel listModel = new DrawingListModel();
		objects.addDrawingModelListener(listModel);
		JList<GeometricalObject> list = new JList<>(listModel);
		list.setVisible(true);
		list.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 20));
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					@SuppressWarnings("unchecked")
					JList<GeometricalObject> list = (JList<GeometricalObject>) e
							.getSource();
					int index = list.locationToIndex(e.getPoint());
					GeometricalObject obj = (GeometricalObject) list.getModel()
							.getElementAt(index);
					PropertyDialog prop = (PropertyDialog) obj
							.getPropertyDialog();
					int answer = JOptionPane.showConfirmDialog(JVDraw.this,
							prop, "Change Property", JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.YES_OPTION) {
						objects.update(prop.getUpdatedObject(), index);
					}
				}
			}
		});
		createMenu();
		createToolbar();
		getContentPane().add(canvas, BorderLayout.CENTER);
		getContentPane().add(list, BorderLayout.LINE_END);
		canvas.setHasChanged(false);
	}

	/**
	 * Creates GUI menu
	 */
	private void createMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem newDraw = new JMenuItem("New");
		JMenuItem open = new JMenuItem("Open");
		JMenuItem save = new JMenuItem("Save");
		JMenuItem saveAs = new JMenuItem("Save as");
		JMenuItem export = new JMenuItem("Export");
		JMenuItem exit = new JMenuItem("Exit");
		saveAs.addActionListener(saveAsFileAction);
		exit.addActionListener(exitAction);
		newDraw.addActionListener(newFileAction);
		save.addActionListener(saveFileAction);
		open.addActionListener(openFileAction);
		fileMenu.add(newDraw);
		fileMenu.add(open);
		fileMenu.add(save);
		fileMenu.add(saveAs);
		fileMenu.addSeparator();
		fileMenu.add(export);
		fileMenu.addSeparator();
		fileMenu.add(exit);
		menuBar.add(fileMenu);
		setJMenuBar(menuBar);
	}

	/**
	 * Creates toolbar and status label and adds them to the window.
	 */
	private void createToolbar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(true);
		ButtonGroup shapeChooseGroup = new ButtonGroup();
		JToggleButton line = new JToggleButton("Line");
		JToggleButton circle = new JToggleButton("Circle");
		JToggleButton filledCircle = new JToggleButton("Filled Circle");
		shapeChooseGroup.add(line);
		shapeChooseGroup.add(circle);
		shapeChooseGroup.add(filledCircle);
		line.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				canvas.setObjectType(ShapeType.LINE);
			}
		});
		circle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				canvas.setObjectType(ShapeType.CIRCLE);
			}
		});
		filledCircle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				canvas.setObjectType(ShapeType.FILLEDCIRCLE);
			}
		});
		JColorArea foregroundColor = new JColorArea();
		foregroundColor.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		foregroundColor.setToolTipText("Foreground Color");
		foregroundColor
				.addMouseListener(new ColorChooseAction(foregroundColor));
		foregroundColor.addColorChangeListener(canvas);
		toolBar.add(foregroundColor);
		JColorArea backgroundColor = new JColorArea(Color.BLUE,
				ColorType.BACKGROUND);
		backgroundColor.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		backgroundColor.setToolTipText("Background Color");
		backgroundColor
				.addMouseListener(new ColorChooseAction(backgroundColor));
		backgroundColor.addColorChangeListener(canvas);
		toolBar.add(backgroundColor);
		JStatusLabel status = new JStatusLabel(
				backgroundColor.getCurrentColor(),
				foregroundColor.getCurrentColor());
		status.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		foregroundColor.addColorChangeListener(status);
		backgroundColor.addColorChangeListener(status);
		toolBar.addSeparator();
		toolBar.add(line);
		toolBar.add(circle);
		toolBar.add(filledCircle);
		getContentPane().add(status, BorderLayout.PAGE_END);
		getContentPane().add(toolBar, BorderLayout.PAGE_START);
		line.setSelected(true);
	}

	/**
	 * Action for saving as file to .jvd format.
	 */
	private ActionListener saveAsFileAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					return "JVD file (.jvd)";
				}

				@Override
				public boolean accept(File arg0) {
					if (arg0.isDirectory()) {
						return true;
					}
					if (arg0.getName().endsWith(".jvd")) {
						return true;
					}
					return false;
				}
			});
			int result = fileChooser.showSaveDialog(JVDraw.this);
			if (result == JFileChooser.APPROVE_OPTION) {
				File output = fileChooser.getSelectedFile();
				if (!output.getName().endsWith(".jvd")) {
					JOptionPane.showMessageDialog(JVDraw.this,
							"Invalid extension", "Invalid extension",
							JOptionPane.ERROR_MESSAGE);
					this.actionPerformed(arg0);
					return;
				}
				if (output.exists()) {
					int response = JOptionPane.showConfirmDialog(JVDraw.this,
							"File already exist. Replace file?");
					if (response != JOptionPane.OK_OPTION) {
						this.actionPerformed(arg0);
						return;
					}
				}
				try {
					boolean save = JVDFile.exportAsText(objects,
							output.toPath());
					if (!save) {
						JOptionPane.showMessageDialog(JVDraw.this,
								"Error while saving file", "Error",
								JOptionPane.ERROR_MESSAGE);
						this.actionPerformed(arg0);
						return;
					} else {
						lastSavedFile = output.toPath();
						canvas.setHasChanged(false);
						JOptionPane.showMessageDialog(JVDraw.this,
								"Drawing sucessfully saved to " + output,
								"Drawing saved!",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (IOException e) {
					JOptionPane.showMessageDialog(JVDraw.this,
							"Error while creating or writing file", "Error",
							JOptionPane.ERROR_MESSAGE);
					this.actionPerformed(arg0);
					return;
				}
			}
		}
	};

	/**
	 * Save action.
	 */
	private ActionListener saveFileAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (lastSavedFile == null) {
				if (canvas.isHasChanged()) {
					saveAsFileAction.actionPerformed(null);
				}
			} else {
				if (canvas.isHasChanged()) {
					try {
						boolean save = JVDFile.exportAsText(objects,
								lastSavedFile);
						if (!save) {
							JOptionPane.showMessageDialog(JVDraw.this,
									"Error while saving file", "Error",
									JOptionPane.ERROR_MESSAGE);
							return;
						} else {
							canvas.setHasChanged(false);
							JOptionPane.showMessageDialog(JVDraw.this,
									"Drawing sucessfully saved to "
											+ lastSavedFile, "Drawing saved!",
									JOptionPane.INFORMATION_MESSAGE);
						}
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(JVDraw.this,
								"Error while creating or writing file",
								"Error", JOptionPane.ERROR_MESSAGE);
						return;
					}
				}
			}
		}
	};

	/**
	 * Action before exiting.
	 */
	private ActionListener exitAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (canvas.isHasChanged()) {
				int response = JOptionPane.showConfirmDialog(JVDraw.this,
						"Save drawing before exit?");
				if (response == JOptionPane.OK_OPTION) {
					saveAsFileAction.actionPerformed(null);
					System.exit(0);
				}
				if (response == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
			System.exit(0);
		}
	};

	/**
	 * New File action.
	 */
	private ActionListener newFileAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (canvas.isHasChanged()) {
				int response = JOptionPane.showConfirmDialog(JVDraw.this,
						"Save old drawing?");
				if (response == JOptionPane.OK_OPTION) {
					saveAsFileAction.actionPerformed(null);
				}
				if (response == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
			lastSavedFile = null;
			objects.clear();
			canvas.setHasChanged(false);
		}
	};

	private ActionListener openFileAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println(canvas.isHasChanged());
			if (canvas.isHasChanged()) {
				int response = JOptionPane.showConfirmDialog(JVDraw.this,
						"Save old drawing?");
				if (response == JOptionPane.OK_OPTION) {
					saveAsFileAction.actionPerformed(null);
					newFileAction.actionPerformed(e);
				}
			}
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					return "JVD file (.jvd)";
				}

				@Override
				public boolean accept(File arg0) {
					if (arg0.isDirectory()) {
						return true;
					}
					if (arg0.getName().endsWith(".jvd")) {
						return true;
					}
					return false;
				}
			});
			int response = fileChooser.showOpenDialog(JVDraw.this);
			if (response == JOptionPane.OK_OPTION) {
				File source = fileChooser.getSelectedFile();
				if (!source.getName().endsWith(".jvd")) {
					this.actionPerformed(e);
				}
				try {
					List<GeometricalObject> shapes = JVDFile.importFromFile(source.toPath());
					for(GeometricalObject shape : shapes) {
						objects.add(shape);
					}
				} catch (JVDParserException | IOException e1) {
					JOptionPane
							.showMessageDialog(JVDraw.this, e1.getMessage(),
									"Error while importing!",
									JOptionPane.ERROR_MESSAGE);
					actionPerformed(e);
				}
			}
		}
	};
	
	private ActionListener exportAction = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.setFileFilter(new FileNameExtensionFilter("JPEG file", ".jpg", ".jpeg"));
			fileChooser.setFileFilter(new FileNameExtensionFilter("PNG file", ".png"));
			fileChooser.setFileFilter(new FileNameExtensionFilter("GIF file", ".gif"));
			if (JFileChooser.APPROVE_OPTION != fileChooser.showOpenDialog(JVDraw.this)) {
                return;
			}
			File output = fileChooser.getSelectedFile();
			if (output.exists()) {
                int result = JOptionPane.showOptionDialog(null,
                                "Do you want to overrite the file?", "Warning",
                                JOptionPane.DEFAULT_OPTION,
                                JOptionPane.WARNING_MESSAGE, null, new String[]{"OK", "CANCEL"}, "OK");
                if (result != JOptionPane.YES_OPTION) {
                        return;
                }
			}
			
		}
	};
}
