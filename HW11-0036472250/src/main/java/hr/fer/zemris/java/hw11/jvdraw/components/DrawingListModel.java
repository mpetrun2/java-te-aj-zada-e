/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components;

import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener;

import javax.swing.AbstractListModel;

/**
 * @author Marin Petrunić 0036472250
 * 
 */
public class DrawingListModel extends AbstractListModel<GeometricalObject>
		implements DrawingModelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * model containing {@link GeometricalObject}.
	 */
	private DrawingModel objects;

	@Override
	public int getSize() {
		return objects.getSize();
	}

	@Override
	public GeometricalObject getElementAt(int index) {
		return objects.getObject(index);
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		this.objects = source;
		fireIntervalAdded(this, index0, index1);
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		this.objects = source;
		fireIntervalAdded(this, index0, index1);
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		this.objects = source;
		fireContentsChanged(this, index0, index1);

	}

}
