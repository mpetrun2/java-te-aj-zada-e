/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components;

import java.awt.Color;

import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;
import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;

import javax.swing.JLabel;

/**
 * Provides status message label of currently selected colors.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class JStatusLabel extends JLabel implements ColorChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * color from background.
	 */
	private Color background;

	/**
	 * color from foreground.
	 */
	private Color foreGround;

	/**
	 * Connstructor.
	 * 
	 * @param background
	 *            background color provider
	 * @param foreGround
	 *            foreGroundColorProvider
	 */
	public JStatusLabel(Color background, Color foreGround) {
		super();
		this.background = background;
		this.foreGround = foreGround;
		setText(background, foreGround);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener#newColorSelected
	 * (hr.fer.zemris.java.hw11.jvdraw.components.IColorProvider,
	 * java.awt.Color, java.awt.Color)
	 */
	@Override
	public void newColorSelected(IColorProvider source, Color oldColor,
			Color newColor) {
		if(source.getColorType() == ColorType.BACKGROUND) {
			background = newColor;
		}
		if(source.getColorType() == ColorType.FOREGROUND) {
			foreGround = newColor;
		}
		setText(background, foreGround);
	}

	/**
	 * Creates message from background and foregroundColor.
	 * 
	 * @param backgroundColor
	 * @param foregroundColor
	 */
	public void setText(Color backgroundColor, Color foregroundColor) {
		setText("Foreground color: (" + foregroundColor.getRed() + ", "
				+ foregroundColor.getGreen() + ", " + foregroundColor.getBlue()
				+ "), " + "Background color: (" + backgroundColor.getRed()
				+ ", " + backgroundColor.getGreen() + ", "
				+ backgroundColor.getBlue() + ").");
	}

}
