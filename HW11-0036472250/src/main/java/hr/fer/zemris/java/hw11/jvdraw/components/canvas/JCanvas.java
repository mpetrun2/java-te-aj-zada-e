/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.canvas;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.HashMap;
import java.util.Map;

import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.CircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.FilledCircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.LineObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.ShapeType;
import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;
import hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener;

import javax.swing.JComponent;

/**
 * Represents canvas on which user can draw {@link GeometricalObject}s.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class JCanvas extends JComponent implements DrawingModelListener,
		ColorChangeListener {

	/**
	 * Type of {@link GeometricalObject} to be drawed.
	 */
	private ShapeType type = ShapeType.LINE;

	/**
	 * {@link GeometricalObject} foreground {@link Color}
	 */
	private Color background;

	/**
	 * {@link GeometricalObject} background {@link Color}
	 */
	private Color foreground;
	
	/**
	 * {@link DrawingModel} hols objects to be painted.
	 */
	private DrawingModel objects;

	/**
	 * Starting point of drawed object.
	 */
	private Point startPoint = null;
	
	/**
	 * Temporary {@link GeometricalObject}.
	 */
	private GeometricalObject temporaryObject;
	
	/**
	 * Holds number of specific object type appearance.
	 */
	private Map<ShapeType, Integer> objectTypeCount = new HashMap<>();
	
	/**
	 * Flag if some objects have been changed.
	 */
	private boolean hasChanged = false;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public JCanvas() {

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (startPoint == null) {
					startPoint = e.getPoint();
				} else {
					temporaryObject = null;
					objects.add(createObject(startPoint, e.getPoint(), type, String.valueOf(getObjectCount(type))));
					startPoint = null;
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if(startPoint != null) {
					temporaryObject = createObject(startPoint, e.getPoint(), type, "temp");
					repaint();
				}
			}
		});
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		objects = source;
		repaint();
		hasChanged = true;
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		objects = source;
		repaint();
		hasChanged = true;
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		objects = source;
		repaint();
		hasChanged = true;
	}

	@Override
	public void newColorSelected(IColorProvider source, Color oldColor,
			Color newColor) {
		if (source.getColorType() == ColorType.BACKGROUND) {
			background = newColor;
		}
		if (source.getColorType() == ColorType.FOREGROUND) {
			foreground = newColor;
		}
	}

	@Override
	public void paint(Graphics g) {
		Dimension dim = getSize();
		Insets ins = getInsets();
		g.setColor(Color.WHITE);
		g.fillRect(ins.left, ins.top, (int) dim.getWidth() - ins.left
				- ins.right, (int) dim.getHeight() - ins.top - ins.bottom);
		g.setColor(Color.GRAY);
		for (int i = ins.left, end = (int) (dim.getWidth() - ins.right); i < end; i += 20) {
			g.drawLine(i, ins.top, i, (int) dim.getHeight() - ins.bottom
					- ins.top);
		}
		for (int i = ins.top, end = (int) (dim.getHeight() - ins.bottom); i < end; i += 20) {
			g.drawLine(ins.left, i,
					(int) dim.getWidth() - ins.left - ins.right, i);
		}
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(3));
		if (objects != null) {
			for (int i = 0, end = objects.getSize(); i < end; i++) {
				objects.getObject(i).paint(g);
				;
			}
		}
		if(temporaryObject != null) {
			temporaryObject.paint(g);
		}

	}

	/**
	 * Creates {@link GeometricalObject} and adds it to the {@link DrawingModel}.
	 * 
	 * @param startPoint
	 * @param endPoint
	 * @param type
	 */
	private GeometricalObject createObject(Point startPoint, Point endPoint, ShapeType type, String name) {
		switch (type) {
			case LINE: {
				
				return new LineObject(startPoint, endPoint, name, foreground);
			}
			case CIRCLE: {
				return new CircleObject(startPoint, endPoint,name, foreground);
			}
			case FILLEDCIRCLE: {
				return new FilledCircleObject(startPoint, endPoint, name, foreground, background);
			}
			default: {
				
			}
			break;
		}
		return temporaryObject;
	}

	/**
	 * Sets {@link GeometricalObject} that will be drawed
	 * 
	 * @param type
	 *            {@link ShapeType}
	 */
	public void setObjectType(ShapeType type) {
		this.type = type;
	}
	
	/**
	 * Gets number of {@link ShapeType}.
	 * @param type
	 * @return
	 */
	private int getObjectCount(ShapeType type) {
		if(objectTypeCount.containsKey(type)) {
			objectTypeCount.put(type, objectTypeCount.get(type)+1);
			return objectTypeCount.get(type);
		} else {
			objectTypeCount.put(type, 1);
			return 1;
		}
	}

	/**
	 * @return the hasChanged
	 */
	public boolean isHasChanged() {
		return hasChanged;
	}

	/**
	 * @param hasChanged the hasChanged to set
	 */
	public void setHasChanged(boolean hasChanged) {
		this.hasChanged = hasChanged;
	}
	
	

}
