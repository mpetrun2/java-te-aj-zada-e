/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.color;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JColorChooser;

/**
 * Action when {@link JColorArea} is clicked
 * @author Marin Petrunić 0036472250
 * 
 */
public class ColorChooseAction extends MouseAdapter {

	/**
	 * {@link JColorArea} instance
	 */
	private JColorArea initialColor;

	/**
	 * Constructor
	 * @param color {@link JColorArea} that is source of color
	 */
	public ColorChooseAction(JColorArea color) {
		initialColor = color;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Color newColor = JColorChooser.showDialog(initialColor.getParent(),
				"Choose " + initialColor.getToolTipText(),
				initialColor.getColor());
		if(newColor != null) {
			initialColor.setColor(newColor);
		}
	}

}
