/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.color;

/**
 * Types of color.
 * @author Marin Petrunić 0036472250
 *
 */
public enum ColorType {
	
	BACKGROUND,
	FOREGROUND

}
