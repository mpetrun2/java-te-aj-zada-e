/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.color;

import java.awt.Color;

/**
 * Provider of selected color property
 * @author Marin Petrunić 0036472250
 *
 */
public interface IColorProvider {

	/**
	 * Provides color property.
	 * @return
	 */
	public Color getCurrentColor();
	
	/**
	 * Provides type of color.
	 * @return
	 */
	public ColorType getColorType();
}
