/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.color;

import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

/**
 * Represents square 15x15 component with selected background.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class JColorArea extends JComponent implements IColorProvider {

	/**
	 * Component color.
	 */
	private Color color;
	
	/**
	 * ColorType.
	 */
	private ColorType type;
	
	
	
	/**
	 * List of color observers.
	 */
	private List<ColorChangeListener> listeners = new ArrayList<>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor. Sets component color.
	 * 
	 * @param color
	 *            component color
	 * @throws IllegalArgumentException if color is null
	 */
	public JColorArea(Color color, ColorType type) {
		if (color == null) {
			throw new IllegalArgumentException("Color cannot be null!");
		}
		this.color = color;
		this.type = type;
	}

	/**
	 * Constructor. Creates new {@link JColorArea} with color set to
	 * {@link Color#RED}.
	 */
	public JColorArea() {
		this(Color.RED, ColorType.FOREGROUND);
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(color);
		Dimension dim = getSize();
		Insets ins = getInsets();
		g.fillRect(ins.left, ins.top, (int) dim.getWidth() - ins.left
				- ins.right, (int) dim.getHeight() - ins.top - ins.bottom);
	}
	
	/**
	 * sets given color and ffills background.
	 * @param color new {@link JColorArea} color
	 * @throws IllegalArgumentException if color is null
	 */
	public void setColor(Color color) {
		if(color == null) {
			throw new IllegalArgumentException("Color cannot be null!");
		}
		Color oldColor = this.color;
		this.color = color;
		notifiyColorChangeListeners(oldColor, color);
		repaint();
	}
	
	/**
	 * Returns curent color.
	 * @return
	 */
	public Color getColor() {
		return color;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(30, 30);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(30, 30);
	}

	@Override
	public Color getCurrentColor() {
		return getColor();
	}
	
	/**
	 * Addse {@link ColorChangeListener} to list of listeners.
	 * @param l {@link ColorChangeListener} to be added
	 * @throws IllegalArgumentException if null reference sent
	 */
	public void addColorChangeListener(ColorChangeListener l) {
		if(l == null) {
			throw new IllegalArgumentException("Listener cannot be null!");
		}
		listeners.add(l);
		notifiyColorChangeListeners(color, color);
	}
	
	/**
	 * Removes listener from a list of {@link ColorChangeListener}
	 * if it exist
	 * @param l {@link ColorChangeListener} to be removed
	 */
	public void removeColorChangeListener(ColorChangeListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Notifies all listeners about {@link Color} change.
	 * @param oldColor
	 * @param newColor
	 */
	private void notifiyColorChangeListeners(Color oldColor, Color newColor) {
		for(ColorChangeListener l : listeners) {
			l.newColorSelected(this, oldColor, newColor);
		}
	}

	@Override
	public ColorType getColorType() {
		return type;
	}
}
