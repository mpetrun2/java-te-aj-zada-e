/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.property;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorChooseAction;
import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;
import hr.fer.zemris.java.hw11.jvdraw.components.color.JColorArea;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.CircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * {@link PropertyDialog} for modifiying {@link CircleObject} properties.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class CirclePropertyDialog extends JPanel implements PropertyDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Updated {@link CircleObject} instance.
	 */
	private CircleObject newCircle;

	/**
	 * Constructor.
	 * @param circle original {@link CircleObject}
	 */
	public CirclePropertyDialog(CircleObject circle) {
		newCircle = new CircleObject(circle.getStartPoint(),
				circle.getEndPoint(), circle.getName(), circle.getForeground());
		newCircle.setRadius(circle.getRadius());
		JPanel p  = new JPanel();
		p.setLayout(new GridLayout(5, 2));
		p.add(new JLabel("Name :"));
		final JTextField name = new JFormattedTextField(circle.getName());
		name.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
		});
		p.add(name);
		p.add(new JLabel("Start x :"));
		final JSpinner x1 = new JSpinner();
		x1.setValue(circle.getStartPoint().x);
		p.add(x1);
		p.add(new JLabel("Start y :"));
		x1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.getStartPoint().x = (int) x1.getValue();
			}
		});
		final JSpinner y1 = new JSpinner();
		y1.setValue(circle.getStartPoint().y);
		y1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.getStartPoint().y = (int) y1.getValue();
			}
		});
		p.add(y1);
		p.add(new JLabel("Radius:"));
		final JSpinner radius = new JSpinner();
		radius.setValue(circle.getRadius());
		radius.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.setRadius((int)radius.getValue());
			}
		});
		p.add(radius);
		p.add(new JLabel("Color :"));
		JColorArea color = new JColorArea(circle.getForeground(), ColorType.FOREGROUND);
		color
		.addMouseListener(new ColorChooseAction(color));
		color.addColorChangeListener(new ColorChangeListener() {
			
			@Override
			public void newColorSelected(IColorProvider source, Color oldColor,
					Color newColor) {
				newCircle.setForeground(newColor);
				
			}
		});
		p.add(color);
		add(p, BorderLayout.CENTER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.property.PropertyDialog#
	 * getUpdatedObject()
	 */
	@Override
	public GeometricalObject getUpdatedObject() {
		return newCircle;
	}

}
