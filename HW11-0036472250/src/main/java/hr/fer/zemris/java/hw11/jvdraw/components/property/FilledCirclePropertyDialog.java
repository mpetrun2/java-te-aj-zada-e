/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.property;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorChooseAction;
import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;
import hr.fer.zemris.java.hw11.jvdraw.components.color.JColorArea;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.FilledCircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * {@link PropertyDialog} able to modify
 * propertis of {@link FilledCircleObject}.
 * @author Marin Petrunić 0036472250
 *
 */
public class FilledCirclePropertyDialog extends JPanel implements
		PropertyDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Updated instance of {@link FilledCircleObject}.
	 */
	private FilledCircleObject newCircle;
	
	/**
	 * Constructor.
	 * @param circle original {@link FilledCircleObject}
	 */
	public FilledCirclePropertyDialog(FilledCircleObject circle) {
		newCircle = new FilledCircleObject(circle.getStartPoint(),
				circle.getEndPoint(), circle.getName(), circle.getForeground(), circle.getBackground());
		newCircle.setRadius(circle.getRadius());
		JPanel p  = new JPanel();
		p.setLayout(new GridLayout(6, 2));
		p.add(new JLabel("Name :"));
		final JTextField name = new JFormattedTextField(circle.getName());
		name.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				newCircle.setName(name.getText());
			}
		});
		p.add(name);
		p.add(new JLabel("Start x :"));
		final JSpinner x1 = new JSpinner();
		x1.setValue(circle.getStartPoint().x);
		p.add(x1);
		p.add(new JLabel("Start y :"));
		x1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.getStartPoint().x = (int) x1.getValue();
			}
		});
		final JSpinner y1 = new JSpinner();
		y1.setValue(circle.getStartPoint().y);
		y1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.getStartPoint().y = (int) y1.getValue();
			}
		});
		p.add(y1);
		p.add(new JLabel("Radius:"));
		final JSpinner radius = new JSpinner();
		radius.setValue(circle.getRadius());
		radius.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newCircle.setRadius((int)radius.getValue());
			}
		});
		p.add(radius);
		p.add(new JLabel("Foreground Color :"));
		JColorArea color = new JColorArea(circle.getForeground(), ColorType.FOREGROUND);
		color
		.addMouseListener(new ColorChooseAction(color));
		color.addColorChangeListener(new ColorChangeListener() {
			
			@Override
			public void newColorSelected(IColorProvider source, Color oldColor,
					Color newColor) {
				newCircle.setForeground(newColor);
				
			}
		});
		p.add(color);
		p.add(new JLabel("Background Color :"));
		JColorArea backgroudColor = new JColorArea(circle.getBackground(), ColorType.BACKGROUND);
		backgroudColor
		.addMouseListener(new ColorChooseAction(backgroudColor));
		backgroudColor.addColorChangeListener(new ColorChangeListener() {
			
			@Override
			public void newColorSelected(IColorProvider source, Color oldColor,
					Color newColor) {
				newCircle.setBackground(newColor);
				
			}
		});
		p.add(backgroudColor);
		add(p, BorderLayout.CENTER);
	}

	
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.property.PropertyDialog#getUpdatedObject()
	 */
	@Override
	public GeometricalObject getUpdatedObject() {
		return newCircle;
	}

}
