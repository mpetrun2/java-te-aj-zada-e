/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.property;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorChooseAction;
import hr.fer.zemris.java.hw11.jvdraw.components.color.ColorType;
import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;
import hr.fer.zemris.java.hw11.jvdraw.components.color.JColorArea;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.LineObject;
import hr.fer.zemris.java.hw11.jvdraw.observers.ColorChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Property change dialog for changing 
 * properties of {@link LineObject}.
 * @author Marin Petrunić 0036472250
 *
 */
public class LinePropertyDialog extends JPanel implements PropertyDialog{
	
	/**
	 * Updated {@link LineObject}
	 */
	private LineObject newLine;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor.
	 * @param line original {@link LineObject}
	 */
	public LinePropertyDialog(LineObject line) {
		newLine = new LineObject(line.getStartPoint(), line.getEndPoint(), line.getName(), line.getColor());
		JPanel p  = new JPanel();
		p.setLayout(new GridLayout(6, 2));
		p.add(new JLabel("Name :"));
		final JTextField name = new JFormattedTextField(line.getName());
		name.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				newLine.setName(name.getText());
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				newLine.setName(name.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				newLine.setName(name.getText());
			}
		});
		p.add(name);
		p.add(new JLabel("x1 :"));
		final JSpinner x1 = new JSpinner();
		x1.setValue(line.getStartPoint().x);
		p.add(x1);
		p.add(new JLabel("y1 :"));
		x1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newLine.getStartPoint().x = (int) x1.getValue();
			}
		});
		final JSpinner y1 = new JSpinner();
		y1.setValue(line.getStartPoint().y);
		y1.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newLine.getStartPoint().y = (int) y1.getValue();
			}
		});
		p.add(y1);
		p.add(new JLabel("x2 :"));
		final JSpinner x2 = new JSpinner();
		x2.setValue(line.getEndPoint().x);
		x2.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newLine.getEndPoint().x = (int) x2.getValue();
			}
		});
		p.add(x2);
		p.add(new JLabel("y2 :"));
		final JSpinner y2 = new JSpinner();
		y2.setValue(line.getEndPoint().y);
		y2.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				newLine.getEndPoint().y = (int) y2.getValue();
			}
		});
		p.add(y2);
		p.add(new JLabel("Color :"));
		JColorArea color = new JColorArea(line.getColor(), ColorType.FOREGROUND);
		color
		.addMouseListener(new ColorChooseAction(color));
		color.addColorChangeListener(new ColorChangeListener() {
			
			@Override
			public void newColorSelected(IColorProvider source, Color oldColor,
					Color newColor) {
				newLine.setColor(newColor);
				
			}
		});
		p.add(color);
		add(p, BorderLayout.CENTER);
	}

	@Override
	public GeometricalObject getUpdatedObject() {
		return newLine;
	}

}
