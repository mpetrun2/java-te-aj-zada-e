/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.property;

import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public interface PropertyDialog {
	
	/**
	 * Returns {@link GeometricalObject} with updated propertys.
	 * @return
	 */
	public abstract GeometricalObject getUpdatedObject();
}
