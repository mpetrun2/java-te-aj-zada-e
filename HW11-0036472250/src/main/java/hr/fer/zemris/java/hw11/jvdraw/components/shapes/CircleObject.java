/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import hr.fer.zemris.java.hw11.jvdraw.components.property.CirclePropertyDialog;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

/**
 * Circle object that can draw himself on {@link Graphics} object.
 * @author Marin Petrunić 0036472250
 *
 */
public class CircleObject extends GeometricalObject {
	
	/**
	 * Circle color.
	 */
	private Color foreground;
	
	/**
	 * Circle radius.
	 */
	private int radius;


	/**
	 * @param startPoint
	 * @param endPoint
	 */
	public CircleObject(Point startPoint, Point endPoint, String name, Color foreground) {
		super(startPoint, endPoint, name);
		this.foreground = foreground;
	}
	
	public CircleObject(Point startPoint, int radius, String name, Color foreground) {
		super(startPoint, startPoint, name);
		this.foreground = foreground;
		this.radius = radius;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		g.setColor(foreground);
		if(radius == 0) {
			radius = (int) startPoint.distance(endPoint);
		}
		g.drawOval(startPoint.x - radius, startPoint.y - radius, 2 * radius, 2 * radius);
	}
	
	@Override
	public String toString() {
		return "Circle " + name;
	}

	@Override
	public JPanel getPropertyDialog() {
		return new CirclePropertyDialog(this);
	}
	
	/**
	 * @return the foreground
	 */
	public Color getForeground() {
		return foreground;
	}

	/**
	 * @param foreground the foreground to set
	 */
	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}

	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.CIRCLE;
	}

	@Override
	public String getData() {
		String data = new String();
		data += getType() + " "
				+ name + " " + startPoint.x + " "
				+ startPoint.y + " "
				+ radius + " "
				+ foreground.getRed() + " "
				+ foreground.getGreen() + " "
				+ foreground.getBlue();
		return data;
	}
	
	

}
