/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener;

/**
 * Interface for classes holding colections of {@link GeometricalObject}s.
 * @author Marin Petrunić 0036472250
 *
 */
public interface DrawingModel {
	
	/**
	 * Get number of {@link GeometricalObject}s.
	 * @return
	 */
	 public int getSize();
	 
	 /**
	  * 
	  * @param index
	  * @return
	  */
	 public GeometricalObject getObject(int index);
	 
	 /**
	  * Add new {@link GeometricalObject} to the collection.
	  * @param object
	  */
	 public void add(GeometricalObject object);
	 
	 /**
	  * Removes object at given index.
	  * @param index
	  */
	 public void remove(int index);
	 
	 /**
	  * Removes all objects.
	  */
	 public void clear();
	 
	 /**
	  * Element at index position is modified.
	  * @param index
	  */
	 public void update(GeometricalObject object, int index);
	 
	 /**
	  * Adds new {@link DrawingModelListener} to the list
	  * of listeners
	  * @param l {@link DrawingModelListener} to be added
	  */
	 public void addDrawingModelListener(DrawingModelListener l);
	 
	 /**
	  * Removes {@link DrawingModelListener} from list of listeners
	  * @param l {@link DrawingModelListener} to be removed
	  */
	 public void removeDrawingModelListener(DrawingModelListener l);

}
