/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener;

/**
 * Implementation of {@link DrawingModel} representing
 * {@link GeometricalObject} model.
 * @author Marin Petrunić 0036472250
 *
 */
public class DrawingModelImpl implements DrawingModel {
	
	/**
	 * List of {@link GeometricalObject}.
	 */
	private List<GeometricalObject> objects = new ArrayList<>();
	
	/**
	 * list of {@link DrawingModelListener}
	 */
	private List<DrawingModelListener> listeners = new ArrayList<>();

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel#getSize()
	 */
	@Override
	public int getSize() {
		return objects.size();
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel#getObject(int)
	 */
	@Override
	public GeometricalObject getObject(int index) {
		return objects.get(index);
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel#add(hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject)
	 */
	@Override
	public void add(GeometricalObject object) {
		objects.add(object);
		fireObjectsAdded(objects.size()-1, objects.size()-1);
	}
	
	@Override
	public void remove(int index) {
		objects.remove(index);
		fireObjectsRemoved(index, index);
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel#addDrawingModelListener(hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener)
	 */
	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		listeners.add(l);
		fireObjectsChange(0, objects.size()-1);
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel#removeDrawingModelListener(hr.fer.zemris.java.hw11.jvdraw.observers.DrawingModelListener)
	 */
	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		listeners.add(l);
	}
	
	/**
	 * Notifies all listeners that objects are changed.
	 * @param startIndex
	 * @param endIndex
	 */
	private void fireObjectsChange(int startIndex, int endIndex) {
		for(DrawingModelListener l : listeners) {
			l.objectsChanged(this, startIndex, endIndex);
		}
	}
	
	/**
	 * Notifies all listeners that objects are added.
	 * @param startIndex
	 * @param endIndex
	 */
	private void fireObjectsAdded(int startIndex, int endIndex) {
		for(DrawingModelListener l : listeners) {
			l.objectsAdded(this, startIndex, endIndex);
		}
	}
	
	/**
	 * Notidies all listeners that some objects are removed.
	 * @param startIndex
	 * @param endIndex
	 */
	private void fireObjectsRemoved(int startIndex, int endIndex) {
		for(DrawingModelListener l : listeners) {
			l.objectsRemoved(this, startIndex, endIndex);
		}
	}

	@Override
	public void clear() {
		int size = objects.size();
		objects.clear();
		fireObjectsRemoved(0, size-1);
	}

	@Override
	public void update(GeometricalObject object, int index) {
		objects.remove(index);
		objects.add(index, object);
		fireObjectsChange(index, index);
	}

}
