/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import hr.fer.zemris.java.hw11.jvdraw.components.property.FilledCirclePropertyDialog;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

/**
 * Object that knows how to draw
 * himself on {@link Graphics} object.
 * @author Marin Petrunić 0036472250
 *
 */
public class FilledCircleObject extends GeometricalObject {

	/**
	 * Fill color.
	 */
	private Color background;
	
	/**
	 * Outline color.
	 */
	private Color foreground;
	
	/**
	 * Circle radius.
	 */
	private int radius;
	
	/**
	 * Constructor.
	 * @param startPoint
	 * @param endPoint
	 * @param foreground
	 * @param background
	 */
	public FilledCircleObject(Point startPoint, Point endPoint, String name, Color foreground, Color background) {
		super(startPoint, endPoint, name);
		this.background = background;
		this.foreground = foreground;
	}
	
	public FilledCircleObject(Point startPoint, int radius, String name, Color foreground, Color background) {
		super(startPoint, startPoint, name);
		this.radius = radius;
		this.background = background;
		this.foreground = foreground;
	}
	

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		g.setColor(background);
		if(radius == 0) {
			radius = (int) startPoint.distance(endPoint);
		}
		g.fillOval(startPoint.x - radius, startPoint.y - radius, 2 * radius, 2 * radius);
		g.setColor(foreground);
		g.drawOval(startPoint.x - radius, startPoint.y - radius, 2 * radius, 2 * radius);
	}
	
	@Override
	public String toString() {
		return "Filled Circle " + name;
	}

	@Override
	public JPanel getPropertyDialog() {
		return new FilledCirclePropertyDialog(this);
	}

	/**
	 * @return the background
	 */
	public Color getBackground() {
		return background;
	}

	/**
	 * @param background the background to set
	 */
	public void setBackground(Color background) {
		this.background = background;
	}

	/**
	 * @return the foreground
	 */
	public Color getForeground() {
		return foreground;
	}

	/**
	 * @param foreground the foreground to set
	 */
	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}

	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.FILLEDCIRCLE;
	}

	@Override
	public String getData() {
		String data = new String();
		data += getType() + " "
				+ name + " " + startPoint.x + " "
				+ startPoint.y + " "
				+ radius + " "
				+ foreground.getRed() + " "
				+ foreground.getGreen() + " "
				+ foreground.getBlue() + " "
				+ background.getRed() + " "
				+ background.getGreen() + " "
				+ background.getBlue();
		return data;
	}

	
}
