/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import hr.fer.zemris.java.hw11.jvdraw.components.property.PropertyDialog;

import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

/**
 * Abstract model of paintable {@link GeometricalObject}.
 * @author Marin Petrunić 0036472250
 *
 */
public abstract class GeometricalObject {
	
	/**
	 * start point.
	 */
	protected Point startPoint;
	
	/**
	 * endpoint.
	 */
	protected Point endPoint;
	
	/**
	 * Object name.
	 */
	protected String name;
	
	@SuppressWarnings("unused")
	private GeometricalObject() {
	}
	
	
	/**
	 * Default constructor.
	 * @param startPoint
	 * @param endPoint
	 * @throws IllegalArgumentException if one coordiante is null
	 */
	public GeometricalObject(Point startPoint, Point endPoint, String name) {
		if(startPoint == null || endPoint == null) {
			throw new IllegalArgumentException("Coordinates cannot be null.");
		}
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.name = name;
	}
	
	
	
	/**
	 * @return the startPoint
	 */
	public Point getStartPoint() {
		return startPoint;
	}


	/**
	 * @param startPoint the startPoint to set
	 */
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}


	/**
	 * @return the endPoint
	 */
	public Point getEndPoint() {
		return endPoint;
	}


	/**
	 * @param endPoint the endPoint to set
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Returns specific {@link PropertyDialog}.
	 * @return
	 */
	public abstract JPanel getPropertyDialog();
	
	/**
	 * Paints specific object on given {@link Graphics}.
	 * @param g
	 */
	public abstract void paint(Graphics g);
	
	/**
	 * Returns {@link ShapeType} of specific object.
	 * @return
	 */
	public abstract ShapeType getType();
	
	/**
	 * Returns string representation of specific
	 * {@link GeometricalObject} in format <p>
	 * <code>{@link ShapeType} name startPoint endPoint foreground background</code>
	 * </p>
	 * @return
	 */
	public abstract String getData();

}
