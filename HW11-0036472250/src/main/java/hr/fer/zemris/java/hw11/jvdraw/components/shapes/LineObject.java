/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

import hr.fer.zemris.java.hw11.jvdraw.components.property.LinePropertyDialog;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

/**
 * Object that knows how to draw himself on
 * {@link Graphics} object.
 * @author Marin Petrunić 0036472250
 *
 */
public class LineObject extends GeometricalObject {
	
	/**
	 * Line color.
	 */
	private Color color;
	
	/**
	 * Constructor.
	 * @param startPoint
	 * @param endPoint
	 * @param foreground
	 */
	public LineObject(Point startPoint, Point endPoint, String name, Color foreground) {
		super(startPoint, endPoint, name);
		color = foreground;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(color);
		g.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
	}
	
	@Override
	public String toString() {
		return "Line " + name;
	}

	@Override
	public JPanel getPropertyDialog() {
		return new LinePropertyDialog(this);
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public ShapeType getType() {
		return ShapeType.LINE;
	}

	@Override
	public String getData() {
		String data = new String();
		data += getType() + " "
				+ name + " " + startPoint.x + " "
				+ startPoint.y + " "
				+ endPoint.x + " "
				+ endPoint.y + " "
				+ color.getRed() + " "
				+ color.getGreen() + " "
				+ color.getBlue();
		return data;
	}
	
	

}
