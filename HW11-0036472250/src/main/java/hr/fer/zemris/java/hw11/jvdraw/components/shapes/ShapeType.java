/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.components.shapes;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public enum ShapeType {

	 LINE,
	 CIRCLE,
	 FILLEDCIRCLE
}
