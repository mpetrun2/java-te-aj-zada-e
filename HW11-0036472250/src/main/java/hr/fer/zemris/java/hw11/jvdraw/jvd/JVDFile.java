/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.jvd;

import java.awt.Color;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.hw11.jvdraw.components.shapes.CircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.FilledCircleObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.GeometricalObject;
import hr.fer.zemris.java.hw11.jvdraw.components.shapes.LineObject;

/**
 * Class for writing {@link GeometricalObject} into formated file with extension
 * ".jvd".
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class JVDFile {

	/**
	 * {@link DrawingModel} instance containing objects to export.
	 */
	private DrawingModel objects;

	/**
	 * Constructor.
	 * 
	 * @param objects
	 */
	public JVDFile(DrawingModel objects) {
		this.objects = objects;
	}

	/**
	 * Exports given {@link DrawingModel} objects to ".jvd" file with given
	 * fileName.
	 * 
	 * @param objects
	 *            object to be exported
	 * @param destination
	 *            path to destination folder
	 * @param fileName
	 *            name of exported file
	 * @return
	 * @throws IOException
	 */
	public static boolean exportAsText(DrawingModel objects, Path destination)
			throws IOException {
		JVDFile writer = new JVDFile(objects);
		return writer.exportObjectsAsText(destination);
	}

	public static List<GeometricalObject> importFromFile(Path source)
			throws IOException, JVDParserException {
		JVDFile reader = new JVDFile(null);
		return reader.importObjectsFromFile(source);
	}

	public List<GeometricalObject> importObjectsFromFile(Path source)
			throws IOException, JVDParserException {
		List<GeometricalObject> shapes = new ArrayList<>();
		Scanner r = new Scanner(Files.newBufferedReader(source,
				StandardCharsets.UTF_8));
		while (r.hasNext()) {
			String line = r.nextLine();
			if (line.isEmpty())
				break;
			String[] shapeParams = line.split(" ");
			switch (shapeParams[0]) {
			case "LINE": {
				if (shapeParams.length != 9) {
					throw new JVDParserException(
							"Invalid number of LINE arguments");
				}
				shapes.add(new LineObject(new Point(Integer
						.parseInt(shapeParams[2]), Integer
						.parseInt(shapeParams[3])), new Point(Integer
						.parseInt(shapeParams[4]), Integer
						.parseInt(shapeParams[5])), shapeParams[1], new Color(
						Integer.parseInt(shapeParams[6]), Integer
								.parseInt(shapeParams[7]), Integer
								.parseInt(shapeParams[8]))));
			}
				break;
			case "CIRCLE": {
				if (shapeParams.length != 8) {
					throw new JVDParserException(
							"Invalid number of CIRCLE arguments");
				}
				shapes.add(new CircleObject(new Point(Integer
						.parseInt(shapeParams[2]), Integer
						.parseInt(shapeParams[3])), Integer
						.parseInt(shapeParams[4]), shapeParams[1], new Color(
						Integer.parseInt(shapeParams[5]), Integer
								.parseInt(shapeParams[6]), Integer
								.parseInt(shapeParams[7]))));
			}
				break;
			case "FILLEDCIRCLE": {
				if (shapeParams.length != 11) {
					throw new JVDParserException(
							"Invalid number of FILLEDCIRCLE arguments");
				}
				shapes.add(new FilledCircleObject(new Point(Integer
						.parseInt(shapeParams[2]), Integer
						.parseInt(shapeParams[3])), Integer
						.parseInt(shapeParams[4]), shapeParams[1], new Color(
						Integer.parseInt(shapeParams[5]), Integer
								.parseInt(shapeParams[6]), Integer
								.parseInt(shapeParams[7])), new Color(Integer
						.parseInt(shapeParams[8]), Integer
						.parseInt(shapeParams[9]), Integer
						.parseInt(shapeParams[10]))));
			}
				break;
			default: {
				throw new JVDParserException("Unknown Geometrical Object");
			}
			}
		}
		r.close();
		return shapes;
	}

	public boolean exportObjectsAsText(Path destination) throws IOException {
		if (destination == null) {
			return false;
		}
		if (!Files.exists(destination)) {
			Files.createFile(destination);
		}
		BufferedWriter w = Files.newBufferedWriter(destination,
				StandardCharsets.UTF_8);
		for (int i = 0, end = objects.getSize(); i < end; i++) {
			w.write(objects.getObject(i).getData() + "\n");
		}
		w.flush();
		return true;
	}
}
