/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.observers;

import java.awt.Color;

import hr.fer.zemris.java.hw11.jvdraw.components.color.IColorProvider;

/**
 * Listener for change of color property.
 * @author Marin Petrunić 0036472250
 *
 */
public interface ColorChangeListener {
	/**
	 * This method will be called if color changes
	 * @param source color provider
	 * @param oldColor old selected color
	 * @param newColor new selected color
	 */
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
