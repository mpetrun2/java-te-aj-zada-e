/**
 * 
 */
package hr.fer.zemris.java.hw11.jvdraw.observers;

import hr.fer.zemris.java.hw11.jvdraw.components.shapes.DrawingModel;

/**
 * Interface for {@link DrawingModel} listeners.
 * @author Marin Petrunić 0036472250
 *
 */
public interface DrawingModelListener {
	
	/**
	 * Notification when some objects are added to the {@link DrawingModel}.
	 * @param source {@link DrawingModel} changed
	 * @param index0 first index of added object
	 * @param index1 last index of added object
	 */
	 public void objectsAdded(DrawingModel source, int index0, int index1);
	 
	 /**
	  * Notification when some objects are removed from {@link DrawingModel}.
	  * @param source {@link DrawingModel} changed
	  * @param index0 first index of removed object
	  * @param index1 last index of removed object
	  */
	 public void objectsRemoved(DrawingModel source, int index0, int index1);
	 
	 /**
	  * Notification when some objects in {@link DrawingModel} are changed.
	  * @param source {@link DrawingModel} changed
	  * @param index0 first index of changed objects
	  * @param index1 last index of changed objects
	  */
	 public void objectsChanged(DrawingModel source, int index0, int index1);

}
