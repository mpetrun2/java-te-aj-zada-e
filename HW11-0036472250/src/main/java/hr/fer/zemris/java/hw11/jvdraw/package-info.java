/**
 * Package containing all classes needed for JVDraw to work properly.
 * @author Marin Petrunić 0036472250
 *
 */
package hr.fer.zemris.java.hw11.jvdraw;