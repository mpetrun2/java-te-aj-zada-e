/**
 * 
 */
package hr.fer.zemris.java.custom.collections;

/**
 * @author Marin Petrunić 0036472250
 *
 * Class that is representation of array of Object references
 * Duplicate Objects are allowed, but forbids inserting  <code>null</code> reference
 */
public class ArrayBackedIndexedCollection {
	
	/**
	 * Test program for {@link ArrayBackedIndexedCollection} class
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayBackedIndexedCollection col = new  ArrayBackedIndexedCollection(2);
		col.add(new Integer(20));
		col.add("New York");
		col.add("San Francisco");  // here the internal array is reallocated
		System.out.println(col.contains("New York")); // writes: true
		col.remove(1); // removes "New York"; shifts "San Francisco" to position 1
		col.insert("Los Angeles", 1);
		System.out.println(col.get(1)); 
		System.out.println(col.get(2)); // writes: "San Francisco"
		System.out.println(col.size()); // writes: 3
	}
	
	
	/**
	 * Current number of Object in Array
	 */
	private Integer size=0;
	
	/**
	 * Maximum Array capacity
	 */
	private Integer capacity=0;
	
	/**
	 * Array of Object references
	 */
	private Object[] elements;
	
	/**
	 * Default constructor
	 * 
	 * Sets variable <code>capacity</code> to default value.
	 * Default capacity is <b>16</b>
	 * Initialize variable (<code>elements</code>) to maximum capacity defined in variable <code>capacity</code>
	 */
	public ArrayBackedIndexedCollection(){
		this.capacity=16;
		this.setArrayCapacitiy(this.capacity);
	}

	/**
	 * Constructor that sets maximum capcity defined in <code>initialCapacity</code> and initialize array with this capacity
	 * @param initialcapacity requested maximum array capacity
	 * @throws IllegalArgumentException if argument is less then <b>1</b>
	 */
	public ArrayBackedIndexedCollection(Integer initialCapacity) {
		if(initialCapacity < 1) throw new IllegalArgumentException("Vrijednost argumenta initialCapacity mora biti veća ili jednaka 1.");
		this.setArrayCapacitiy(initialCapacity);
	}
	
	/**
	 * private method for increasing array capacity
	 * @param capacity new maximum capacity
	 */
	private void setArrayCapacitiy(int capacity){
		Object[] tmp=this.elements;
		this.elements=new Object[capacity];
		for(int i=0; i<this.size; i++)
		{
			this.elements[i]=tmp[i];
		}
		this.capacity=capacity;
	}
	
	/**
	 * Examine if size of array is <b>0</b>
	 * @return <code>true</code> if empty, <code>false</code> otherwise.
	 */
	public boolean isEmpty(){
		return 	this.size > 0 ? false : true;
	}
	
	/**
	 * Counting elements in array.
	 * @return <code>int</code> number of elements in array
	 */
	public int size(){
		return this.size;
	}
	
	
	/**
	 * Adds <code>Object</code> in array of references to first available place
	 * @param value <code>Object</code> added to array
	 */
	public void add(Object value){
		this.insert(value, this.size);
	}
	
	/**
	 * Retrieves Object at given position
	 * 	Object will <b>not</b> be removed
	 * @param index position of requested Object
	 * @return requested Object
	 * @throws IndexOutOfBoundsException if given position is out of range
	 */
	public Object get(int index){
		if(index<0 || index>this.size-1) throw new IndexOutOfBoundsException("Index mora biti veći od -1 i manji od "+this.size+".");
		return this.elements[index];
	}
	
	/**
	 * Removes Object reference at given position.
	 * Object with position higher than removed will be shifted one position lower(left).
	 * Remove object cannot be restored.
	 * @param index position of Object to remove
	 * @throws IndexOutOfBoundsException if given position is out of range
	 */
	public void remove(int index){
		if(index<0 || index >this.size-1) throw new IndexOutOfBoundsException("Index mora biti veći od -1 i manji od "+this.size+".");
		for(; index<this.size-1; index++){
			this.elements[index]=this.elements[index+1];
		}
		this.elements[index]=null;
		this.size--;
	}
	
	/**
	 * Inserts Object at given position
	 * If position points at first available place method behaves like method <code>add()</code>
	 * @param value Object to insert
	 * @param position inserting position
	 * @throws IllegalArgumentException if Object equals <code>null</code>
	 * @throws IndexOutOfBoundsException if given position is out of range
	 */
	public void insert(Object value, int position){
		if(value==null)	throw new IllegalArgumentException("Objekt nemože biti null.");
		if(position<0 || position>this.size)	throw new IndexOutOfBoundsException("Index mora biti veći od -1 i manji od "+this.size+".");
		if(this.size==this.capacity){
			this.setArrayCapacitiy(this.capacity*2);
		}
		int index=position;
		System.arraycopy(this.elements, index, this.elements, index+1, this.size-index);
		this.elements[index]=value;
		this.size++;
	}
	
	/**
	 * Search position of given Object.
	 * @param value search Object
	 * @return int  position of given Object  or <b>-1</b> if such object doesn't exist
	 * @throws IllegalArgumentException if given Object equals <code>null</code>
	 */
	public int indexOf(Object value){
		if(value==null)	throw new IllegalArgumentException("Ne može se tražiti objekt koji je jednak null.");
		for(int index=0;index<this.size();index++)
		{
			if(value.equals(this.elements[index])) return index;
		}
		return -1;
	}
	
	/**
	 * Examine if array contains given Object
	 * @param value Object to be searched
	 * @return <code>true</code> if given object is found, <code>false</code> otherwise
	 */
	public boolean contains(Object value){
		return (this.indexOf(value) == -1) ? false : true;
	}
	
	/**
	 * Removes all Object references from array.
	 * New array size is <b>0</b>
	 */
	public void clear()
	{
		this.elements=new Object[this.capacity];
		this.size=0;
	}
	
}
