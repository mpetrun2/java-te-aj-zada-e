/**
 * 
 */
package hr.fer.zemris.java.custom.collections;

/**
 * @author Marin Petrunić 0036472250
 * Exception derived from {@link RuntimeException}
 * It is thrown when trying to pop element from empty stack
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6307697858263274779L;

	/**
	 * 
	 */
	public EmptyStackException() {
		
	}

	/**
	 * @param message
	 */
	public EmptyStackException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public EmptyStackException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyStackException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyStackException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
