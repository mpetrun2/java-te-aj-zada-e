/**
 * 
 */
package hr.fer.zemris.java.custom.collections;

/**
 * @author Marin Petrunić  0036472250
 *
 * Stack implementation
 * Using ArrayBackedIndexedCollection as data storage
 */
public class ObjectStack {

	/**
	 * ArrayBackedIndexedCollection instance that is used
	 * as data storage
	 */
	private ArrayBackedIndexedCollection stack;
	
	/**
	 * Constructor creates new array with default size
	 * Array size is dynamically increased as stack grows
	 */
	public ObjectStack(){
		this.stack=new ArrayBackedIndexedCollection();
	}
	
	/**
	 * Constructor creates new array with given size
	 * Array size is dynamically increased as stack grows
	 */
	public ObjectStack(int capacity){
		this.stack=new ArrayBackedIndexedCollection(capacity);
	}
	
	/**
	 * Determing if stack is empty or not
	 * Stack is empty if his size equals <code>0</code>
	 * @return <code>true</code> if size equals <code>0</code> else it returns <code>false</code>
	 */
	public boolean isEmpty(){
		return this.stack.isEmpty();
	}
	
	/**
	 * Checking stack size
	 * Stack size is number of elements that contains
	 * @return <code>int</code> value of elemnts at stack
	 */
	public int size(){
		return this.stack.size();
	}
	
	/**
	 * Puts element at top of stack
	 * @param Object reference that is meant to go on top of stack
	 */
	public void push(Object value){
		this.stack.add(value);
	}
	
	/**
	 * Removes element that is pushed last to stack
	 * @return Object reference that was last pushed on stack
	 * @throws EmptyStackException if stack is empty
	 */
	public Object pop(){
		if(this.stack.size() == 0) throw new EmptyStackException("Stog je prazan");
		Object value=this.stack.get(this.stack.size()-1);
		this.stack.remove(this.stack.size()-1);
		return value;
	}
	
	/**
	 * Preview last Object pushed to stack
	 * It doesnt remove that Object reference from stack
	 * @return last pushed Object on stack
	 * @throws EmptyStackException if stack is empty
	 */
	public Object peek(){
		if(this.stack.size() == 0) throw new EmptyStackException("Stog je prazan");
		Object value=this.stack.get(this.stack.size()-1);
		return value;
	}
	
	/**
	 * Removes all Objects from stack
	 * Stack size is then <code>zero</code>
	 */
	public void clear(){
		this.stack.clear();
	}

	
}
