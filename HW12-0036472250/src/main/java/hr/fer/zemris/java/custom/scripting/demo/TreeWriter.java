/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.demo;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.EndNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Nodes visiting using Visitor pattern.
 * @author Marin Petrunić 0036472250
 *
 */
public class TreeWriter {

	/**
	 * Accepts path to smart script file as signel argument.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			throw new IllegalArgumentException("Expected only path to smart script file");
		}
		Path smartScriptFile = Paths.get(args[0]);
		if(!Files.exists(smartScriptFile)) {
			throw new IllegalArgumentException("Smart script file doesnt exist " + smartScriptFile);
		}
		String docBody = "";
		try {
			Scanner r = new Scanner(Files.newBufferedReader(smartScriptFile, StandardCharsets.UTF_8));
			while(r.hasNext()) {
				docBody += r.nextLine() + "\n";
			}
			r.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SmartScriptParser p = new SmartScriptParser(docBody);
		INodeVisitor visitor = new WriteVisitor();
		p.getDocumentNode().accept(visitor);
	}
	
	public static class WriteVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.asText());
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print(node.asText());
		}

		@Override
		public void visitEndNode(EndNode node) {
			System.out.print(node.asText());
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.print(node.asText());
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			System.out.print(node.asText());
		}
		
	}

}
