/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.EndNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;
import hr.fer.zemris.java.custom.scripting.tokens.commands.Decfmtfunction;
import hr.fer.zemris.java.custom.scripting.tokens.commands.DupCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.Function;
import hr.fer.zemris.java.custom.scripting.tokens.commands.PParamDelCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.PParamGetCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.PParamSetCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.ParamGetCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.SetMimeTypeCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.SinFunction;
import hr.fer.zemris.java.custom.scripting.tokens.commands.SwapCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.TParamDelCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.TParamGetCommand;
import hr.fer.zemris.java.custom.scripting.tokens.commands.TParamSetCommand;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Engine used for executing smart scripts.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class SmartScriptEngine {

	/**
	 * Top most node of smartscript.
	 */
	private DocumentNode documentNode;

	/**
	 * Given {@link RequestContext}.
	 */
	private RequestContext requestContext;

	/**
	 * {@link ObjectMultistack} for containing variables.
	 */
	private ObjectMultistack multistack = new ObjectMultistack();

	/**
	 * Constructor.
	 * 
	 * @param documentNode
	 *            top most node of smartscript
	 * @param requestContext
	 *            http request/response
	 * @throws IllegalArgumentException
	 *             if some arguments were null
	 */
	public SmartScriptEngine(DocumentNode documentNode,
			RequestContext requestContext) {
		if (documentNode == null || requestContext == null) {
			throw new IllegalArgumentException("Arguments must no be null");
		}
		this.documentNode = documentNode;
		this.requestContext = requestContext;
	}

	/**
	 * Executes smartscript and returns its result through
	 * {@link RequestContext}.
	 */
	public void execute() {
		documentNode.accept(visitor);
	}

	/**
	 * Visitor that walks through smartscript tree and resolve its nodes.
	 */
	private INodeVisitor visitor = new INodeVisitor() {

		private Map<String, Function> functions = new HashMap<String, Function>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				put("@sin", new SinFunction());
				put("@decfmt", new Decfmtfunction());
				put("@dup", new DupCommand());
				put("@swap", new SwapCommand());
				put("@setMimeType", new SetMimeTypeCommand());
				put("@paramGet", new ParamGetCommand());
				put("@pparamGet", new PParamGetCommand());
				put("@pparamSet", new PParamSetCommand());
				put("@pparamDel", new PParamDelCommand());
				put("@tparamGet", new TParamGetCommand());
				put("@tparamSet", new TParamSetCommand());
				put("@tparamDel", new TParamDelCommand());
			};
		};

		@Override
		public void visitTextNode(TextNode node) {
			if(node.asText().isEmpty()) return;
			String text = node.asText();
			try {
				requestContext.write(text);
			} catch (IOException e) {
				System.out
						.println("Unable to output text using RequestContext");
			}
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			multistack.push(node.getVariable(),
					new ValueWrapper(node.getStartExpression()));
			ValueWrapper end = new ValueWrapper(node.getEndExpression());
			ValueWrapper step = new ValueWrapper(node.getStepExpression());
			while (multistack.peek(node.getVariable()).numCompare(end) < 0) {
				for (int i = 0, limit = node.numberOfChildren(); i < limit; i++) {
					node.getChild(i).accept(this);
				}
				ValueWrapper var = multistack.pop(node.getVariable());
				var.increment(step);
				multistack.push(node.getVariable(), var);
			}
		}

		@Override
		public void visitEndNode(EndNode node) {
			// no purpose here
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<Object> objects = new Stack<>();
			Token[] tokens = node.getTokens();
			for (int i = 0, end = tokens.length; i < end; i++) {
				processToken(tokens[i], objects);
			}
			for (Object object : objects) {
				try {
					requestContext.write(object.toString());
				} catch (IOException e) {
					System.out.println("Unable to print object " + object
							+ " to request conterxt.");
					System.out.println(e.getMessage());
				}
			}
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			// BNO JOB
		}

		/**
		 * Process given token.
		 * @param token
		 * @param tempStack
		 */
		private void processToken(Token token, Stack<Object> tempStack) {
			if (token instanceof TokenConstantDouble) {
				tempStack.push(Double.parseDouble(token.asText()));
			}
			if (token instanceof TokenConstantInteger) {
				tempStack.push(Integer.parseInt(token.asText()));
			}
			if (token instanceof TokenString) {
				tempStack.push(((TokenString) token).getString());
			}
			if (token instanceof TokenVariable) {
				tempStack.push(((ValueWrapper) multistack.peek(token.asText()))
						.getValue());
			}
			if (token instanceof TokenOperator) {
				tempStack.push(executeOperator(
						new ValueWrapper(tempStack.pop()), new ValueWrapper(
								tempStack.pop()), token.asText()).getValue());
			}
			if (token instanceof TokenFunction) {
				functions.get(token.asText())
						.execute(requestContext, tempStack);
			}
		}

		/**
		 * Executes operation and returns result as Double.
		 * 
		 * @param ar1
		 *            operator 1
		 * @param ar2
		 *            operator 2
		 * @param operator
		 * @return
		 */
		private ValueWrapper executeOperator(ValueWrapper ar1,
				ValueWrapper ar2, String operator) {
			switch (operator) {
			case "+": {
				ar1.increment(ar2.getValue());
				return ar1;
			}
			case "-": {
				ar1.decrement(ar2.getValue());
				return ar1;
			}
			case "*": {
				ar1.multiply(ar2.getValue());
				return ar1;
			}
			case "/": {
				ar1.divide(ar2.getValue());
				return ar1;
			}
			default: {
				System.out.println("Unknown operator.");
				return new ValueWrapper(0);
			}
			}
		}

	};

}
