/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * @author Marin Petrunić 0036472250
 *
 * Root node for parsed document
 */
public class DocumentNode extends Node {

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitDocumentNode(this);
		for(int i = 0, end = this.numberOfChildren(); i < end; i++) {
			this.getChild(i).accept(visitor);
		}
	}
	 
}
