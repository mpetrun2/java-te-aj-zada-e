/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.Token;

/**
 * @author Marin Petrunić 0036472250
 * Echo node is marked with {$= something $} tag
 */
public class EchoNode extends Node {
	
	private Token[] tokens;

	/**
	 * Creates EchoNode with the set of tokens
	 * @param tokens Tokens/parameters of this node
	 */
	public EchoNode(Token[] tokens) {
		super();
		this.tokens = tokens;
	}
	

	/**
	 * @return all EchoNode tokens
	 */
	public Token[] getTokens() {
		return this.tokens.clone();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText(){
		String text="{$= ";
		for(Token token:this.tokens){
			text+=token.asText()+" ";
		}
		return text+"$}";
	}


	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}
	
}
