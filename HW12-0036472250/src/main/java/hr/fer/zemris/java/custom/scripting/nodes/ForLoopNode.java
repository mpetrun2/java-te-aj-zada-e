/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * @author Marin Petrunić 0036472250
 * 
 * Class that represents For-loop
 */
public class ForLoopNode extends Node {

	private TokenVariable variable;
	private Token startExpression;
	private Token endExpression;
	private Token stepExpression;

	/**
	 * Constructor for ForLoopNode
	 * @param variable loop variable
	 * @param startExpression start value
	 * @param endExpression end value
	 * @param stepExpression step value
	 */
	public ForLoopNode(TokenVariable variable, Token startExpression,
			Token endExpression, Token stepExpression) {
		super();
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	/**
	 * @return the loop variable
	 */
	public String getVariable() {
		return variable.asText();
	}

	/**
	 * @return the startExpression
	 */
	public String getStartExpression() {
		return startExpression.asText();
	}

	/**
	 * @return the endExpression
	 */
	public String getEndExpression() {
		return endExpression.asText();
	}

	/**
	 * @return the stepExpression
	 */
	public String getStepExpression() {
		return stepExpression.asText();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText(){
		return "{$FOR "+this.variable.asText()+" "+this.startExpression.asText()
					+" "+this.endExpression.asText()+" "+this.stepExpression.asText()+" $}";
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
		for(int i = 0, end = this.numberOfChildren(); i < end; i++) {
			this.getChild(i).accept(visitor);
		}
	}
	
	

}
