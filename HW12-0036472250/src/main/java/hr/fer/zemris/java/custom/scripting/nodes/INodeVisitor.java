/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Visitor for {@link Node} implementations.
 * @author Marin Petrunić 0036472250
 *
 */
public interface INodeVisitor {

	/**
	 * Action upon visiting {@link TextNode}.
	 * @param node {@link TextNode} currently visited.
	 */
	public void  visitTextNode(TextNode node);
	
	/**
	 * Action upon visiting {@link ForLoopNode}.
	 * @param node {@link ForLoopNode} currently visited
	 */
	public void visitForLoopNode(ForLoopNode node);
	
	/**
	 * Notification upon visiting {@link EndNode}.
	 * @param node {@link EndNode} currently visited.
	 */
	public void visitEndNode(EndNode node);
	
	/**
	 * Action upon visiting {@link EchoNode}.
	 * @param node {@link EchoNode} currently visited.
	 */
	public void visitEchoNode(EchoNode node);
	
	/**
	 * Notification upon visiting {@link DocumentNode}.
	 * @param node {@link DocumentNode} currently visited.
	 */
	public void visitDocumentNode(DocumentNode node);
	
}
