/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.parser;

/**
 * @author Marin Petrunić 0036472250
 * This Exception is thrown when some error pops while parsing document.
 *  It is derived from {@link RuntimeException}
 */
public class SmartScriptParserException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7873703860495022568L;

	/**
	 * 
	 */
	public SmartScriptParserException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public SmartScriptParserException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public SmartScriptParserException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SmartScriptParserException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SmartScriptParserException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
