/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić  0036472250
 *
 * Base class from which other Tokens will be derived
 */
public class Token {
	
	/**
	 * This method returns empty String for this class
	 * @return empty <code>String</code>
	 */
	public String asText(){
		return "";
	}
	
	
}
