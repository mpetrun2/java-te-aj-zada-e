/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 * This class represents Double variable token
 */
public class TokenConstantDouble extends Token {

	/**
	 * Constant amount
	 */
	private double value;
	
	/**
	 * Constructor for initializing constant value
	 * @param <b>value</b> - Double constant value
	 */
	public TokenConstantDouble(Double value) {
		this.value=value;
	}
	
	/**
	 * @return <code>String</code> representation of Token value
	 */
	@Override
	public String asText(){
		return String.valueOf(this.value);
	}
}
