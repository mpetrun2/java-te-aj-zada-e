/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 * This class represents Integer variable token
 */
public class TokenConstantInteger extends Token {

	/**
	 * Constant amount
	 */
	private int value;
	
	/**
	 * Constructor for initializing constant value
	 * @param <b>value</b> - Integer constant value
	 */
	public TokenConstantInteger(Integer value) {
		this.value=value;
	}
	
	/**
	 * @return <code>String</code> representation of Token value
	 */
	@Override
	public String asText(){
		return String.valueOf(this.value);
	}

}
