/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 *
 * This class represents function token
 */
public class TokenFunction extends Token {

	/**
	 * Function name
	 */
	private String name;
	
	/**
	 * Constructor for initializing function name
	 * @param <b>name</b> - Function name
	 */
	public TokenFunction(String name) {
		this.name=name;
	}
	
	/**
	 * @return <code>String</code> name of the function
	 */
	@Override
	public String asText(){
		return "@"+this.name;
	}

}
