/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 *
 * This class represents operator token
 */
public class TokenOperator extends Token {

	/**
	 * Operator symbol
	 */
	private String symbol;
	
	/**
	 * Constructor for initializing operator symbol as String
	 * @param <b>symbol</b> - operator symbol
	 */
	public TokenOperator(String symbol) {
		this.symbol=symbol;
	}
	
	/**
	 * @return <code>String</code> operator symbol
	 */
	@Override
	public String asText(){
		return this.symbol;
	}

}
