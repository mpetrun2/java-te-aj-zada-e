/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 *	This class represents String variable token
 */
public class TokenString extends Token {

	/**
	 * Constant amount
	 */
	private String value;
	
	/**
	 * Constructor for initializing Token value
	 * @param <b>value</b> - String value
	 */
	public TokenString(String value) {
		this.value=value;
	}
	
	public String getString() {
		return value;
	}
	
	/**
	 * @return <code>String</code> this token contains
	 */
	@Override
	public String asText(){
		return "\""+this.value+"\"";
	}

}
