/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * @author Marin Petrunić 0036472250
 *
 * This class represents variable name
 */
public class TokenVariable extends Token {
	
	/**
	 * Name of token variable
	 */
	private String name;
	
	/**
	 * Constructor for setting variable <b>name</b>
	 * 
	 * @param name	variable name
	 */
	public TokenVariable(String name)
	{
		this.name=name;
	}

	@Override
	/**
	 * @return String representation of variable <b>name</b>
	 */
	public String asText()
	{
		return this.name;
	}
}
