/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.text.DecimalFormat;
import java.util.Stack;

/**
 * Formats  decimal number.
 * @author Marin Petrunić 0036472250
 *
 */
public class Decfmtfunction implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		DecimalFormat f = new DecimalFormat((String) stack.pop());
		stack.push(f.format(stack.pop()));
	}

}
