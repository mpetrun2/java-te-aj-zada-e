/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Interface for smart script functions.
 * @author Marin Petrunić 0036472250
 *
 */
public interface Function {

	/**
	 * Executes command functionality.
	 * @param requestContex
	 * @param stack
	 */
	public void execute(RequestContext requestContex, Stack<Object> stack);
}
