/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Gets value in persisten params with key got from top of stack and pushes
 * value to stack.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class PParamGetCommand implements Function {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(
	 * hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		String def = (String) stack.pop();
		String val = requestContex.getPersistentParameter((String) stack.pop());
		if (val == null) {
			stack.push(def);
		} else {
			stack.push(val);
		}
	}

}
