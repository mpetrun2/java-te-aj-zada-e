/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Returns from {@link RequestContext} params andd pushes onto stack.
 * @author Marin Petrunić 0036472250
 *
 */
public class ParamGetCommand implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		String def = stack.pop().toString();
		String val = requestContex.getParameter(stack.pop().toString());
		if( val == null) {
			stack.push(def);
		} else {
			stack.push(val);
		}
	}

}
