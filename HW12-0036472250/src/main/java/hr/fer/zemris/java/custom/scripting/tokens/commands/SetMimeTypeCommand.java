/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Sets {@link RequestContext} mime type.
 * @author Marin Petrunić 0036472250
 *
 */
public class SetMimeTypeCommand implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		requestContex.setMimeType((String) stack.pop());
	}

}
