/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Implementation of sinus function.
 * @author Marin Petrunić 0036472250
 *
 */
public class SinFunction implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		Object obj = stack.pop();
		if(obj instanceof Integer) {
			Integer argument = (Integer) obj;
			Double argumentDouble = Math.toRadians(argument);
			stack.push(Math.sin(argumentDouble));
		} else {
			Double argument = (Double) obj;
			argument = Math.toRadians(argument);
			stack.push(Math.sin(argument));
		}	
	}

}
