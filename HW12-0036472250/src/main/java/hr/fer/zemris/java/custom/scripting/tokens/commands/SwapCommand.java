/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Swap two top most elements.
 * @author Marin Petrunić 0036472250
 *
 */
public class SwapCommand implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		Object elem1 = stack.pop();
		Object elem2 = stack.pop();
		stack.push(elem1);
		stack.push(elem2);
	}

}
