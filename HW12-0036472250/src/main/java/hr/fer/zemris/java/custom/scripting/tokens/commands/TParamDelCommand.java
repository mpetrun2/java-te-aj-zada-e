/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Deletes temporary parameter from {@link RequestContext}.
 * @author Marin Petrunić 0036472250
 *
 */
public class TParamDelCommand implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		requestContex.removeTemporaryParameter((String)stack.pop());
	}

}
