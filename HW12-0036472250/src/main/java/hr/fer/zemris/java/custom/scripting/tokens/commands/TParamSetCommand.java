/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.tokens.commands;

import hr.fer.zemris.java.webserver.RequestContext;

import java.util.Stack;

/**
 * Sets temporary parameter in {@link RequestContext}.
 * @author Marin Petrunić 0036472250
 *
 */
public class TParamSetCommand implements Function {

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.tokens.commands.Function#execute(hr.fer.zemris.java.webserver.RequestContext, java.util.Stack)
	 */
	@Override
	public void execute(RequestContext requestContex, Stack<Object> stack) {
		requestContex.setTemporaryParameter(stack.pop().toString(), stack.pop().toString());
	}

}
