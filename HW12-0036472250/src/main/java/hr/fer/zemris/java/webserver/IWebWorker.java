/**
 * 
 */
package hr.fer.zemris.java.webserver;

/**
 * WebWorker interface.
 * @author Marin
 *
 */
public interface IWebWorker {

	/**
	 * Method for processing given request.
	 * @param context
	 */
	public void processRequest(RequestContext context);
}
