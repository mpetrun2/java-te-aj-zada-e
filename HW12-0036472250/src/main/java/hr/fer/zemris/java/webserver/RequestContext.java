/**
 * 
 */
package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.ws.http.HTTPBinding;

/**
 * Class for sending {@link HTTPBinding} requests.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class RequestContext {

	/**
	 * Request output.
	 */
	private OutputStream outputStream;

	/**
	 * Charset for writing to {@link OutputStream}.
	 */
	private Charset charset;

	/**
	 * Text encoding
	 */
	private String encoding = "UTF-8";

	/**
	 * HTTP status code.
	 */
	private int statusCode = 200;

	/**
	 * HTTP status text.
	 */
	private String statusText = "OK";

	/**
	 * HTTP content mime-type.
	 */
	private String mimeType = "text/html";

	private Map<String, String> parameters;

	private Map<String, String> temporaryParameters = new HashMap<>();

	private Map<String, String> persistentParameters;

	List<RCCookie> outputCookies;

	/**
	 * Flag that determines if header is already generated and sent.
	 */
	private boolean headerGenerated = false;

	/**
	 * @param outputStream
	 * @param parameters
	 * @param persistentParameters
	 * @param outputCookies
	 */
	public RequestContext(OutputStream outputStream,
			Map<String, String> parameters,
			Map<String, String> persistentParameters,
			List<RCCookie> outputCookies) {
		super();
		if (outputStream == null) {
			throw new IllegalArgumentException("Output stream cannot be null");
		}
		this.outputStream = outputStream;
		if (parameters == null) {
			this.parameters = new HashMap<>();
		} else {
			this.parameters = new HashMap<>(parameters);
		}
		if (persistentParameters == null) {
			this.persistentParameters = new HashMap<>();
		} else {
			this.persistentParameters = persistentParameters;
		}
		if (outputCookies == null) {
			this.outputCookies = new ArrayList<>();
		} else {
			this.outputCookies = new ArrayList<>(outputCookies);
		}
	}

	/**
	 * @param encoding
	 *            the encoding to set
	 */
	public void setEncoding(String encoding) {
		if (headerGenerated) {
			throw new RuntimeException("Headers are already generated.");
		}
		this.encoding = encoding;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		if (headerGenerated) {
			throw new RuntimeException("Headers are already generated.");
		}
		this.statusCode = statusCode;
	}

	/**
	 * @param statusText
	 *            the statusText to set
	 */
	public void setStatusText(String statusText) {
		if (headerGenerated) {
			throw new RuntimeException("Headers are already generated.");
		}
		this.statusText = statusText;
	}

	/**
	 * @param mimeType
	 *            the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		if (headerGenerated) {
			throw new RuntimeException("Headers are already generated.");
		}
		this.mimeType = mimeType;
	}

	/**
	 * Method that retrieves value from parameters map (or null if no
	 * association exists).
	 * 
	 * @param name
	 * @return
	 */
	public String getParameter(String name) {
		return parameters.get(name);
	}

	/**
	 * Method that retrieves names of all parameters in parameters map. Its a
	 * read only set.
	 * 
	 * @return
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(parameters.keySet());
	}

	/**
	 * Method that retrieves value from persistentParameters map (or null if no
	 * association exists).
	 * 
	 * @param name
	 * @return
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(name);
	}

	/**
	 * Method that retrieves names of all parameters in persistent parameters
	 * map. Result is unnmodifiable.
	 * 
	 * @return
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(persistentParameters.keySet());
	}

	/**
	 * Method that stores a value to persistentParameters map.
	 * 
	 * @param name
	 * @param value
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(name, value);
	}

	/**
	 * Method that removes a value from persistentParameters map.
	 * 
	 * @param name
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(name);
	}

	/**
	 * Method that retrieves value from temporaryParameters map (or null if no
	 * association exists).
	 * 
	 * @param name
	 * @return
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(name);
	}

	/**
	 * Method that retrieves names of all parameters in temporary parameters
	 * map. Result is unmopdiffiable.
	 * 
	 * @return
	 */
	public Set<String> getTemporaryParameterNames() {
		return Collections.unmodifiableSet(temporaryParameters.keySet());
	}

	/**
	 * Method that stores a value to temporaryParameters map.
	 * 
	 * @param name
	 * @param value
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(name, value);
	}

	/**
	 * Method that removes a value from temporaryParameters map.
	 * 
	 * @param name
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(name);
	}

	/**
	 * Writes given data to {@link RequestContext} outputStream. If no headers
	 * are generated also generates and send them too.
	 * 
	 * @param data
	 *            to be outputed
	 * @return
	 * @throws IOException
	 *             on outputStream error
	 */
	public RequestContext write(byte[] data) throws IOException {
		if (!headerGenerated) {
			String header = generateHeaders()+"\r\n";
			outputStream.write(header.getBytes(charset));
			headerGenerated = true;
		}
		outputStream.write(data);
		outputStream.flush();
		return this;
	}

	/**
	 * Encodes given text with current charset and writes it to
	 * {@link RequestContext} outputStream.
	 * 
	 * @param text
	 *            to be outoputed
	 * @return
	 * @throws IOException
	 *             on outputStream error
	 */
	public RequestContext write(String text) throws IOException {
		if (text == null) {
			text = "";
		}
		charset = Charset.forName(encoding);
		write(text.getBytes(charset));
		return this;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Method generates http headers.
	 * 
	 * @return
	 */
	private String generateHeaders() {
		charset = Charset.forName(encoding);
		String header = "HTTP/1.1 " + statusCode + " " + statusText + "\r\n"
				+ "Content-Type: " + mimeType;
		if (mimeType.startsWith("text/")) {
			header += ";charset=" + encoding;
		}
		header += "\r\n";
		for (RCCookie cookie : outputCookies) {
			header += "Set-Cookie: " + cookie.name + "= " + cookie.value+ "\r\n";
//			if (cookie.domain != null) {
//				header += "; domain= " + cookie.domain+ "\r\n";
//			}
//			if (cookie.path != null) {
//				header += "; path=" + cookie.path;
//			}
////			header += "; Max-Age= 0";
//			header += "; HttpOnly\r\n";
		}
		return header;
	}

	/**
	 * Class representing single cookie with his properties.
	 * 
	 * @author Marin Petrunić 0036472250
	 * 
	 */
	public static class RCCookie {

		/**
		 * Cookie name.
		 */
		private String name;

		/**
		 * Cookie value.
		 */
		private String value;

		/**
		 * Cookie domain.
		 */
		private String domain;

		/**
		 * Cookie path.
		 */
		private String path;

		/**
		 * 
		 */
		private Integer maxAge;

		/**
		 * Constructor.
		 * 
		 * @param name
		 * @param value
		 * @param domain
		 * @param path
		 * @param maxAge
		 */
		public RCCookie(String name, String value, String domain, String path,
				Integer maxAge) {
			super();
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @return the domain
		 */
		public String getDomain() {
			return domain;
		}

		/**
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @return the maxAge
		 */
		public Integer getMaxAge() {
			return maxAge;
		}

	}

	/**
	 * Adds given {@link RCCookie} to list of output Cookies.
	 * 
	 * @param rcCookie
	 */
	public void addRCCookie(RCCookie rcCookie) {
		if (rcCookie == null) {
			throw new IllegalArgumentException("Cookie cannot be null");
		}
		outputCookies.add(rcCookie);
	}
}
