/**
 * 
 */
package hr.fer.zemris.java.webserver;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class representing http server able to execute smartscripts.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class SmartHttpServer {

	/**
	 * Server adress.
	 */
	private String adress;

	/**
	 * Server port.
	 */
	private int port;

	/**
	 * Max number of server threads.
	 */
	private int workerThreads;

	/**
	 * Server session timeout.
	 */
	private int sessionTimeout;

	/**
	 * Map of supported mime types.
	 */
	private Map<String, String> mimeTypes = new HashMap<>();

	/**
	 * Reference to serverThread.
	 */
	private ServerThread serverThread;

	/**
	 * server thread pool.
	 */
	private ExecutorService threadPool;

	/**
	 * Path to server document root directory.
	 */
	private Path documentRoot;
	
	/**
	 * Collections of sessions.
	 */
	private Map<String, SessionMapEntry> sessions = 
			new HashMap<String, SmartHttpServer.SessionMapEntry>();
	
	/**
	 * Random session id generator.
	 */
	private Random sessionRandom = new Random();


	/**
	 * Map with path to worker as key and instance of {@link IWebWorker} as
	 * value.
	 */
	private Map<String, IWebWorker> workersMap = new HashMap<>();

	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException(
					"Expected path to server configuration file.");
		}
		SmartHttpServer server = new SmartHttpServer(args[0]);
		server.start();
	}

	/**
	 * Constructor. Loads configuration files.
	 * 
	 * @param configFileName
	 *            path to configuration files.
	 */
	public SmartHttpServer(String configFileName) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(configFileName);
			prop.load(input);
			adress = prop.getProperty("server.adress", "127.0.0.1");
			port = Integer.parseInt(prop.getProperty("server.port", "80"));
			workerThreads = Integer.parseInt(prop.getProperty(
					"server.workerThreads", "10"));
			documentRoot = Paths.get(prop.getProperty("server.documentRoot",
					"./"));
			loadMimeTypes(prop.getProperty("server.mimeConfig", "./config/"));
			sessionTimeout = Integer.parseInt(prop.getProperty(
					"server.timeout", "600"));
			loadWorkers(prop.getProperty("server.workers"));
		} catch (IOException e) {
			System.out.println(e.getMessage());
			if (input != null) {
				try {
					input.close();
				} catch (IOException ignorable) {
				}
			}
		}
	}

	/**
	 * Loads workers from given property file.
	 * 
	 * @param workersPropertyPath
	 */
	private void loadWorkers(String workersPropertyPath) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(workersPropertyPath);
			prop.load(input);
			for (Entry<Object, Object> entry : prop.entrySet()) {
				Class<?> referenceToClass = null;
				try {
					referenceToClass = this.getClass().getClassLoader()
							.loadClass((String) entry.getValue());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				Object newObject = null;
				try {
					newObject = referenceToClass.newInstance();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				IWebWorker iww = (IWebWorker) newObject;
				if (entry.getKey() != null && iww != null) {
					workersMap.put((String) entry.getKey(), iww);
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			if (input != null) {
				try {
					input.close();
				} catch (IOException ignorable) {
				}
			}
		}
	}

	/**
	 * Loads mimeType properties.
	 * 
	 * @param mimeConfigFilePath
	 *            path to mime config file.
	 */
	private void loadMimeTypes(String mimeConfigFilePath) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(mimeConfigFilePath);
			prop.load(input);
			for (Entry<Object, Object> entry : prop.entrySet()) {
				mimeTypes.put((String) entry.getKey(),
						(String) entry.getValue());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			if (input != null) {
				try {
					input.close();
				} catch (IOException ignorable) {
				}
			}
		}
	}

	/**
	 * Starts server thread if not already started and inits worker Threads.
	 */
	protected synchronized void start() {
		if (serverThread == null) {
			serverThread = new ServerThread();
		}
		threadPool = Executors.newFixedThreadPool(workerThreads);
		if (!serverThread.isAlive()) {
			serverThread.start();
		}
	}

	/**
	 * Stops server thread and shutdowns threadPool.
	 */
	protected synchronized void stop() {
		if (serverThread != null) {
			try {
				serverThread.join(0);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
		threadPool.shutdown();
	}

	/**
	 * Class representing rserver Thread.
	 * 
	 * @author Marin Petrunić 0036472250
	 * 
	 */
	protected class ServerThread extends Thread {
		@SuppressWarnings("resource")
		@Override
		public void run() {
			ServerSocket serverSocket = null;
			try {
				serverSocket = new ServerSocket(port);
			} catch (IOException e) {
				e.getMessage();
				return;
			}
			while (true) {
				Socket client = null;
				try {
					client = serverSocket.accept();
					ClientWorker cw = new ClientWorker(client);
					threadPool.submit(cw);
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}

			}
		}
	}

	private class ClientWorker implements Runnable {
		private Socket csocket;
		@SuppressWarnings("unused")
		private PushbackInputStream istream;
		private OutputStream ostream;
		private String version;
		private String method;
		private Map<String, String> params = new HashMap<String, String>();
		private Map<String, String> permPrams = null;
		private List<RCCookie> outputCookies = new ArrayList<RequestContext.RCCookie>();
		private String SID;

		public ClientWorker(Socket csocket) {
			super();
			this.csocket = csocket;
		}

		@Override
		public void run() {
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			List<String> request = readRequest();
			if (request.size() < 1) {
				returnErrorResponse(400, "Header not found.");
				return;
			}
			checkSession(request);
			String requestedPath = parseFirstHeaderLine(request.get(0));
			String[] splited = requestedPath.split("\\?");
			Path path = Paths.get(splited[0]);
			String paramString = "";
			if (splited.length > 1) {
				paramString = splited[1];
			}
			RequestContext rc = null;
			
			parseParameters(paramString);
			try {
				rc = new RequestContext(csocket.getOutputStream(), params,
						permPrams, outputCookies);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (workersMap.containsKey("/" + path.getFileName())) {
				rc.setStatusCode(200);
				rc.setStatusText("OK");
				workersMap.get("/" + path.getFileName()).processRequest(rc);
				try {
					csocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
			if (path.startsWith("/ext/")) {
				Class<?> referenceToClass = null;
				try {
					referenceToClass = this
							.getClass()
							.getClassLoader()
							.loadClass(
									"hr.fer.zemris.java.webserver.workers."
											+ path.getFileName());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				Object newObject = null;
				try {
					newObject = referenceToClass.newInstance();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				IWebWorker iww = (IWebWorker) newObject;
				iww.processRequest(rc);
				try {
					csocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
			path = documentRoot.resolve("." + path);
			if (!path.startsWith(documentRoot)) {
				returnErrorResponse(403, "Access Forbidden");
			}
			if (!Files.exists(path) || !Files.isRegularFile(path)
					|| !Files.isReadable(path)) {
				returnErrorResponse(404, "File not found");
			}
			String extension = path.toString().substring(
					path.toString().lastIndexOf(".") + 1);
			String mimeType = findMimeType(extension);
			rc.setMimeType(mimeType);
			rc.setStatusCode(200);
			rc.setStatusText("OK");
			if (extension.equals("smscr")) {
				List<String> documentLines = null;
				try {
					documentLines = Files.readAllLines(path,
							StandardCharsets.UTF_8);
				} catch (IOException e) {
					System.out.println("Unable to read file");
					returnErrorResponse(400, "Error");
					return;
				}
				StringBuffer document = new StringBuffer();
				for (String line : documentLines) {
					document.append(line + "\r\n");
				}
				SmartScriptParser parser = new SmartScriptParser(
						document.toString());
				SmartScriptEngine se = new SmartScriptEngine(
						parser.getDocumentNode(), rc);
				se.execute();
			} else {
				try {
					System.out.println(path);
					byte[] data = Files.readAllBytes(path);
					rc.setMimeType(findMimeType(extension));
					csocket.getOutputStream().write(data);
					csocket.getOutputStream().flush();
				} catch (IOException e) {
					System.out.println("Unable to read file.");
					returnErrorResponse(400, "Error");
				}
			}
			try {
				csocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/**
		 * Checks for existing session id if there is not one
		 * generates new sessions id.
		 * @param request
		 */
		private void checkSession(List<String> request) {
			String sidCandidate = "";
			for(String line : request) {
				if(line.startsWith("Cookie:")) {
					line = line.substring(line.indexOf(":")+2);
					String[] cookies = line.split(";");
					for(int i = 0; i < cookies.length; i++) {
						if(cookies[i].startsWith("sid")) {
							sidCandidate = cookies[i].split("=")[1];
						}
					}
				}
			}
			if(sidCandidate.isEmpty()) {
				sidCandidate = generateSid();
				SessionMapEntry sessionMap = new SessionMapEntry();
				sessions.put(sidCandidate, sessionMap);
				outputCookies.add(new RCCookie("sid", sidCandidate, "localhost", "/", null));
				permPrams = sessionMap.map;
			} else {
				SessionMapEntry sessionMap = sessions.get(sidCandidate);
				if(sessionMap == null) {
					sessionMap = new SessionMapEntry();
					sessionMap.validUntil = new Date().getTime() + sessionTimeout;
				}
				if(sessionMap.validUntil < new Date().getTime()) {
					sidCandidate = generateSid();
					sessionMap = new SessionMapEntry();
					sessions.put(sidCandidate, sessionMap);
					outputCookies.add(new RCCookie("sid", sidCandidate, "localhost", "/", null));
				} else {
					sessionMap.validUntil = new Date().getTime() + sessionTimeout;
				}
				permPrams = sessionMap.map;
			}
			SID = sidCandidate;
		}

		/**
		 * Generates new sessions id.
		 * @return
		 */
		private String generateSid() {
			StringBuilder sid = new StringBuilder();
			for(int i = 0; i < 20; i++) {
				char c = (char) (sessionRandom.nextInt(26)+'a');
				sid.append(c);
			}
			return sid.toString();
			
		}

		/**
		 * Finds appropriate mime type for given extension.
		 * 
		 * @param extension
		 *            file extension
		 * @return
		 */
		private String findMimeType(String extension) {
			if (mimeTypes.containsKey(extension)) {
				return mimeTypes.get(extension);
			}
			return "application/octet-stream";
		}

		/**
		 * Fills param map with in paramString.
		 * 
		 * @param paramString
		 */
		private void parseParameters(String paramString) {
			String[] paramPairs = paramString.split("\\&");
			if (paramPairs[0].isEmpty())
				return;
			for (int i = 0; i < paramPairs.length; i++) {
				params.put(paramPairs[i].split("\\=")[0],
						paramPairs[i].split("\\=")[1]);
			}
		}

		/**
		 * Parses first line of header.
		 * 
		 * @param line
		 * @return
		 */
		private String parseFirstHeaderLine(String line) {
			String[] data = line.split(" ");
			method = data[0];
			version = data[2];
			if (!method.equals("GET")
					|| (!version.equals("HTTP/1.0") && !version
							.equals("HTTP/1.1"))) {
				returnErrorResponse(400, "Unsupported request method.");
			}
			return data[1];
		}

		/**
		 * Returns http response with given status code and status text.
		 * 
		 * @param statusCode
		 * @param statusText
		 */
		private void returnErrorResponse(int statusCode, String statusText) {
			String responseHeader = "HTTP/1.1 " + statusCode + " " + statusText
					+ "\r\n\r\n";
			try {
				ostream.write(responseHeader
						.getBytes(StandardCharsets.ISO_8859_1));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/**
		 * Reads header from {@link Socket} request.
		 * 
		 * @return
		 */
		private List<String> readRequest() {
			List<String> header = new ArrayList<>();
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(csocket.getInputStream(),
								StandardCharsets.ISO_8859_1));
				String line = reader.readLine();
				while (line != null) {
					if (line.isEmpty()) {
						break;
					}
					header.add(line);
					line = reader.readLine();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			return header;
		}
	}
	
	/**
	 * Session coockie fo specific sid.
	 * @author Marin
	 *
	 */
	private static class SessionMapEntry {
		/**
		 * Session id.
		 */
		String sid;
		
		/**
		 * expiration time.
		 */
		long validUntil;
		
		/**
		 * Map of cookies for this sid.
		 */
		Map<String, String> map;
		
		public SessionMapEntry() {
			super();
			this.map = new ConcurrentHashMap<>();
		}
	}

}
