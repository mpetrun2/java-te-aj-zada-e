/**
 * 
 */
package hr.fer.zemris.java.webserver.workers;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * @author Marin
 * 
 */
public class CircleWorker implements IWebWorker {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.webserver.IWebWorker#processRequest(hr.fer.zemris.
	 * java.webserver.RequestContext)
	 */
	@Override
	public void processRequest(RequestContext context) {
		BufferedImage bim = new BufferedImage(200, 200,
				BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g2d = bim.createGraphics();
		g2d.setColor(Color.GREEN);
		g2d.fillOval(0, 0, 200, 200);
		g2d.setColor(Color.RED);
		g2d.drawOval(0, 0, 200, 200);
		g2d.dispose();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			File output = new File("saved.png");
			ImageIO.write(bim, "png", bos);
			context.setMimeType("image/png");
			context.write(Files.readAllBytes(output.toPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
