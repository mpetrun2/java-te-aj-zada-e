/**
 * 
 */
package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * @author Marin
 * 
 */
public class EchoParams implements IWebWorker {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * hr.fer.zemris.java.webserver.IWebWorker#processRequest(hr.fer.zemris.
	 * java.webserver.RequestContext)
	 */
	@Override
	public void processRequest(RequestContext context) {
		context.setMimeType("text/html");
		StringBuilder table = new StringBuilder();
		table.append("<html><body><table>");
		for (String paramName : context.getParameterNames()) {
			table.append("<tr><td>");
			table.append(paramName + "</td><td>"
					+ context.getParameter(paramName) + "</td></tr>");
		}
		table.append("</table></body></html>");
		try {
			context.write(table.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
