/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

/**
 * Java Bean.
 * Represents signle Band and its propertys.
 * @author Marin Petrunić 0036472250
 *
 */
public class Band implements Comparable<Band> {
	
	private Integer id;
	
	private String name;
	
	private String songLink;

	/**
	 * Constructor.
	 * @param id
	 * @param name
	 * @param songLink
	 */
	public Band(int id, String name, String songLink) {
		super();
		this.id = id;
		this.name = name;
		this.songLink = songLink;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the songLink
	 */
	public String getSongLink() {
		return songLink;
	}
	
	

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param songLink the songLink to set
	 */
	public void setSongLink(String songLink) {
		this.songLink = songLink;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Band)) {
			return false;
		}
		Band other = (Band) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Band o) {
		return id.compareTo(o.getId());
	}
	
	

}
