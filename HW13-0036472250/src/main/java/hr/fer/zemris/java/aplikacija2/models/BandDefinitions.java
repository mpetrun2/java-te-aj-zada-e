/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Model for working with band definitions.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class BandDefinitions {

	/**
	 * Band definition file.
	 */
	Path bandDefinitionFile;

	/**
	 * Constructor.
	 * 
	 * @param bandFilePath
	 *            path to band definition file
	 */
	public BandDefinitions(String bandFilePath) {
		bandDefinitionFile = Paths.get(bandFilePath);
		if (!Files.exists(bandDefinitionFile)) {
			System.out.println("Band definition file doesn't exist. "
					+ bandFilePath);
		}
	}

	/**
	 * Returns sorted list of {@link Band}s.
	 * 
	 * @return
	 */
	public List<Band> getBands() {
		List<Band> bands = new ArrayList<>();
		List<String> bandDefinitions = getBandDefinitions();
		for (String definition : bandDefinitions) {
			String[] bandArguments = definition.split("\t");
			if (bandArguments.length != 3) {
				System.out
						.println("Band definition has illegal number of arguments.Skipped.");
				continue;
			}
			Band newBand = new Band(Integer.parseInt(bandArguments[0]),
					bandArguments[1], bandArguments[2]);
			bands.add(newBand);
		}
		Collections.sort(bands);
		return bands;
	}

	/**
	 * Returns list of band definitions from file.
	 * 
	 * @return
	 */
	private List<String> getBandDefinitions() {
		try {
			return Files.readAllLines(bandDefinitionFile,
					StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	/**
	 * Checks if given band id exist in band definitions.
	 * 
	 * @param bandID
	 * @return
	 */
	public boolean bandIDExist(int bandID) {
		List<Band> bands = getBands();
		for (Band band : bands) {
			if (band.getId() == bandID) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns signle {@link Band} with given ID.
	 * In case band doesn't exist null is returned;
	 * @param bandID
	 * @return
	 */
	public Band getBand(int bandID) {
		List<Band> bands = getBands();
		for(Band band : bands) {
			if(band.getId() == bandID) {
				return band;
			}
		}
		return null;
	}

}
