/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

/**
 * Contains data file path propertys.
 * @author Marin Petrunić 0036472250
 *
 */
public class DataPaths {
	
	/**
	 * Returns path to file with band definition data.
	 * @return
	 */
	public static String getBandDefinitionPath() {
		return "/WEB-INF/data/glasanje-definicija.txt";
	}
	
	/**
	 * Returns path to file with vote definition data.
	 * @return
	 */
	public static String getVotesDefinitionPath() {
		return "/WEB-INF/data/glasanje-rezultati.txt";
	}

}
