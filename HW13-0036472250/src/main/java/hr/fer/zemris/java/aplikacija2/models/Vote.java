/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

/**
 * Java Bean.
 * Represents single band vote.
 * @author Marin Petrunić 0036472250
 *
 */
public class Vote implements Comparable<Vote>{

	/**
	 * Band ID.
	 */
	private int bandID;
	
	/**
	 * Number of band votes.
	 */
	private Integer numberOfVotes;

	/**
	 * Constructor.
	 * @param bandID
	 * @param numberOfVotes
	 */
	public Vote(int bandID, int numberOfVotes) {
		super();
		this.bandID = bandID;
		this.numberOfVotes = numberOfVotes;
	}

	/**
	 * @return the bandID
	 */
	public int getBandID() {
		return bandID;
	}

	/**
	 * @param bandID the bandID to set
	 */
	public void setBandID(int bandID) {
		this.bandID = bandID;
	}

	/**
	 * @return the numberOfVotes
	 */
	public int getNumberOfVotes() {
		return numberOfVotes;
	}

	/**
	 * @param numberOfVotes the numberOfVotes to set
	 */
	public void setNumberOfVotes(int numberOfVotes) {
		this.numberOfVotes = numberOfVotes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bandID;
		result = prime * result + numberOfVotes;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Vote)) {
			return false;
		}
		Vote other = (Vote) obj;
		if (bandID != other.bandID) {
			return false;
		}
		if (numberOfVotes != other.numberOfVotes) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Vote o) {
		return -1 * numberOfVotes.compareTo(o.getNumberOfVotes());
	}

	
}
