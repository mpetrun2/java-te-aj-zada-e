/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Model for working with band votes.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class VoteDefinitions {

	/**
	 * Path to file with votes data.
	 */
	Path pathToVotesFile;

	/**
	 * Constructor.
	 * 
	 * @param pathtoVotesFile
	 */
	public VoteDefinitions(String pathtoVotesFile) {
		this.pathToVotesFile = Paths.get(pathtoVotesFile);
		if (!Files.exists(pathToVotesFile)) {
			try {
				Files.createFile(pathToVotesFile);
			} catch (IOException e) {
				System.out.println("Failes to creates votes file."
						+ e.getMessage());
			}
		}
	}

	/**
	 * Returns list of {@link Vote}s sorted by number of votes;
	 * @return
	 */
	public List<Vote> getVotes() {
		List<Vote> votes = new ArrayList<>();
		for (String definition : getVotesDefinition()) {
			String[] voteArguments = definition.split("\t");
			if (voteArguments.length != 2) {
				System.out
						.println("Invalid number of vote arguments. Vote skipped");
			}
			votes.add(new Vote(Integer.parseInt(voteArguments[0]), Integer
					.parseInt(voteArguments[1])));
		}
		Collections.sort(votes);
		return votes;
	}

	/**
	 * Returns {@link Map} with {@link Band} as key and number
	 * of votes that this band has got as value.
	 * @param voteResults
	 * @param bd
	 * @return
	 */
	public Map<Band, Integer> getVoteResults(List<Vote> voteResults, BandDefinitions bd) {
		Map<Band, Integer> result = new LinkedHashMap<>();
		for(Vote vote : voteResults) {
			Band band = bd.getBand(vote.getBandID());
			if(band != null) {
				result.put(band, vote.getNumberOfVotes());
			}
		}
		return result;
	}
	
	/**
	 * Returns votes definitions as list of strings.
	 * 
	 * @return
	 */
	private List<String> getVotesDefinition() {
		try {
			return Files.readAllLines(pathToVotesFile, StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.out.println("Failed to read votes file. " + e.getMessage());
		}
		return new ArrayList<>();
	}
	
	/**
	 * Returns {@link Map} with {@link Band} as key and number
	 * of votes that this band has got as value.
	 * @param voteResults
	 * @param bd
	 * @return
	 */
	public Map<Band, Integer> getVoteWinners(List<Vote> voteResults, BandDefinitions bd) {
		Map<Band, Integer> winners = new LinkedHashMap<>();
		int maxVotes = voteResults.get(0).getNumberOfVotes();
		for(Vote vote : voteResults) {
			if(vote.getNumberOfVotes() < maxVotes) {
				break;
			}
			Band band = bd.getBand(vote.getBandID());
			if(band != null) {
				winners.put(band, vote.getNumberOfVotes());
			}
		}
		return winners;
	}
	
	/**
	 * Increase votes for band with given id by one.
	 * @param bandID
	 */
	public void increaseVoteNumber(int bandID) {
		List<Vote> votes = getVotes();
		boolean voted = false;
		for(Vote vote : votes) {
			if(vote.getBandID() == bandID) {
				vote.setNumberOfVotes(vote.getNumberOfVotes() + 1);
				voted = true;
			}
		}
		if(voted == false) {
			votes.add(new Vote(bandID, 1));
		}
		writeVotestoFile(votes);
	}
	
	/**
	 * Decrease votes for band with given id by one.
	 * @param bandID
	 */
	public void decreaseVoteNumber(int bandID) {
		List<Vote> votes = getVotes();
		for(Vote vote : votes) {
			if(vote.getBandID() == bandID) {
				vote.setNumberOfVotes(vote.getNumberOfVotes() - 1);
			}
		}
		writeVotestoFile(votes);
	}

	/**
	 * Writes new vote definitions to file.
	 * @param votes
	 */
	private void writeVotestoFile(List<Vote> votes) {
		BufferedWriter w = null;
		try {
			w = Files.newBufferedWriter(pathToVotesFile, StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			System.out.println("Failes to write to votes file. " + e.getMessage());
		}
		for(Vote vote : votes) {
			try {
				w.write(vote.getBandID()+"\t"+vote.getNumberOfVotes()+"\n");
			} catch (IOException e) {
				System.out.println("Failes to write to votes file. " + e.getMessage());
			}
		}
		try {
			w.flush();
			w.close();
		} catch (IOException ignorable) {
		}
	}

	/**
	 * Returns total number of votes.
	 * @param votes
	 * @return
	 */
	public int getTotalNumberOfVotes(List<Vote> votes) {
		int votesNumber = 0;
		for(Vote vote : votes) {
			votesNumber += vote.getNumberOfVotes();
		}
		return votesNumber;
	}
}
