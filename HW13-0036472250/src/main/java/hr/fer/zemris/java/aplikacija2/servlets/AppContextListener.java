/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Stores time when server started into servlet context.
 * @author Marin Petrunić 0036472250
 *
 */
public class AppContextListener implements ServletContextListener {

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("ServletContextListener started");
		sce.getServletContext().setAttribute("ServerStartedTime", System.currentTimeMillis());
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		sce.getServletContext().removeAttribute("ServerStartedTime");
	}

}
