/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Generates random font color for storie.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="funnyStory", urlPatterns={"/stories/funny.jsp"})
public class FunnyStory extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			Random rand = new Random();
			int r = rand.nextInt(255);
			int g = rand.nextInt(255);
			int b = rand.nextInt(255);
			req.setAttribute("r", r);
			req.setAttribute("g", g);
			req.setAttribute("b", b);
			req.getRequestDispatcher("/WEB-INF/pages/stories.jsp").forward(req, resp);
	}
}
