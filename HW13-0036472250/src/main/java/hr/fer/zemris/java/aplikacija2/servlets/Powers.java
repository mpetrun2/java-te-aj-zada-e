/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Generates xls file with content coresponding
 * top givne parameters. If some parameter is invalid,
 * error message will be forwarded.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="powers", urlPatterns={"/powers"})
public class Powers extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Integer a = null;
		Integer b = null;
		Integer n = null;
		String message = null;
		try {
			a = Integer.parseInt(req.getParameter("a"));
			b = Integer.parseInt(req.getParameter("b"));
			n = Integer.parseInt(req.getParameter("n"));
		} catch(Exception e) {
			message = "Failed to generate Excell file(.xls). Parameters invalid!";
		}
		if(a < -100 || a > 100 || b < -100 || b > 100 || n < 1 || n > 5) {
			message = "Failed to generate Excell file(.xls). Parameters invalid!";
		}
		if(message != null) {
			req.setAttribute("message",	message);
			req.getRequestDispatcher("/WEB-INF/pages/powersError.jsp").forward(req, resp);
			return;
		}
		HSSFWorkbook hwb = new HSSFWorkbook();
		for(int i = 1; i <= n; i++) {
			HSSFSheet sheet = hwb.createSheet(String.valueOf(i)+". strana");
			for(int j = 0, num = a; num <= b; j++, num++) {
				HSSFRow row = sheet.createRow(j);
				row.createCell(0).setCellValue(num);
				row.createCell(1).setCellValue(Math.pow(num, i));
			}
			
		}
		resp.setContentType("application/vnd.ms-excel");
		hwb.write(resp.getOutputStream());
		resp.flushBuffer();
	}
}
