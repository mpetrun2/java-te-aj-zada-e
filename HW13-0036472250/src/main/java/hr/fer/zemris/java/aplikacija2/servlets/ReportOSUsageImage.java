/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 * Generates OS usage pie chart. JFreeChart library used.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name="reportsImage", urlPatterns={"/reportImage"})
public class ReportOSUsageImage extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("image/png");
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Linux", 29);
		dataset.setValue("Mac", 20);
		dataset.setValue("Windows", 51);
		JFreeChart chart = ChartFactory.createPieChart3D("OS usage", // chart title
				dataset, // data
				true, // include legend
				true, false);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setSize(600, 400);
		BufferedImage image = new BufferedImage(600, 400, BufferedImage.TYPE_INT_RGB);
		chartPanel.paint(image.getGraphics());
		ImageIO.write(image, "png", resp.getOutputStream());
		resp.flushBuffer();
	}
}
