/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for setting background color to session.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="setColor", urlPatterns={"/setColor"})
public class SetColor extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("ovo je retardirano");
		String colorParam = req.getParameter("color");
		if(colorParam != null) {
			req.getSession().setAttribute("pickedBgCol", colorParam);
		}
		resp.sendRedirect("./colors.jsp");
	}
}
