/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for calculating table of squares.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="squares", urlPatterns={"/squares"})
public class Squares extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Integer a = 0;
		Integer b = 20;
		try {
			a = Integer.parseInt(req.getParameter("a"));
			b = Integer.parseInt(req.getParameter("b"));
		} catch(Exception e) {
			System.out.println(req.getContextPath() + " - Error while parsing parameters.");
		}
		if(a > b) {
			Integer temp = a;
			a = b;
			b = temp;
		}
		if(b > a + 20) {
			b = a + 20;
		}
	
		List<Integer> squares = new ArrayList<>();
		for(int i = a; i <= b; i++) {
			squares.add(i * i);
		}
		req.setAttribute("squares", squares);
		req.getRequestDispatcher("/WEB-INF/pages/squares.jsp").forward(req, resp);
	}
}
