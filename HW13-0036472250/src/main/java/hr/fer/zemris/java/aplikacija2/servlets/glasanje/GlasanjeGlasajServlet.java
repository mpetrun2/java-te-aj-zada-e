/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets.glasanje;

import hr.fer.zemris.java.aplikacija2.models.BandDefinitions;
import hr.fer.zemris.java.aplikacija2.models.DataPaths;
import hr.fer.zemris.java.aplikacija2.models.VoteDefinitions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Represents signle vote for some band given as parameter.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasaj", urlPatterns = { "/glasanje-glasaj" })
public class GlasanjeGlasajServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Integer bandID = null;
		try {
			bandID = Integer.parseInt(req.getParameter("id"));
		} catch (Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
		}
		BandDefinitions bd = new BandDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getBandDefinitionPath()));
		if (bd.bandIDExist(bandID)) {
			VoteDefinitions vd = new VoteDefinitions(req.getServletContext()
					.getRealPath(DataPaths.getVotesDefinitionPath()));
			vd.increaseVoteNumber(bandID);
		}
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
}
