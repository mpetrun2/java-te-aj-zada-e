/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets.glasanje;

import hr.fer.zemris.java.aplikacija2.models.Band;
import hr.fer.zemris.java.aplikacija2.models.BandDefinitions;
import hr.fer.zemris.java.aplikacija2.models.DataPaths;
import hr.fer.zemris.java.aplikacija2.models.Vote;
import hr.fer.zemris.java.aplikacija2.models.VoteDefinitions;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 * Generates vote result pie chart and sends it.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanjeGrafika", urlPatterns = { "/glasanje-grafika" })
public class GlasanjeGrafika extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		VoteDefinitions vd = new VoteDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getVotesDefinitionPath()));
		List<Vote> votes = vd.getVotes();
		Map<Band, Integer> results = vd.getVoteResults(
				votes,
				new BandDefinitions(req.getServletContext().getRealPath(
						DataPaths.getBandDefinitionPath())));
		resp.setContentType("image/png");
		int allVotes = vd.getTotalNumberOfVotes(votes);
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (Entry<Band, Integer> vote : results.entrySet()) {
			dataset.setValue(vote.getKey().getName(), (double) vote.getValue()
					/ allVotes * 100);
		}
		JFreeChart chart = ChartFactory.createPieChart3D("Rezultati glasanja", // chart
																				// title
				dataset, // data
				true, // include legend
				true, false);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setSize(600, 400);
		BufferedImage image = new BufferedImage(600, 400,
				BufferedImage.TYPE_INT_RGB);
		chartPanel.paint(image.getGraphics());
		ImageIO.write(image, "png", resp.getOutputStream());
		resp.flushBuffer();
	}
}
