/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets.glasanje;

import hr.fer.zemris.java.aplikacija2.models.Band;
import hr.fer.zemris.java.aplikacija2.models.BandDefinitions;
import hr.fer.zemris.java.aplikacija2.models.DataPaths;
import hr.fer.zemris.java.aplikacija2.models.Vote;
import hr.fer.zemris.java.aplikacija2.models.VoteDefinitions;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Gets all the neccessary data for displaying vote results accordingly.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanjeRezultati", urlPatterns = { "/glasanje-rezultati" })
public class GlasanjeRezultati extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		VoteDefinitions vd = new VoteDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getVotesDefinitionPath()));
		BandDefinitions bd = new BandDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getBandDefinitionPath()));
		List<Vote> votes = vd.getVotes();
		Map<Band, Integer> voteResults = vd.getVoteResults(votes, bd);
		req.setAttribute("voteResults", voteResults);
		Map<Band, Integer> voteWinners = vd.getVoteWinners(votes, bd);
		req.setAttribute("voteWinners", voteWinners);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req,
				resp);
	}
}
