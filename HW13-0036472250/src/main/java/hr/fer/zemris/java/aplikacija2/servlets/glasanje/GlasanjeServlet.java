/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets.glasanje;

import hr.fer.zemris.java.aplikacija2.models.BandDefinitions;
import hr.fer.zemris.java.aplikacija2.models.DataPaths;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Displays list of available bands to vote for.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanje", urlPatterns = { "/glasanje" })
public class GlasanjeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		BandDefinitions bd = new BandDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getBandDefinitionPath()));
		req.setAttribute("bands", bd.getBands());
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(
				req, resp);
	}
}
