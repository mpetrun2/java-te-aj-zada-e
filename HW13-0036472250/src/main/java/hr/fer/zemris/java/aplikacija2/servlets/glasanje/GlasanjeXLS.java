/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.servlets.glasanje;

import hr.fer.zemris.java.aplikacija2.models.Band;
import hr.fer.zemris.java.aplikacija2.models.BandDefinitions;
import hr.fer.zemris.java.aplikacija2.models.DataPaths;
import hr.fer.zemris.java.aplikacija2.models.Vote;
import hr.fer.zemris.java.aplikacija2.models.VoteDefinitions;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Returns band voting results as .xls file.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="glasanjeXLS", urlPatterns={"/glasanje-xls"})
public class GlasanjeXLS extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		VoteDefinitions vd = new VoteDefinitions(req.getServletContext()
				.getRealPath(DataPaths.getVotesDefinitionPath()));
		List<Vote> votes = vd.getVotes();
		Map<Band, Integer> results = vd.getVoteResults(
				votes,
				new BandDefinitions(req.getServletContext().getRealPath(
						DataPaths.getBandDefinitionPath())));
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Rezultati glasanja");
		int i = 0;
		HSSFRow header = sheet.createRow(i++);
		header.createCell(0).setCellValue("Ime benda");
		header.createCell(1).setCellValue("Broj glasova");
		for(Entry<Band, Integer> vote : results.entrySet()) {
			HSSFRow row = sheet.createRow(i++);
			row.createCell(0).setCellValue(vote.getKey().getName());
			row.createCell(1).setCellValue(vote.getValue());
		}
		resp.setContentType("application/vnd.ms-excel");
		hwb.write(resp.getOutputStream());
		resp.flushBuffer();
	}
}
