/**
 * 
 */
package hr.fer.zemris.java.aplikacija2.models;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class VoteTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija2.models.Vote#Vote(int, int)}.
	 */
	@Test
	public void testVote() {
		new Vote(3, 10);
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija2.models.Vote#getBandID()}.
	 */
	@Test
	public void testGetBandID() {
		Vote vote = new Vote(3, 10);
		assertEquals(3, vote.getBandID());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija2.models.Vote#setBandID(int)}.
	 */
	@Test
	public void testSetBandID() {
		Vote vote = new Vote(3, 10);
		vote.setBandID(1);
		assertEquals(1, vote.getBandID());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija2.models.Vote#getNumberOfVotes()}.
	 */
	@Test
	public void testGetNumberOfVotes() {
		Vote vote = new Vote(3, 10);
		assertEquals(10, vote.getNumberOfVotes());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija2.models.Vote#setNumberOfVotes(int)}.
	 */
	@Test
	public void testSetNumberOfVotes() {
		Vote vote = new Vote(3, 10);
		vote.setNumberOfVotes(9);
		assertEquals(9, vote.getNumberOfVotes());
	}

}
