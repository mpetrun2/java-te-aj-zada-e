<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bands Voting!</title>
	</head>
	<% Object color = session.getAttribute("pickedBgCol"); %>
	<% if(color == null) { %>
	<body>
	<% } else { %>
	<body style="background-color: <%= color %>">
	<% } %>
		<h1>Glasanje za omiljeni bend:</h1>
 		<p>Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali!</p>
 		<ol>
 			<c:forEach var="b" items="${bands}">
 				<li><a href="./glasanje-glasaj?id=${b.id}">${b.name}</a></li>
 			</c:forEach>
 		</ol>
		<a href="./">&lt;- Home</a>
	</body>
</html>