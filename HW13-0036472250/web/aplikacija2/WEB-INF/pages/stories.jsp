<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Select Color</title>
	</head>
	<% Object color = session.getAttribute("pickedBgCol"); %>
	<% if(color == null) { %>
	<body>
	<% } else { %>
	<body style="background-color: <%= color %>">
	<% } %>
		<% Integer r = (Integer)request.getAttribute("r"); %>
		<% Integer g = (Integer)request.getAttribute("g"); %>
		<% Integer b = (Integer)request.getAttribute("b"); %>
		<h1>A man talking to God:</h1> 
		<p style="color: rgb(${r}, ${g}, ${b});">
			The man: “God, how long is a million years?”<br>
			God: “To me, it’s about a minute.”<br>
			The man: “God, how much is a million dollars?”<br>
			God: “To me it’s a penny.”<br>
			The man: “God, may I have a penny?”<br>
			God: “Wait a minute.”<br>
		</p>
		<a href="../">&lt- Home</a>
	</body>
</html>