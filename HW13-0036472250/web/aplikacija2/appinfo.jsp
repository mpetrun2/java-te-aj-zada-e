<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Server Uptime</title>
	</head>
	<% Object color = session.getAttribute("pickedBgCol"); %>
	<% if(color == null) { %>
	<body>
	<% } else { %>
	<body style="background-color: <%= color %>">
	<% } %>
		<h1>Server Uptime:</h1>
		<% Long uptime = System.currentTimeMillis() - (Long) application.getAttribute("ServerStartedTime"); %>
		<p>
		<%= TimeUnit.MILLISECONDS.toDays(uptime) %> days,
		<% uptime = uptime - TimeUnit.DAYS.toMillis(TimeUnit.MILLISECONDS.toDays(uptime)); %>
		<%= TimeUnit.MILLISECONDS.toHours(uptime) %> hours, 
		<% uptime = uptime - TimeUnit.HOURS.toMillis(TimeUnit.MILLISECONDS.toHours(uptime)); %>
		<%= TimeUnit.MILLISECONDS.toMinutes(uptime) %> minutes, 
		<% uptime = uptime - TimeUnit.MINUTES.toMillis(TimeUnit.MILLISECONDS.toMinutes(uptime)); %>
		<%= TimeUnit.MILLISECONDS.toSeconds(uptime) %> seconds
		</p>
		<a href="./">&lt- Home</a>
	</body>
</html>