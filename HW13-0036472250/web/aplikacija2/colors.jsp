<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Select Color</title>
	</head>
	<% Object color = session.getAttribute("pickedBgCol"); %>
	<% if(color == null) { %>
	<body>
	<% } else { %>
	<body style="background-color: <%= color %>">
	<% } %>
		<h1>Select color:</h1> 
		<a href="./setColor?color=white">WHITE</a>
		<a href="./setColor?color=red">RED</a>
		<a href="./setColor?color=green">GREEN</a>
		<a href="./setColor?color=cyan">CYAN</a>
		<br>
		<a href="./">&lt;- Home</a>
	</body>
</html>