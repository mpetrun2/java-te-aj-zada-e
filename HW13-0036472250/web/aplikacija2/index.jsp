<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Welcome!</title>
	</head>
	<% Object color = session.getAttribute("pickedBgCol"); %>
	<% if(color == null) { %>
	<body>
	<% } else { %>
	<body style="background-color: <%= color %>">
	<% } %>
		<h1>Welcome!</h1>
		<h3><a href="./colors.jsp" >Background color chooser!</a> </h3>
		<h3><a href="./squares?a=100&b=200" >Squares!</a> </h3>
		<h3><a href="./stories/funny.jsp" >Funny Story!</a> </h3>
		<h3><a href="./reports.jsp" >OS Usage!</a> </h3>
		<h3><a href="./powers?a=1&b=100&n=3" >Powers!</a></h3>
		<h3><a href="./appinfo.jsp" >Server Uptime!</a></h3>
		<h3><a href="./glasanje" >Band Voting!</a></h3>
	</body>
</html>