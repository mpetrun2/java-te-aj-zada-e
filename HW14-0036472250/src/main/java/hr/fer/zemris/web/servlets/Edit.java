/**
 * 
 */
package hr.fer.zemris.web.servlets;

import hr.fer.zemris.web.servlets.paths.DataPaths;
import hr.fer.zemris.web.workshops.WorkShop;
import hr.fer.zemris.web.workshops.WorkShopDatabase;
import hr.fer.zemris.web.workshops.WorkShopForm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller that generates data for editing single {@link WorkShop}.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/edit")
public class Edit extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user") == null) {
			req.setAttribute("error", "Autentification required.");
			req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
			return;
		}
		Long workshopID = null;
		try {
			workshopID = Long.parseLong(req.getParameter("id"));
		} catch (Exception e) {
			resp.sendRedirect("./new");
			return;
		}
		String databaseDirectory = req.getServletContext().getRealPath(DataPaths.getPathToDatabase());
		WorkShopDatabase wd = WorkShopDatabase.load(databaseDirectory);
		if(wd.getWorkShop(workshopID) == null) {
			resp.sendRedirect("./new");
			return;
		}
		WorkShop ws = wd.getWorkShop(workshopID);
		WorkShopForm wf = new WorkShopForm();
		wf.fillFromWorkShop(ws);
		req.setAttribute("workshop", wf);
		req.setAttribute("equipment", wd.getEquipmentSet());
		req.setAttribute("audience", wd.getAudienceSet());
		req.setAttribute("duration", wd.getDurationSet());
		req.getRequestDispatcher("/WEB-INF/pages/workshopForm.jsp").forward(req, resp);
	}
}
