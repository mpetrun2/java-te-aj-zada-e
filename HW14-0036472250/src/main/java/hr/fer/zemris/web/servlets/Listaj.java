/**
 * 
 */
package hr.fer.zemris.web.servlets;

import hr.fer.zemris.web.servlets.paths.DataPaths;
import hr.fer.zemris.web.workshops.WorkShop;
import hr.fer.zemris.web.workshops.WorkShopDatabase;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Gets list of {@link WorkShop}, sorts it
 * and forward it to listaj.jsp.
 * @author Marin
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name="listaj", urlPatterns={"/listaj","/"})
public class Listaj extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user") != null) {
			req.setAttribute("user", req.getSession().getAttribute("current.user"));
		}
		String databaseDirectory = req.getServletContext().getRealPath(
				DataPaths.getPathToDatabase());
		WorkShopDatabase wd = WorkShopDatabase.load(databaseDirectory);
		List<WorkShop> workshops = wd.getWorkShops();
		req.setAttribute("workshops", workshops);
		req.getRequestDispatcher("/WEB-INF/pages/listaj.jsp").forward(req, resp);
	}
}
