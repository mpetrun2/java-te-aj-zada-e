/**
 * 
 */
package hr.fer.zemris.web.servlets;

import hr.fer.zemris.web.workshops.User;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for generating login form and logging user.
 * @author Marin
 *
 */
@SuppressWarnings("serial")
@WebServlet("/login")
public class Login extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user") != null) {
			resp.sendRedirect("./listaj");
			return;
		}
		req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getParameter("username");
		User user = User.checkCredentials(username, req.getParameter("password"));
		if(user != null) {
			req.getSession().setAttribute("current.user", user);
			resp.sendRedirect("./listaj");
			return;
		} else {
			req.setAttribute("error", "Invalid username or password.");
			req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
			return;
		}
	}
}
