/**
 * 
 */
package hr.fer.zemris.web.servlets;

import hr.fer.zemris.web.workshops.WorkShop;
import hr.fer.zemris.web.workshops.WorkShopDatabase;
import hr.fer.zemris.web.workshops.WorkShopForm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Glass for generating data for empty {@link WorkShop}
 * form.
 * @author Marin
 *
 */
@SuppressWarnings("serial")
@WebServlet("/new")
public class New extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user") == null) {
			req.setAttribute("error", "Autentification required.");
			req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
			return;
		}
		String databasePath = req.getServletContext().getRealPath("/WEB-INF/data/baza");
		WorkShopDatabase wd = WorkShopDatabase.load(databasePath);
		req.setAttribute("equipment", wd.getEquipmentSet());
		req.setAttribute("audience", wd.getAudienceSet());
		req.setAttribute("duration", wd.getDurationSet());
		req.setAttribute("workshop", new WorkShopForm());
		req.getRequestDispatcher("/WEB-INF/pages/workshopForm.jsp").forward(req, resp);
	}
}
