/**
 * 
 */
package hr.fer.zemris.web.servlets;

import java.io.IOException;

import hr.fer.zemris.web.servlets.paths.DataPaths;
import hr.fer.zemris.web.workshops.WorkShop;
import hr.fer.zemris.web.workshops.WorkShopDatabase;
import hr.fer.zemris.web.workshops.WorkShopForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for saving sent {@link WorkShop} into
 * database.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/save")
public class Save extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		if(req.getSession().getAttribute("current.user") == null) {
			req.setAttribute("error", "Autentification required.");
			req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
			return;
		}
		String databaseDirectory = req.getServletContext().getRealPath(DataPaths.getPathToDatabase());
		WorkShopDatabase wd = WorkShopDatabase.load(databaseDirectory);
		WorkShopForm wf = new WorkShopForm();
		if(req.getParameter("method").equals("Reset")) {
			req.setAttribute("workshop", wf);
			req.setAttribute("equipment", wd.getEquipmentSet());
			req.setAttribute("audience", wd.getAudienceSet());
			req.setAttribute("duration", wd.getDurationSet());
			req.getRequestDispatcher("/WEB-INF/pages/workshopForm.jsp").forward(req, resp);
			return;
		}
		wf.fillFromHttpRequest(req);
		wf.validate();
		if(wf.errorExists()) {
			req.setAttribute("workshop", wf);
			req.setAttribute("equipment", wd.getEquipmentSet());
			req.setAttribute("audience", wd.getAudienceSet());
			req.setAttribute("duration", wd.getDurationSet());
			req.getRequestDispatcher("/WEB-INF/pages/workshopForm.jsp").forward(req, resp);
			return;
		}
		WorkShop ws = new WorkShop(null);
		wf.fillWorkShop(ws, wd);
		wd.addWorkShop(ws);
		wd.save();
		resp.sendRedirect("./listaj");
	}
}
