/**
 * 
 */
package hr.fer.zemris.web.servlets.paths;

/**
 * Class containing static methods that return paths to resources.
 * @author Marin
 *
 */
public class DataPaths {

	/**
	 * Return path to database folder.
	 * @return
	 */
	public static String getPathToDatabase() {
		return "/WEB-INF/data/baza/";
	}
}
