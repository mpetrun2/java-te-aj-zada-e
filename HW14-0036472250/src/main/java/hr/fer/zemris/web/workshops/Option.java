/**
 * 
 */
package hr.fer.zemris.web.workshops;

/**
 * Representing signle read-only option.
 * @author Marin Petrunić 0036472250
 *
 */
public class Option implements Comparable<Option> {
	
	/**
	 * Unique option id.
	 */
	private String id;
	
	/**
	 * Option value.
	 */
	private String value;
	
	/**
	 * Constructor.
	 * @param id
	 * @param value
	 */
	public Option(String id, String value) {
		super();
		if(id == null) {
			throw new IllegalArgumentException("Id cannot be null");
		}
		try {
			Long idNum = Long.parseLong(id);
			if(idNum <= 0) {
				throw new IllegalArgumentException("Id must be positive");
			}
		} catch(NumberFormatException e) {
			throw new IllegalArgumentException("Id must be valid Long number.");
		}
		this.id = id;
		this.value = value;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public int compareTo(Option o) {
		return this.getId().compareTo(o.getId());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Option other = (Option) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + "\t" + value;
	}
}
