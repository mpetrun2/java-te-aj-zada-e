/**
 * 
 */
package hr.fer.zemris.web.workshops;

/**
 * Class representing single user
 * with its propertys.
 * @author Marin
 *
 */
public class User {
	
	
	
	/**
	 * User username.
	 */
	private String username;

	/**
	 * User password
	 */
	private String password;
	
	/**
	 * User name.
	 */
	private String name;

	/**
	 * User lastname.
	 */
	private String prezime;

	/**
	 * Constructor.
	 * @param username
	 * @param password
	 * @param name
	 * @param prezime
	 */
	public User(String username, String password, String name, String prezime) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.prezime = prezime;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the prezime
	 */
	public String getPrezime() {
		return prezime;
	}

	/**
	 * @param prezime the prezime to set
	 */
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	/**
	 * Checks if given user credentials exist and are valid.
	 * @param username
	 * @param password
	 * @return
	 */
	public static User checkCredentials(String username, String password) {
		if(username.equals("aante") && password.equals("tajna")) {
			return new User("aante", "tajna", "Ante", "Anić");
		}
		return null;
	}
}
