/**
 * 
 */
package hr.fer.zemris.web.workshops;

import java.util.Set;
import java.util.TreeSet;

/**
 * Class representing single WorkShop record 
 * with correct propertys.
 * @author Marin Petrunić 0036472250
 *
 */
public class WorkShop implements Comparable<WorkShop>{

	/**
	 * Unique workshop id.
	 */
	private Long id;
	
	/**
	 * Workshop name.
	 */
	private String name;
	
	/**
	 * Workshop event date.
	 */
	private String date;
	
	/**
	 * Set of needed equipment.
	 */
	private Set<Option> equipment; 
	
	/**
	 * Duration of workshop.
	 */
	private Option duration;
	
	/**
	 * Workshop audience. 
	 */
	private Set<Option> audience;
	
	/**
	 * Maximum number of workshop participants.
	 */
	private Integer maxParticipants;
	
	/**
	 * WorkShop contact email.
	 */
	private String email;
	
	/**
	 * Workshop notice.
	 */
	private String notice;
	
	/**
	 * Constructor.
	 * @param id
	 */
	public WorkShop(Long id) {
		this.setId(id);
	}

	/**
	 * Constructor.
	 * @param id
	 * @param name
	 * @param date
	 * @param equipment
	 * @param duration
	 * @param audience
	 * @param maxParticipants
	 * @param email
	 * @param notice
	 */
	public WorkShop(Long id, String name, String date, Set<Option> equipment,
			Option duration, Set<Option> audience, Integer maxParticipants,
			String email, String notice) {
		if(id == null) {
			throw new IllegalArgumentException("Workshop id cannot be null.");
		}
		this.id = id;
		this.name = name;
		this.date = date;
		if(equipment == null) {
			this.equipment = new TreeSet<>();
		} else {
			this.equipment = equipment;
		}
		this.duration = duration;
		if(audience == null) {
			this.audience = new TreeSet<>();
		} else {
			this.audience = audience;
		}
		this.maxParticipants = maxParticipants;
		this.email = email;
		this.notice = notice;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 * @throws IllegalArgumentException if id is null.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the equipment
	 */
	public Set<Option> getEquipment() {
		return equipment;
	}

	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(Set<Option> equipment) {
		this.equipment = equipment;
	}

	/**
	 * @return the duration
	 */
	public Option getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Option duration) {
		this.duration = duration;
	}

	/**
	 * @return the audience
	 */
	public Set<Option> getAudience() {
		return audience;
	}

	/**
	 * @param audience the audience to set
	 */
	public void setAudience(Set<Option> audience) {
		this.audience = audience;
	}

	/**
	 * @return the maxParticipants
	 */
	public Integer getMaxParticipants() {
		return maxParticipants;
	}

	/**
	 * @param maxParticipants the maxParticipants to set
	 */
	public void setMaxParticipants(Integer maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkShop other = (WorkShop) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return id + "\t" + name + "\t" + date
				+ "\t" + maxParticipants + "\t" + duration.getId()
				+ "\t" + email + "\t" + encodeValue(notice);
	}

	/**
	 * Escapes dangerous characters.
	 * @param notice string to be escaped
	 * @return
	 */
	private String encodeValue(String notice) {
		notice = notice.replaceAll("\n", "\\\\n");
		notice = notice.replaceAll("\t", "\\\\t");
		notice = notice.replaceAll("\\\\", "\\\\\\\\");
		return notice;
	}

	@Override
	public int compareTo(WorkShop o) {
		int result = this.getDate().compareTo(o.getDate());
		if(result == 0) {
			result = this.getName().compareTo(o.getName());
		}
		return result;
	}
	
	
}
