/**
 * 
 */
package hr.fer.zemris.web.workshops;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Class representing database with {@link WorkShop} definition and gives
 * methods for adding, editing and deleting {@link WorkShop}.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
public class WorkShopDatabase {

	/**
	 * {@link Path} to folder with database files.
	 */
	private Path databasepath;

	/**
	 * Set with all possible equipment values.
	 */
	private Map<Long, Option> equipmentSet;

	/**
	 * Set with all possible audience values.
	 */
	private Map<Long, Option> audienceSet;

	/**
	 * set with all possible duration values.
	 */
	private Map<Long, Option> durationSet;

	/**
	 * Map that contains {@link WorkShop} id as key and {@link List} of
	 * equipment {@link Option}s as value.
	 */
	private Map<Long, List<Option>> workShopEquipment;

	/**
	 * Map that contains {@link WorkShop} id as key and {@link List} of audience
	 * {@link Option}s as value.
	 */
	private Map<Long, List<Option>> workShopAudience;

	/**
	 * List of {@link WorkShop}s existing in database.
	 */
	private List<WorkShop> workShops = new ArrayList<>();

	/**
	 * Constructor.
	 * 
	 * @param databasePath
	 *            {@link Path} to folder with database files
	 * @throws IllegalArgumentException
	 *             if given {@link Path} is null or not a directory
	 */
	public WorkShopDatabase(Path databasePath) {
		if (databasePath == null || !Files.isDirectory(databasePath)) {
			throw new IllegalArgumentException(
					"Path to database cannot be null");
		}
		this.databasepath = databasePath;
		try {
			this.audienceSet = loadAudience();
		} catch (IOException e) {
			System.err.println("Error while loading audience database file.");
		}
		try {
			this.equipmentSet = loadEquipment();
		} catch (IOException e) {
			System.err.println("Error while loading equipment database file.");
		}
		try {
			this.durationSet = loadDuration();
		} catch (IOException e) {
			System.err.println("Error while loading duration database file.");
		}
		try {
			workShopAudience = loadWorkShopAudience();
		} catch (IOException e) {
			System.err.println("Error while loading workshop audience file.");
		}
		try {
			workShopEquipment = loadWorkShopEquipment();
		} catch (IOException e) {
			System.err.println("Error while loading workshop equipment file.");
		}
		try {
			workShops = loadWorkShops();
		} catch (IOException e) {
			System.err.println("Error while loading workshops file.");
		}
	}

	/**
	 * Method for loading data from database file into {@link Set}.
	 * 
	 * @return
	 * @throws IOException
	 *             if some error occurred while reading from file.
	 */
	private Map<Long, Option> loadEquipment() throws IOException {
		Path equipmentDatabase = Paths.get(databasepath + "/oprema.txt");
		List<String> equipmentDef = readDatabaseFile(equipmentDatabase);
		Map<Long, Option> equipment = processDatabaseLines(equipmentDef);
		return equipment;
	}

	/**
	 * Method for loading audience data from database file into {@link Set};
	 * 
	 * @return
	 * @throws IOException
	 *             if error occurred while reading from file.
	 */
	private Map<Long, Option> loadAudience() throws IOException {
		Path audienceDatabase = Paths.get(databasepath + "/publika.txt");
		List<String> audienceDef = readDatabaseFile(audienceDatabase);
		Map<Long, Option> audience = processDatabaseLines(audienceDef);
		return audience;
	}

	/**
	 * Method for loading duration data from database file into {@link Set}.
	 * 
	 * @return
	 * @throws IOException
	 *             if error occurred while reading from file
	 */
	private Map<Long, Option> loadDuration() throws IOException {
		Path durationDatabase = Paths.get(databasepath + "/trajanje.txt");
		List<String> durationDef = readDatabaseFile(durationDatabase);
		Map<Long, Option> duration = processDatabaseLines(durationDef);
		return duration;
	}

	/**
	 * Fills map with {@link WorkShop} specific list of needed equipment.
	 * 
	 * @return
	 * @throws IOException
	 *             if error while reading file occurred
	 */
	private Map<Long, List<Option>> loadWorkShopEquipment() throws IOException {
		Path workShopEquipmentDatabase = Paths.get(databasepath
				+ "/radionice_oprema.txt");
		List<String> equipmentLines = readDatabaseFile(workShopEquipmentDatabase);
		return connectWorkShopData(equipmentLines, equipmentSet);
	}

	/**
	 * Fills map with {@link WorkShop} specific list of equipment.
	 * 
	 * @return
	 * @throws IOException
	 *             if error while reading file occurred
	 */
	private Map<Long, List<Option>> loadWorkShopAudience() throws IOException {
		Path workShopAudienceDatabase = Paths.get(databasepath
				+ "/radionice_publika.txt");
		List<String> audienceLines = readDatabaseFile(workShopAudienceDatabase);
		return connectWorkShopData(audienceLines, audienceSet);
	}

	/**
	 * Loads {@link WorkShop} from its database file into list of
	 * {@link WorkShop}s.
	 * 
	 * @return
	 * @throws IOException
	 */
	private List<WorkShop> loadWorkShops() throws IOException {
		Path workShopDatabase = Paths.get(databasepath + "/radionice.txt");
		List<String> workShopLines = readDatabaseFile(workShopDatabase);
		List<WorkShop> workShops = new ArrayList<>();
		for (String workShopLine : workShopLines) {
			try {
				String[] workshopArguments = workShopLine.split("\t");
				Long id = Long.parseLong(workshopArguments[0]);
				if(workShopAudience.get(id) == null) {
					workShopAudience.put(id, new ArrayList<Option>());
				}
				if(workShopEquipment.get(id) == null) {
					workShopEquipment.put(id, new ArrayList<Option>());
				}
				String notice = "";
				if(workshopArguments.length > 6) {
					notice = workshopArguments[6];
				}
				WorkShop ws = new WorkShop(id,
						workshopArguments[1],
						workshopArguments[2],
						new TreeSet<>(workShopEquipment.get(id)),
						durationSet.get(Long.parseLong(workshopArguments[4])),
						new TreeSet<>(workShopAudience.get(id)),
						Integer.parseInt(workshopArguments[3]),
						workshopArguments[5],
						processValue(notice));
				workShops.add(ws);
			} catch (Exception e) {
				System.err
						.println("Error while parsing workshop line. Line skipped");
				continue;
			}
		}
		return workShops;
	}

	/**
	 * Connects id of option in list of option connections with values of
	 * corresponding Option in given option Set.
	 * 
	 * @param connections
	 * @param options
	 * @return
	 */
	private Map<Long, List<Option>> connectWorkShopData(
			List<String> connections, Map<Long, Option> options) {
		Map<Long, List<Option>> workShopEquipment = new HashMap<>();
		for (String connection : connections) {
			Long optionID = null;
			Long workshopID = null;
			try {
				String[] connectionArguments = connection.split("\t");
				workshopID = Long.parseLong(connectionArguments[0]);
				optionID = Long.parseLong(connectionArguments[1]);
			} catch (Exception e) {
				System.err
						.println("Invalid workshop data line. Line skipped.");
				continue;
			}
			Option option = options.get(optionID);
			if (option != null) {
				if(!workShopEquipment.containsKey(workshopID)) {
					workShopEquipment.put(workshopID, new ArrayList<Option>());
				}
				workShopEquipment.get(workshopID).add(option);
			}
		}
		return workShopEquipment;	
	}

	/**
	 * Reads all lines from given database file. If file doesn't exist new file
	 * is created and empty {@link List} is returned.
	 * 
	 * @param databaseFile
	 *            database file to read from.
	 * @return
	 * @throws IOException
	 *             if error while reading file occured.
	 */
	private List<String> readDatabaseFile(Path databaseFile) throws IOException {
		if (!Files.exists(databaseFile)) {
			Files.createFile(databaseFile);
		}
		return Files.readAllLines(databaseFile, StandardCharsets.UTF_8);
	}

	/**
	 * Process given database line and returns {@link List} with containing
	 * {@link Option}s. Database Lines must be formated like : id value
	 * 
	 * @param databaseLines
	 *            given database line to be processed
	 * @return
	 */
	private Map<Long, Option> processDatabaseLines(List<String> databaseLines) {
		Map<Long, Option> options = new TreeMap<>();
		for (String line : databaseLines) {
			try {
				String[] option = line.split("\t");
				options.put(Long.parseLong(option[0]), new Option(option[0],
						option[1]));
			} catch (Exception e) {
				System.err
						.println("Error while parsing database lines. Line ignored.");
				continue;
			}
		}
		return options;
	}

	/**
	 * Decodes given string by replacing escaped sequences with its original
	 * signs.
	 * 
	 * @param string
	 *            to be decoded.
	 * @return
	 */
	private String processValue(String string) {
		string = string.replaceAll("\\\\t", "\t");
		string = string.replaceAll("\\\\n", "\n");
		string = string.replaceAll("\\\\\\\\", "\\\\");
		return string;
	}

	/**
	 * Gets existing {@link WorkShop} by its id or null if {@link WorkShop} with
	 * given id doesnt exist.
	 * 
	 * @param workshopID
	 * @return
	 */
	public WorkShop getWorkShop(Long workshopID) {
		for (WorkShop ws : workShops) {
			if (Long.compare(workshopID, ws.getId()) == 0) {
				return ws;
			}
		}
		return null;
	}
	
	/**
	 * Returns unmodifiable list of {@link WorkShop}s.
	 * @return
	 */
	public List<WorkShop> getWorkShops() {
		Collections.sort(workShops);
		return Collections.unmodifiableList(workShops);
	}

	/**
	 * Adds new {@link WorkShop} to database.
	 * 
	 * @param ws
	 *            {@link WorkShop}
	 */
	public void addWorkShop(WorkShop ws) {
		if(ws.getId() == null) {
			Random r = new Random();
			while(true) {
				Long id = r.nextLong();
				if(id <= 0 || idExist(id)) {
					continue;
				}
				ws.setId(id);
				workShops.add(ws);
				workShopEquipment.put(id, new ArrayList<>(ws.getEquipment()));
				workShopAudience.put(id, new ArrayList<>(ws.getAudience()));
				break;
			}
		} else {
			workShops.set(workShops.indexOf(ws), ws);
			workShopEquipment.put(ws.getId(), new ArrayList<>(ws.getEquipment()));
			workShopAudience.put(ws.getId(), new ArrayList<>(ws.getAudience()));
		}
	}

	/**
	 * Checks if some workshop contains given id.
	 * @param id {@link WorkShop} id
	 * @return
	 */
	private boolean idExist(Long id) {
		for(WorkShop ws : workShops) {
			if(ws.getId() == id) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the unmodifiable equipmentSet
	 */
	public Map<Long, Option> getEquipmentSet() {
		return Collections.unmodifiableMap(equipmentSet);
	}

	/**
	 * @return the unmodifiable audienceSet
	 */
	public Map<Long, Option> getAudienceSet() {
		return Collections.unmodifiableMap(audienceSet);
	}

	/**
	 * @return the unmodifiable durationSet
	 */
	public Map<Long, Option> getDurationSet() {
		return Collections.unmodifiableMap(durationSet);
	}

	/**
	 * Saves current database to correct files. If files doesn't exist new files
	 * are created.
	 * 
	 * @param directory
	 *            directory where database will be saved.
	 */
	public void save(Path directory) {
		try {
			Files.createDirectories(directory);
		} catch (IOException e) {
			System.err
					.println("Something went wrong while creating directory.");
		}
		saveOptions(equipmentSet, Paths.get(directory + "/oprema.txt"));
		saveOptions(audienceSet, Paths.get(directory + "/publika.txt"));
		saveOptions(durationSet, Paths.get(directory + "/trajanje.txt"));
		saveConnections(workShopAudience,
				Paths.get(directory + "/radionice_publika.txt"));
		saveConnections(workShopEquipment,
				Paths.get(directory + "/radionice_oprema.txt"));
		saveWorkShops(workShops, Paths.get(directory + "/radionice.txt"));
	}

	/**
	 * Saves database into their original source directory.
	 */
	public void save() {
		this.save(databasepath);
	}

	/**
	 * Saves given {@link Option}s map and saves it to file, each {@link Option}
	 * is saved in new line.
	 * 
	 * @param options
	 * @param path
	 */
	private void saveOptions(Map<Long, Option> options, Path path) {
		if (!Files.exists(path)) {
			try {
				Files.createFile(path);
			} catch (IOException e) {
				System.err.println("Failed to create file. " + path);
			}
		}
		List<String> lines = new ArrayList<>();
		for (Entry<Long, Option> option : options.entrySet()) {
			lines.add(option.getValue().toString());
		}
		try {
			Files.write(path, lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Error while writing database file. " + path);
		}
	}

	/**
	 * Saves workshop connection into correct
	 * database files.
	 * @param connections to be saved
	 * @param path database file into connections will be saved
	 */
	private void saveConnections(Map<Long, List<Option>> connections, Path path) {
		List<String> data = new ArrayList<>();
		for (Entry<Long, List<Option>> connection : connections.entrySet()) {
			for (Option option : connection.getValue()) {
				data.add(connection.getKey() + "\t" + option.getId());
			}
		}
		try {
			Files.write(path, data, StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Failed to write database file.");
		}
	}
	
	/**
	 * Saves all workshops to correct file.
	 * @param workShops
	 * @param path
	 */
	private void saveWorkShops(List<WorkShop> workShops, Path path) {
		List<String> lines = new ArrayList<>();
		for(WorkShop ws : workShops) {
			lines.add(ws.toString());
		}
		try {
			Files.write(path, lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Error while writing workshops to file. " + path);
		}
	}

	/**
	 * Returns instance od {@link WorkShopDatabase} with loaded database files.
	 * 
	 * @param pathToDatabase
	 *            path to folder with database files.
	 * @return {@link WorkShopDatabase}
	 */
	public static WorkShopDatabase load(String pathToDatabase) {
		return new WorkShopDatabase(Paths.get(pathToDatabase));
	}

	/**
	 * Gets Equipment {@link Option} by its id.
	 * @param equipmentTypeID
	 * @return
	 */
	public Option getEquipmentOption(Long equipmentTypeID) {
		return equipmentSet.get(equipmentTypeID);
	}

	/**
	 * Gets audience {@link Option} by its id.
	 * @param audienceTypeID
	 * @return
	 */
	public Option getAudienceOption(Long audienceTypeID) {
		return audienceSet.get(audienceTypeID);
	}

	
	/**
	 * Returns duration {@link Option}
	 * by its id.
	 * @param durationTypeID
	 * @return
	 */
	public Option getDurationOption(Long durationTypeID) {
		return durationSet.get(durationTypeID);
	}
}
