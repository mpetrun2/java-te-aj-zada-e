/**
 * 
 */
package hr.fer.zemris.web.workshops;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

/**
 * Represents model for displaying data
 * into html form.
 * @author Marin Petrunić 0036472250
 *
 */
public class WorkShopForm {

	/**
	 * {@link WorkShop} id.
	 */
	private String id;
	
	/**
	 * {@link WorkShop} name.
	 */
	private String name;
	
	/**
	 * {@link WorkShop} event date.
	 */
	private String date;
	
	/**
	 * {@link Set} of equipment {@link Option} id
	 * needed by {@link WorkShop}.
	 */
	private Set<String> equipment;
	
	/**
	 * Id of {@link WorkShop} duration option.
	 */
	private String duration; 
	
	/**
	 * {@link Set} of audience {@link Option} id
	 * needed by {@link WorkShop}.
	 */
	private Set<String> audience;
	
	/**
	 * Maximum number of {@link WorkShop} participants.
	 */
	private String maxParticipants;
	
	/**
	 * {@link WorkShop} contact email.
	 */
	private String email;
	
	/**
	 * {@link WorkShop} notice.
	 */
	private String notice;
	
	/**
	 * Map of errors occurred while validating.
	 * Key of map are name of fields being validated,
	 * and value is error message detail.
	 */
	private Map<String, String> errors;
	
	/**
	 * Constructor.
	 */
	public WorkShopForm() {
		
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the equipment
	 */
	public Set<String> getEquipment() {
		return equipment;
	}

	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(Set<String> equipment) {
		this.equipment = equipment;
	}

	/**
	 * @return the durationID
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param durationID the durationID to set
	 */
	public void setDuration(String durationID) {
		this.duration = durationID;
	}

	/**
	 * @return the audience
	 */
	public Set<String> getAudience() {
		return audience;
	}

	/**
	 * @param audience the audience to set
	 */
	public void setAudience(Set<String> audience) {
		this.audience = audience;
	}

	/**
	 * @return the maxParticipants
	 */
	public String getMaxParticipants() {
		return maxParticipants;
	}

	/**
	 * @param maxParticipants the maxParticipants to set
	 */
	public void setMaxParticipants(String maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * @param notice the notice to set
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}


	/**
	 * Checks if some errors occurred while validating.
	 * @return
	 */
	public boolean errorExists() {
		if(errors == null || errors.size() == 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if there is error
	 * for given field name.
	 * @param errorName
	 * @return
	 */
	public boolean hasError(String errorName) {
		if(errors == null) {
			return false;
		}
		return errors.containsKey(errorName);
	}
	
	/**
	 * Returns error message for given name
	 * or empty string if there is no such error.
	 * @param errorName
	 * @return
	 */
	public String getError(String errorName) {
		if(hasError(errorName)) {
			return errors.get(errorName);
		}
		return "";
	}

	/**
	 * Fills {@link WorkShopForm} data with 
	 * info from given {@link WorkShop}.
	 * @param ws
	 */
	public void fillFromWorkShop(WorkShop ws) {
		if(ws.getId() != null) {
			this.id = ws.getId().toString();
		}
		this.audience = new TreeSet<>();
		for(Option audienceType : ws.getAudience()) {
			audience.add(audienceType.getId());
		}
		this.date = ws.getDate();
		if(ws.getDuration() != null || ws.getDuration().getId()!=null) {
			this.duration = ws.getDuration().getId();
		}
		this.email = ws.getEmail();
		this.equipment = new TreeSet<>();
		for(Option equipmentType : ws.getEquipment()) {
			equipment.add(equipmentType.getId());
		}
		this.maxParticipants = ws.getMaxParticipants().toString();
		this.name = ws.getName();
		this.notice = ws.getNotice();
	}

	/**
	 * Fills {@link WorkShopForm} data with
	 * informations from given {@link HttpServletRequest}.
	 * @param req
	 */
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.id = req.getParameter("id");
		String[] audienceParameters = req.getParameterValues("audience");
		audience = new TreeSet<>();
		if(audienceParameters != null) {
			for(String adienceTypeID : audienceParameters) {
				audience.add(adienceTypeID);
			}
		}
		date = req.getParameter("date");
		duration = req.getParameter("duration");
		email = req.getParameter("email");
		equipment = new TreeSet<>();
		String[] equipmentParameters = req.getParameterValues("equipment");
		if(equipmentParameters != null) {
			for(String equipmentTypeID : equipmentParameters) {
				equipment.add(equipmentTypeID);
			}
		}
		maxParticipants = req.getParameter("maxParticipants");
		name = req.getParameter("name");
		notice = req.getParameter("notice");
	}

	/**
	 * Fills given {@link WorkShop} with data from current
	 * {@link WorkShopForm}.
	 * @param ws {@link WorkShop} to be filled
	 * @param wd {@link WorkShopDatabase} to get {@link Option}s
	 */
	public void fillWorkShop(WorkShop ws, WorkShopDatabase wd) {
		if(id != null && !id.isEmpty()) {
			ws.setId(Long.parseLong(id));
		}
		Set<Option> audience = new TreeSet<>();
		for(String audienceTypeID : this.audience) {
			Option o = wd.getAudienceOption(Long.parseLong(audienceTypeID));
			if(o != null) {
				audience.add(o);
			}
		}
		ws.setAudience(audience);
		ws.setDate(date);
		Option d = wd.getDurationOption(Long.parseLong(duration));
		if(d != null) {
			ws.setDuration(d);
		}
		ws.setEmail(email);
		Set<Option> equipment = new TreeSet<>();
		for(String equipmentTypeID : this.equipment) {
			Option o = wd.getEquipmentOption(Long.parseLong(equipmentTypeID));
			if(o != null) {
				equipment.add(o);	
			}
		}
		ws.setEquipment(equipment);
		ws.setMaxParticipants(Integer.parseInt(maxParticipants));
		ws.setName(name);
		ws.setNotice(notice);
	}

	/**
	 * Validates property of current
	 * {@link WorkShopForm}, if some errors
	 * occurred while validating
	 * error map will be filled.
	 */
	public void validate() {
		errors = new HashMap<>();
		if(id != null && !id.isEmpty()) {
			try{
				Long id = Long.parseLong(this.id);
				if(id <= 0) {
					errors.put("id", "Invalid id number.");
				}
			} catch(NumberFormatException e) {
				errors.put("id", "Invalid id number format.");
			}
		}
		for(String audienceID : audience) {
			try {
				Long.parseLong(audienceID);
			} catch(NumberFormatException e) {
				errors.put("audience", "Invalid audience option selected.");
			}
		}
		try {
			Long.parseLong(duration);
		} catch (NumberFormatException e) {
			errors.put("duration", "Invalid duration option selected.");
		}
		if(!email.matches("^[a-zA-Z0-9\\-]+@[a-zA-Z0-9]+\\.[a-z]{2,3}$")) {
			errors.put("email", "Email isnt valid.");
		}
		for(String equipmentID : equipment) {
			try {
				Long.parseLong(equipmentID);
			} catch(NumberFormatException e) {
				errors.put("equipment", "Invalid equipment option selected.");
			}
		}
		try {
			Integer n = Integer.parseInt(maxParticipants);
			if(n <= 0) {
				errors.put("maxParticipants", "Invalid maximum participants number.");
			}
		} catch (NumberFormatException e) {
			errors.put("maxParticipants", "Invalid maximum participants number.");
		}
		if(name.isEmpty()) {
			errors.put("name", "Invalid name.");
		}
		if(date.trim().isEmpty()) {
			errors.put("date", "Date field cannot be empty.");
		}
	}
	
}
