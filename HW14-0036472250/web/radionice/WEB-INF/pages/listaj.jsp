<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>WorkShop List!</title>
	</head>
	<body>
		<div style="position: absolute; top: 0; right: 0; padding: 10px;">
			<c:if test="${user==null}">
				<span>Anonimni Korisnik</span>
				<a href="./login">Login</a>
			</c:if>
			<c:if test="${user!=null}">
				<span>${user.username }</span>
				<a href="./logout">Logout</a>
			</c:if>
		</div>
		<h1>WorkShop List</h1>
		<table>
			<tr><th>WorkShop</th><th>Event Date</th></tr>
			<c:forEach var="ws" items="${workshops}">
				<tr><td><a href="./edit?id=${ws.id}">${ws.name}</a></td><td>${ws.date}</td></tr>
			</c:forEach>
		</table>
		<a href="./new">New WorkShop</a>
	</body>
</html>