<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login!</title>
	</head>
	<body>
		<h1>Login</h1>
		<form action="./login" method="post">
			<table>
				<c:if test="${error != null}">
					<tr >
						<td colspan="2">
							<span style="color:red; font-style: italic;">${error}</span>
						</td>
					</tr>
				</c:if>
				<tr>
					<td><i>Username</i></td>
					<td><input type="text" name="username" size="20" value="${user.username}"/></td>
				</tr>
				<tr>
					<td><i>Password</i></td>
					<td><input type="password" name="password" size="20" /></td>
				</tr>
				<tr>
					<td><a href="./listaj">Home</a></td>
					<td><input type="submit" value="Login"></td>
				</tr>
			</table>
		</form>
	</body>
</html>