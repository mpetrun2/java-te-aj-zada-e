<%@ page language="java" contentType="text/html; charset=UTF-8"
	session="true" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New WorkShop</title>
<style type="text/css">
	.error {
		color: red;
		font-style: italic;
	}
</style>
</head>
<body>
	<c:if test="${workshop.id != null}">
		<h1>Edit WorkShop!</h1>
	</c:if>
	<c:if test="${workshop.id == null}">
		<h1>Create new WorkShop!</h1>
	</c:if>
	<form action="./save" method="post">
		<input type="hidden" name="id" value="${workshop.id}">

		<table>
			<tr>
				<c:if test="${workshop.hasError('name')}">
					<tr>
						<td><span class="error">${workshop.getError("name") }</span></td>
					</tr>
				</c:if>
				<td><i>Name</i></td>
				<td><input type="text" name="name" size="40"
					value="${workshop.name}"></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('date') }">
					<tr>
						<td><span class="error">${workshop.getError("date") }</span></td>
					</tr>
				</c:if>
				<td><i>Date</i></td>
				<td><input type="text" name="date" size="20"
					value="${workshop.date }"></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('equipment') }">
					<tr>
						<td><span class="error">${workshop.getError("equipment") }</span></td>
					</tr>
				</c:if>
				<td><i>Equipment</i></td>
				<td><select name="equipment" size="5" multiple="multiple">
						<c:forEach var="e" items="${equipment}">
							<c:choose>
								<c:when
									test="${workshop.getEquipment().contains(e.value.getId())}">
									<option value="${e.value.getId()}" selected="selected">${e.value.getValue()}</option>
								</c:when>
								<c:otherwise>
									<option value="${e.value.getId()}">${e.value.getValue()}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('duration') }">
					<tr><td><span class="error">${workshop.getError("duration") }</span>
					</td></tr>
				</c:if>
				<td><i>Duration</i></td>
				<td><select name="duration">
						<c:forEach var="d" items="${duration}">
							<c:choose>
								<c:when test="${workshop.getDuration().equals(d.value.getId())}">
									<option value="${d.value.getId()}" selected="selected">${d.value.getValue()}</option>
								</c:when>
								<c:otherwise>
									<option value="${d.value.getId()}">${d.value.getValue()}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('audience') }">
					<tr><td><span class="error">${workshop.getError("audience") }</span>
					</td></tr>
				</c:if>
				<td><i>Audience</i></td>
				<td><c:forEach var="a" items="${audience}">
						<c:choose>
							<c:when
								test="${workshop.getAudience().contains(a.value.getId())}">
								<input type="checkbox" name="audience"
									value="${a.value.getId()}" checked="checked" />${a.value.getValue()}<br>
							</c:when>
							<c:otherwise>
								<input type="checkbox" name="audience"
									value="${a.value.getId()}" />${a.value.getValue()}<br>
							</c:otherwise>
						</c:choose>
					</c:forEach></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('maxParticipants') }">
					<tr><td><span class="error">${workshop.getError("maxParticipants") }</span>
					</td></tr>
				</c:if>
				<td><i>Maximum Participants</i></td>
				<td><input type="number" width="4" maxlength="10"
					name="maxParticipants" value="${workshop.maxParticipants}" /></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('email') }">
					<tr><td><span class="error">${workshop.getError("email") }</span>
					</td></tr>
				</c:if>
				<td><i>Email</i></td>
				<td><input type="text" name="email" size="40"
					value="${workshop.email }"></td>
			</tr>

			<tr>
				<c:if test="${workshop.hasError('notice') }">
					<tr><td><span class="error">${workshop.getError("notice") }</span>
					</td></tr>
				</c:if>
				<td><i>Notice</i></td>
				<td><textarea name="notice" cols="40" rows="5">${workshop.notice }</textarea></td>
			</tr>

			<tr>
				<td><a href="./listaj">Home</a></td>
				<td><input type="submit" name="method" value="Reset" /><input
					type="submit" name="method" value="Save" /></td>
			</tr>

		</table>

	</form>
</body>
</html>