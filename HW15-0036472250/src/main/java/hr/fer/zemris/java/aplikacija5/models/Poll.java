/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.models;

/**
 * Single poll definition.
 * @author Marin Petrunić 0036472250
 *
 */
public class Poll {
	
	/**
	 * Poll ID.
	 */
	private Long id;
	
	/**
	 * Poll title.
	 */
	private String title;
	
	/**
	 * Poll message.
	 */
	private String message;

	/**
	 * @param id
	 * @param title
	 * @param message
	 */
	public Poll(Long id, String title, String message) {
		super();
		this.id = id;
		this.title = title;
		this.message = message;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
