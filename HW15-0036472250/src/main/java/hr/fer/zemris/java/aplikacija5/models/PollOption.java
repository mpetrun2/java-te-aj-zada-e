/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.models;

/**
 * Single poll option.
 * @author Marin Petrunić 0036472250
 *
 */
public class PollOption implements Comparable<PollOption> {

	/**
	 * Poll option id.
	 */
	private Long id;
	
	/**
	 * Poll option title
	 */
	private String optionTitle = "";
	
	/**
	 * Poll option link.
	 */
	private String optionLink = "";

	/**
	 * ID of parent poll.
	 */
	private Long pollID = (long) 0;
	
	/**
	 * Votes given to this poll option.
	 */
	private long votesCount = 0;

	/**
	 * @param id
	 * @param optionTitle
	 * @param optionLink
	 * @param pollID
	 * @param votesCount
	 */
	public PollOption(Long id, String optionTitle, String optionLink,
			Long pollID, long votesCount) {
		super();
		this.id = id;
		this.optionTitle = optionTitle;
		this.optionLink = optionLink;
		this.pollID = pollID;
		this.votesCount = votesCount;
	}
	
	public PollOption() {
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the optionTitle
	 */
	public String getOptionTitle() {
		return optionTitle;
	}

	/**
	 * @param optionTitle the optionTitle to set
	 */
	public void setOptionTitle(String optionTitle) {
		this.optionTitle = optionTitle;
	}

	/**
	 * @return the optionLink
	 */
	public String getOptionLink() {
		return optionLink;
	}

	/**
	 * @param optionLink the optionLink to set
	 */
	public void setOptionLink(String optionLink) {
		this.optionLink = optionLink;
	}

	/**
	 * @return the pollID
	 */
	public Long getPollID() {
		return pollID;
	}

	/**
	 * @param pollID the pollID to set
	 */
	public void setPollID(Long pollID) {
		this.pollID = pollID;
	}

	/**
	 * @return the votesCount
	 */
	public long getVotesCount() {
		return votesCount;
	}

	/**
	 * @param votesCount the votesCount to set
	 */
	public void setVotesCount(long votesCount) {
		this.votesCount = votesCount;
	}

	/**
	 * Increments votes number by 1.
	 */
	public void incrementVoteCount() {
		votesCount++;
	}
	
	/**
	 * Decremens number of votes by 1.
	 */
	public void decrementVoteCount() {
		votesCount--;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PollOption)) {
			return false;
		}
		PollOption other = (PollOption) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(PollOption o) {
		return -1 * Long.compare(votesCount, o.getVotesCount());
	}
	
	@Override
	public String toString() {
		return id + " " + optionTitle + " " + optionLink + " " +pollID + " " +votesCount;
	}
}
