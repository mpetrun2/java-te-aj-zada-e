package hr.fer.zemris.java.aplikacija5.models.dao;

import hr.fer.zemris.java.aplikacija5.models.Poll;
import hr.fer.zemris.java.aplikacija5.models.PollOption;

import java.util.List;

/**
 * Sučelje prema podsustavu za perzistenciju podataka.
 * 
 * @author marcupic
 *
 */
public interface DAO {

	/**
	 * Method for getting all {@link Poll}s in database.
	 * @return
	 */
	public List<Poll> getPolls();
	
	/**
	 * Method for inserting given {@link Poll} in data.
	 * @param poll
	 */
	public Long insertPoll(Poll poll);
	
	/**
	 * Checks if Poll under given name exist in data.
	 * @param pollName
	 * @return
	 */
	public boolean hasPoll(String pollName);
	
	/**
	 * Method for getting {@link List} of
	 * {@link PollOption} for given poll ID.
	 * @param pollID
	 * @return
	 */
	public List<PollOption> getPollOptions(long pollID);
	
	/**
	 * Get {@link PollOption} by his id.
	 * @param pollOptionID
	 * @return
	 */
	public PollOption getPollOption(long pollOptionID);
	
	/**
	 * Inserts given {@link PollOption} in data.
	 * @param pollOption
	 * @return
	 */
	public long insertPollOption(PollOption pollOption);
	
	/**
	 * Inserts given {@link List} of {@link PollOption}s in data.
	 * @param pollOptions
	 * @return
	 */
	public List<Long> insertPollOptions(List<PollOption> pollOptions);
	
	/**
	 * Updates given poll option in data.
	 */
	public void updatePollOption(PollOption polloption);
}