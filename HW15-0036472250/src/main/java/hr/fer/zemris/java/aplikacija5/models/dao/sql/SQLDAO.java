package hr.fer.zemris.java.aplikacija5.models.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.aplikacija5.models.Poll;
import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAO;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOException;

/**
 * Ovo je implementacija podsustava DAO uporabom tehnologije SQL. Ova konkretna
 * implementacija očekuje da joj veza stoji na raspolaganju preko
 * {@link SQLConnectionProvider} razreda, što znači da bi netko prije no što
 * izvođenje dođe do ove točke to trebao tamo postaviti. U web-aplikacijama
 * tipično rješenje je konfigurirati jedan filter koji će presresti pozive
 * servleta i prije toga ovdje ubaciti jednu vezu iz connection-poola, a po
 * zavrsetku obrade je maknuti.
 * 
 * @author Marin Petrunić
 */
public class SQLDAO implements DAO {

	@Override
	public List<Poll> getPolls() {
		List<Poll> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("select * from Polls");
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while (rs != null && rs.next()) {
						Poll poll = new Poll(rs.getLong(1), rs.getString(2),
								rs.getString(3));
						polls.add(poll);
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while getting polls.", e);
		}
		return polls;
	}

	@Override
	public Long insertPoll(Poll poll) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		Long pollID = null;
		try {
			pst = con.prepareStatement(
					"insert into Polls(title, message) values(?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, poll.getTitle());
			pst.setString(2, poll.getMessage());
			pst.executeUpdate();
			ResultSet rset = pst.getGeneratedKeys();
			try {
				if (rset != null && rset.next()) {
					pollID = rset.getLong(1);
				}
			} finally {
				try {
					rset.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while inserting Poll.", e);
		} finally {
			try { pst.close(); } catch (Exception ignorable){};
		}
		return pollID;
	}

	@Override
	public List<PollOption> getPollOptions(long pollID) {
		List<PollOption> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("select * from PollOptions where pollID=?");
			pst.setLong(1, pollID);
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while (rs != null && rs.next()) {
						PollOption poll = new PollOption(rs.getLong(1),
								rs.getString(2), rs.getString(3),
								rs.getLong(4), rs.getLong(5));
						polls.add(poll);
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while getting pollOption.", e);
		}
		return polls;
	}

	@Override
	public PollOption getPollOption(long pollOptionID) {
		PollOption pollOption = null;
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("select * from PollOptions where id=?");
			pst.setLong(1, pollOptionID);
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while (rs != null && rs.next()) {
						pollOption = new PollOption(rs.getLong(1),
								rs.getString(2), rs.getString(3),
								rs.getLong(4), rs.getLong(5));
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while getting pollOption.", e);
		}
		return pollOption;
	}

	@Override
	public long insertPollOption(PollOption pollOption) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		Long pollID = null;
		try {
			pst = con.prepareStatement(
					"insert into PollOptions(optionTitle, optionlink, pollID, votesCount) values(?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, pollOption.getOptionTitle());
			pst.setString(2, pollOption.getOptionLink());
			pst.setLong(3, pollOption.getPollID());
			pst.setLong(4, pollOption.getVotesCount());
			pst.executeUpdate();
			ResultSet rset = pst.getGeneratedKeys();
			try {
				if (rset != null && rset.next()) {
					pollID = rset.getLong(1);
				}
			} finally {
				try {
					rset.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while inserting PollOption. ", e);
		} finally {
			try { pst.close(); } catch (Exception ignorable){};
		}
		return pollID;
	}

	@Override
	public List<Long> insertPollOptions(List<PollOption> pollOptions) {
		List<Long> pollIDs = new ArrayList<>();
		for (PollOption pollOption : pollOptions) {
			pollIDs.add(this.insertPollOption(pollOption));
		};
		return pollIDs;
	}

	@Override
	public void updatePollOption(PollOption pollOption) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(
					"update PollOptions set optionTitle=?, optionLink=?, pollID=?, votesCount=? where id=?",
					Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, pollOption.getOptionTitle());
			pst.setString(2, pollOption.getOptionLink());
			pst.setLong(3, pollOption.getPollID());
			pst.setLong(4, pollOption.getVotesCount());
			pst.setLong(5, pollOption.getId());
			pst.executeUpdate();
		} catch (Exception e) {
			throw new DAOException("Error while updating PollOption.", e);
		}  finally {
			try { pst.close(); } catch (Exception ignorable){};
		}
	}

	@Override
	public boolean hasPoll(String pollName) {
		boolean found = false;
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("select * from Polls where title=?");
			pst.setString(1, pollName);
			try {
				ResultSet rs = pst.executeQuery();
				try {
					while (rs != null && rs.next()) {
						found = true;
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception e) {
			throw new DAOException("Error while getting poll.", e);
		}
		return found;
	}

}