/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Represents signle vote for some band given as parameter.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasaj", urlPatterns = { "/glasanje-glasaj" })
public class GlasanjeGlasajServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Long pollOptionID = null;
		try {
			pollOptionID = Long.parseLong(req.getParameter("id"));
		} catch (Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
		}
		PollOption pollOption = DAOProvider.getDao().getPollOption(pollOptionID);
		pollOption.incrementVoteCount();
		DAOProvider.getDao().updatePollOption(pollOption);
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati?pollID="+pollOption.getPollID());
	}
}
