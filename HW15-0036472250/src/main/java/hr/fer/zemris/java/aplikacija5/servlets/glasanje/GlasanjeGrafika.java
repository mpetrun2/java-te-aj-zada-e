/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAO;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 * Generates vote result pie chart and sends it.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanjeGrafika", urlPatterns = { "/glasanje-grafika" })
public class GlasanjeGrafika extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Long pollId = null;
		try {
			pollId = Long.parseLong(req.getParameter("pollID"));
		} catch (Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/");
		}
		DAO dao = DAOProvider.getDao();
		List<PollOption> pollOptions = dao.getPollOptions(pollId);
		resp.setContentType("image/png");
		long totalVotes = 0;
		for (PollOption pollOption : pollOptions) {
			totalVotes += pollOption.getVotesCount();
		}
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (PollOption pollOption : pollOptions) {
			dataset.setValue(pollOption.getOptionTitle(), (double) pollOption.getVotesCount()
					/ totalVotes * 100);
		}
		JFreeChart chart = ChartFactory.createPieChart3D("Rezultati glasanja", // chart
																				// title
				dataset, // data
				true, // include legend
				true, false);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setSize(600, 400);
		BufferedImage image = new BufferedImage(600, 400,
				BufferedImage.TYPE_INT_RGB);
		chartPanel.paint(image.getGraphics());
		ImageIO.write(image, "png", resp.getOutputStream());
		resp.flushBuffer();
	}
}
