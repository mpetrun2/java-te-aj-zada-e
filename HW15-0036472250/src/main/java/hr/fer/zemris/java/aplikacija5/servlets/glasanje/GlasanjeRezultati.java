/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Gets all the neccessary data for displaying vote results accordingly.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanjeRezultati", urlPatterns = { "/glasanje-rezultati" })
public class GlasanjeRezultati extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Long pollID = null;
		try {
			pollID = Long.parseLong(req.getParameter("pollID"));
		} catch(Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/");
			return;
		}
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollID);
		Collections.sort(pollOptions);
		req.setAttribute("voteResults", pollOptions);
		List<PollOption> winners = new ArrayList<>();
		for (PollOption pollOption : pollOptions) {
			if(winners.isEmpty() || winners.get(0).getVotesCount() == pollOption.getVotesCount()) {
				winners.add(pollOption);
			} else {
				break;
			}
		}
		req.setAttribute("voteWinners", winners);
		req.setAttribute("pollID", pollID);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req,
				resp);
	}
}
