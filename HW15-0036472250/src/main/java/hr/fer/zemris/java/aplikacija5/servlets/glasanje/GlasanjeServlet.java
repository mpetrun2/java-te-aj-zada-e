/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.Poll;
import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Displays list of available bands to vote for.
 * 
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "glasanje", urlPatterns = { "/glasanje" })
public class GlasanjeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Long pollID = null;
		try {
			pollID = Long.parseLong(req.getParameter("pollID"));
		} catch (Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/");
			return;
		}
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollID);
		List<Poll> polls = DAOProvider.getDao().getPolls();
		for (Poll poll : polls) {
			if(poll.getId().equals(pollID)) {
				req.setAttribute("poll", poll);
			}
		}
		req.setAttribute("options", pollOptions);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(
				req, resp);
	}
}
