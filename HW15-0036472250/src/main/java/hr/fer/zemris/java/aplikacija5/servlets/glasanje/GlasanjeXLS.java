/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Returns band voting results as .xls file.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet(name="glasanjeXLS", urlPatterns={"/glasanje-xls"})
public class GlasanjeXLS extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Long pollID = null;
		try {
			pollID = Long.parseLong(req.getParameter("pollID"));
		} catch (Exception e) {
			System.out.println("Wrong parameter name. " + req.getServletPath()
					+ ". " + e.getMessage());
			resp.sendRedirect(req.getContextPath() + "/");
		}
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Rezultati glasanja");
		int i = 0;
		HSSFRow header = sheet.createRow(i++);
		header.createCell(0).setCellValue("Ime benda");
		header.createCell(1).setCellValue("Broj glasova");
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollID);
		for(PollOption pollOption : pollOptions) {
			HSSFRow row = sheet.createRow(i++);
			row.createCell(0).setCellValue(pollOption.getOptionTitle());
			row.createCell(1).setCellValue(pollOption.getVotesCount());
		}
		resp.setContentType("application/vnd.ms-excel");
		hwb.write(resp.getOutputStream());
		resp.flushBuffer();
	}
}
