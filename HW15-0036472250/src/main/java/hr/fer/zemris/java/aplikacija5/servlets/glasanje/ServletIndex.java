/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import hr.fer.zemris.java.aplikacija5.models.Poll;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/index.html")
public class ServletIndex extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<Poll> polls = DAOProvider.getDao().getPolls();
		req.setAttribute("polls", polls);
		req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(
				req, resp);
	}
}
