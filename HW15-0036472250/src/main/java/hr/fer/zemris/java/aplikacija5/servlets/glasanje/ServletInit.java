/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.servlets.glasanje;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hr.fer.zemris.java.aplikacija5.models.Poll;
import hr.fer.zemris.java.aplikacija5.models.PollOption;
import hr.fer.zemris.java.aplikacija5.models.dao.DAO;
import hr.fer.zemris.java.aplikacija5.models.dao.DAOProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for initializing default poll and their poll options.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/init")
public class ServletInit extends HttpServlet {

	/**
	 * Map of default polls.
	 */
	private static final Map<Integer, Poll> defaultPolls;
	static {
		defaultPolls = new HashMap<>();
		defaultPolls.put(0, new Poll(null, "Band voting!", "Vote for your favorite band."));
		defaultPolls.put(1, new Poll(null, "Favorite color voting!", "Vote for your favorite color."));
	}
	
	private static final Map<Integer, List<PollOption>> dafaultPollOptions;
	static {
		dafaultPollOptions = new HashMap<>();
		dafaultPollOptions.put(0, new ArrayList<>(Arrays.asList(new PollOption[] {
			new PollOption((long) 0, "The Beatles", "http://www.geocities.com/~goldenoldies/TwistAndShout-Beatles.mid", (long)0, 0),
			new PollOption((long) 0, "The Platters", "http://www.geocities.com/~goldenoldies/TwistAndShout-Beatles.mid", (long)1, 0),
			new PollOption((long) 0, "The Beach Boys", "http://www.geocities.com/~goldenoldies/SurfinUSA-BeachBoys.mid", (long)1, 0),
			new PollOption((long) 0, "The Four Seasons", "http://www.geocities.com/~goldenoldies/BigGirlsDontCry-FourSeasons.mid", (long)1, 0),
			new PollOption((long) 0, "The Marcels", "http://www.geocities.com/~goldenoldies/Bluemoon-Marcels.mid", (long)1, 0),
			new PollOption((long) 0, "The Everly Brothers", "http://www.geocities.com/~goldenoldies/All.I.HaveToDoIsDream-EverlyBrothers.mid", (long)1, 0),
			new PollOption((long) 0, "The Mamas And The Papas", "http://www.geocities.com/~goldenoldies/CaliforniaDreaming-Mamas-Papas.mid", (long)1, 0)
		})));
		dafaultPollOptions.put(1, new ArrayList<>(Arrays.asList(new PollOption[] {
				new PollOption((long) 0, "Blue", "http://3.bp.blogspot.com/-DRC4w4c54Y0/T0otkbsk6qI/AAAAAAAAAGI/KGMLntMhWEs/s1600/blue.jpg", (long)1, 0),
				new PollOption((long) 0, "Red", "http://informationng.com/wp-content/uploads/2012/08/red-color1.jpg", (long)1, 0),
				new PollOption((long) 0, "Green", "http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Auto_Racing_Green.svg/800px-Auto_Racing_Green.svg.png", (long)1, 0),
				new PollOption((long) 0, "White", "http://www.ledr.com/colours/white.jpg", (long)1, 0),
				new PollOption((long) 0, "Purple", "http://trendwallpaper.com/wp-content/uploads/2014/05/Purple-Background-Wallpapers.jpg", (long)1, 0)
		})));
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		DAO dao = DAOProvider.getDao();
		for (Entry<Integer, Poll> entry : defaultPolls.entrySet()) {
			if(!dao.hasPoll(entry.getValue().getTitle())) {
				Long pollID = dao.insertPoll(entry.getValue());
				if(pollID != null) {
					for (PollOption option : dafaultPollOptions.get(entry.getKey())) {
						option.setPollID(pollID);
						dao.insertPollOption(option);
					}
				}
			}
		}
		resp.sendRedirect(req.getContextPath());
	}
	
}
