/**
 * 
 */
package hr.fer.zemris.java.aplikacija5.models;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class PollTest {

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#Poll(java.lang.Long, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testPoll() {
		new Poll((long) 1, "asd", "asdasd");
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#getId()}.
	 */
	@Test
	public void testGetId() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		assertEquals("Expected id 1.",new Long(1), test.getId());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#setId(java.lang.Long)}.
	 */
	@Test
	public void testSetId() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		test.setId(new Long(2));
		assertEquals("Expected id 2.",new Long(2), test.getId());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#getTitle()}.
	 */
	@Test
	public void testGetTitle() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		assertEquals("Expected poll title.","asd", test.getTitle());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#setTitle(java.lang.String)}.
	 */
	@Test
	public void testSetTitle() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		test.setTitle("test");
		assertEquals("Expected poll title.","test", test.getTitle());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#getMessage()}.
	 */
	@Test
	public void testGetMessage() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		assertEquals("Expected poll message.","asdasd", test.getMessage());
	}

	/**
	 * Test method for {@link hr.fer.zemris.java.aplikacija5.models.Poll#setMessage(java.lang.String)}.
	 */
	@Test
	public void testSetMessage() {
		Poll test = new Poll((long) 1, "asd", "asdasd");
		test.setMessage("test poruka");
		assertEquals("Expected poll message.","test poruka", test.getMessage());
	}

}
