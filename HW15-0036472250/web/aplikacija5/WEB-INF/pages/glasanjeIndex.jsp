<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${poll.title}</title>
	</head>
	<body>
		<h1>${poll.title}</h1>
 		<p>${poll.message}</p>
 		<ol>
 			<c:forEach var="p" items="${options}">
 				<li><a href="./glasanje-glasaj?pollID=${poll.id}&id=${p.id}">${p.optionTitle}</a></li>
 			</c:forEach>
 		</ol>
		<a href="./">&lt;- Home</a>
	</body>
</html>