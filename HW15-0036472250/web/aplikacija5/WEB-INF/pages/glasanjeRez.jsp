<%@ page language="java" contentType="text/html; charset=UTF-8"
	session="true" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
table.rez td {
	text-align: center;
}
</style>

<title>Voting results!</title>
</head>
<body>
	<h1>Rezultati glasanja</h1>
	<p>Ovo su rezultati glasanja.</p>
	<table border="1" cellspacing="0" class="rez">
		<thead>
			<tr>
				<th>Bend</th>
				<th>Broj glasova</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="vr" items="${voteResults}">
				<tr>
					<td>${vr.optionTitle}</td>
					<td>${vr.votesCount}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<h2>Grafički prikaz rezultata</h2>
 	<img alt="Pie-chart" src="./glasanje-grafika?pollID=${pollID}" width="600" height="400" />
 	<h2>Rezultati u XLS formatu</h2>
 	<p>Rezultati u XLS formatu dostupni su <a href="./glasanje-xls?pollID=${pollID}">ovdje</a></p>
	<h2>Razno</h2>
	<p>Primjeri pjesama pobjedničkih bendova:</p>
	<ul>
		<c:forEach var="ww" items="${voteWinners}">
			<li>
				<a href="${ww.optionLink}" target="_blank" >${ww.optionTitle}</a>
			</li>
		</c:forEach>
	</ul>
	<a href="./">&lt;- Home</a>
</body>
</html>