<%@ page language="java" contentType="text/html; charset=UTF-8" session="true"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Choose voting</title>
	</head>
	<body>
		<h1>Choose voting aplication:</h1>
 		<ol>
 			<c:forEach var="p" items="${polls}">
 				<li><a href="./glasanje?pollID=${p.id}">${p.title}</a></li>
 			</c:forEach>
 			<c:if test="${polls.isEmpty()}">
 				<li><a href="./init">Initialize Voting Aplications</a></li>
 			</c:if>
 		</ol>
		<a href="./">&lt;- Home</a>
	</body>
</html>