package hr.fer.zemris.java.tecaj_14.dao;

import java.util.List;

import hr.fer.zemris.java.tecaj_14.model.BlogComment;
import hr.fer.zemris.java.tecaj_14.model.BlogEntry;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

public interface DAO {

	/**
	 * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
	 * vraća <code>null</code>.
	 * 
	 * @param id ključ zapisa
	 * @return entry ili <code>null</code> ako entry ne postoji
	 * @throws DAOException ako dođe do pogreške pri dohvatu podataka
	 */
	public BlogEntry getBlogEntry(Long id) throws DAOException;
	
	/**
	 * Returns true if there already exist user with given nick.
	 * False otherwise.
	 * @param nick user nick
	 * @return
	 * @throws DAOException if there is error while returning data
	 */
	public boolean hasUserNick(String nick) throws DAOException;
	
	/**
	 * Returns true if there already exist user with given nick.
	 * False otherwise.
	 * @param nick user nick
	 * @return
	 * @throws DAOException if there is error while returning data
	 */
	public BlogUser getUserNick(String nick) throws DAOException;
	
	/**
	 * Saves given user.
	 * @return
	 */
	public void saveUser(BlogUser user) throws DAOException;
	
	/**
	 * Returns list of all users.
	 * @return
	 * @throws DAOException
	 */
	public List<BlogUser> getUsers() throws DAOException;
	
	/**
	 * Returns user that has exactly the same username and password.
	 * It verifyies user credentials.
	 * @param username
	 * @param password
	 * @return
	 * @throws DAOException
	 */
	public BlogUser getUser(String username, String password) throws DAOException;

	/**
	 * Saves given {@link BlogEntry}.
	 * @param be
	 */
	public void saveBlogEntry(BlogEntry be) throws DAOException;

	/**
	 * Saves given {@link BlogComment}.
	 * @param bc
	 * @throws DAOException
	 */
	public void saveBlogComment(BlogComment bc) throws DAOException;
}