package hr.fer.zemris.java.tecaj_14.dao.jpa;

import java.util.List;

import hr.fer.zemris.java.tecaj_14.dao.DAO;
import hr.fer.zemris.java.tecaj_14.dao.DAOException;
import hr.fer.zemris.java.tecaj_14.model.BlogComment;
import hr.fer.zemris.java.tecaj_14.model.BlogEntry;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

public class JPADAOImpl implements DAO {

	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(
				BlogEntry.class, id);
		return blogEntry;
	}

	@Override
	public boolean hasUserNick(String nick) throws DAOException {
		@SuppressWarnings("unchecked")
		List<BlogUser> users = (List<BlogUser>) JPAEMProvider
				.getEntityManager()
				.createQuery("select u from BlogUser as u where u.nick=:nick")
				.setParameter("nick", nick).getResultList();
		return users.isEmpty() ? false : true;
	}

	@Override
	public void saveUser(BlogUser user) throws DAOException {
		JPAEMProvider.getEntityManager().persist(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogUser> getUsers() throws DAOException {
		return (List<BlogUser>) JPAEMProvider.getEntityManager()
				.createQuery("select u from BlogUser as u").getResultList();
	}

	@Override
	public BlogUser getUser(String username, String password)
			throws DAOException {
		@SuppressWarnings("unchecked")
		List<BlogUser> users = (List<BlogUser>) JPAEMProvider
				.getEntityManager()
				.createQuery("select u from BlogUser as u where u.nick=:nick and u.password=:password")
				.setParameter("nick", username).setParameter("password", password).getResultList();
		if(users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	@Override
	public BlogUser getUserNick(String nick) throws DAOException {
		@SuppressWarnings("unchecked")
		List<BlogUser> users = (List<BlogUser>) JPAEMProvider
				.getEntityManager()
				.createQuery("select u from BlogUser as u where u.nick=:nick")
				.setParameter("nick", nick).getResultList();
		if(users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	@Override
	public void saveBlogEntry(BlogEntry be) {
		JPAEMProvider.getEntityManager().persist(be);
	}

	@Override
	public void saveBlogComment(BlogComment bc) throws DAOException {
		JPAEMProvider.getEntityManager().persist(bc);
	}

	
}