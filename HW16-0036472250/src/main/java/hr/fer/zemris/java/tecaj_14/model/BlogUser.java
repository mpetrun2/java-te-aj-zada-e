/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.model;

import hr.fer.zemris.java.tecaj_14.dao.DAO;
import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

/**
 * Class representing single user data.
 * 
 * @author Marin Petrunić
 * 
 */
@Entity
@Cacheable(true)
@Table(name = "blog_user")
public class BlogUser {

	private Long id;
	private String firstName;
	private String lastName;
	private String nick;
	private String email;
	private String password;
	private List<BlogEntry> blogEntrys;

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	@Column(length = 80, nullable = false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Column(length = 100, nullable = false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the nick
	 */
	@Column(unique = true, length = 20)
	public String getNick() {
		return nick;
	}

	/**
	 * @param nick
	 *            the nick to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return the email
	 */
	@Column(length = 100, nullable = false)
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	@Column(length = 40, nullable = false)
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	

	/**
	 * @return the blogEntrys
	 */
	@OneToMany(mappedBy="author")
	public List<BlogEntry> getBlogEntrys() {
		return blogEntrys;
	}

	/**
	 * @param blogEntrys the blogEntrys to set
	 */
	public void setBlogEntrys(List<BlogEntry> blogEntrys) {
		this.blogEntrys = blogEntrys;
	}

	/**
	 * Validates current {@link BlogUser} data against specific rules. If all
	 * rules are satisfied empty map is returned else will map contain error
	 * reports.
	 * 
	 * @return
	 */
	public Map<String, String> validate() {
		HashMap<String, String> errors = new HashMap<>();
		DAO dao = DAOProvider.getDAO();
		if (dao.hasUserNick(nick)) {
			errors.put("username", "Username already exist.");
		}
		if (nick.isEmpty() || nick.length() > 20) {
			errors.put("username", "Invalid username.");
		}
		if (firstName.isEmpty() || firstName.length() > 80) {
			errors.put("firstname", "Invalid name.");
		}
		if (lastName.isEmpty() || lastName.length() > 100) {
			errors.put("lastname", "Invalid last name.");
		}
		if (!email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,4}")) {
			errors.put("email", "Invalid email.");
		}
		if (password.isEmpty()) {
			errors.put("password", "Invalid passoword.");
		}
		return errors;
	}

	/**
	 * Fills {@link BlogUser} data from {@link HttpServletRequest}.
	 * 
	 * @param req
	 */
	public void fillFromRequest(HttpServletRequest req) {
		setPassword(processPassword(req.getParameter("password")));
		setNick(processInput(req.getParameter("username")));
		setFirstName(processInput(req.getParameter("firstname")));
		setLastName(processInput(req.getParameter("lastname")));
		setEmail(processInput(req.getParameter("email")));
	}

	/**
	 * Process given parameter and returns
	 * 
	 * @param parameter
	 * @return
	 */
	private String processInput(String parameter) {
		if (parameter == null) {
			return "";
		}
		return parameter;
	}
	
	/**
	 * Processes user password.
	 * @param password
	 * @return
	 */
	private String processPassword(String password) {
		try {
			return byteToHexChar(encrypt(password));
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Encrypts given input.
	 * @param x
	 * @return
	 * @throws Exception
	 */
	public byte[] encrypt(String x) throws Exception {
		java.security.MessageDigest digest = null;
		digest = java.security.MessageDigest.getInstance("SHA-1");
		digest.reset();
		digest.update(x.getBytes("UTF-8"));
		return digest.digest();
	}

	/**
	 * Converts given byte array to hex char string.
	 * 
	 * @param mdbytes
	 *            given array off bytes
	 * @return
	 */
	public String byteToHexChar(byte[] mdbytes) {
		StringBuffer digestedSignature = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			digestedSignature.append(Integer.toString(
					(mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return digestedSignature.toString();
	}
}
