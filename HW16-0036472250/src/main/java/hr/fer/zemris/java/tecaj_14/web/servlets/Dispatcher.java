package hr.fer.zemris.java.tecaj_14.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Dispatches request to servlets baseed on request url parts.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/servleti/author/*")
public class Dispatcher extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String urlInfo = req.getPathInfo();
		urlInfo = urlInfo.substring(1);
		String[] parameters = urlInfo.split("/");
		
		if(parameters.length == 1) {
			EntryListServlet en = new EntryListServlet();
			en.doGet(req, resp);
			return;
		}
		if(parameters.length == 2 && (urlInfo.endsWith("new") || urlInfo.endsWith("edit"))) {
			NewEditEntryServlet ne = new NewEditEntryServlet();
			ne.doGet(req, resp);
			return;
		}
		if(parameters.length == 2) {
			SingleEntryServlet se = new SingleEntryServlet();
			se.doGet(req, resp);
			return;
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String urlInfo = req.getPathInfo();
		urlInfo = urlInfo.substring(1);
		if(urlInfo.endsWith("/")) {
			urlInfo = urlInfo.substring(0, urlInfo.length());
		}
		String[] parameters = urlInfo.split("/");
		if(parameters.length == 2 && (urlInfo.endsWith("new") || urlInfo.endsWith("edit"))) {
			NewEditEntryServlet ne = new NewEditEntryServlet();
			ne.doPost(req, resp);
			return;
		}
		if(parameters.length == 2) {
			SingleEntryServlet se = new SingleEntryServlet();
			se.doPost(req, resp);
			return;
		}
	}
}
