/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.web.servlets;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Displays list of blog entrys for single user.
 * @author Marin Petrunić
 *
 */
@SuppressWarnings("serial")
public class EntryListServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getPathInfo().substring(1).split("/")[0];
		BlogUser bu = DAOProvider.getDAO().getUserNick(username);
		if(bu==null) {
			resp.sendError(404);
			return;
		}
		req.setAttribute("user", bu.getNick());
		if(username.equals(req.getSession().getAttribute("current.user.username")))
		{
			req.setAttribute("authorized", true);
		}
		req.setAttribute("blogEntrys", bu.getBlogEntrys());
		req.getRequestDispatcher("/WEB-INF/pages/entrys.jsp").forward(req, resp);
	}
}
