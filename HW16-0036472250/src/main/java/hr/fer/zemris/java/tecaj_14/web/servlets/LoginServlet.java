/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.web.servlets;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet for logging user.
 * @author Marin Petrunić
 *
 */
@SuppressWarnings("serial")
@WebServlet("/servleti/login")
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		BlogUser bu = new BlogUser();
		try {
			password = bu.byteToHexChar(bu.encrypt(password));
		} catch (Exception e) {;
		}
		BlogUser user = DAOProvider.getDAO().getUser(username, password);
		if(user != null) {
			HttpSession session = req.getSession();
			session.setAttribute("current.user.id", user.getNick());
			session.setAttribute("current.user.username", user.getNick());
			session.setAttribute("current.user.firstname", user.getFirstName());
			session.setAttribute("current.user.lastname", user.getLastName());
			session.setAttribute("current.user.email", user.getEmail());
			resp.sendRedirect("./main");
			return;
		}
		req.setAttribute("username", username);
		req.setAttribute("error", "Invalid username/password.");
		req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);;
		
	}
}
