package hr.fer.zemris.java.tecaj_14.web.servlets;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet of home page.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
@WebServlet("/servleti/main")
public class MainServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<BlogUser> users = DAOProvider.getDAO().getUsers();
		req.setAttribute("userID", req.getSession().getAttribute("current.user.id"));
		req.setAttribute("username", req.getSession().getAttribute("current.user.username"));
		req.setAttribute("authors", users);
		req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req, resp);
	}
	
}
