/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.web.servlets;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogEntry;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet providing function for creating and editing
 * {@link BlogEntry}s.
 * @author Marin Petrunić 0036472250
 * 
 */
@SuppressWarnings("serial")
public class NewEditEntryServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String uri = req.getRequestURI();
		String username = uri.split("/")[4];
		req.setAttribute("user",
				req.getSession().getAttribute("current.user.username"));
		if (username.equals(req.getSession().getAttribute(
				"current.user.username"))) {
			req.setAttribute("authorized", true);
		} else {
			resp.sendRedirect("../../login");
			return;
		}
		BlogEntry be = null;
		if (req.getPathInfo().startsWith("new")
				|| req.getParameter("eid") == null) {
			req.setAttribute("action", "New Form");
			be = new BlogEntry();
		} else {

			req.setAttribute("action", "Edit Form");
			be = DAOProvider.getDAO().getBlogEntry(
					Long.parseLong(req.getParameter("eid")));
		}
		req.setAttribute("blogEntry", be);
		req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req,
				resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String uri = req.getRequestURI();
		String username = uri.split("/")[4];
		req.setAttribute("user",
				req.getSession().getAttribute("current.user.username"));
		if (username.equals(req.getSession().getAttribute(
				"current.user.username"))) {
			req.setAttribute("authorized", true);
		} else {
			resp.sendRedirect(req.getServletContext() + "/login");
			return;
		}
		
		BlogEntry be = new BlogEntry();
		if (req.getPathInfo().startsWith("new")
				|| req.getParameter("eid") == null) {
			req.setAttribute("action", "New Form");
		} else {
			req.setAttribute("action", "Edit Form");
			be = DAOProvider.getDAO().getBlogEntry(
					Long.parseLong(req.getParameter("eid")));
		}
		be.fillFromHttpRequest(req);
		Map<String, String> errors = be.validate();
		if (!errors.isEmpty()) {
			req.setAttribute("errors", errors);
			req.setAttribute("blogEntry", be);
			req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req,
					resp);
			return;
		} else {
			if(uri.endsWith("new")) {
				be.setCreatedAt(new Date());
			}
			be.setLastModifiedAt(new Date());
			be.setAuthor(DAOProvider.getDAO()
					.getUserNick(
							(String) req.getSession().getAttribute(
									"current.user.username")));
			DAOProvider.getDAO().saveBlogEntry(be);
			resp.sendRedirect(req.getContextPath()+"/servleti/author/"+req.getAttribute("user"));
		}
		
	}

}
