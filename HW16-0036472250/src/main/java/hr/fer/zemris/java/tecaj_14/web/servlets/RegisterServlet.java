/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.web.servlets;

import java.io.IOException;
import java.util.Map;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for displaying and registering new {@link BlogUser}.
 * @author Marin Petrunić
 *
 */
@SuppressWarnings("serial")
@WebServlet("/servleti/register")
public class RegisterServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		BlogUser bu = new BlogUser();
		req.setAttribute("user", bu);
		req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		BlogUser bu = new BlogUser();
		bu.fillFromRequest(req);
		Map<String, String> errors = bu.validate();
		if(!errors.isEmpty()) {
			bu.setPassword("");
			req.setAttribute("errors", errors);
			req.setAttribute("user", bu);
			req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
			return;
		}
		DAOProvider.getDAO().saveUser(bu);
		resp.sendRedirect("./main");
	}
}
