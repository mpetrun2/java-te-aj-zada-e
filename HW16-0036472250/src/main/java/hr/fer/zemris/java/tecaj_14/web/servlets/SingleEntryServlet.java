/**
 * 
 */
package hr.fer.zemris.java.tecaj_14.web.servlets;

import hr.fer.zemris.java.tecaj_14.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_14.model.BlogComment;
import hr.fer.zemris.java.tecaj_14.model.BlogEntry;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for displaying single blog entry.
 * @author Marin Petrunić 0036472250
 *
 */
@SuppressWarnings("serial")
public class SingleEntryServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		String username = uri.split("/")[4];
		req.setAttribute("username",username);
		req.setAttribute("user",
				req.getSession().getAttribute("current.user.username"));
		if (username.equals(req.getSession().getAttribute(
				"current.user.username"))) {
			req.setAttribute("authorized", true);
		}
		String entryID = uri.split("/")[5];
		Long id = null;
		try {
			id = Long.parseLong(entryID);
		} catch (Exception e) {
			resp.sendError(404);
			return;
		}
		BlogEntry be = DAOProvider.getDAO().getBlogEntry(id);
		if(be == null) {
			resp.sendError(404);
			return;
		}
		req.setAttribute("blogEntry", be);
		req.getRequestDispatcher("/WEB-INF/pages/Prikaz.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String uri = req.getRequestURI();
		String username = uri.split("/")[4];
		req.setAttribute("username",username);
		req.setAttribute("user",
				req.getSession().getAttribute("current.user.username"));
		if (username.equals(req.getSession().getAttribute(
				"current.user.username"))) {
			req.setAttribute("authorized", true);
		}
		String entryID = uri.split("/")[5];
		Long id = null;
		try {
			id = Long.parseLong(entryID);
		} catch (Exception e) {
			resp.sendError(404);
			return;
		}
		BlogEntry be = DAOProvider.getDAO().getBlogEntry(id);
		if(be == null) {
			resp.sendError(404);
			return;
		}
		BlogComment bc = new BlogComment().fillFromHttpRequest(req);
		bc.setUsersEMail((String) req.getSession().getAttribute("current.user.email"));
		bc.setPostedOn(new Date());
		bc.setBlogEntry(be);
		Map<String, String> errors = bc.validate();
		if(!errors.isEmpty()) {
			req.setAttribute("errors", errors);
			req.setAttribute("comment", bc);
			req.setAttribute("blogEntry", be);
			req.getRequestDispatcher("/WEB-INF/pages/Prikaz.jsp").forward(req, resp);
		} else {
			DAOProvider.getDAO().saveBlogComment(bc);
			resp.sendRedirect("./"+id);
			return;
		}
	}

}
