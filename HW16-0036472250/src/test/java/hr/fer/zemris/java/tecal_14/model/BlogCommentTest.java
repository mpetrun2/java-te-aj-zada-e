/**
 * 
 */
package hr.fer.zemris.java.tecal_14.model;

import static org.junit.Assert.*;

import java.util.Date;

import hr.fer.zemris.java.tecaj_14.model.BlogComment;
import hr.fer.zemris.java.tecaj_14.model.BlogEntry;

import org.junit.Test;

/**
 * @author Marin Petrunić 0036472250
 *
 */
public class BlogCommentTest {

	@Test
	public void test() {
		BlogComment bc = new BlogComment();
		bc.setBlogEntry(new BlogEntry());
		bc.setPostedOn(new Date());
		bc.setMessage("asdasdasd");
		bc.setUsersEMail("email");
		assertEquals("Expected email email.", "email", bc.getUsersEMail());
	}

}
