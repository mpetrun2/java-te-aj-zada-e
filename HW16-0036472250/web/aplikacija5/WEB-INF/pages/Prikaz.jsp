<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page
	import="hr.fer.zemris.java.tecaj_14.model.BlogEntry,hr.fer.zemris.java.tecaj_14.model.BlogComment"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	BlogEntry blogEntry = (BlogEntry) request.getAttribute("blogEntry");
%>
<html>
<body>

	<%
		if (blogEntry == null) {
	%>
	Nema unosa.
	<%
		} else {
	%>
	<h1><%=blogEntry.getTitle()%></h1>
	<p><%=blogEntry.getText()%></p>
	<%
		if (!blogEntry.getComments().isEmpty()) {
	%>
	<ul>
		<%
			for (BlogComment c : blogEntry.getComments()) {
		%>
		<li><div style="font-weight: bold">
				[Korisnik=<%=c.getUsersEMail()%>]
				<%=c.getPostedOn()%></div>
			<div style="padding-left: 10px;"><%=c.getMessage()%></div></li>
		<%
			}
		%>
	</ul>
	<%
		}
	%>
	<%
		}
	%>
	<c:if test="${user != null}">
	<form action="" method="post">
		<table>
			<tr>
			<td colspan="2" style="color: red; font-size: 0.9em;">${errors.get("message")}</td>
			</tr>
			<tr>
				<td>New Comment:</td>
				<td><textarea rows="4" cols="40" name="message">${comment.message}</textarea></td>
			</tr>
			<tr>
			<td>
				<input type="submit" value="Comment" name="Comment">
			</td>
			</tr>
		</table>
	</form>
	</c:if>
	<c:if test="${authorized}">
		<a href="../${username}/new">New Blog</a><br>
	</c:if>
	<a href="../${username}">Back</a>
</body>
</html>