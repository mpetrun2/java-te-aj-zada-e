<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="hr.fer.zemris.java.tecaj_14.model.BlogUser"%>
<%@page import="java.util.List"%>
<html>
<head>
	<title>${action}</title>
	<style type="text/css">
		.error{
			color:red;
			font-size: 0.9em;
		}
	</style>
</head>
  <body>
	<h1>${action}</h1>
	<form action="" method="POST">
		<table>
			<tr>
				<td colspan="2"><span class="error">${errors.get('title')}</span></td>
			</tr>
			<tr>
				<td>Title: </td>
				<td><input type="text" value="${blogEntry.title}" name="title" size="20"></td>
			</tr>
			<tr>
				<td colspan="2"><span class="error">${errors.get('text')}</span></td>
			</tr>
			<tr>
				<td>Text: </td>
				<td><textarea name="text" >${blogEntry.text}</textarea></td>
			</tr>
			<tr>
				<td><a href="../${user}">Back</a></td>
				<td><input type="submit" name="save" value="Save"/></td>
			</tr>
		</table>
	</form>
  </body>
</html>