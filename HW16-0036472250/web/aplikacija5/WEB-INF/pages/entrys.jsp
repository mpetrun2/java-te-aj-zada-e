<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@page import="hr.fer.zemris.java.tecaj_14.model.BlogUser"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Blog Entrys</title>
</head>
  <body>
  <h1>${user}</h1>
  <c:forEach var="e" items="${blogEntrys}">
  	<a href="./${user}/${e.id}">${e.title}</a>
  	<c:if test="${authorized}">
  		<a href="./${user}/edit?eid=${e.id}">Edit</a>
  	</c:if>
  	<br>
  </c:forEach>
  <a href="../main">Home</a>
  <c:if test="${authorized}">
  		<a href="./${user}/new">New</a>
  	</c:if>
  </body>
</html>