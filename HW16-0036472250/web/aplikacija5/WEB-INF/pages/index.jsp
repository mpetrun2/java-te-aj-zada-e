<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@page import="hr.fer.zemris.java.tecaj_14.model.BlogUser"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Home</title>
</head>
  <body>
  <c:choose>
  	<c:when test="${userID != null}">
  	<span>${username}</span>
  	<a href="./logout">Logout</a>
  </c:when>
  <c:otherwise>
  	<h1>Login</h1>
		<form action="./login" method="post">
			<table>
				<tr>
					<td><i>Username: </i></td>
					<td><input type="text" name="username" size="20"/></td>
				</tr>
				<tr>
					<td><i>Password: </i></td>
					<td><input type="password" name="password" size="20" /></td>
				</tr>
				<tr>
					<td><a href="./register">Register</a></td>
					<td><input type="submit" value="Login"></td>
				</tr>
			</table>
		</form>
  </c:otherwise>
	
  </c:choose>
	<h1>Authors</h1>
	<c:forEach var="a" items="${authors}">
		<a href="./author/${a.nick}">${a.firstName} ${a.lastName}</a><br>
	</c:forEach>
  </body>
</html>