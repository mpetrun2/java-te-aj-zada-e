<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@page import="hr.fer.zemris.java.tecaj_14.model.BlogUser"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Logging</title>
</head>
  <body>
  	<h1>Login</h1>
		<form action="./login" method="post">
			<table>
				<tr>
					<td colspan="2"><span style="color:red;font-size: 0.9em;">${error}</span></td>
				</tr>
				<tr>
					<td><i>Username: </i></td>
					<td><input type="text" name="username" size="20" value="${username}"/></td>
				</tr>
				<tr>
					<td><i>Password: </i></td>
					<td><input type="password" name="password" size="20" /></td>
				</tr>
				<tr>
					<td><a href="./register">Register</a>&nbsp<a href="./main">Home</a></td>
					<td><input type="submit" value="Login"></td>
				</tr>
			</table>
		</form>
  </body>
</html>