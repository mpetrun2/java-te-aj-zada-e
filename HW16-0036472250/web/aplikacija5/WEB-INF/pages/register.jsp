<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="hr.fer.zemris.java.tecaj_14.model.BlogUser"%>
<%@page import="java.util.List"%>
<html>
<head>
	<title>Register</title>
	<style type="text/css">
		.error{
			color:red;
			font-size: 0.9em;
		}
	</style>
</head>
  <body>
	<h1>Register</h1>
	<form action="" method="POST">
		<table>
			<tr>
				<td colspan="2"><span class="error">${errors.get('username')}</span></td>
			</tr>
			<tr>
				<td>Username: </td>
				<td><input type="text" value="${user.getNick()}" name="username" size="20"></td>
			</tr>
			<tr>
				<td colspan="2"><span class="error">${errors.get('password')}</span></td>
			</tr>
			<tr>
				<td>Password: </td>
				<td><input type="password" name="password" size="20" /></td>
			</tr>
			<tr>
				<td colspan="2"><span class="error">${errors.get('firstname')}</span></td>
			</tr>
			<tr>
				<td>First name: </td>
				<td><input type="text" value="${user.getFirstName()}" name="firstname" size="40" /></td>
			</tr>
			<tr>
				<td colspan="2"><span class="error">${errors.get('lastname')}</span></td>
			</tr>
			<tr>
				<td>Last name: </td>
				<td><input type="text" value="${user.getLastName()}" name="lastname" size="40"/></td>	
			</tr>
			<tr>
				<td colspan="2"><span class="error">${errors.get('email')}</span></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="text" value="${user.getEmail()}" name="email" size="60"/></td>
			</tr>
			<tr>
				<td><a href="./main">Home</a></td>
				<td><input type="submit" name="register" value="Register"/></td>
			</tr>
		</table>
	</form>
  </body>
</html>